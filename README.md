BICEPS
=======
The Basic Integrated Clinical Environment Protocol Specification (BICEPS) is a communication protocol specification that facilitates medical device interoperability in a distributed system of medical devices that follows the SOMDA architecture paradigm. BICEPS has the objective to allow communication between participants in a distributed system of medical devices in high-acuity environments that directly interact with, monitor, provide treatment to, or are by some other means directly associated with a single patient.

BICEPS is part of the [openSDC project](http://sourceforge.net/projects/opensdc/).

openSDC
========
The openSDC libraries are communication library that facilitate the development of distributed systems of medical devices in high-acuity environments.

OpenSDC has been developed in an [Dräger](http://http://www.draeger.com/)-internal technology project called “Device & System Connectivity” (DSC) that had the goal to meet increasing demand for medical device interoperability in an Integrated Clinical Environment (ICE).  An ICE is a distributed system of medical devices for one clinical workplace that may have an external interface to other systems. ASTM F2761-1:2009 describes the components that are required for safe and effective “Plug & Play” operation of an ICE in high-acuity environments.
The project had the objective to develop an efficient, future-proof architecture, protocol stack, and middleware that satisfies the derived requirements and facilitates the implementation of the concept of an ICE.  The middleware is comprised of the openSDC libraries.

For more details on the architecture and the protocol stack see [the technical whitepaper](http://sourceforge.net/projects/opensdc/files/framework/1.0-BETA_02/Documentation/TechnicalWhitepaper.pdf/download).

Intended Use
-------
The openSDC libraries are a reference implementation of the published documents and are not intended to be used in clinical trials, clinical studies or in clinical routine use.
See "IntendedUse.txt" for more details.

Components
-------
* BICEPS - BICEPS implements the BICEPS specification.
    * BICEPS Common - Common components of the implementation of the BICEPS protocol including messages, services, and utils.
	* BICEPS Device - Component that implements the framework for a BICEPS Device.
	* BICEPS Client - Component that implements the framework for a BICEPS Client.
* MDPWS - An extension to JDPWS that implements the extensions of the MDPWS specification on top of the DPWS specification. Furthermore, a QoS framework as well as some QoS Policies are included.
* JDPWS - JDPWS implements the DPWS specification. It is a modified version of the [JMEDS beta 4 library](https://sourceforge.net/projects/ws4d-javame/) that allows the plugin of the QoS framework as well as the MDPWS extensions. Furthermore, it contains bugfixes that are currently not patched into the JMEDS beta 4 library.

Standardization
-------
OpenSDC is a reference implementation of the upcoming IEEE standards for service-oriented point-of-care medical device interoperability. The PARs are:

* [P11073-10702](https://development.standards.ieee.org/P887100033/par): Standard for Domain Information & Service Model for Service-Oriented Point-of-Care Medical Device Communication
	* "The scope of this standard is the definition and structuring of information that is communicated in a distributed system of point-of-care medical (PoC) medical devices and medical IT systems that need to exchange data or safely control networked PoC medical devices by defining a participant information model and service model. The definition of transport serialization is outside the scope of this standard."
	* implemented by BICEPS
* [P11073-20701](https://development.standards.ieee.org/P887300033/par): Standard for Service-Oriented Medical Device Exchange Architecture & Protocol Binding
	* "The scope of this standard is a service-oriented medical device architecture and communication protocol specification for distributed system of point-of-care medical (PoC) medical devices and medical IT systems that need to exchange data or safely control networked PoC medical devices by identifying the functional components, their communication relationships as well as the binding of the components and communication relationships to protocol specifications."
* [P11073-20702](https://development.standards.ieee.org/P887200033/par): Standard for Medical Devices Communication Profile for Web Services
	* "The scope of this standard is a communication protocol specification for a distributed system of point-of-care (PoC) medical devices and medical IT systems that need to exchange data or safely control networked PoC medical devices by defining a profile for Web service specifications and defining additional Web service specifications as part of this standard."
	* implemented by MDPWS and JDPWS



License
-------
The openSDC libraries and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at (http://www.eclipse.org/legal/epl-v10.html).


Notice
-------
* Protocol may change in the future due to additional requirements identified during standardization. For this reason until a standard has been established all version of the openSDC libraries are released as beta version.
* The package names will be changed in release beta02.


Branching
-------
The repository branching is applied as defined in [Vincent Driessen's branching model](http://nvie.com/posts/a-successful-git-branching-model/). A Git extension that provides according high-level repository operations can be found [here] (https://github.com/nvie/gitflow).