/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client;

import java.io.IOException;
import java.io.InputStream;

import org.ws4d.java.communication.CommunicationManagerRegistry;
import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.impl.DefaultBicepsClientImpl;
import com.draeger.medical.mdpws.framework.MDPWSFramework;

public class BicepsClientFramework {

	private final static BicepsClientFramework INSTANCE=new BicepsClientFramework(); 
	private boolean initQoSFramework=Boolean.parseBoolean(System.getProperty("MDPWS.InitQoSFramework", "true"));

	public static BicepsClientFramework getInstance() {
		return INSTANCE;
	}



	public boolean isQoSFrameworkEnabled() {
		return initQoSFramework;
	}

	public void setup(String[] runtimeArgs,URI propertiesFile) throws IOException
	{
		if (!MDPWSFramework.getInstance().isRunning())
		{
			MDPWSFramework.getInstance().start(runtimeArgs);

			if(propertiesFile!=null)
			{
				InputStream propInputStream=PlatformSupport.getInstance().getToolkit().getResourceAsStream(propertiesFile);
				Properties.getInstance().read(propInputStream );
			}


			int tps= FrameworkProperties.getInstance().getThreadPoolSize();
			tps=tps<0?Integer.MAX_VALUE:tps;
			if (Log.isInfo()) Log.info("BICEPSClientFramework: Trying to set thread pool size to "+tps+".");

			if (tps<50 && Log.isWarn()){
				Log.warn("BICEPSClientFramework: Thread pool size of "+tps+" may be to small.");
			}

			if (!PlatformSupport.getInstance().getToolkit().getThreadPool().setSize(tps))
			{
				if (Log.isWarn()) Log.warn("BICEPSClientFramework: Could not restrict thread pool to "+tps+".");
			}else if (Log.isInfo()){
				Log.info("BICEPSClientFramework: Set thread pool size to "+tps+": OK.");
			}

			//TODO SSch Maybe disable this
			FrameworkProperties.getInstance().setKillOnShutdownHook(true);

			if (Log.isInfo()) Log.info("Initializing QoSFramework: "+initQoSFramework);

			if (initQoSFramework) MDPWSFramework.getInstance().initializeQoSFramework();
		}
	}

	public void shutdown()
	{
		getClient().stop();
		CommunicationManagerRegistry.stopAll();
		MDPWSFramework.getInstance().kill();
		if (Log.isInfo()) Log.info("BICEPS client framework stopped.");

	}

	public IBICEPSClient getClient()
	{
		IBICEPSClient client=DefaultBicepsClientImpl.getInstance(); //TODO SSch remove dependency
		return client;
	}
}
