/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client;

import java.math.BigDecimal;

import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.NumericValue;
import com.draeger.medical.biceps.common.model.StringMetricState;
import com.draeger.medical.biceps.common.model.StringMetricValue;

public class BicepsClientHelper {
	
	public static StringMetricValue getObservedNumericValue(StringMetricState state){
		StringMetricValue retVal=null;
		if (state!=null)
		{
			retVal=state.getObservedValue();
		}
		return retVal;
	}
	
	public static String getStringValueFromObservedValue(StringMetricValue obsValue){
		String retVal=null;
		if (obsValue!=null)
		{
			retVal=obsValue.getValue();
		}
		return retVal;
	}
	
	public static String getStringValue(StringMetricState state){
		String retVal=null;
		StringMetricValue obsValue=getObservedNumericValue(state);
		retVal=getStringValueFromObservedValue(obsValue);
		return retVal;
	}
	
	public static NumericValue getObservedNumericValue(NumericMetricState state){
		NumericValue retVal=null;
		if (state!=null)
		{
			retVal=state.getObservedValue();
		}
		return retVal;
	}
	
	public static BigDecimal getNumericValue(NumericMetricState state){
		BigDecimal retVal=null;
		NumericValue obsValue=getObservedNumericValue(state);
		retVal=getNumericValueFromObservedValue(obsValue);
		return retVal;
	}

	public static BigDecimal getNumericValueFromObservedValue(NumericValue obsValue) {
		BigDecimal b=null;
		if (obsValue !=null)b=obsValue.getValue();
		
		return b;
	}
}
