/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.search.NetworkSearchQuery;
import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.MdibListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.mdib.MDIBStructure;

/**
 * An entrance point for an application that acts as a service consumer in a BICEPS ecosystem.
 */
public interface IBICEPSClient {
	
	//client control
	/**
	 * Stops this {@link IBICEPSClient} instance.
	 */
	public abstract void stop();

	
	//Client config
	/**
	 * Checks if plug and play is enabled.
	 *
	 * @return <code>true</code>, if is plug and play is enabled
	 */
	public abstract boolean isPnpEnabled();

	/**
	 * Configures the plug and play capabilities (implicit discovery) of this {@link IBICEPSClient} instance.
	 *
	 * @param pnpEnabled if <code>true</code>, this {@link IBICEPSClient} instance will react on implicit discovery messages
	 */
	public abstract void setPnpEnabled(boolean pnpEnabled);

	/**
	 * Triggers a connect to the specified device.
	 *
	 * @param devRef the device reference
	 */
	public abstract void connectDevice(DeviceReference devRef);

	/**
	 * Disconnect from the remote device and remove all cached data of the device.
	 *
	 * @param devRef the device reference
	 */
	public abstract void disconnectDevice(DeviceReference devRef);
	
	/**
	 * Thread-safe list of UUID strings that represents a black list of devices with which communication is not desired. 
	 * Devices with a UUID that are on the list will not be forwarded to listeners.
	 * 
	 * @return Thread-safe list of blacklisted device uuids.
	 */
	public abstract List<String> getDeviceBlacklist();

	
	/**
	 * Thread-safe list of UUID strings that represents a white list of devices with which communication is desired. 
	 * Devices with a UUID that is not on the list will not be forwarded to listeners.
	 * 
	 * @return Thread-safe list of whitelisted device uuids.
	 */
	public abstract List<String> getDeviceWhitelist();
	
	/**
	 * Checks if is VM local device detection enabled.
	 *
	 * @return <code>true</code>, if is VM local device detection is enabled
	 */
	public abstract boolean isVMLocalDeviceDetectionEnabled();

	/**
	 * Sets the VM local device detection enabled.
	 *
	 * @param vmLocalDetectionEnabled if <code>true</code>, local device detection is enabled
	 */
	public abstract void setVMLocalDeviceDetectionEnabled(boolean vmLocalDetectionEnabled);
 
	
	//Mdib changes
	/**
	 * Adds a subscription for a {@link MdibListener}.
	 *
	 * @param listener the listener
	 */
	public abstract void subscribe(MdibListener listener);

	/**
	 * Removes a subscription for a {@link MdibListener}.
	 *
	 * @param listener the listener
	 */
	public abstract void unsubscribe(MdibListener listener);
	
	//	Search in network
	/**
	 * Search devices in the network.
	 *
	 * @param search the search
	 */
	public abstract void searchDevices(NetworkSearchQuery search);
	
	/**
	 * Gets the context discovery.
	 *
	 * @return the context discovery
	 */
	public abstract ContextDiscovery getContextDiscovery();
	
	
	//Get access to the network communication adapter
	/**
	 * Gets the communication adapter.
	 *
	 * @return the communication adapter
	 */
	public abstract CommunicationAdapter getCommunicationAdapter();
	

	//General proxy handling
	/**
	 * Releases a proxy.
	 *
	 * @param proxy the proxy
	 */
	public abstract void releaseDedicatedProxy(BICEPSProxy proxy);
	
	/**
	 * Release all proxies for a medical device system proxy.
	 *
	 * @param mdsProxy the mds proxy
	 */
	public abstract void releaseAllProxiesForMedicalDeviceSystemProxy(BICEPSMedicalDeviceSystem mdsProxy);
	
	//Medical Device System
	/**
	 * Retrieve medical device system for device reference.
	 *
	 * @param devRef the dev ref
	 * @return the list
	 */
	public abstract List<BICEPSMedicalDeviceSystem> retrieveMedicalDeviceSystemForDeviceReference(DeviceReference devRef);
	
	/**
	 * Gets the medical device systems.
	 *
	 * @param epr the epr
	 * @return the medical device systems
	 */
	public abstract Set<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems(EndpointReference epr);
	
	/**
	 * Gets the medical device systems.
	 *
	 * @return the medical device systems
	 */
	public abstract Set<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems();
	
	/**
	 * Gets the MDIB search structure.
	 *
	 * @param epr the epr
	 * @return the MDIB search structure
	 */
	public abstract MDIBStructure getMDIBSearchStructure(EndpointReference epr);
	
	/**
	 * Gets the MDIB search structure.
	 *
	 * @return the MDIB search structure
	 */
	public abstract Set<MDIBStructure> getMDIBSearchStructure();
	
	
	//Metrics
	
	/**
	 * Gets the metric.
	 *
	 * @param <T> the generic type
	 * @param proxyUniqueID the proxy unique id
	 * @return the metric
	 */
	public abstract  <T extends BICEPSMetric> T getMetric(ProxyUniqueID proxyUniqueID);

	/**
	 * Gets the metrics.
	 *
	 * @param <T> the generic type
	 * @param clientMedicalDeviceSystem the client medical device system
	 * @return the metrics
	 */
	public abstract <T extends BICEPSMetric> List<T> getMetrics(BICEPSMedicalDeviceSystem clientMedicalDeviceSystem);

	/**
	 * Gets the metrics.
	 *
	 * @param <T> the generic type
	 * @param typeCode the type code
	 * @return the metrics
	 */
	public abstract <T extends BICEPSMetric> List<T>  getMetrics(CodedValue typeCode);

	/**
	 * Gets the metrics.
	 *
	 * @param <T> the generic type
	 * @param clientMedicalDeviceSystem the client medical device system
	 * @param codeFilter the code filter
	 * @return the metrics
	 */
	public abstract <T extends BICEPSMetric> List<T> getMetrics(BICEPSMedicalDeviceSystem clientMedicalDeviceSystem, CodedValue codeFilter);

	
	//AlertConditions
	
	/**
	 * Gets the alert condition.
	 *
	 * @param <T> the generic type
	 * @param proxyUniqueID the proxy unique id
	 * @return the alert condition
	 */
	public abstract <T extends BICEPSAlertCondition> T getAlertCondition(ProxyUniqueID proxyUniqueID);

	/**
	 * Gets the alert conditions.
	 *
	 * @param <T> the generic type
	 * @return the alert conditions
	 */
	public abstract <T extends BICEPSAlertCondition> List<T> getAlertConditions();

	/**
	 * Gets the alert conditions.
	 *
	 * @param <T> the generic type
	 * @param mds the mds
	 * @return the alert conditions
	 */
	public abstract <T extends BICEPSAlertCondition> List<T> getAlertConditions(BICEPSMedicalDeviceSystem mds);

	/**
	 * Gets the alert conditions.
	 *
	 * @param <T> the generic type
	 * @param typeCode the type code
	 * @return the alert conditions
	 */
	public abstract <T extends BICEPSAlertCondition> List<T> getAlertConditions(CodedValue typeCode);


	//AlertSystems
	
	/**
	 * Gets the alert system.
	 *
	 * @param <T> the generic type
	 * @param proxyUniqueID the proxy unique id
	 * @return the alert system
	 */
	public abstract <T extends BICEPSAlertSystem> T getAlertSystem(ProxyUniqueID proxyUniqueID);
	
	/**
	 * Gets the alert systems.
	 *
	 * @param <T> the generic type
	 * @return the alert systems
	 */
	public abstract <T extends BICEPSAlertSystem> List<T> getAlertSystems();

	/**
	 * Gets the alert systems.
	 *
	 * @param <T> the generic type
	 * @param mds the mds
	 * @return the alert systems
	 */
	public abstract <T extends BICEPSAlertSystem> List<T> getAlertSystems(BICEPSMedicalDeviceSystem mds);

	
	//AlertSignals
	
	/**
	 * Gets the alert signal.
	 *
	 * @param <T> the generic type
	 * @param proxyUniqueID the proxy unique id
	 * @return the alert signal
	 */
	public abstract <T extends BICEPSAlertSignal> T getAlertSignal(ProxyUniqueID proxyUniqueID);

	/**
	 * Gets the alert signals.
	 *
	 * @param <T> the generic type
	 * @return the alert signals
	 */
	public abstract <T extends BICEPSAlertSignal> List<T> getAlertSignals();

	/**
	 * Gets the alert signals.
	 *
	 * @param <T> the generic type
	 * @param mds the mds
	 * @return the alert signals
	 */
	public abstract <T extends BICEPSAlertSignal> List<T> getAlertSignals(BICEPSMedicalDeviceSystem mds);

	/**
	 * Gets the alert signals.
	 *
	 * @param <T> the generic type
	 * @param typeCode the type code
	 * @return the alert signals
	 */
	public abstract <T extends BICEPSAlertSignal> List<T> getAlertSignals(CodedValue typeCode);



	//Context
	
	/**
	 * Gets the MDS contexts.
	 *
	 * @param <T> the generic type
	 * @return the MDS contexts
	 */
	public abstract <T extends BICEPSMDSContext> List<T> getMDSContexts();

	/**
	 * Gets the MDS contexts.
	 *
	 * @param <T> the generic type
	 * @param mds the mds
	 * @return the MDS contexts
	 */
	public abstract <T extends BICEPSMDSContext> List<T> getMDSContexts(BICEPSMedicalDeviceSystem mds);


	
	//Maneuver
	
	/**
	 * Gets the maneuver.
	 *
	 * @param <T> the generic type
	 * @param proxyUniqueID the proxy unique id
	 * @return the maneuver
	 */
	public abstract <T extends BICEPSManeuver> T getManeuver(ProxyUniqueID proxyUniqueID);
	
	/**
	 * Gets the maneuvers.
	 *
	 * @param <T> the generic type
	 * @return the maneuvers
	 */
	public abstract <T extends BICEPSManeuver> List<T> getManeuvers();
	
	/**
	 * Gets the maneuvers.
	 *
	 * @param <T> the generic type
	 * @param mds the mds
	 * @return the maneuvers
	 */
	public abstract <T extends BICEPSManeuver> List<T> getManeuvers(BICEPSMedicalDeviceSystem mds);
	
	/**
	 * Gets the maneuvers.
	 *
	 * @param <T> the generic type
	 * @param typeCode the type code
	 * @return the maneuvers
	 */
	public abstract <T extends BICEPSManeuver> List<T> getManeuvers(CodedValue typeCode);
	
	
	//	Streams
	
	/**
	 * Gets the stream.
	 *
	 * @param proxyUniqueID the proxy unique id
	 * @return the stream
	 */
	public abstract BICEPSStream getStream(ProxyUniqueID proxyUniqueID);
	
	/**
	 * Gets the streams.
	 *
	 * @param <T> the generic type
	 * @param medicalDeviceSystem the medical device system
	 * @return the streams
	 */
	public abstract <T extends BICEPSStream> List<T> getStreams(BICEPSMedicalDeviceSystem medicalDeviceSystem);

	/**
	 * Gets the devices.
	 *
	 * @param <T> the generic type
	 * @return the devices
	 */
	public abstract <T extends MDSDescriptor> List<T> getDevices();
	
	/**
	 * Gets the safety information for an {@link EndpointReference}s.
	 *
	 * @return the safety information for {@link EndpointReference}s.
	 */
	public abstract HashMap<EndpointReference, HashMap<String, BICEPSSafetyInformationPolicyElement>> getSafetyInformationMap();

}
