/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.callbacks;

import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;


public interface MDIBObjectLifeCycleListener {
	public abstract void invokationStateChanged(int transactionId, InvocationState newState,InvocationError errorCode, String errorMsg);
	public abstract void handleRemove();
	public abstract void handleSubscriptionEnded(SubscriptionEndCodeType reason);
}
