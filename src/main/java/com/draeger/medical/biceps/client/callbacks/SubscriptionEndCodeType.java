/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.callbacks;

import org.ws4d.java.types.URI;

public enum SubscriptionEndCodeType
{
    DeliveryFailure("http://schemas.xmlsoap.org/ws/2004/08/eventing/DeliveryFailure"),
    SourceShuttingDown("http://schemas.xmlsoap.org/ws/2004/08/eventing/SourceShuttingDown"),
    SourceCancelling("http://schemas.xmlsoap.org/ws/2004/08/eventing/SourceCancelling"),
    Timeout("/Timeout");

    final String value;

    private SubscriptionEndCodeType(String value)
    {
        this.value = value;
    }

    public boolean equals(URI reason)
    {
        return reason != null && reason.toString().equals(value);
    }

    public static SubscriptionEndCodeType fromUri(URI reason)
    {
        if (reason == null) throw new NullPointerException();
        String path = reason.getPath();
        int index = path.lastIndexOf("/");
        if (index != -1)
        {
            return valueOf(path.substring(index + 1));
        }
        else
            throw new IllegalArgumentException();
    }
}
