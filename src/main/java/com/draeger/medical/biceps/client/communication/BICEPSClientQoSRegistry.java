/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;

public class BICEPSClientQoSRegistry {
	private static final BICEPSClientQoSRegistry instance=new BICEPSClientQoSRegistry();

	public static BICEPSClientQoSRegistry getInstance() {
		return instance;
	}

	private BICEPSClientQoSRegistry(){
		//void
	}

	private final HashMap<String,Set<BICEPSQoSPolicy>> actionToQoS=new HashMap<String,Set<BICEPSQoSPolicy>>();

	public boolean containsPoliciesForAction(Object action) {
		return actionToQoS.containsKey(action);
	}

	public Set<BICEPSQoSPolicy> getQoSPolicyPolicyForAction(Object action) {
		return actionToQoS.get(action);
	}

	public boolean hasPoliciesForAction() {
		return actionToQoS.isEmpty();
	}

	public Set<String> actionsWithPolicies() {
		return actionToQoS.keySet();
	}

	public BICEPSQoSPolicy put(String action, BICEPSQoSPolicy policy) {
		Set<BICEPSQoSPolicy> policiesForAction = getQoSPolicyPolicyForAction(action);
		if (policiesForAction==null)
		{
			policiesForAction=new HashSet<BICEPSQoSPolicy>();
			actionToQoS.put(action, policiesForAction);
		}

		policiesForAction.add(policy);
		
		return policy;
		

	}

	public Set<BICEPSQoSPolicy> remove(Object action) {
		return actionToQoS.remove(action);
	}

	public Collection<Set<BICEPSQoSPolicy>> getAllPolicies() {
		return actionToQoS.values();
	}



}
