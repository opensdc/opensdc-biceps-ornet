/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;

public class BICEPSSafetyInformationPolicyElement implements BICEPSInvokeContextElement {
	private final BICEPSSafetyInformationQoSPolicy safetyInformationPolicy;
	private SafetyInformationPolicy mdpwsPolicy;

	public BICEPSSafetyInformationPolicyElement(
			BICEPSSafetyInformationQoSPolicy safetyInformationPolicy) {
		super();
		this.safetyInformationPolicy = safetyInformationPolicy;
	}

	public BICEPSSafetyInformationQoSPolicy getSafetyInformationPolicy() {
		return safetyInformationPolicy;
	}
	
	
	public SafetyInformationPolicy getMdpwsPolicy() {
		return mdpwsPolicy;
	}

	public void setMdpwsPolicy(SafetyInformationPolicy mdpwsPolicy) {
		if (this.mdpwsPolicy!=null && Log.isWarn()) Log.warn("Override mdpwsPolicy:"+this.mdpwsPolicy+" with "+mdpwsPolicy);
		this.mdpwsPolicy = mdpwsPolicy;
	}
	
}
