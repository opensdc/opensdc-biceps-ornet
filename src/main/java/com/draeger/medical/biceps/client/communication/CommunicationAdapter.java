/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.search.NetworkSearchQuery;
import com.draeger.medical.biceps.client.communication.impl.BICEPSClientAliveWatchDog;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.mdib.MDIBStructure;

public interface CommunicationAdapter {

	public abstract boolean add(CommunicationAdapterCallback arg0);

	public abstract boolean remove(CommunicationAdapterCallback arg0);

	public abstract boolean isPnpEnabled();

	public abstract void setPnpEnabled(boolean pnpEnabled);

	/**
	 * Connect to device and register for network-related discovery events.
	 *
	 * @param devRef the dev ref
	 * @param mdibContainer the mdib container
	 */
	public abstract void connectToDevice(DeviceReference devRef,MDIBStructure mdibContainer);

	public abstract BICEPSClientAliveWatchDog startBICEPSClientWatchDog(DeviceReference devRef);
	
	public abstract BICEPSClientAliveWatchDog startBICEPSClientWatchDog(DeviceReference devRef, int maxWatchDogTimeout, int maxWatchDogRetries);
	
	public abstract void stop();

	public abstract CommunicationContainer<MDIB, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveMdib(DeviceReference devRef) throws InvocationException, TimeoutException;
	
	public abstract CommunicationContainer<MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveMDDescription(DeviceReference devRef) throws InvocationException, TimeoutException;
	
	public abstract CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveDescription(DeviceReference devRef, String handle) throws InvocationException, TimeoutException;
	
	public abstract CommunicationContainer<ContainmentTree, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveContainmentTreeInfo(DeviceReference devRef, String handle) throws InvocationException, TimeoutException;
	
	public abstract CommunicationContainer<MDState, ?> retrieveStates(DeviceReference devRef) throws InvocationException,TimeoutException;
	
	public abstract CommunicationContainer<MDState, ?> retrieveStates(DeviceReference devRef, Collection<String> handles) throws InvocationException,TimeoutException;
	
	public abstract CommunicationContainer<MDState, ?> retrieveStates(DeviceReference devRef, String handle) throws InvocationException,TimeoutException;

	//XXX SSch Avoid
	public abstract void unsubscribeAsDeviceListener(Set<EndpointReference> keySet);

	public abstract void searchDevice(NetworkSearchQuery search);

	public abstract ProxyCommunication createProxyCommunication(MDIBStructure mdib, Descriptor descriptor, HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation);

	public abstract void registerMdibReports(EndpointReference networkDeviceEndpoint);
	
	public abstract void tickInterfaces(DeviceReference devRef);

	public abstract void setDeviceReferenceWhitelist(List<String> whitelist);
	
	public abstract void setDeviceReferenceBlacklist(List<String> blacklist);

	public abstract void setVMLocalDeviceDetectionEnabled(boolean vmLocalDeviceDetectionEnabled);
	
	public abstract ContextDiscovery getContextDiscovery();

	public abstract void deviceRemovedByWatchdog(DeviceReference deviceReference);

}
