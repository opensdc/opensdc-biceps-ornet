/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

import org.ws4d.java.client.SearchParameter;
import org.ws4d.java.service.reference.DeviceReference;

import com.draeger.medical.biceps.common.model.DescriptionModificationReport;
import com.draeger.medical.biceps.common.model.MDIB;

/**
 * Callback interface for network communication events.
 */
public interface CommunicationAdapterCallback {
	
	/**
	 * Invoked when announced its upcoming absence.
	 *
	 * @param devRef the device reference
	 */
	public void deviceBye(DeviceReference devRef);

	/**
	 * Invoked when a device reference is added to the device reference repository.
	 *
	 * @param devRef the device reference
	 */
	public void addDevice(DeviceReference devRef);

	/**
	 * Invoked when a search has been triggered and no devices were found.
	 *
	 * @param search the {@link SearchParameter} that has been used to trigger the search
	 */
	public void noDevicesFound(SearchParameter search);
	
	/**
	 * Invoked when a description in the {@link MDIB} has been modified.
	 *
	 * @param devRef the device reference to the network device where the MDIB is hosted
	 * @param report the {@link DescriptionModificationReport} that contains the information
	 */
	public void modifiedDescription(DeviceReference devRef, DescriptionModificationReport report);

	/**
	 * Invoked when the network endpoint or metadata od a device has been changed.
	 *
	 * @param deviceRef the device reference
	 */
	public void deviceChanged(DeviceReference deviceRef);
}
