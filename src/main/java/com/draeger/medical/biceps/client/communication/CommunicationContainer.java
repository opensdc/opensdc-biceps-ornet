/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

public class CommunicationContainer<M,C> {
	private final M messageElem;
	private final C messageContext;
	private final long sequenceNumber;

	public CommunicationContainer(M messageElem, C msgContext, long sequenceNumber) {
		super();
		this.messageElem = messageElem;
		this.messageContext=msgContext;
		this.sequenceNumber=sequenceNumber;
	}

	public M getMessageElem() {
		return messageElem;
	}

	public C getMessageContext() {
		return messageContext;
	}
	
	public long getSequenceNumber()
	{
		return this.sequenceNumber;
	}
}
