/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication;

import java.io.IOException;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.types.EndpointReference;

//import com.draeger.medical.biceps.client.mdib.MDIBStructure;

public interface SubscriptionManager {

	public abstract boolean removeMDIBSubscription(ClientSubscription subscription);

	public abstract void createStreamSubscription(EndpointReference networkDeviceEndpoint, String action) throws IOException, TimeoutException;

	public abstract void removeClientSubscription(ClientSubscription cs, boolean unsubscribe);

	

	//public abstract MDIBStructure getMDIBRepForSubscription(ClientSubscription cs);

	public abstract ClientSubscription getClientSubscription(EndpointReference networkDeviceEndpoint, String portType, String outputAction) throws EventingException, TimeoutException;
	
	public abstract void unregisterMdibReports() throws EventingException, TimeoutException;

	public abstract void unregisterMdibReports(EndpointReference networkDeviceEndpoint, boolean communicationFailed) throws EventingException, TimeoutException;

	public abstract void registerMdibReports(EndpointReference networkDeviceEndpoint);

}
