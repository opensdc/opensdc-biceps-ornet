/*******************************************************************************
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.discovery.context;

import java.util.List;

import org.ws4d.java.service.reference.DeviceReference;

import com.draeger.medical.biceps.common.context.ContextHelper.InstanceIdentifierWithType;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;


/**
 * A utility for discovering context information that is communicated by means of implicit or explicit discovery messages.
 */
public interface ContextDiscovery {
	
	/**
	 * Returns a list of instance identifiers that have been discovered in the network and that are related to a context descriptor.
	 * 
	 * @return List of discovered context instance identifiers.
	 */
	public List<InstanceIdentifier> getDiscoveredContextIds();
	
	/**
	 * Returns a list of instance identifiers that have been discovered in the network and that are related to a location context descriptor.
	 * 
	 * @return List of discovered context instance identifiers.
	 */
	public List<InstanceIdentifier> getDiscoveredLocationIds();
	
	/**
	 * Returns a list of instance identifiers that have been discovered in the network and that are related to a patient context descriptor.
	 * 
	 * @return List of discovered context instance identifiers.
	 */
	public List<InstanceIdentifier> getDiscoveredPatientIds();
	
	/**
	 * Returns a list of instance identifiers that have been discovered in the network and that are related to an ensemble context descriptor.
	 * 
	 * @return List of discovered context instance identifiers.
	 */
	public List<InstanceIdentifier> getDiscoveredEnsembleIds();
		
	/**
	 * Returns a list of instance identifiers that have been discovered in the network and that are related to the context descriptor class.
	 *
	 * @param <T> the generic type
	 * @param descriptorClass the descriptor class
	 * @return  List of discovered context instance identifiers.
	 */
	public <T extends AbstractContextDescriptor> List<InstanceIdentifier> getDiscoveredIdsForDescriptorType(Class<T> descriptorClass);
	
	/**
	 * Returns a list of device references that have been discovered in the network and that match at least one element in the id list.
	 *
	 * @param <T> the generic type
	 * @param descriptorClass the descriptor class
	 * @param ids the ids
	 * @return  List of discovered device references that match the ids.
	 */
	public <T extends AbstractContextDescriptor> List<DeviceReference>  getDeviceReferencesForInstanceIdentifiers(Class<T> descriptorClass, List<InstanceIdentifier> ids);

	/**
	 * Returns a list of ids with context descriptor type that are in the scope of the device.
	 *
	 * @param deviceReference the device reference
	 * @return  ids with context descriptor.
	 */
	public List<InstanceIdentifierWithType<AbstractContextDescriptor>>  getInstanceIdentifiersForDeviceReferences(DeviceReference deviceReference);
	
	/**
	 * Update with endpoint information.
	 *
	 * @param devRef the dev ref
	 */
	public void updateWithEndpointInformation(DeviceReference devRef);


	/**
	 * Clear the discovered context information.
	 */
	public void clear();
	
	
	/**
	 *  Fill the context information repository.
	 *
	 * @param doSearch if <code>true</code>, a network discovery will be performed.
	 */
	public void discover(boolean doSearch);
	
	
	/**
	 * Adds the listener.
	 *
	 * @param listener the listener
	 */
	public void addListener(ContextDiscoveryListener listener);
	
	/**
	 * Removes the listener.
	 *
	 * @param listener the listener
	 */
	public void removeListener(ContextDiscoveryListener listener);
}
