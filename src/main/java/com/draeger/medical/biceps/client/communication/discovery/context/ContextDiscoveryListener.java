/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.discovery.context;

import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;


/**
 * The listener interface for receiving contextDiscovery events.
 * The class that is interested in processing a contextDiscovery
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addListener<code> method. When
 * the contextDiscovery event occurs, that object's appropriate
 * method is invoked.
 *
 */
public interface ContextDiscoveryListener {
	
	/**
	 * Invoked when context update occurs.
	 *
	 * @param <T> the generic type
	 * @param contextDescriptorClass the context descriptor class
	 */
	public abstract <T extends AbstractContextDescriptor> void contextUpdated(Class<T> contextDescriptorClass);
}
