/**
 * 
 */
package com.draeger.medical.biceps.client.communication.discovery.context.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.concurrency.ThreadPool;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.dispatch.IDeviceServiceRegistry;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscoveryListener;
import com.draeger.medical.biceps.common.context.ContextHelper;
import com.draeger.medical.biceps.common.context.ContextHelper.InstanceIdentifierWithType;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;
import com.draeger.medical.biceps.common.model.OperatorContextDescriptor;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.util.ModelComparer;

/**
 * A default implementation for a context discovery repository. 
 * See {@link ContextDiscovery}.
 */
public class DefaultContextDiscovery implements ContextDiscovery {	

	private static final List<ContextDiscoveryListener> listeners=new CopyOnWriteArrayList<ContextDiscoveryListener>();
	private volatile HashMap<Class<? extends AbstractContextDescriptor>,List<InstanceIdentifier>> contextTypeMap=new HashMap<Class<? extends AbstractContextDescriptor>,List<InstanceIdentifier>>();
	private final CommunicationAdapter communicationAdapter;

	/**
	 * Instantiates a new default context discovery.
	 *
	 * @param communicationAdapter the communication adapter
	 */
	public DefaultContextDiscovery(CommunicationAdapter communicationAdapter)
	{
		this.communicationAdapter=communicationAdapter;
		initialize();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredContextIds()
	 */
	@Override
	public List<InstanceIdentifier> getDiscoveredContextIds() {
		List<InstanceIdentifier> contextIds=new ArrayList<InstanceIdentifier>();
		for (List<InstanceIdentifier> iiList : contextTypeMap.values()) {
			contextIds.addAll(iiList);
		}

		return contextIds;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredIdsForDescriptorType(java.lang.Class)
	 */
	@Override
	public <T extends AbstractContextDescriptor> List<InstanceIdentifier> getDiscoveredIdsForDescriptorType(Class<T> descriptorClass)
	{
		return contextTypeMap.get(descriptorClass);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredLocationIds()
	 */
	@Override
	public List<InstanceIdentifier> getDiscoveredLocationIds() {
		return getDiscoveredIdsForDescriptorType(LocationContextDescriptor.class);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredPatientIds()
	 */
	@Override
	public List<InstanceIdentifier> getDiscoveredPatientIds() {
		return getDiscoveredIdsForDescriptorType(PatientAssociationDescriptor.class);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredEnsembleIds()
	 */
	@Override
	public List<InstanceIdentifier> getDiscoveredEnsembleIds() {
		return getDiscoveredIdsForDescriptorType(EnsembleContextDescriptor.class);
	}

	private synchronized void updateContextWithEndpointInfo(final DeviceReference deviceRef) {
		Set<Class<AbstractContextDescriptor>> modifiedLists=new HashSet<Class<AbstractContextDescriptor>>(6);
		try {

			URI[] scopesAsArray = deviceRef.getScopesAsArray(true);
			if (scopesAsArray!=null )
			{
				for (int i=0;i<scopesAsArray.length;i++)
				{
					InstanceIdentifierWithType<AbstractContextDescriptor> createTypedInstanceIdentifier = ContextHelper.createTypedInstanceIdentifier(scopesAsArray[i]);

					if (createTypedInstanceIdentifier!=null 
							&& createTypedInstanceIdentifier.getInstanceIdentifier()!=null)
					{
						List<InstanceIdentifier> iiList= contextTypeMap.get(createTypedInstanceIdentifier.getIdentifierType());
						// VKa: iiList might be null, create as empty?
						if (iiList==null) {
							iiList = new ArrayList<InstanceIdentifier>();
							contextTypeMap.put(createTypedInstanceIdentifier.getIdentifierType(), iiList);
						}
						
						boolean contains = containedInList(createTypedInstanceIdentifier, iiList);
						if (!contains){
							iiList.add(createTypedInstanceIdentifier.getInstanceIdentifier());
							modifiedLists.add(createTypedInstanceIdentifier.getIdentifierType());

						}
					}
				}
			}
		} catch (TimeoutException e) {
			Log.info(e);
			Log.printStackTrace(e);
		}

		if (!modifiedLists.isEmpty()){
			contextTypeMapUpdated(modifiedLists);
		}

	}

	private void contextTypeMapUpdated(Set<Class<AbstractContextDescriptor>> modifiedLists) 
	{
		if (listeners!=null && !listeners.isEmpty())
		{
			for (ContextDiscoveryListener listener : listeners) {
				for (Class<AbstractContextDescriptor> modifiedDescriptorTypeLists : modifiedLists) {
					listener.contextUpdated(modifiedDescriptorTypeLists);
				}
			}
		}
	}

	private boolean containedInList(
			InstanceIdentifierWithType<AbstractContextDescriptor> createTypedInstanceIdentifier,
			List<InstanceIdentifier> iiList) {
		boolean contains=false;
		for (InstanceIdentifier instanceIdentifier : iiList) {
			if( ModelComparer.equals(instanceIdentifier,createTypedInstanceIdentifier.getInstanceIdentifier()))
			{
				contains=true;
				break;
			}
		}
		return contains;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#updateWithEndpointInformation(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public void updateWithEndpointInformation(final DeviceReference deviceRef) 
	{
		Toolkit toolkit = PlatformSupport.getInstance().getToolkit();
		if (toolkit!=null)
		{
			ThreadPool tPool=toolkit.getThreadPool();
			if (tPool!=null)
			{
				tPool.execute(new Runnable() {

					@Override
					public void run() {
						updateContextWithEndpointInfo(deviceRef);
					}
				});
			}
		}

	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#clear()
	 */
	@Override
	public void clear() {
		contextTypeMap.clear();

	}


	private void initialize() {

		List<InstanceIdentifier> patientIds=new CopyOnWriteArrayList<InstanceIdentifier>();
		//		patientIds.add(defaultPatientId);
		contextTypeMap.put(PatientAssociationDescriptor.class,patientIds );

		List<InstanceIdentifier> locationIds=new CopyOnWriteArrayList<InstanceIdentifier>();
		//		locationIds.add(defaultLocationId);
		contextTypeMap.put(LocationContextDescriptor.class,locationIds );

		List<InstanceIdentifier> ensembleIds=new CopyOnWriteArrayList<InstanceIdentifier>();
		//		ensembleIds.add(defaultLocationId);
		contextTypeMap.put(EnsembleContextDescriptor.class,ensembleIds );

		List<InstanceIdentifier> opertorIds=new CopyOnWriteArrayList<InstanceIdentifier>();
		//		opertorIds.add(defaultOtherId);
		contextTypeMap.put(OperatorContextDescriptor.class,opertorIds );

	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#discover()
	 */
	@Override
	public void discover(final boolean doSearch) {
		Toolkit toolkit = PlatformSupport.getInstance().getToolkit();
		if (toolkit!=null)
		{
			ThreadPool tPool=toolkit.getThreadPool();
			if (tPool!=null)
			{
				tPool.execute(new Runnable() {

					@Override
					public void run() {
						discoverEnvironment(doSearch);
					}
				});
			}
		}
	}

	private final void discoverEnvironment(boolean doSearch)
	{
		initialize();

		//Use Device registry
		DataStructure allRegisteredDevices = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getAllRegisteredDevices();
		Iterator it=allRegisteredDevices.iterator();
		while(it.hasNext())
		{
			updateWithEndpointInformation((DeviceReference)it.next());
		}

		//trigger search
		if (doSearch && this.communicationAdapter!=null)
		{
			this.communicationAdapter.searchDevice(null);
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#addListener(com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscoveryListener)
	 */
	@Override
	public void addListener(ContextDiscoveryListener listener) {
		if (listener!=null)
		{
			if (!listeners.contains(listener))
				listeners.add(listener);
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#removeListener(com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscoveryListener)
	 */
	@Override
	public void removeListener(ContextDiscoveryListener listener) {
		if (listener!=null)
		{
			if (listeners.contains(listener))
				listeners.remove(listener);
		}

	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDiscoveredIdsForDescriptorType(java.lang.Class, java.util.List)
	 */
	@Override
	public <T extends AbstractContextDescriptor> List<DeviceReference> getDeviceReferencesForInstanceIdentifiers(
			Class<T> descriptorClass, List<InstanceIdentifier> ids) {
		ArrayList<DeviceReference> deviceReferences=new ArrayList<DeviceReference>();

		IDeviceServiceRegistry deviceServiceRegistry = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry();
		if (deviceServiceRegistry!=null)
		{
			DataStructure devReferences = deviceServiceRegistry.getAllRegisteredDevices();

			if (devReferences!=null)
			{			
				Iterator iterator = devReferences.iterator();

				while(iterator.hasNext())
				{
					DeviceReference deviceRef = (DeviceReference) iterator.next();
					try{
						URI[] scopesAsArray = deviceRef.getScopesAsArray(true);
						if (scopesAsArray!=null )
						{
							for (int i=0;i<scopesAsArray.length;i++)
							{
								InstanceIdentifierWithType<AbstractContextDescriptor>  createTypedInstanceIdentifier = ContextHelper.createTypedInstanceIdentifier(scopesAsArray[i]);

								if (createTypedInstanceIdentifier!=null 
										&& createTypedInstanceIdentifier.getInstanceIdentifier()!=null)
								{
									if (isMatchingId(createTypedInstanceIdentifier, descriptorClass,ids))
									{
										deviceReferences.add(deviceRef);
									}
								}
							}
						}
					}catch(CommunicationException e){
						Log.info(e);
					}
				}
			}
		}

		return deviceReferences;
	}


	private boolean isMatchingId(
			InstanceIdentifierWithType<AbstractContextDescriptor>  createTypedInstanceIdentifier,
			Class<?> descriptorClass, List<InstanceIdentifier> ids) {
		boolean isMatching=false;

		if (descriptorClass.isAssignableFrom(createTypedInstanceIdentifier.getIdentifierType()))
		{
			for (InstanceIdentifier instanceIdentifier : ids) {
				if (instanceIdentifier!=null)
				{
					isMatching=ModelComparer.equals(instanceIdentifier, createTypedInstanceIdentifier.getInstanceIdentifier());
					if (isMatching) break;
				}
			}
		}

		return isMatching;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery#getDeviceReferencesForInstanceIdentifiers(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public List<InstanceIdentifierWithType<AbstractContextDescriptor>> getInstanceIdentifiersForDeviceReferences(DeviceReference deviceReference) {
		List<InstanceIdentifierWithType<AbstractContextDescriptor>> retVal=new ArrayList<InstanceIdentifierWithType<AbstractContextDescriptor>>();

		if (deviceReference!=null)
		{			
			try{
				URI[] scopesAsArray = deviceReference.getScopesAsArray(true);
				if (scopesAsArray!=null )
				{
					for (int i=0;i<scopesAsArray.length;i++)
					{
						InstanceIdentifierWithType<AbstractContextDescriptor>  createTypedInstanceIdentifier = ContextHelper.createTypedInstanceIdentifier(scopesAsArray[i]);

						if (createTypedInstanceIdentifier!=null 
								&& createTypedInstanceIdentifier.getInstanceIdentifier()!=null)
						{
							retVal.add(createTypedInstanceIdentifier);
						}
					}
				}
			}catch(CommunicationException e){
				Log.info(e);
			}
		}

		return retVal;
	}


}
