/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.impl;

import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.util.Log;
public class BICEPSClientAliveWatchDog implements Runnable
{
	private final DeviceReference devRef;
	private final WatchDogCallback callback;

	private int maxWatchDogTimeout=Integer.parseInt(System.getProperty("BICEPS.BICEPSClientAliveWatchDog.MaxWatchDogTimeout", "5000"));
	private int maxWatchDogRetries=Integer.parseInt(System.getProperty("BICEPS.BICEPSClientAliveWatchDog.MaxWatchDogRetries", "1"));;
	private volatile boolean stopped = false;
	private final ResponseCallback responseCallback=new ResponseCallback();
	private boolean touched=false;
	private volatile int watchDogTimeoutCnt=0;

	public BICEPSClientAliveWatchDog(DeviceReference devRef, WatchDogCallback callback, String metricHandle)
	{
		this.devRef = devRef;
		this.callback = callback;

		if (Log.isDebug())Log.debug("Watchdog:"+this.devRef);
	}


	public void touch()
	{
		if (Log.isDebug()) Log.debug("Watchdog touched");

		touched=true;
	}

	public boolean isStopped() {
		return stopped;
	}

	public void start() 
	{
		PlatformSupport.getInstance().getToolkit().getThreadPool().execute(this);
	}

	public void stopBICEPSClientWatchdog()
	{
		this.stopped = true;	
		responseCallback.comState=ResponseCallback.NOT_RUNNING;
	}
	
	
	public int getMaxWatchDogTimeout() {
		return maxWatchDogTimeout;
	}


	public void setMaxWatchDogTimeout(int maxWatchDogTimeout) {
		this.maxWatchDogTimeout = maxWatchDogTimeout;
	}


	public int getMaxWatchDogRetries() {
		return maxWatchDogRetries;
	}


	public void setMaxWatchDogRetries(int maxWatchDogRetries) {
		this.maxWatchDogRetries = maxWatchDogRetries;
	}


	@Override
	public void run() 
	{
		while(!isStopped())
		{

			try {
				long currentWatchDogTimeout=maxWatchDogTimeout;
				if (!touched)
				{
					long startTS=System.currentTimeMillis();
					defaultAliveInvoke();
					responseCallback.comState=ResponseCallback.NOT_RUNNING;
					currentWatchDogTimeout=maxWatchDogTimeout-(System.currentTimeMillis()-startTS);
				}else{
					touched=false;
				}
				Thread.sleep(currentWatchDogTimeout>0?currentWatchDogTimeout:1);
			} catch (Exception e) {
				watchDogTimeoutCnt++;
				if (watchDogTimeoutCnt>maxWatchDogRetries)
				{
					if (Log.isInfo()){
						Log.info("Watchdog needs to bark! DeviceReference"+this.devRef+" TimeoutCnt:"+watchDogTimeoutCnt+"/"+maxWatchDogRetries);
						Log.info(e);
					}
					stopBICEPSClientWatchdog();
					callback.action(this);
					watchDogTimeoutCnt=0;
				}
			}
		}
	}

	private void defaultAliveInvoke() throws Exception 
	{
		if (responseCallback.comState==ResponseCallback.NOT_RUNNING)
		{
			responseCallback.comState=ResponseCallback.PENDLING;
			devRef.probeDeviceDirectly(responseCallback);

			//see http://sourceforge.net/p/opensdc/tickets/143/
			if (responseCallback.comState==ResponseCallback.PENDLING)
			{
				synchronized(responseCallback){
					responseCallback.wait(maxWatchDogTimeout/maxWatchDogRetries);
				}
			}
		}
		switch (responseCallback.comState)
		{
		case ResponseCallback.OK:
			break;
		default:
			throw new Exception("Direct Probe failed for "+devRef);
		}
	}

	public DeviceReference getDeviceReference() {
		return devRef;
	}

}

