/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.impl;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.message.InvokeMessage;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.BicepsClientFramework;
import com.draeger.medical.biceps.client.IBICEPSClient;
import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.CommunicationContainer;
import com.draeger.medical.biceps.client.communication.SubscriptionManager;
import com.draeger.medical.biceps.client.proxy.BICEPSControlProxy;
import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.ReportProvider;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertConditionControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSignalControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSystemControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContextElement;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertConditionState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSignalState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSystemState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMetricState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.client.utils.InvokeHelper;
import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.model.AbstractAlertReport;
import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractContextChangedReport;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractMetricReport;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertReportPart;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ContextChangedReportPart;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.EpisodicAlertReport;
import com.draeger.medical.biceps.common.model.EpisodicMetricReport;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricReportPart;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationInvokedReport;
import com.draeger.medical.biceps.common.model.OperationInvokedReportPart;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReport;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReportPart;
import com.draeger.medical.biceps.common.model.PeriodicAlertReport;
import com.draeger.medical.biceps.common.model.PeriodicMetricReport;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.WaveformStream;
import com.draeger.medical.biceps.common.model.util.MetricDescriptorUtil;
import com.draeger.medical.biceps.mdib.MDIBStructure;
import com.draeger.medical.mdpws.client.MDPWSClient.DefaultStreamHandler;
import com.draeger.medical.mdpws.domainmodel.impl.client.ProxyStreamSource;

@SuppressWarnings("rawtypes")
public class DefaultReportProvider extends DefaultStreamHandler implements
ReportProvider {
	private final Map<ClientSubscription, SequenceNumberInformation> cs2SequenceNumber = new ConcurrentHashMap<ClientSubscription, SequenceNumberInformation>();

	private final Map<ClientSubscription, List<BICEPSMetric>> cs2Metric = new ConcurrentHashMap<ClientSubscription, List<BICEPSMetric>>();
	private final Map<BICEPSMetric, Set<ClientSubscription>> metric2Cs = new ConcurrentHashMap<BICEPSMetric, Set<ClientSubscription>>();
	private final Map<BICEPSMetric, ClientSubscription> metricSetter2Cs = new ConcurrentHashMap<BICEPSMetric, ClientSubscription>();

	private final Map<ClientSubscription, List<BICEPSAlertCondition>> cs2AlertCondition = new ConcurrentHashMap<ClientSubscription, List<BICEPSAlertCondition>>();
	private final Map<BICEPSAlertCondition, ClientSubscription> alertCondition2Cs = new ConcurrentHashMap<BICEPSAlertCondition, ClientSubscription>();
	private final Map<BICEPSAlertCondition, ClientSubscription> alertConditionSetter2Cs = new ConcurrentHashMap<BICEPSAlertCondition, ClientSubscription>();

	private final Map<ClientSubscription, List<BICEPSAlertSystem>> cs2AlertSystem = new ConcurrentHashMap<ClientSubscription, List<BICEPSAlertSystem>>();
	private final Map<BICEPSAlertSystem, ClientSubscription> alertSystem2Cs = new ConcurrentHashMap<BICEPSAlertSystem, ClientSubscription>();
	private final Map<BICEPSAlertSystem, ClientSubscription> alertSystemSetter2Cs = new ConcurrentHashMap<BICEPSAlertSystem, ClientSubscription>();

	private final Map<ClientSubscription, List<BICEPSAlertSignal>> cs2AlertSignal = new ConcurrentHashMap<ClientSubscription, List<BICEPSAlertSignal>>();
	private final Map<BICEPSAlertSignal, ClientSubscription> alertSignal2Cs = new ConcurrentHashMap<BICEPSAlertSignal, ClientSubscription>();
	private final Map<BICEPSAlertSignal, ClientSubscription> alertSignalSetter2Cs = new ConcurrentHashMap<BICEPSAlertSignal, ClientSubscription>();

	private final Map<ClientSubscription, List<BICEPSMDSContextElement>> cs2Context = new ConcurrentHashMap<ClientSubscription, List<BICEPSMDSContextElement>>();
	private final Map<BICEPSMDSContextElement, Set<ClientSubscription>> context2Cs = new ConcurrentHashMap<BICEPSMDSContextElement, Set<ClientSubscription>>();
	private final Map<BICEPSMDSContextElement, ClientSubscription> contextSetter2Cs = new ConcurrentHashMap<BICEPSMDSContextElement, ClientSubscription>();

	private final Map<ClientSubscription, List<BICEPSManeuver>> cs2Maneuver = new ConcurrentHashMap<ClientSubscription, List<BICEPSManeuver>>();
	private final Map<BICEPSManeuver, ClientSubscription> maneuver2Cs = new ConcurrentHashMap<BICEPSManeuver, ClientSubscription>();

	private final Map<ProxyUniqueID, BICEPSStream> metricHandle2Stream = new ConcurrentHashMap<ProxyUniqueID, BICEPSStream>();

	private final SubscriptionManager subscriptionManager;
	private final CommunicationAdapter communicationAdapter;

	public DefaultReportProvider(SubscriptionManager subscriptionManager,
			CommunicationAdapter communicationAdapter) {
		this.subscriptionManager = subscriptionManager;
		this.communicationAdapter = communicationAdapter;
	}

	@Override
	public void stop() {
		handleStop(metric2Cs);
		handleStop(alertCondition2Cs);
		handleStop(alertSignal2Cs);
		handleStop(context2Cs);
		handleStop(maneuver2Cs);
		this.cs2SequenceNumber.clear();
		// TODO MVo Unsubscribe stream handler!
	}

	private void handleStop(Map map) {
		if (map != null) {
			for (Object o : map.keySet()) {
				if (o instanceof List) {
					List l = (List) o;
					if (!l.isEmpty()) {
						for (Object lo : l) {
							handleRemovedItem(lo);
						}
					}
				} else {
					handleRemovedItem(o);
				}
			}
		}
	}

	private void handleRemovedItem(Object o) {
		if (o instanceof BICEPSProxy) {
			((BICEPSProxy) o).removed();
		} else {
			Log.debug("Could not handle remove item in DefaultReportProvider: "
					+ o);
		}
	}

	private final DeviceReference getDeviceReferenceFromEPR(
			EndpointReference networkDeviceEndpoint) {
		return DeviceServiceRegistryProvider.getInstance()
				.getDeviceServiceRegistry()
				.getStaticDeviceReference(networkDeviceEndpoint);
	}

	public void subscribeReport(BICEPSMetric metric) {
		if (metric != null) {
			// ClientStateProxy<? extends MetricDescriptor, ? extends
			// MetricListener<?,?, ?>, AbstractMetricState> stateProxy =
			// metric.getStateProxy();
			BICEPSMetricState stateProxy = metric.getStateProxy();
			if (stateProxy != null) {
				MetricDescriptor mDescriptor = stateProxy.getDescriptor();

				ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);

				if (mDescriptor != null && !metric2Cs.containsKey(metric)) {
					if (mDescriptor.getHandle() != null)
						stateHandlesReferences.add(mDescriptor.getHandle());

					ClientSubscription cs = null;
					try {
						if (MetricDescriptorUtil.isRetrievable(
								mDescriptor,
								MetricRetrievability.EPISODIC)) 
						{
							cs = this.subscriptionManager
									.getClientSubscription(
											metric.getEndpointReference(),
											MDPWSConstants.PORTTYPE_REPORT_SERVICE,
											MDPWSConstants.ACTION_EPISODIC_METRIC_REPORT);
						}
						if (cs == null
								&& MetricDescriptorUtil.isRetrievable(
										mDescriptor,
										MetricRetrievability.PERIODIC)
								) {
							cs = this.subscriptionManager
									.getClientSubscription(
											metric.getEndpointReference(),
											MDPWSConstants.PORTTYPE_REPORT_SERVICE,
											MDPWSConstants.ACTION_PERIODIC_METRIC_REPORT);
						}

					} catch (TimeoutException e) {
						Log.warn(e);
					} catch (EventingException e) {
						Log.warn(e);
					}
					if (cs != null) {
						List<BICEPSMetric> list = getBICEPSMetricsForSubscription(cs);
						if (list == null) {
							list = new CopyOnWriteArrayList<BICEPSMetric>();
							cs2Metric.put(cs, list);
						}
						list.add(metric);
						//						metric2Cs.put(metric, cs);
						addToProxy2CSMap(metric, cs, metric2Cs);
					} else {
						if (Log.isWarn())
							Log.warn("Could not subscribe to any Event for Client Metric: "
									+ stateProxy.getHandleOnEndpointReference()+" episodic:"+MetricDescriptorUtil.isRetrievable(mDescriptor,MetricRetrievability.EPISODIC)+" Periodic:"+MetricDescriptorUtil.isRetrievable(mDescriptor,MetricRetrievability.PERIODIC)+" "+MetricDescriptorUtil.getRetrievabilityOptions(mDescriptor));
					}

					// Setting related subscriptions
					BICEPSControlProxy settingProxy = stateProxy.getControlProxy();
					if (settingProxy!=null){
						if (settingProxy.getOperationDescriptors() != null) {
							for (OperationDescriptor opDesc : settingProxy
									.getOperationDescriptors()) {
								stateHandlesReferences.add(opDesc.getHandle());
							}
						}
						subscribeToRemoteOperationEvents(metric,metricSetter2Cs,cs2Metric);
					}

					EndpointReference proxyNetworkEndpoint = metric.getEndpointReference();
					updateStates(metric,stateHandlesReferences, proxyNetworkEndpoint);

					//					if (settingProxy != null
					//							&& !metricSetter2Cs.containsKey(metric)) {
					//						if (settingProxy.getOperationDescriptors() != null) {
					//							for (OperationDescriptor opDesc : settingProxy
					//									.getOperationDescriptors()) {
					//								stateHandlesReferences.add(opDesc.getHandle());
					//							}
					//
					//						}
					//						
					//						cs = null;
					//						try {
					//							cs = this.subscriptionManager
					//							.getClientSubscription(
					//									metric.getEndpointReference(),
					//									MDPWSConstants.PORTTYPE_REPORT_SERVICE,
					//									MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT);
					//
					//							if (cs != null) {
					//								List<BICEPSMetric> list = getBICEPSMetricsForSubscription(cs);
					//								if (list == null) {
					//									list = new CopyOnWriteArrayList<BICEPSMetric>();
					//									cs2Metric.put(cs, list);
					//								}
					//								list.add(metric);
					//								metricSetter2Cs.put(metric, cs);
					//
					//								cs = this.subscriptionManager
					//								.getClientSubscription(
					//										metric.getEndpointReference(),
					//										MDPWSConstants.PORTTYPE_REPORT_SERVICE,
					//										MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT);
					//
					//								if (cs!=null)
					//								{
					//									list = getBICEPSMetricsForSubscription(cs);
					//									if (list == null) {
					//										list = new CopyOnWriteArrayList<BICEPSMetric>();
					//										cs2Metric.put(cs, list);
					//									}
					//									list.add(metric);
					//								}
					//							}
					//						} catch (Exception e) {
					//							if (Log.isWarn())
					//								Log.warn(e.getMessage());
					//							Log.printStackTrace(e);
					//						}
					//					}
					//
					//					EndpointReference proxyNetworkEndpoint = metric.getEndpointReference();
					//					updateStateAfterSubscription(metric,stateHandlesReferences, proxyNetworkEndpoint);

				} else {
					if (Log.isDebug())
						Log.debug("Already subscribed " + metric
								+ " or descriptor [" + metric.getDescriptor()
								+ "] not available.");
				}
			}
		}
	}

	private List<State> updateStates(BICEPSProxy proxy,
			List<String> stateHandlesReferences,
			EndpointReference proxyNetworkEndpoint) {
		List<State> retVal=new ArrayList<State>();
		DeviceReference devRef = getDeviceReferenceFromEPR(proxyNetworkEndpoint);
		if (devRef != null) {
			try {
				CommunicationContainer<MDState, ?> comCtr = this.communicationAdapter
						.retrieveStates(devRef, stateHandlesReferences);
				if (comCtr != null) {
					MDState mdStates = comCtr.getMessageElem();
					if (mdStates != null && mdStates.getStates() != null) {
						for (State state : mdStates.getStates()) {
							BICEPSProxyUtil.updateBICEPSProxy(state, proxy,BigInteger.valueOf( comCtr.getSequenceNumber()));
							retVal.add(state);
						}
					}
				}

			} catch (Exception e) {
				Log.warn(e);
			}
		}
		return retVal;
	}

	public void unsubscribeReport(BICEPSMetric metric) {

		if (metric != null) {
			BICEPSMetricState stateProxy = metric.getStateProxy();
			if (stateProxy != null) {
				unsubscribeNonSettingReports(metric, cs2Metric, metric2Cs);

				if (metricSetter2Cs.containsKey(metric)) {
					ClientSubscription cs = metricSetter2Cs.remove(metric);
					List<BICEPSMetric> list = getBICEPSMetricsForSubscription(cs);
					list.remove(metric);
					if (list.isEmpty()) {
						cs2Metric.remove(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}
			}
		}
	}

	public boolean isReportSubscribed(BICEPSMetric metric) {
		boolean retVal = false;
		if (metric != null && metric2Cs != null)
			retVal = metric2Cs.containsKey(metric);
		return retVal;
	}

	public boolean isReportSubscribed(BICEPSProxy metric,
			Map<?, ClientSubscription> map) {
		boolean retVal = false;
		if (metric != null && map != null)
			retVal = map.containsKey(metric);
		return retVal;
	}

	public void subscribeReport(BICEPSAlertCondition alert) {
		if (alert != null) {
			BICEPSAlertConditionState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				//				if (!alertCondition2Cs.containsKey(alert)) {
				AlertConditionDescriptor mDescriptor = stateProxy.getDescriptor();

				ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);

				if (mDescriptor != null && !alertCondition2Cs.containsKey(alert)) {
					if (mDescriptor.getHandle() != null)
						stateHandlesReferences.add(mDescriptor.getHandle());

					ClientSubscription cs = null;
					try {
						cs = this.subscriptionManager.getClientSubscription(
								alert.getEndpointReference(),
								MDPWSConstants.PORTTYPE_REPORT_SERVICE,
								MDPWSConstants.ACTION_EPISODIC_ALERT_REPORT);
						if (cs == null) {
							cs = this.subscriptionManager
									.getClientSubscription(
											alert.getEndpointReference(),
											MDPWSConstants.PORTTYPE_REPORT_SERVICE,
											MDPWSConstants.ACTION_PERIODIC_ALERT_REPORT);
						}
					} catch (TimeoutException e) {
						Log.warn(e);
					} catch (EventingException e) {
						Log.warn(e);
					}
					if (cs != null) {
						List<BICEPSAlertCondition> list = cs2AlertCondition
								.get(cs);
						if (list == null) {
							list = new CopyOnWriteArrayList<BICEPSAlertCondition>();
							cs2AlertCondition.put(cs, list);
						}
						list.add(alert);
						alertCondition2Cs.put(alert, cs);
					}

					//					BICEPSAlertConditionControl settingProxy = stateProxy
					//					.getControlProxy();
					//					if (settingProxy != null
					//							&& !alertConditionSetter2Cs.containsKey(alert)) {
					//						cs = null;
					//						try {
					//							cs = this.subscriptionManager
					//							.getClientSubscription(
					//									alert.getEndpointReference(),
					//									MDPWSConstants.PORTTYPE_REPORT_SERVICE,
					//									MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT);
					//						} catch (EventingException e) {
					//							Log.warn(e.getMessage());
					//							Log.printStackTrace(e);
					//						} catch (TimeoutException e) {
					//							Log.warn(e.getMessage());
					//							Log.printStackTrace(e);
					//						}
					//						if (cs != null) {
					//							List<BICEPSAlertCondition> list = cs2AlertCondition
					//							.get(cs);
					//							if (list == null) {
					//								list = new CopyOnWriteArrayList<BICEPSAlertCondition>();
					//								cs2AlertCondition.put(cs, list);
					//							}
					//							list.add(alert);
					//							alertConditionSetter2Cs.put(alert, cs);
					//						}
					//					}
					BICEPSAlertConditionControl settingProxy = stateProxy.getControlProxy();
					if (settingProxy!=null){
						if (settingProxy.getOperationDescriptors() != null) {
							for (OperationDescriptor opDesc : settingProxy
									.getOperationDescriptors()) {
								stateHandlesReferences.add(opDesc.getHandle());
							}
						}
						subscribeToRemoteOperationEvents(alert,alertConditionSetter2Cs,cs2AlertCondition);
					}

					EndpointReference proxyNetworkEndpoint = alert.getEndpointReference();
					updateStates(alert,stateHandlesReferences, proxyNetworkEndpoint);
				} else {
					if (Log.isDebug())
						Log.debug("Already subscribed " + alert
								+ " or descriptor [" + alert.getDescriptor()
								+ "] not available.");
				}

			}
		}
	}

	public void subscribeReport(BICEPSAlertSystem alert) {
		if (alert != null) {
			BICEPSAlertSystemState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				//				if (!alertSystem2Cs.containsKey(alert)) {
				AlertSystemDescriptor mDescriptor = stateProxy.getDescriptor();

				ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);

				if (mDescriptor != null && !alertSystem2Cs.containsKey(alert)) {
					if (mDescriptor.getHandle() != null)
						stateHandlesReferences.add(mDescriptor.getHandle());

					ClientSubscription cs = null;
					try {
						cs = this.subscriptionManager.getClientSubscription(
								alert.getEndpointReference(),
								MDPWSConstants.PORTTYPE_REPORT_SERVICE,
								MDPWSConstants.ACTION_EPISODIC_ALERT_REPORT);
						if (cs == null) {
							cs = this.subscriptionManager
									.getClientSubscription(
											alert.getEndpointReference(),
											MDPWSConstants.PORTTYPE_REPORT_SERVICE,
											MDPWSConstants.ACTION_PERIODIC_ALERT_REPORT);
						}
					} catch (TimeoutException e) {
						Log.warn(e);
					} catch (EventingException e) {
						Log.warn(e);
					}
					if (cs != null) {
						List<BICEPSAlertSystem> list = cs2AlertSystem.get(cs);
						if (list == null) {
							list = new CopyOnWriteArrayList<BICEPSAlertSystem>();
							cs2AlertSystem.put(cs, list);
						}
						list.add(alert);
						alertSystem2Cs.put(alert, cs);
					}

					BICEPSAlertSystemControl settingProxy = stateProxy.getControlProxy();
					if (settingProxy!=null){
						if (settingProxy.getOperationDescriptors() != null) {
							for (OperationDescriptor opDesc : settingProxy
									.getOperationDescriptors()) {
								stateHandlesReferences.add(opDesc.getHandle());
							}
						}
						subscribeToRemoteOperationEvents(alert,alertSystemSetter2Cs,cs2AlertSystem);
					}

					EndpointReference proxyNetworkEndpoint = alert.getEndpointReference();
					updateStates(alert,stateHandlesReferences, proxyNetworkEndpoint);

				}
			}
		}
	}

	public void subscribeReport(BICEPSAlertSignal alert) {
		if (alert != null) {
			BICEPSAlertSignalState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				//				if (!alertSignal2Cs.containsKey(alert)) {
				AlertSignalDescriptor mDescriptor = stateProxy.getDescriptor();

				ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);

				if (mDescriptor != null && !alertSystem2Cs.containsKey(alert)) {
					if (mDescriptor.getHandle() != null)
						stateHandlesReferences.add(mDescriptor.getHandle());

					ClientSubscription cs = null;
					try {
						cs = this.subscriptionManager.getClientSubscription(
								alert.getEndpointReference(),
								MDPWSConstants.PORTTYPE_REPORT_SERVICE,
								MDPWSConstants.ACTION_EPISODIC_ALERT_REPORT);
						if (cs == null) {
							cs = this.subscriptionManager
									.getClientSubscription(
											alert.getEndpointReference(),
											MDPWSConstants.PORTTYPE_REPORT_SERVICE,
											MDPWSConstants.ACTION_PERIODIC_ALERT_REPORT);
						}
					} catch (TimeoutException e) {
						Log.warn(e);
					} catch (EventingException e) {
						Log.warn(e);
					}
					if (cs != null) {
						List<BICEPSAlertSignal> list = cs2AlertSignal.get(cs);
						if (list == null) {
							list = new CopyOnWriteArrayList<BICEPSAlertSignal>();
							cs2AlertSignal.put(cs, list);
						}
						list.add(alert);
						alertSignal2Cs.put(alert, cs);
					}

					BICEPSAlertSignalControl settingProxy = stateProxy.getControlProxy();
					if (settingProxy!=null){
						if (settingProxy.getOperationDescriptors() != null) {
							for (OperationDescriptor opDesc : settingProxy
									.getOperationDescriptors()) {
								stateHandlesReferences.add(opDesc.getHandle());
							}
						}
						subscribeToRemoteOperationEvents(alert,alertSignalSetter2Cs,cs2AlertSignal);
					}

					EndpointReference proxyNetworkEndpoint = alert.getEndpointReference();
					updateStates(alert,stateHandlesReferences, proxyNetworkEndpoint);

				}
			}
		}
	}

	public void unsubscribeReport(BICEPSAlertCondition alert) {
		if (alert != null) {
			BICEPSAlertConditionState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				if (alertCondition2Cs.containsKey(alert)) {
					ClientSubscription cs = alertCondition2Cs.remove(alert);
					List<BICEPSAlertCondition> list = cs2AlertCondition.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertCondition.remove(cs);
						notifyClientSubscriptionInvalid(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}

				if (alertConditionSetter2Cs.containsKey(alert)) {
					ClientSubscription cs = alertConditionSetter2Cs
							.remove(alert);
					List<BICEPSAlertCondition> list = cs2AlertCondition.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertCondition.remove(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}
			}
		}
	}

	public void unsubscribeReport(BICEPSAlertSystem alert) {
		if (alert != null) {
			BICEPSAlertSystemState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				if (alertSystem2Cs.containsKey(alert)) {
					ClientSubscription cs = alertSystem2Cs.remove(alert);
					List<BICEPSAlertSystem> list = cs2AlertSystem.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertSystem.remove(cs);
						notifyClientSubscriptionInvalid(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}

				if (alertSystemSetter2Cs.containsKey(alert)) {
					ClientSubscription cs = alertSystemSetter2Cs.remove(alert);
					List<BICEPSAlertSystem> list = cs2AlertSystem.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertSystem.remove(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}
			}
		}
	}

	public void unsubscribeReport(BICEPSAlertSignal alert) {
		if (alert != null) {
			BICEPSAlertSignalState stateProxy = alert.getStateProxy();
			if (stateProxy != null) {
				if (alertSignal2Cs.containsKey(alert)) {
					ClientSubscription cs = alertSignal2Cs.remove(alert);
					List<BICEPSAlertSignal> list = cs2AlertSignal.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertSignal.remove(cs);
						notifyClientSubscriptionInvalid(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}

				if (alertSignalSetter2Cs.containsKey(alert)) {
					ClientSubscription cs = alertSignalSetter2Cs.remove(alert);
					List<BICEPSAlertSignal> list = cs2AlertSignal.get(cs);
					list.remove(alert);
					if (list.isEmpty()) {
						cs2AlertSignal.remove(cs);
						this.subscriptionManager.removeClientSubscription(cs,
								true);
					}
				}
			}
		}
	}

	public boolean isReportSubscribed(BICEPSAlertSystem alert) {
		return (alert != null && alertSystem2Cs.containsKey(alert));
	}

	public boolean isReportSubscribed(BICEPSAlertCondition alert) {
		return (alert != null && alertCondition2Cs.containsKey(alert));
	}

	public boolean isReportSubscribed(BICEPSAlertSignal alert) {
		return (alert != null && alertSignal2Cs.containsKey(alert));
	}

	public void subscribeReport(BICEPSMDSContextElement proxy) {
		if (proxy != null) {
			// ClientStateProxy<? extends MetricDescriptor, ? extends
			// MetricListener<?,?, ?>, AbstractMetricState> stateProxy =
			// metric.getStateProxy();
			BICEPSMDSContextElementState stateProxy = proxy.getStateProxy();
			if (stateProxy != null) {
				AbstractContextDescriptor descriptor = stateProxy.getDescriptor();

				ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);

				if (descriptor != null && !context2Cs.containsKey(proxy)) {

					ArrayList<ArrayList<String>> eventNames = createPatientAssociationStateReportEventNames();
					EndpointReference proxyNetworkEndpoint = proxy.getEndpointReference();

					subscribeNonSettingReports(proxy, descriptor,
							stateHandlesReferences, eventNames, cs2Context,
							context2Cs, MDPWSConstants.PORTTYPE_CONTEXT_SERVICE);

					// Setting related subscriptions
					BICEPSControlProxy settingProxy = stateProxy.getControlProxy();
					if (settingProxy!=null){
						if (settingProxy.getOperationDescriptors() != null) {
							for (OperationDescriptor opDesc : settingProxy
									.getOperationDescriptors()) {
								stateHandlesReferences.add(opDesc.getHandle());
							}
						}
						subscribeToRemoteOperationEvents(proxy,contextSetter2Cs,cs2Context);
					}

					updateStates(proxy, stateHandlesReferences,
							proxyNetworkEndpoint);

				} else {
					if (Log.isDebug())
						Log.debug("Already subscribed " + proxy
								+ " or descriptor [" + proxy.getDescriptor()
								+ "] not available.");
				}
			}
		}
	}

	private ArrayList<ArrayList<String>> createPatientAssociationStateReportEventNames() {
		ArrayList<ArrayList<String>> eventNames = new ArrayList<ArrayList<String>>();
		ArrayList<String> associationStateEventList = new ArrayList<String>(2);
		associationStateEventList.add(MDPWSConstants.ACTION_EPISODIC_CONTEXT_CHANGED_REPORT);
		associationStateEventList.add(MDPWSConstants.ACTION_PERIODIC_CONTEXT_CHANGED_REPORT);


		eventNames.add(associationStateEventList);
		return eventNames;
	}





	private <P extends BICEPSProxy, D extends Descriptor> void subscribeNonSettingReports(
			P proxy, D descriptor, ArrayList<String> stateHandlesReferences,
			ArrayList<ArrayList<String>> events,
			Map<ClientSubscription, List<P>> cs2Proxy,
			Map<P, Set<ClientSubscription>> proxy2Cs, String servicePortType) {

		if (events == null || events.isEmpty() || events.get(0).isEmpty())
			return;

		if (descriptor.getHandle() != null && !stateHandlesReferences.contains(descriptor.getHandle()))
			stateHandlesReferences.add(descriptor.getHandle());

		for (ArrayList<String> eventNames : events) {

			ClientSubscription cs = null;
			try {
				for (int i = 0; i < eventNames.size() && cs == null; i++) {
					String eventName = eventNames.get(i);
					cs = this.subscriptionManager.getClientSubscription(
							proxy.getEndpointReference(),
							servicePortType, eventName);
				}

			} catch (TimeoutException e) {
				Log.info(e);
			} catch (EventingException e) {
				Log.info(e);
			}
			if (cs != null) {
				List<P> list = cs2Proxy.get(cs);
				if (list == null) {
					list = new CopyOnWriteArrayList<P>();
					cs2Proxy.put(cs, list);
				}
				list.add(proxy);
				addToProxy2CSMap(proxy, cs, proxy2Cs);
			} else {
				if (Log.isWarn())
					Log.warn("Could not subscribe to event on device:"
							+ proxy.getHandleOnEndpointReference());
			}

		}

	}

	private <P extends BICEPSProxy> void addToProxy2CSMap(P proxy,
			ClientSubscription cs, Map<P, Set<ClientSubscription>> proxy2Cs) {

		if (cs != null && proxy != null) {
			Set<ClientSubscription> subscriptions = null;
			if (!proxy2Cs.containsKey(proxy)) {
				subscriptions = new CopyOnWriteArraySet<ClientSubscription>();
				proxy2Cs.put(proxy, subscriptions);
			} else {
				subscriptions = proxy2Cs.get(proxy);
			}
			subscriptions.add(cs);
		}

	}

	public void unsubscribeReport(BICEPSMDSContextElement context) {
		if (context != null) {
			unsubscribeNonSettingReports(context, cs2Context, context2Cs);
			if (contextSetter2Cs.containsKey(context)) {
				ClientSubscription cs = contextSetter2Cs.remove(context);
				List<BICEPSMDSContextElement> list = cs2Context.get(cs);
				if (list!=null) list.remove(context);
				if (list==null || list.isEmpty()) {
					cs2Context.remove(cs);
					this.subscriptionManager.removeClientSubscription(cs, true);
				}
			}
		}
	}

	private <P extends BICEPSProxy> void unsubscribeNonSettingReports(P proxy,
			Map<ClientSubscription, List<P>> cs2Proxy,
			Map<P, Set<ClientSubscription>> proxy2Cs) {
		if (proxy2Cs.containsKey(proxy)) {
			Set<ClientSubscription> csList = proxy2Cs.remove(proxy);
			for (ClientSubscription cs : csList) {
				List<P> list = cs2Proxy.get(cs);
				if (list!=null) list.remove(proxy);
				if (list==null || list.isEmpty()) {
					cs2Proxy.remove(cs);
					notifyClientSubscriptionInvalid(cs);
					this.subscriptionManager.removeClientSubscription(cs, true);
				}
			}
		}
	}

	public boolean isReportSubscribed(BICEPSMDSContextElement context) {
		if (context==null) return false;

		return context2Cs.containsKey(context);
	}

	public void subscribeReport(BICEPSManeuver maneuver) {
		//		subscribeToRemoteOperationEvents(maneuver,maneuver2Cs,cs2Maneuver);
		if (maneuver!=null)
		{
			ArrayList<String> stateHandlesReferences = new ArrayList<String>(2);
			BICEPSControlProxy settingProxy = maneuver.getControlProxy();
			if (settingProxy!=null){
				if (settingProxy.getOperationDescriptors() != null) {
					for (OperationDescriptor opDesc : settingProxy
							.getOperationDescriptors()) {
						stateHandlesReferences.add(opDesc.getHandle());
					}
				}
				subscribeToRemoteOperationEvents(maneuver,maneuver2Cs,cs2Maneuver);
			}

			EndpointReference proxyNetworkEndpoint = maneuver.getEndpointReference();
			updateStates(maneuver,stateHandlesReferences, proxyNetworkEndpoint);
		}
	}

	private <T extends BICEPSProxy> void subscribeToRemoteOperationEvents(T diceProxy, Map<T, ClientSubscription> proxy2CSMapping, Map<ClientSubscription, List<T>> cs2ProxyMapping) {
		if (diceProxy != null && !proxy2CSMapping.containsKey(diceProxy)) {
			// Subscribe for OperationInvoked report.
			ClientSubscription cs = null;
			try {
				cs = this.subscriptionManager.getClientSubscription(
						diceProxy.getEndpointReference(),
						MDPWSConstants.PORTTYPE_REPORT_SERVICE,
						MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT);

				if (cs != null) {
					List<T> list = cs2ProxyMapping.get(cs);
					if (list == null) {
						list = new CopyOnWriteArrayList<T>();
						cs2ProxyMapping.put(cs, list);
					}
					list.add(diceProxy);
					proxy2CSMapping.put(diceProxy, cs);

					cs = this.subscriptionManager
							.getClientSubscription(
									diceProxy.getEndpointReference(),
									MDPWSConstants.PORTTYPE_REPORT_SERVICE,
									MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT);

					if (cs!=null)
					{
						list = cs2ProxyMapping.get(cs);
						if (list == null) {
							list = new CopyOnWriteArrayList<T>();
							cs2ProxyMapping.put(cs, list);
						}
						list.add(diceProxy);
					}
				}
			} catch (Exception e) {
				Log.warn(e);
			}
		}

	}

	public void unsubscribeReport(BICEPSManeuver maneuver) {
		if (maneuver2Cs.containsKey(maneuver)) {
			ClientSubscription cs = maneuver2Cs.remove(maneuver);
			List<BICEPSManeuver> list = cs2Maneuver.get(cs);
			list.remove(maneuver);
			if (list.isEmpty()) {
				cs2Maneuver.remove(cs);
				notifyClientSubscriptionInvalid(cs);
				this.subscriptionManager.removeClientSubscription(cs, true);
			}
		}
	}

	public boolean isReportSubscribed(BICEPSManeuver maneuver) {
		return maneuver2Cs.containsKey(maneuver);
	}

	public void subscribeReport(BICEPSStream stream) {

		if (stream != null) {
			ProxyUniqueID streamHandle = stream.getProxyUniqueID();// getStreamHandle(stream);
			if (streamHandle != null
					&& !metricHandle2Stream.containsValue(streamHandle)) {
				try {
					this.subscriptionManager.createStreamSubscription(
							stream.getEndpointReference(),
							MDPWSConstants.ACTION_WAVEFORM_STREAM);

					metricHandle2Stream.put(streamHandle, stream);
				} catch (IOException e) {
					Log.warn(e);
				} catch (TimeoutException e) {
					Log.warn(e);
				}
			}
		}
	}

	// private String getStreamHandle(BICEPSStream stream) {
	// String streamHandle=null;
	// if (stream!=null)
	// {
	// RealTimeSampleArrayMetricDescriptor descriptor = stream.getDescriptor();
	// if (descriptor!=null )
	// {
	// streamHandle=descriptor.getHandle();
	// }
	// }
	// return streamHandle;
	// }

	public void unsubscribeReport(BICEPSStream stream) {
		if (stream != null) {
			ProxyUniqueID streamHandle = stream.getProxyUniqueID();// getStreamHandle(stream);
			if (streamHandle != null) {
				metricHandle2Stream.remove(streamHandle);
				// TODO MVo Unsubscribe stream handler!
			}
		}
	}

	public boolean isReportSubscribed(BICEPSStream stream) {
		boolean retVal = false;
		if (stream != null) {
			ProxyUniqueID streamHandle = stream.getProxyUniqueID();
			if (streamHandle != null) {
				retVal = metricHandle2Stream.containsKey(streamHandle);
			}
		}
		return retVal;
	}

	@Override
	public void eventReceived(ClientSubscription subscription, URI actionURI,
			IParameterValue payload) {
		SequenceNumberInformation seqInfo = cs2SequenceNumber.get(subscription);
		boolean seqInfoWasCreated = false;
		if (seqInfo == null) {
			seqInfo = new SequenceNumberInformation();
			seqInfoWasCreated = true;
		}

		boolean eventHandled=false;
		//check metrics
		if (cs2Metric.containsKey(subscription)) {
			eventHandled=handleMetricEvent(subscription, actionURI, payload, seqInfo);
		} 
		//check alert signals & conditions & systems
		if (cs2AlertCondition.containsKey(subscription) || cs2AlertSignal.containsKey(subscription) || cs2AlertSystem.containsKey(subscription)) {
			eventHandled=handleAlertEvent(subscription, actionURI, payload, seqInfo);
		}
		//check context
		if (cs2Context.containsKey(subscription)) {
			if (Log.isDebug()) Log.debug("ActionURI:" + actionURI);
			if (MDPWSConstants.ACTION_EPISODIC_CONTEXT_CHANGED_REPORT_URI
					.equalsWsdRfc3986(actionURI)
					|| MDPWSConstants.ACTION_PERIODIC_CONTEXT_CHANGED_REPORT_URI
					.equalsWsdRfc3986(actionURI)) {
				synchronized (context2Cs) {
					AbstractContextChangedReport report=InvokeHelper.getObjectFromParameterValue(payload);
					if (report != null) {
						eventHandled=handlePatientAssociationStateReport(subscription, report);
					}
				}
				//			} else if (MDPWSConstants.ACTION_EPISODIC_PATIENT_REPORT_URI
				//					.equalsWsdRfc3986(actionURI)|| MDPWSConstants.ACTION_PERIODIC_PATIENT_REPORT_URI
				//					.equalsWsdRfc3986(actionURI)) {
				//				AbstractContextReport report = null;
				//				if (payload instanceof IParameterValue){
				//					report=ParameterValueToSchemaMessageServiceHelper
				//							.getInstance().convertPVToX((IParameterValue) payload);
				//				}
				//				if (report != null) {
				//					eventHandled=handlePatientDataStateReport(subscription, report);
				//				}

			} else if (actionURI
					.equalsWsdRfc3986(MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT_URI)) {
				synchronized (contextSetter2Cs) {
					OperationInvokedReport report=InvokeHelper.getObjectFromParameterValue(payload);

					if (report != null) {
						seqInfo.setOperationInvokedLastSequence(report
								.getSequenceNumber());// SSch: we do not check
						// the sequence at this
						// point as applications
						// might also be
						// interested in old
						// invocation states
					}

					if (seqInfo.isDirty())
						eventHandled=handlePatientOperationReport(subscription, report);
				}
			}
		} 
		//check maneuver
		if (cs2Maneuver.containsKey(subscription)) {
			if (actionURI.equalsWsdRfc3986(MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT_URI)) {
				synchronized (maneuver2Cs) {
					OperationInvokedReport report=InvokeHelper.getObjectFromParameterValue(payload);

					if (report != null) {
						seqInfo.setOperationInvokedLastSequence(report.getSequenceNumber());
						// SSch: we do not check
						// the sequence at this
						// point as applications
						// might also be
						// interested in old
						// invocation states
						eventHandled=handleOperationInvokedReportReport(subscription, report,cs2Maneuver );
					}
				}
			} else if (MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI
					.equalsWsdRfc3986(actionURI) 
					|| MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI
					.equalsWsdRfc3986(actionURI)) {
				synchronized (maneuver2Cs) {
					OperationalStateChangedReport report=InvokeHelper.getObjectFromParameterValue(payload);

					if (report != null) {
						seqInfo.setOperationalStateChangedInvokedLastSequence(report.getSequenceNumber());
						eventHandled=handleOperationalStateReport(subscription, report,cs2Maneuver);
					}
				}
			}else{
				if (Log.isInfo())
					Log.info("Unhandled Event of type "+actionURI+" for Maneuver");
			}
		}



		if (seqInfoWasCreated && seqInfo.isDirty())
			cs2SequenceNumber.put(subscription, seqInfo);

		if (!eventHandled)
		{
			if (Log.isDebug())
				Log.debug("Event not handled [Subscription: "+subscription+" event type:"+actionURI+" PV:"+payload+"].");

		}
	}

	private boolean handleAlertEvent(ClientSubscription subscription,
			URI actionURI, IParameterValue payload,
			SequenceNumberInformation seqInfo) {
		boolean eventHandled=false;
		if (MDPWSConstants.ACTION_EPISODIC_ALERT_REPORT_URI
				.equalsWsdRfc3986(actionURI)
				|| MDPWSConstants.ACTION_PERIODIC_ALERT_REPORT_URI
				.equalsWsdRfc3986(actionURI)) {
			AbstractAlertReport report=InvokeHelper.getObjectFromParameterValue(payload);

			if (report != null) {
				if (report instanceof EpisodicAlertReport
						&& seqInfo.getEpisodicAlertReportLastSequence().compareTo(report
								.getSequenceNumber()) < 0) {
					seqInfo.setEpisodicAlertReportLastSequence(report
							.getSequenceNumber());

				} else if (report instanceof PeriodicAlertReport
						&& seqInfo.getPeriodicAlertReportLastSequence().compareTo(report
								.getSequenceNumber())<0) {
					seqInfo.setPeriodicAlertReportLastSequence(report
							.getSequenceNumber());
				}
				if (seqInfo.isDirty())
					eventHandled=handleAlertStateReport(subscription, report);

			}
		} else if (MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT_URI
				.equalsWsdRfc3986(actionURI)) {
			// TODO SSch lookup alertSignal???
			OperationInvokedReport report=InvokeHelper.getObjectFromParameterValue(payload);
			if (report != null) {
				seqInfo.setOperationInvokedLastSequence(report
						.getSequenceNumber());// SSch: we do not check the
				// sequence at this point as
				// applications might also
				// be interested in old
				// invocation states
				eventHandled=handleAlertOperationReport(subscription, report);
			}
		} else if (MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI
				.equalsWsdRfc3986(actionURI) 
				|| MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI
				.equalsWsdRfc3986(actionURI)) {
			OperationalStateChangedReport report=InvokeHelper.getObjectFromParameterValue(payload);

			if (report!=null)
				eventHandled=handleAlertOperationalStateChangedreport(subscription, report, seqInfo);
		}
		return eventHandled;
	}

	private boolean handleAlertOperationalStateChangedreport(
			ClientSubscription subscription,
			OperationalStateChangedReport report, SequenceNumberInformation seqInfo) {
		boolean eventHandled=false;
		if (report != null) {
			seqInfo.setOperationalStateChangedInvokedLastSequence(report.getSequenceNumber());
			synchronized (alertSignalSetter2Cs) {
				eventHandled=handleOperationalStateReport(subscription, report,cs2AlertSignal);
			}
			if (!eventHandled)
			{
				synchronized (alertConditionSetter2Cs) {
					eventHandled=handleOperationalStateReport(subscription, report,cs2AlertCondition);
				}
			}
			if (!eventHandled)
			{
				synchronized (alertSystemSetter2Cs) {
					eventHandled=handleOperationalStateReport(subscription, report,cs2AlertSystem);
				}
			}
		}

		return eventHandled;
	}

	private boolean handleMetricEvent(ClientSubscription subscription,
			URI actionURI, IParameterValue payload,
			SequenceNumberInformation seqInfo) {
		boolean eventHandled=false;
		if (MDPWSConstants.ACTION_EPISODIC_METRIC_REPORT_URI
				.equalsWsdRfc3986(actionURI)
				|| MDPWSConstants.ACTION_PERIODIC_METRIC_REPORT_URI
				.equalsWsdRfc3986(actionURI)) {
			synchronized (metric2Cs) {
				AbstractMetricReport report=InvokeHelper.getObjectFromParameterValue(payload);

				if (report != null) {
					if (report instanceof EpisodicMetricReport) {
						if (seqInfo.getEpisodicMetricReportLastSequence().compareTo(report
								.getSequenceNumber())<0) {
							seqInfo.setEpisodicMetricReportLastSequence(report
									.getSequenceNumber());
						}
					} else if (report instanceof PeriodicMetricReport) {
						if (seqInfo.getPeriodicMetricReportLastSequence().compareTo(report
								.getSequenceNumber())<0) {
							seqInfo.setPeriodicMetricReportLastSequence(report
									.getSequenceNumber());
						}
					}

					if (seqInfo.isDirty()) {
						eventHandled=handleMetricReport(subscription, report);
					} else {
						if (Log.isInfo())
							Log.info("Could not handle metric report due to wrong message order. Message: "
									+ report.getSequenceNumber()
									+ " Latest:"
									+ seqInfo.episodicMetricReportLastSequence);
					}

				}
			}
		} else if (MDPWSConstants.ACTION_OPERATION_INVOKED_REPORT_URI
				.equalsWsdRfc3986(actionURI)) {
			synchronized (metricSetter2Cs) {
				OperationInvokedReport report = null;
				report=InvokeHelper.getObjectFromParameterValue(payload);

				if (report != null) {
					seqInfo.setOperationInvokedLastSequence(report
							.getSequenceNumber()); 
					// TODO SSch: we do not check
					// the sequence at this
					// point as applications
					// might also be interested
					// in old invocation states
					eventHandled=handleMetricOperationReport(subscription, report);
				}
			}
		} else if (MDPWSConstants.ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI
				.equalsWsdRfc3986(actionURI) ) {
			synchronized (metricSetter2Cs) {
				OperationalStateChangedReport report=InvokeHelper.getObjectFromParameterValue(payload);

				if (report != null) {
					seqInfo.setOperationalStateChangedInvokedLastSequence(report
							.getSequenceNumber());
					eventHandled=handleOperationalStateReport(subscription, report,cs2Metric);
				}
			}
		}
		return eventHandled;
	}

	private <T extends BICEPSProxy> boolean handleOperationalStateReport(
			ClientSubscription subscription,
			OperationalStateChangedReport report, Map<ClientSubscription, List<T>> subscriptionsLookupMap) {
		boolean eventHandled=false;
		HashMap<ProxyUniqueID, T> subscribedProxies = this.getValidClientProxiesForSubscription(subscription,subscriptionsLookupMap );

		if (subscribedProxies != null && subscribedProxies.size() > 0) {
			EndpointReference deviceEPR = null;
			if (subscription.getServiceReference() != null && subscription.getServiceReference().getParentDeviceRef() != null)
			{
				deviceEPR = subscription.getServiceReference().getParentDeviceRef().getEndpointReference();
			}
			for (OperationalStateChangedReportPart reportPart : report.getReportDetails()) {
				if (reportPart.getOperations() != null) {
					for (OperationState opState : reportPart.getOperations()) {
						if (opState.getReferencedDescriptor() != null) {
							//							String operationProxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(opState.getReferencedDescriptor(),handleOnDeviceEndpointReference);

							T subscribedProxy = subscribedProxies.get(ProxyUniqueID.create(opState.getReferencedDescriptor(), deviceEPR));
							if (subscribedProxy==null){
								ProxyUniqueID clientProxyUniqueID =null;
								IBICEPSClient client = BicepsClientFramework.getInstance().getClient();
								MDIBStructure mdibStructure = client.getMDIBSearchStructure(deviceEPR);
								if (mdibStructure != null) {
									OperationDescriptor opDesc = mdibStructure.findOperation(opState.getReferencedDescriptor());
									if (opDesc != null) {
										//										clientProxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(opDesc.getOperationTarget(),handleOnDeviceEndpointReference);
										clientProxyUniqueID=ProxyUniqueID.create(opDesc.getOperationTarget(), deviceEPR);
									}
								}
								subscribedProxy=subscribedProxies.get(clientProxyUniqueID);
							}


							if (subscribedProxy != null) {
								BICEPSProxyUtil.updateBICEPSProxy(opState,subscribedProxy, report.getSequenceNumber());
								eventHandled=true;
							}
						}
					}
				}
			}
		}
		return eventHandled;
	}

	@Override
	public void subscriptionEndReceived(ClientSubscription subscription,
			URI reason) {
		if (cs2Metric.containsKey(subscription)) {
			List<BICEPSMetric> metricsToRemove = cs2Metric.remove(subscription);
			for (BICEPSMetric e : metricsToRemove) {
				if (metricSetter2Cs.containsKey(e)) {
					// Don't notify if OperationInvokedReport of a setting has
					// ended.
					metricSetter2Cs.remove(e);
				} else {
					e.subscriptionEnded(SubscriptionEndCodeType.fromUri(reason));
					metric2Cs.remove(e);
				}
			}
		} else if (cs2AlertCondition.containsKey(subscription)) {
			List<BICEPSAlertCondition> list = cs2AlertCondition
					.remove(subscription);
			for (BICEPSAlertCondition e : list) {
				if (alertConditionSetter2Cs.containsKey(e)) {
					// Don't notify if OperationInvokedReport of a setting has
					// ended.
					alertConditionSetter2Cs.remove(e);
				} else {
					e.subscriptionEnded(SubscriptionEndCodeType.fromUri(reason));
					alertCondition2Cs.remove(e);
				}
			}
		} else if (cs2AlertSignal.containsKey(subscription)) {
			List<BICEPSAlertSignal> list = cs2AlertSignal.remove(subscription);
			for (BICEPSAlertSignal e : list) {
				if (alertSignalSetter2Cs.containsKey(e)) {
					// Don't notify if OperationInvokedReport of a setting has
					// ended.
					alertSignalSetter2Cs.remove(e);
				} else {
					e.subscriptionEnded(SubscriptionEndCodeType.fromUri(reason));
					alertSignal2Cs.remove(e);
				}
			}
		} else if (cs2Context.containsKey(subscription)) {
			List<BICEPSMDSContextElement> list = cs2Context.remove(subscription);
			for (BICEPSMDSContextElement e : list) {
				if (contextSetter2Cs.containsKey(e)) {
					// Don't notify if OperationInvokedReport of a setting has
					// ended.
					contextSetter2Cs.remove(e);
				} else {
					e.subscriptionEnded(SubscriptionEndCodeType.fromUri(reason));
					context2Cs.remove(e);
				}
			}
		} else if (cs2Maneuver.containsKey(subscription)) {
			List<BICEPSManeuver> list = cs2Maneuver.remove(subscription);
			for (BICEPSManeuver e : list) {
				e.subscriptionEnded(SubscriptionEndCodeType.fromUri(reason));
				maneuver2Cs.remove(e);
			}
		}
		// TODO MVo else if - can a stream subscription end somehow?
	}

	@Override
	public synchronized void frameReceived(IParameterValue payload,
			InvokeMessage msg, ProtocolData protocolData,
			ProxyStreamSource streamSource) {
		super.frameReceived(payload, msg, protocolData, streamSource);
		WaveformStream s = null;
		s=InvokeHelper.getObjectFromParameterValue(payload);
		if (s != null) {
			for (RealTimeSampleArrayMetricState a : s.getRealTimeSampleArrays()) {
				EndpointReference deviceEndpointReference = getEndpointReferenceFromStreamSource(streamSource);

				ProxyUniqueID clientProxyUniqueID = ProxyUniqueID.create(
						a.getReferencedDescriptor(),
						deviceEndpointReference);
				BICEPSStream relatedBICEPSMetric = metricHandle2Stream
						.get(clientProxyUniqueID);

				if (relatedBICEPSMetric != null) {
					if (isReportSubscribed(relatedBICEPSMetric)) {
						this.notifyProxySubscriber(relatedBICEPSMetric, a, 0);
					}
				} else {
					if (Log.isDebug()) {
						StringBuilder sb = new StringBuilder();
						if (metricHandle2Stream != null) {
							Set<ProxyUniqueID> keys = metricHandle2Stream.keySet();
							for (ProxyUniqueID string : keys) {
								sb.append(string + " ");
							}
						}
						Log.debug("No related matric: clientProxyUniqueID: "
								+ clientProxyUniqueID + " RefDesc:"
								+ a.getReferencedDescriptor()
								+ " storedHandles:" + sb.toString());
					}
				}

			}
		}
	}

	private EndpointReference getEndpointReferenceFromStreamSource(
			ProxyStreamSource streamSource) {
		EndpointReference deviceEndpointReference = null;
		if (streamSource.getService() != null
				&& streamSource.getService().getServiceReference() != null
				&& streamSource.getService().getServiceReference()
				.getParentDeviceRef() != null){
			deviceEndpointReference = streamSource
					.getService().getServiceReference()
					.getParentDeviceRef()
					.getEndpointReference();
		}
		return deviceEndpointReference;
	}

	private boolean handleMetricReport(ClientSubscription subscription,
			AbstractMetricReport report) {
		boolean eventHandled=false;
		if (report != null) {
			HashMap<ProxyUniqueID, BICEPSMetric> subscribedBICEPSMetrics = this.getValidClientProxiesForSubscription(subscription, cs2Metric);
			if (subscribedBICEPSMetrics != null
					&& subscribedBICEPSMetrics.size() > 0) {

				for (MetricReportPart reportPart : report.getReportParts()) {
					for (AbstractMetricState state : reportPart.getMetrics()) {
						EndpointReference deviceEndpointReference = getEndpointReferenceFromClientSubscription(subscription);

						ProxyUniqueID clientProxyUniqueID = ProxyUniqueID
								.create(
										state.getReferencedDescriptor(),
										deviceEndpointReference);
						BICEPSMetric relatedBICEPSMetric = subscribedBICEPSMetrics
								.get(clientProxyUniqueID);

						if (isReportSubscribed(relatedBICEPSMetric)) {
							BigInteger sequenceNumber = report.getSequenceNumber();
							eventHandled=this.notifyProxySubscriber(relatedBICEPSMetric, state,sequenceNumber!=null?sequenceNumber.longValue():-1 );
						}
					}
				}
			}
		}
		return eventHandled;
	}

	private EndpointReference getEndpointReferenceFromClientSubscription(
			ClientSubscription subscription) {
		EndpointReference handleOnDeviceEndpointReference = null;
		if (subscription.getServiceReference() != null
				&& subscription.getServiceReference()
				.getParentDeviceRef() != null){
			handleOnDeviceEndpointReference = subscription
					.getServiceReference()
					.getParentDeviceRef()
					.getEndpointReference();
		}
		return handleOnDeviceEndpointReference;
	}

	private <S extends State> boolean notifyProxySubscriber(BICEPSProxy diceProxy,
			S newState, long sequenceNumber) {
		BICEPSProxyUtil.updateBICEPSProxy(newState, diceProxy, BigInteger.valueOf(sequenceNumber));
		return true;
	}

	private boolean handleAlertStateReport(ClientSubscription subscription,
			AbstractAlertReport report) {
		boolean eventHandled=false;
		if (report != null) {
			HashMap<ProxyUniqueID, BICEPSAlertSignal> subscribedAlertSignals = this
					.getValidAlertSignalProxiesForSubscription(subscription);
			HashMap<ProxyUniqueID, BICEPSAlertCondition> subscribedAlertConditions = this
					.getValidAlertConditionProxiesForSubscription(subscription);
			HashMap<ProxyUniqueID, BICEPSAlertSystem> subscribedAlertSystems = this
					.getValidAlertSystemProxiesForSubscription(subscription);

			if ((subscribedAlertSignals != null && subscribedAlertSignals
					.size() > 0)
					|| (subscribedAlertConditions != null && subscribedAlertConditions
					.size() > 0)
					|| (subscribedAlertSystems != null && subscribedAlertSystems
					.size() > 0)) {

				for (AlertReportPart reportPart : report.getReportParts()) {
					for (AbstractAlertState state : reportPart.getAlertStates()) {
						EndpointReference deviceEndpointReference = getEndpointReferenceFromClientSubscription(subscription);
						ProxyUniqueID clientProxyUniqueID = ProxyUniqueID
								.create(
										state.getReferencedDescriptor(),
										deviceEndpointReference);

						long sequenceNumber=report.getSequenceNumber()!=null?report.getSequenceNumber().longValue():-1;
						eventHandled=notifySubscriberForState(subscribedAlertSignals, sequenceNumber, state,
								clientProxyUniqueID, alertSignal2Cs);
						if (!eventHandled)
							eventHandled=notifySubscriberForState(subscribedAlertConditions,sequenceNumber,
									state, clientProxyUniqueID, alertCondition2Cs);

						if (!eventHandled)
							eventHandled=notifySubscriberForState(subscribedAlertSystems,sequenceNumber, state,
									clientProxyUniqueID, alertSystem2Cs);

					}
				}
			}
		}
		return eventHandled;
	}

	private <T extends BICEPSProxy> boolean notifySubscriberForState(
			HashMap<ProxyUniqueID, T> subscribedProxies, long sequenceNumber, State state,
			ProxyUniqueID clientProxyUniqueID, Map<?, ClientSubscription> map) {
		T relatedAlertSignal = null;
		boolean notifySubscriber = false;
		if (subscribedProxies != null && !subscribedProxies.isEmpty()) {
			relatedAlertSignal = subscribedProxies.get(clientProxyUniqueID);
			notifySubscriber = isReportSubscribed(relatedAlertSignal, map);
		}
		if (notifySubscriber) {
			notifySubscriber=this.notifyProxySubscriber(relatedAlertSignal, state, sequenceNumber);
		}
		return notifySubscriber;
	}

	private boolean handlePatientAssociationStateReport(ClientSubscription subscription,
			AbstractContextChangedReport report) {
		boolean eventHandled=false;
		if (report != null) {
			HashMap<ProxyUniqueID, BICEPSMDSContextElement> subscribedProxies = this
					.getValidClientPatientsForSubscription(subscription);
			if (subscribedProxies != null && subscribedProxies.size() > 0) {

				for (ContextChangedReportPart reportPart : report
						.getReportParts()) {
					List<String> handleRefs = reportPart.getChangedContextStates();

					for (String handleRef : handleRefs) {
						EndpointReference handleOnDeviceEndpointReference = getEndpointReferenceFromClientSubscription(subscription);

						ProxyUniqueID clientProxyUniqueID = ProxyUniqueID
								.create(
										handleRef,
										handleOnDeviceEndpointReference);

						BICEPSMDSContextElement relatedProxy = subscribedProxies
								.get(clientProxyUniqueID);

						if (relatedProxy!=null && isReportSubscribed(relatedProxy)) {

							//retrieve new state
							ArrayList<String> handleRefsForUpdate=new ArrayList<String>(2);

							handleRefsForUpdate.add(handleRef);

							List<State> updateStates = updateStates(relatedProxy,handleRefs,relatedProxy.getEndpointReference());	 



							if (updateStates!=null)
							{
//								State changedState=null;
//								if (relatedProxy.getStateProxy()!=null)
//								{
//									changedState=relatedProxy.getStateProxy().getContextElementItem();
//								}
								for (State changedState : updateStates) {
									eventHandled|=this.notifyProxySubscriber(relatedProxy, changedState, report.getSequenceNumber().longValue());	
								}
								
							}

						}
					}

				}


			}
		}
		return eventHandled;
	}

	private <T extends BICEPSProxy> HashMap<ProxyUniqueID, T> getValidClientProxiesForSubscription(ClientSubscription subscription,Map<ClientSubscription, List<T>> lookupMap) {
		HashMap<ProxyUniqueID, T> filteredLookupMap = new HashMap<ProxyUniqueID, T>();
		List<T> allProxies4Subscription = lookupMap.get(subscription);
		if (allProxies4Subscription != null) {
			for (T proxy : allProxies4Subscription) {
				if (proxy.isValid()) {
					filteredLookupMap.put(proxy.getProxyUniqueID(),proxy);
				}
			}
		}
		return filteredLookupMap;
	}

	private <T extends BICEPSProxy> boolean handleOperationInvokedReportReport(ClientSubscription subscription, OperationInvokedReport report,Map<ClientSubscription, List<T>> lookupMap) {

		boolean operationInvokedReportHandled=false;
		HashMap<ProxyUniqueID, T> subscribedProxies = this.getValidClientProxiesForSubscription(subscription, lookupMap);
		if (subscribedProxies != null && subscribedProxies.size() > 0) {
			EndpointReference deviceEPR = getEndpointReferenceFromClientSubscription(subscription);
			for (OperationInvokedReportPart reportPart : report.getReportDetails()) {
				ProxyUniqueID clientProxyUniqueID = null;
				ProxyUniqueID operationProxyUniqueID =null;
				if (reportPart.getOperationTarget() != null && reportPart.getOperation()!=null) {
					//					clientProxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(
					//							reportPart.getOperationTarget(),
					//							handleOnDeviceEndpointReference);
					//					operationProxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(reportPart.getOperation(), handleOnDeviceEndpointReference);
					clientProxyUniqueID=ProxyUniqueID.create(reportPart.getOperationTarget(), deviceEPR);
					operationProxyUniqueID=ProxyUniqueID.create(reportPart.getOperation(), deviceEPR);
				} else {
					IBICEPSClient client = BicepsClientFramework.getInstance().getClient();
					MDIBStructure mdibStructure = client.getMDIBSearchStructure(deviceEPR);
					if (mdibStructure != null) {
						OperationDescriptor opDesc = mdibStructure.findOperation(reportPart.getOperation());
						if (opDesc != null) {
							//							clientProxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(opDesc.getOperationTarget(),handleOnDeviceEndpointReference);
							//							operationProxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(opDesc.getHandle(), handleOnDeviceEndpointReference);
							clientProxyUniqueID=ProxyUniqueID.create(opDesc.getOperationTarget(), deviceEPR);
							operationProxyUniqueID=ProxyUniqueID.create(opDesc.getHandle(), deviceEPR);
						}
					}

				}

				T subscribedProxy = subscribedProxies.get(clientProxyUniqueID);

				if (subscribedProxy==null) subscribedProxy = subscribedProxies.get(operationProxyUniqueID);

				if (subscribedProxy != null && subscribedProxy.getProxyUniqueID()!=null) {
					if (subscribedProxy.getProxyUniqueID().equals(clientProxyUniqueID) || subscribedProxy.getProxyUniqueID().equals(operationProxyUniqueID)) {
						long sequenceNumber=report.getSequenceNumber()!=null?report.getSequenceNumber().longValue():-1;
						subscribedProxy.requestStateChanged(sequenceNumber,
								reportPart.getTransactionId(),
								reportPart.getOperationState(),
								reportPart.getOperationError(),
								reportPart.getOperationErrorMessage());
						operationInvokedReportHandled=true;
					}
				} else {
					if (Log.isDebug())
						Log.debug("Could not find subscribed proxy for "
								+ clientProxyUniqueID
								+ " created from OperationInvokedReport with OpTarget:"
								+ reportPart.getOperationTarget() + " EPR:"
								+ deviceEPR);
				}
			}
		}
		return operationInvokedReportHandled;
	}

	private boolean handleMetricOperationReport(ClientSubscription subscription,
			OperationInvokedReport report) {
		return handleOperationInvokedReportReport(subscription,report,cs2Metric);
	}

	private boolean handleAlertOperationReport(ClientSubscription subscription,
			OperationInvokedReport report) {

		boolean operationHandled=handleOperationInvokedReportReport(subscription,report,cs2AlertSignal);
		if (!operationHandled) operationHandled=handleOperationInvokedReportReport(subscription,report,cs2AlertCondition);
		if (!operationHandled) operationHandled=handleOperationInvokedReportReport(subscription,report,cs2AlertSystem);
		return operationHandled;
	}

	private boolean handlePatientOperationReport(ClientSubscription subscription,
			OperationInvokedReport report) {
		return handleOperationInvokedReportReport(subscription,report,cs2Context);

	}

	private List<BICEPSMetric> getBICEPSMetricsForSubscription(
			ClientSubscription subscription) {
		return cs2Metric.get(subscription);
	}


	private List<BICEPSMDSContextElement> getBICEPSPatientsForSubscription(
			ClientSubscription subscription) {
		return cs2Context.get(subscription);
	}

	private HashMap<ProxyUniqueID, BICEPSMDSContextElement> getValidClientPatientsForSubscription(
			ClientSubscription subscription) {
		HashMap<ProxyUniqueID, BICEPSMDSContextElement> lookupMap = new HashMap<ProxyUniqueID, BICEPSMDSContextElement>();
		List<BICEPSMDSContextElement> list = getBICEPSPatientsForSubscription(subscription);
		if (list != null) {
			for (BICEPSMDSContextElement proxy : list) {
				if (proxy.isValid()) {
					if (proxy.getProxyUniqueID() != null)
						lookupMap.put(proxy.getProxyUniqueID(), proxy);
				}
			}
		}
		return lookupMap;
	}

	private List<BICEPSAlertSignal> getAlertSignalProxiesForSubscription(
			ClientSubscription subscription) {
		return cs2AlertSignal.get(subscription);
	}

	private HashMap<ProxyUniqueID, BICEPSAlertSignal> getValidAlertSignalProxiesForSubscription(
			ClientSubscription subscription) {
		HashMap<ProxyUniqueID, BICEPSAlertSignal> alertSignalProxiesLookupMap = new HashMap<ProxyUniqueID, BICEPSAlertSignal>();
		List<BICEPSAlertSignal> list = getAlertSignalProxiesForSubscription(subscription);
		if (list != null) {
			for (BICEPSAlertSignal diceMetric : list) {
				if (diceMetric.isValid()) {
					alertSignalProxiesLookupMap.put(
							diceMetric.getProxyUniqueID(), diceMetric);
				}
			}
		}
		return alertSignalProxiesLookupMap;
	}

	private List<BICEPSAlertCondition> getAlertConditionProxiesForSubscription(
			ClientSubscription subscription) {
		return cs2AlertCondition.get(subscription);
	}

	private List<BICEPSAlertSystem> getAlertSystemProxiesForSubscription(
			ClientSubscription subscription) {
		return cs2AlertSystem.get(subscription);
	}

	private HashMap<ProxyUniqueID, BICEPSAlertCondition> getValidAlertConditionProxiesForSubscription(
			ClientSubscription subscription) {
		HashMap<ProxyUniqueID, BICEPSAlertCondition> alertProxiesLookupMap = new HashMap<ProxyUniqueID, BICEPSAlertCondition>();
		List<BICEPSAlertCondition> alertProxies = getAlertConditionProxiesForSubscription(subscription);
		if (alertProxies != null) {
			for (BICEPSAlertCondition alertProxy : alertProxies) {
				if (alertProxy.isValid()) {
					alertProxiesLookupMap.put(alertProxy.getProxyUniqueID(),
							alertProxy);
				}
			}
		}
		return alertProxiesLookupMap;
	}

	private HashMap<ProxyUniqueID, BICEPSAlertSystem> getValidAlertSystemProxiesForSubscription(
			ClientSubscription subscription) {
		HashMap<ProxyUniqueID, BICEPSAlertSystem> alertProxiesLookupMap = new HashMap<ProxyUniqueID, BICEPSAlertSystem>();
		List<BICEPSAlertSystem> alertProxies = getAlertSystemProxiesForSubscription(subscription);
		if (alertProxies != null) {
			for (BICEPSAlertSystem alertProxy : alertProxies) {
				if (alertProxy.isValid()) {
					alertProxiesLookupMap.put(alertProxy.getProxyUniqueID(),
							alertProxy);
				}
			}
		}
		return alertProxiesLookupMap;
	}

	@Override
	public void notifyClientSubscriptionInvalid(ClientSubscription subscription) {
		this.cs2SequenceNumber.remove(subscription);
	}

	@Override
	public void subscribeReport(BICEPSProxy proxy) {
		// TODO SSch use Hashmap for Handler lookup
		if (proxy instanceof BICEPSMetric) {
			subscribeReport((BICEPSMetric) proxy);
		} else if (proxy instanceof BICEPSAlertCondition) {
			subscribeReport((BICEPSAlertCondition) proxy);
		} else if (proxy instanceof BICEPSAlertSystem) {
			subscribeReport((BICEPSAlertSystem) proxy);
		} else if (proxy instanceof BICEPSAlertSignal) {
			subscribeReport((BICEPSAlertSignal) proxy);
		} else if (proxy instanceof BICEPSMedicalDeviceSystem) {
			// TODO SSch check subscribeReport(proxy);
		} else if (proxy instanceof BICEPSStream) {
			subscribeReport((BICEPSStream) proxy);
		} else if (proxy instanceof BICEPSMDSContextElement) {
			subscribeReport((BICEPSMDSContextElement) proxy);
		} else if (proxy instanceof BICEPSManeuver) {
			subscribeReport((BICEPSManeuver) proxy);
		} else {
			if (Log.isWarn())
				Log.warn("Unhandled Subscription Event for " + proxy);
		}
	}

	@Override
	public void unsubscribeReport(BICEPSProxy proxy) {
		// TODO SSch use Hashmap for Handler lookup
		if (proxy instanceof BICEPSMetric) {
			unsubscribeReport((BICEPSMetric) proxy);
		} else if (proxy instanceof BICEPSAlertCondition) {
			unsubscribeReport((BICEPSAlertCondition) proxy);
		} else if (proxy instanceof BICEPSAlertSystem) {
			unsubscribeReport((BICEPSAlertSystem) proxy);
		} else if (proxy instanceof BICEPSAlertSignal) {
			unsubscribeReport((BICEPSAlertSignal) proxy);
		} else if (proxy instanceof BICEPSMedicalDeviceSystem) {
			// TODO SSch check unsubscribeReport(proxy);
		} else if (proxy instanceof BICEPSStream) {
			unsubscribeReport((BICEPSStream) proxy);
		} else if (proxy instanceof BICEPSMDSContextElement) {
			unsubscribeReport((BICEPSMDSContextElement) proxy);
		} else {
			if (Log.isWarn())
				Log.warn("Unhandled Unsubscription Event for " + proxy);
		}
	}


	@Override
	public boolean isReportSubscribed(BICEPSProxy proxy) {
		// TODO SSch use Hashmap for Handler lookup
		if (proxy instanceof BICEPSMetric) {
			return isReportSubscribed((BICEPSMetric) proxy);
		} else if (proxy instanceof BICEPSAlertCondition) {
			return isReportSubscribed((BICEPSAlertCondition) proxy);
		} else if (proxy instanceof BICEPSAlertSystem) {
			return isReportSubscribed((BICEPSAlertSystem) proxy);
		} else if (proxy instanceof BICEPSAlertSignal) {
			return isReportSubscribed((BICEPSAlertSignal) proxy);
		} else if (proxy instanceof BICEPSMedicalDeviceSystem) {
			// TODO SSch check subscribeReport(proxy);
		} else if (proxy instanceof BICEPSStream) {
			return isReportSubscribed((BICEPSStream) proxy);
		} else if (proxy instanceof BICEPSMDSContextElement) {
			return isReportSubscribed((BICEPSMDSContextElement) proxy);
		} else if (proxy instanceof BICEPSManeuver) {
			return isReportSubscribed((BICEPSManeuver) proxy);
		}else {
			if (Log.isWarn())
				Log.warn("Unhandled isReportSubscribed Event for " + proxy);
		}
		return false;
	}

	private class SequenceNumberInformation {
		private BigInteger episodicMetricReportLastSequence = BigInteger.valueOf(-1);
		private BigInteger periodicMetricReportLastSequence = BigInteger.valueOf(-1);
		private BigInteger episodicAlertReportLastSequence = BigInteger.valueOf(-1);
		private BigInteger periodicAlertReportLastSequence = BigInteger.valueOf(-1);

		private boolean isDirty = false;

		BigInteger getEpisodicMetricReportLastSequence() {
			return episodicMetricReportLastSequence;
		}

		void setEpisodicMetricReportLastSequence(
				BigInteger episodicMetricReportLastSequence) {
			this.isDirty = true;
			this.episodicMetricReportLastSequence = episodicMetricReportLastSequence;
		}

		BigInteger getPeriodicMetricReportLastSequence() {
			return periodicMetricReportLastSequence;
		}

		void setPeriodicMetricReportLastSequence(
				BigInteger periodicMetricReportLastSequence) {
			this.isDirty = true;
			this.periodicMetricReportLastSequence = periodicMetricReportLastSequence;
		}

		BigInteger getEpisodicAlertReportLastSequence() {
			return episodicAlertReportLastSequence;
		}

		void setEpisodicAlertReportLastSequence(
				BigInteger episodicAlertReportLastSequence) {
			this.isDirty = true;
			this.episodicAlertReportLastSequence = episodicAlertReportLastSequence;
		}

		BigInteger getPeriodicAlertReportLastSequence() {
			return periodicAlertReportLastSequence;
		}

		void setPeriodicAlertReportLastSequence(
				BigInteger periodicAlertReportLastSequence) {
			this.isDirty = true;
			this.periodicAlertReportLastSequence = periodicAlertReportLastSequence;
		}

		void setOperationInvokedLastSequence(BigInteger operationInvokedLastSequence) {
			this.isDirty = true;
		}

		public void setOperationalStateChangedInvokedLastSequence(
				BigInteger operationalStateChangedInvokedLastSequence) {
			this.isDirty = true;
		}

		boolean isDirty() {
			return isDirty;
		}

	}
}
