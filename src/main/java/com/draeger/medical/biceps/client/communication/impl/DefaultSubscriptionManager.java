/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.configuration.BindingProperties;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.ClientSubscriptionManager;
import org.ws4d.java.eventing.EventSource;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.service.Service;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.DataStructure;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.SubscriptionManager;
import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.mdpws.client.MDPWSClient;
import com.draeger.medical.mdpws.domainmodel.impl.client.MDPWSProxyService;

public class DefaultSubscriptionManager implements SubscriptionManager{

	private final Map<ClientSubscription, EndpointReference>     		mdibSubscriptions      		= new ConcurrentHashMap<ClientSubscription, EndpointReference>();
	private final Map<String, ClientSubscription>            	  		allClientSubscriptions 		= new ConcurrentHashMap<String, ClientSubscription>();
	private final MDPWSClient mdpwsClient;



	public DefaultSubscriptionManager(
			MDPWSClient mdpwsClient) 
	{
		this.mdpwsClient=mdpwsClient;
	}


	@Override
	public void registerMdibReports(EndpointReference networkDeviceEndpoint)
	{
		if (networkDeviceEndpoint!=null)
		{
			subscribeMDIBReport(networkDeviceEndpoint,MDPWSConstants.PORTTYPE_REPORT_SERVICE, MDPWSConstants.ACTION_OBJECT_CREATED_REPORT);
			subscribeMDIBReport(networkDeviceEndpoint,MDPWSConstants.PORTTYPE_REPORT_SERVICE, MDPWSConstants.ACTION_OBJECT_DELETED_REPORT);
			subscribeMDIBReport(networkDeviceEndpoint,MDPWSConstants.PORTTYPE_REPORT_SERVICE, MDPWSConstants.ACTION_MDS_CREATED_REPORT);
			subscribeMDIBReport(networkDeviceEndpoint,MDPWSConstants.PORTTYPE_REPORT_SERVICE, MDPWSConstants.ACTION_MDS_DELETED_REPORT);
		}
	}


	private void subscribeMDIBReport(EndpointReference networkDeviceEndpoint, String portType, String eventAction) {
		ClientSubscription cs = null;
		try
		{
			cs = getClientSubscription(networkDeviceEndpoint, portType,eventAction);
		}
		catch (EventingException e)
		{
			Log.warn(e);
		}
		catch (TimeoutException e)
		{
			Log.warn(e);
		}
		if (cs != null)
		{
			mdibSubscriptions.put(cs, networkDeviceEndpoint);
			cs = null;
		}else{
			if (Log.isWarn())
			{
				Log.warn("Failed to subscribe to "+eventAction+" in "+portType+" on "+networkDeviceEndpoint);
			}
		}
		return;
	}	


	@Override
	public void unregisterMdibReports(EndpointReference networkDeviceEndpoint, boolean communicationFailed) throws EventingException,
	TimeoutException
	{
		if (networkDeviceEndpoint!=null)
		{
			Set<Entry<ClientSubscription, EndpointReference>> entries = mdibSubscriptions.entrySet();
			java.util.Iterator<Entry<ClientSubscription, EndpointReference>> entriesIt = entries.iterator();
			ArrayList<ClientSubscription> entriesToRemove=new ArrayList<ClientSubscription>();

			while (entriesIt.hasNext()) {
				Entry<ClientSubscription, EndpointReference> entry = entriesIt.next();
				if (networkDeviceEndpoint.equals(entry.getValue()))
				{
					ClientSubscription clientSubscription = entry.getKey();
					if (clientSubscription!=null)
					{
						entriesToRemove.add(clientSubscription);
						removeClientSubscription(clientSubscription, !communicationFailed);
					}
				}
			}

			for (ClientSubscription clientSubscription : entriesToRemove) {
				mdibSubscriptions.remove(clientSubscription);	
			}

		}
	}


	@Override
	public void unregisterMdibReports() throws EventingException, TimeoutException
	{
		for (ClientSubscription cs : mdibSubscriptions.keySet())
		{
			removeClientSubscription(cs, true);
		}
		mdibSubscriptions.clear();
	}


	//	public MDIBStructure getMDIBRepForSubscription(ClientSubscription cs)
	//	{
	//		return mdibSubscriptions.get(cs);
	//	}

	//Determine if it is an event or a stream
	@Override
	public ClientSubscription getClientSubscription(EndpointReference networkDeviceEndpoint, String portType, String outputAction) throws EventingException, TimeoutException
	{
		ClientSubscription cs = null;
		QNameSet svcProviderPTs = new QNameSet(QNameFactory.getInstance().getQName(portType));
		DeviceReference devRef = DeviceServiceRegistryProvider.getInstance()
				.getDeviceServiceRegistry().getStaticDeviceReference(networkDeviceEndpoint);
		if (devRef != null)
		{
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(svcProviderPTs);
			if (iSvcRef.hasNext())
			{
				ServiceReference servRef=((ServiceReference) iSvcRef.next());
				EventSource src = servRef.getService().getEventSource(outputAction);
				if (src != null)
				{
					cs = allClientSubscriptions.get(networkDeviceEndpoint+"@"+src.toString());
					if (cs == null)
					{
						DataStructure bindings= BindingProperties.getInstance().getCommunicationBinding(1001);
						cs = src.subscribe(mdpwsClient, 0,bindings);
						allClientSubscriptions.put(networkDeviceEndpoint+"@"+src.toString(), cs);
						if (Log.isDebug()) Log.debug("allClientSubscriptions does not contain CS for eventsource "+src.getOutputAction()+". New CS"+cs.getClientSubscriptionId());

					}else{
						if (Log.isDebug()) Log.debug("allClientSubscriptions contains cs "+cs.getClientSubscriptionId());
					}
					if (Log.isDebug())Log.debug(src+" "+src.getOutputAction()+" "+cs.getClientSubscriptionId()+" "+networkDeviceEndpoint);
				}else{

					Service service = servRef.getService();
					if (service instanceof MDPWSProxyService)
					{
						MDPWSProxyService mdpwsService=(MDPWSProxyService) service;
						if (outputAction==null || mdpwsService.getStreamSource(outputAction)!=null){
							try{
								mdpwsClient.subscribeToStream(mdpwsService, QNameFactory.getInstance().getQName(outputAction));
							}catch(Exception e)
							{
								//TODO SSch Check message type
								throw new TimeoutException(e.getMessage());
							}
						}
					}
				}
			}
		}
		return cs;
	}


	@Override
	public void removeClientSubscription(ClientSubscription cs, boolean unsubscribe)
	{
		if (allClientSubscriptions.containsValue(cs))
		{
			ArrayList<String> keysToRemove = new ArrayList<String>();
			for (Entry<String, ClientSubscription> entry : allClientSubscriptions.entrySet())
			{
				if (entry.getValue().equals(cs))
				{
					keysToRemove.add(entry.getKey());
				}
			}
			for (String key : keysToRemove)
			{
				allClientSubscriptions.remove(key);
			}
			ClientSubscriptionManager.getInstance().removeClientSubscription(cs);
			if (unsubscribe)
			{
				try
				{
					cs.unsubscribe();
				}
				catch (Exception e)
				{
					if (Log.isDebug()) Log.debug(e);
				}
			}
		}
	}


	@Override
	public void createStreamSubscription(EndpointReference networkDeviceEndpoint, String action) throws TimeoutException, IOException
	{
		createStreamSubscription(networkDeviceEndpoint, MDPWSConstants.PORTTYPE_STREAM_SERVICE, action);
	}

	public void createStreamSubscription(EndpointReference networkDeviceEndpoint,String portType, String action)
			throws IOException, TimeoutException
			{
		DeviceReference devRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(networkDeviceEndpoint);
		if (devRef != null)
		{
			QNameSet svcProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(portType));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(svcProviderPTs);
			while (iSvcRef.hasNext())
			{
				// TODO SSch -->>
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				Service svc = svcRef.getService();

				if (!(svc instanceof MDPWSProxyService))
				{
					if (Log.isWarn() && svc!=null)
						Log.warn(svc.toString()+"Not a MDPWSProxyService");
				}
				// TODO <<--
				else
				{
					MDPWSProxyService mdpwsService=(MDPWSProxyService) svc;
					if (Log.isDebug())
						Log.debug("subscribeToStream");

					if (action==null || mdpwsService.getStreamSource(action)!=null){
						mdpwsClient.subscribeToStream(mdpwsService, QNameFactory.getInstance().getQName(action));
					}
				}
			}
		}
			}


	@Override
	public boolean removeMDIBSubscription(ClientSubscription subscription) {
		return (mdibSubscriptions.remove(subscription)!=null);
	}

}
