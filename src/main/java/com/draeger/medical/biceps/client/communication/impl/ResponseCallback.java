/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.communication.impl;

import org.ws4d.java.communication.DefaultResponseCallback;
import org.ws4d.java.communication.ProtocolData;
import org.ws4d.java.message.Message;
import org.ws4d.java.message.discovery.ProbeMatchesMessage;
 class ResponseCallback extends DefaultResponseCallback
{
	static final int NOT_RUNNING=0;
	static final int OK=1;
	static final int PENDLING=2;
	static final int NOK=3;

	int comState=NOT_RUNNING;



	@Override
	public void handle(Message request,
			ProbeMatchesMessage probeMatches, ProtocolData protocolData) {
		//				super.handle(request, probeMatches, protocolData);
		notifyStateChange(OK);
	}

	private void notifyStateChange(int state) {
		this.comState=state;
		synchronized(this){
			this.notifyAll();
		}
	}

	@Override
	public void handleTransmissionException(Message request,
			Exception exception) {
		handleTimeout(request);
	}

	@Override
	public void handleMalformedResponseException(Message request,
			Exception exception) {
		handleTimeout(request);
	}

	@Override
	public void handleTimeout(Message request) 
	{
		notifyStateChange(NOK);
	}

}
