/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.ws4d.java.client.SearchParameter;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.dispatch.IDeviceServiceRegistry;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.IBICEPSClient;
import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback;
import com.draeger.medical.biceps.client.communication.CommunicationContainer;
import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.search.NetworkSearchQuery;
import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.BICEPSProxyManager;
import com.draeger.medical.biceps.client.proxy.callbacks.MdibChangedListener;
import com.draeger.medical.biceps.client.proxy.callbacks.MdibListener;
import com.draeger.medical.biceps.client.proxy.callbacks.MdibMDSListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyManagerProvider;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.DescriptionModificationReport;
import com.draeger.medical.biceps.common.model.DescriptionModificationReportPart;
import com.draeger.medical.biceps.common.model.DescriptionModificationType;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.mdib.MDIBStructure;
import com.draeger.medical.biceps.mdib.impl.MdibRepresentation;

/**
 * Default implementation of {@link IBICEPSClient} and {@link CommunicationAdapterCallback}.
 */
@SuppressWarnings("unchecked")
public class DefaultBicepsClientImpl implements IBICEPSClient, CommunicationAdapterCallback
{


	private static final  List<String> 											DEVICE_WHITELIST			= new CopyOnWriteArrayList<String>();
	static{
		String whiteListEntries=System.getProperty("BICEPS.DefaultBICEPSClientImpl.WhiteListEntries");
		if (whiteListEntries!=null)
		{
			StringTokenizer tokenizer=new StringTokenizer(whiteListEntries, ";");
			while(tokenizer.hasMoreTokens())
			{
				DEVICE_WHITELIST.add(tokenizer.nextToken());
			}

		}

	}

	private static final  List<String> 											DEVICE_BLACKLIST			= new CopyOnWriteArrayList<String>();
	static{
		String blacklistEntries=System.getProperty("BICEPS.DefaultBICEPSClientImpl.BlackListEntries");
		if (blacklistEntries!=null)
		{
			StringTokenizer tokenizer=new StringTokenizer(blacklistEntries, ";");
			while(tokenizer.hasMoreTokens())
			{
				DEVICE_BLACKLIST.add(tokenizer.nextToken());
			}

		}

	}

	private boolean vmLocalDeviceDetectionEnabled																	= false;
	protected List<MdibListener>                                      					mdibListeners          		= new CopyOnWriteArrayList<MdibListener>();
	//	protected final BICEPSProxyStore                                    				proxyStore            		= BICEPSProxyStoreProvider.getInstance().getStore();
	private final Map<EndpointReference, MDIBStructure>      						MDIBStructures    			= new ConcurrentHashMap<EndpointReference, MDIBStructure>();
	private final HashMap<EndpointReference, HashMap<String, BICEPSSafetyInformationPolicyElement>> safetyInformationForEPR    	= new HashMap<EndpointReference, HashMap<String, BICEPSSafetyInformationPolicyElement>>();
	private final Map<EndpointReference, List<BICEPSMedicalDeviceSystem>>      		diceDeviceProxies    		= new ConcurrentHashMap<EndpointReference, List<BICEPSMedicalDeviceSystem>>();
	private final boolean 																startBICEPSClientWatchDog 	= Boolean.parseBoolean(System.getProperty("BICEPS.Client.StartBICEPSClientWatchDog", "true"));
	private final CommunicationAdapter							  						mdpwsClientAdapter;
	private final BICEPSProxyManager 														proxyManager				= BICEPSProxyManagerProvider.getInstance().getFactory();
	private final boolean 																isAutoDeviceAddingEnabled  		= Boolean.parseBoolean(System.getProperty("BICEPS.Client.isAutoDeviceAddingEnabled", "true"));

	private final boolean 																isDescriptorLazyLoadingEnabled	= Boolean.parseBoolean(System.getProperty("BICEPS.Client.isDescriptorLazyLoadingEnabled", "false"));

	private final static IBICEPSClient instance = new DefaultBicepsClientImpl();

	/**
	 * Gets the single instance of DefaultBicepsClientImpl.
	 *
	 * @return single instance of DefaultBicepsClientImpl
	 */
	public static IBICEPSClient getInstance()
	{
		return instance;
	}


	private DefaultBicepsClientImpl()
	{
		super();

		mdpwsClientAdapter=setupMDPWSClient();

	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getSafetyInformationForEPR()
	 */
	@Override
	public HashMap<EndpointReference, HashMap<String, BICEPSSafetyInformationPolicyElement>> getSafetyInformationMap() {
		return safetyInformationForEPR;
	}

	protected CommunicationAdapter setupMDPWSClient() {
		CommunicationAdapter adapter=  new DefaultMDPWSClientAdapter();
		adapter.setDeviceReferenceWhitelist(DEVICE_WHITELIST);
		adapter.setDeviceReferenceBlacklist(DEVICE_BLACKLIST);
		adapter.add(this);
		return adapter;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getCommunicationAdapter()
	 */
	@Override
	public CommunicationAdapter getCommunicationAdapter() {
		return mdpwsClientAdapter;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#stop()
	 */
	@Override
	public void stop()
	{
		getCommunicationAdapter().stop();
		getCommunicationAdapter().unsubscribeAsDeviceListener(MDIBStructures.keySet());
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#isPnpEnabled()
	 */
	@Override
	public boolean isPnpEnabled(){
		return getCommunicationAdapter().isPnpEnabled();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#setPnpEnabled(boolean)
	 */
	@Override
	public void setPnpEnabled(boolean pnpEnabled)
	{
		getCommunicationAdapter().setPnpEnabled(pnpEnabled);
	}

	@Override
	public void connectDevice(DeviceReference devRef) {
		retrieveMedicalDeviceSystemForDeviceReference(devRef);
	}

	@Override
	public void disconnectDevice(DeviceReference devRef) {
		deviceBye(devRef);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getDeviceWhitelist()
	 */
	@Override
	public List<String> getDeviceWhitelist() {
		return DEVICE_WHITELIST;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getDeviceBlacklist()
	 */
	@Override
	public List<String> getDeviceBlacklist() {
		return DEVICE_BLACKLIST;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#isVMLocalDeviceDetectionEnabled()
	 */
	@Override
	public boolean isVMLocalDeviceDetectionEnabled() {
		return vmLocalDeviceDetectionEnabled;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#setVMLocalDeviceDetectionEnabled(boolean)
	 */
	@Override
	public void setVMLocalDeviceDetectionEnabled(boolean vmLocalDetectionEnabled) {
		vmLocalDeviceDetectionEnabled=vmLocalDetectionEnabled;
		mdpwsClientAdapter.setVMLocalDeviceDetectionEnabled(vmLocalDeviceDetectionEnabled);
	}



	//Search
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#searchDevices(com.draeger.medical.biceps.client.communication.discovery.search.NetworkSearchQuery)
	 */
	@Override
	public void searchDevices(NetworkSearchQuery search) 
	{
		this.getCommunicationAdapter().searchDevice(search);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getContextDiscovery()
	 */
	@Override
	public ContextDiscovery getContextDiscovery()
	{
		return this.getCommunicationAdapter().getContextDiscovery();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#subscribe(com.draeger.medical.biceps.client.proxy.callbacks.MdibListener)
	 */
	@Override
	public void subscribe(MdibListener listener)
	{
		if (!mdibListeners.contains(listener))
		{
			mdibListeners.add(listener);
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#unsubscribe(com.draeger.medical.biceps.client.proxy.callbacks.MdibListener)
	 */
	@Override
	public void unsubscribe(MdibListener listener)
	{
		mdibListeners.remove(listener);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMetric(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public  BICEPSMetric getMetric(ProxyUniqueID proxyUniqueID)
	{
		BICEPSMetric cm = getProxyManager().getProxyQueryInterface().getMetric(proxyUniqueID);
		if (cm == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures
					.entrySet())
			{
				cm = getMetricFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID);
				if (cm != null) break;
			}
		}
		return cm;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMetrics(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public  List<BICEPSMetric> getMetrics(BICEPSMedicalDeviceSystem mds)
	{
		List<BICEPSMetric> result = new ArrayList<BICEPSMetric>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getMetricsFromMdib(entry.getValue(),  vms, result);
					}
				}
			}
		}
		return result;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMetrics(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public  List<BICEPSMetric> getMetrics(CodedValue code)
	{
		return getMetrics(null, code);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSignal(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public BICEPSAlertSignal getAlertSignal(ProxyUniqueID proxyUniqueID)
	{
		BICEPSAlertSignal ca = getProxyManager().getProxyQueryInterface().getAlertSignal(proxyUniqueID);
		if (ca == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures
					.entrySet())
			{
				ca = getAlertSignalFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID.getLocalHandle());
				if (ca != null) break;
			}
		}
		return ca;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSignals()
	 */
	@Override
	public List<? extends BICEPSAlertSignal> getAlertSignals()
	{
		List<BICEPSAlertSignal> result = new ArrayList<BICEPSAlertSignal>();
		List<BICEPSAlertSignal> cached = getProxyManager().getProxyQueryInterface().getAlertSignals();

		for (BICEPSAlertSignal ca : cached)
		{
			if (!ca.isValid())
			{
				getProxyManager().handleInvalidProxy(ca);
			}
			else
			{
				result.add(ca);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getAlertSignalsFromMdib(entry.getValue(), entry.getKey(), result);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSignals(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<? extends BICEPSAlertSignal> getAlertSignals(BICEPSMedicalDeviceSystem mds)
	{
		ArrayList<BICEPSAlertSignal> result = new ArrayList<BICEPSAlertSignal>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getAlertSignalsFromMdib(entry.getValue(), entry.getKey(), vms, result);
					}
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSignals(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<? extends BICEPSAlertSignal> getAlertSignals(CodedValue conditionCode)
	{
		//TODO SSch implement
		return null;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertCondition(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public BICEPSAlertCondition getAlertCondition(ProxyUniqueID proxyUniqueID)
	{
		BICEPSAlertCondition ca =  getProxyManager().getProxyQueryInterface().getAlertCondition(proxyUniqueID);
		if (ca == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures
					.entrySet())
			{
				ca = getAlertConditionFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID.getLocalHandle());
				if (ca != null) break;
			}
		}
		return ca;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertConditions()
	 */
	@Override
	public List<? extends BICEPSAlertCondition> getAlertConditions()
	{
		List<BICEPSAlertCondition> result = new ArrayList<BICEPSAlertCondition>();
		List<BICEPSAlertCondition> cached =  getProxyManager().getProxyQueryInterface().getAlertConditions();

		for (BICEPSAlertCondition ca : cached)
		{
			if (!ca.isValid())
			{
				getProxyManager().handleInvalidProxy(ca); //proxyStore.removeAlertCondition(ca);
			}
			else
			{
				result.add(ca);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getAlertConditionsFromMdib(entry.getValue(), entry.getKey(), result);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertConditions(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<? extends BICEPSAlertCondition> getAlertConditions(BICEPSMedicalDeviceSystem mds)
	{
		ArrayList<BICEPSAlertCondition> result = new ArrayList<BICEPSAlertCondition>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getAlertConditionsFromMdib(entry.getValue(), entry.getKey(), vms, result);
					}
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertConditions(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<? extends BICEPSAlertCondition> getAlertConditions(CodedValue code)
	{
		List<BICEPSAlertCondition> result = new ArrayList<BICEPSAlertCondition>();
		List<BICEPSAlertCondition> cached = getProxyManager().getProxyQueryInterface().getAlertConditions(code);

		for (BICEPSAlertCondition ca : cached)
		{
			if (!ca.isValid())
			{
				getProxyManager().handleInvalidProxy(ca);//proxyStore.removeAlertCondition(ca);
			}
			else
			{
				result.add(ca);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getAlertsConditionsFromMdib(entry.getValue(), entry.getKey(), code, result);
		}

		return result;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSystem(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public BICEPSAlertSystem getAlertSystem(ProxyUniqueID proxyUniqueID)
	{
		BICEPSAlertSystem ca = getProxyManager().getProxyQueryInterface().getAlertSystem(proxyUniqueID);
		if (ca == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures
					.entrySet())
			{
				ca = getAlertSystemFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID.getLocalHandle());
				if (ca != null) break;
			}
		}
		return ca;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSystems()
	 */
	@Override
	public List<? extends BICEPSAlertSystem> getAlertSystems()
	{
		List<BICEPSAlertSystem> result = new ArrayList<BICEPSAlertSystem>();
		List<BICEPSAlertSystem> cached = getProxyManager().getProxyQueryInterface().getAlertSystems();

		for (BICEPSAlertSystem ca : cached)
		{
			if (!ca.isValid())
			{
				getProxyManager().handleInvalidProxy(ca);//proxyStore.removeAlertSystem(ca);
			}
			else
			{
				result.add(ca);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getAlertSystemsFromMdib(entry.getValue(), entry.getKey(), result);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getAlertSystems(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<? extends BICEPSAlertSystem> getAlertSystems(BICEPSMedicalDeviceSystem mds)
	{
		ArrayList<BICEPSAlertSystem> result = new ArrayList<BICEPSAlertSystem>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getAlertSystemsFromMdib(entry.getValue(), entry.getKey(), vms, result);
					}
				}
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMDSContexts()
	 */
	@Override
	public List<BICEPSMDSContext> getMDSContexts()
	{
		List<BICEPSMDSContext> result = new ArrayList<BICEPSMDSContext>();
		List<BICEPSMDSContext> cached = getProxyManager().getProxyQueryInterface().getMDSContexts();

		for (BICEPSMDSContext cp : cached)
		{
			if (!cp.isValid())
			{
				getProxyManager().handleInvalidProxy(cp);//proxyStore.removePatient(cp);
			}
			else
			{
				result.add(cp);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getMDSContextsFromMdib(entry.getValue(), entry.getKey(), result);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMDSContexts(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<BICEPSMDSContext> getMDSContexts(BICEPSMedicalDeviceSystem mds)
	{
		List<BICEPSMDSContext> result = new ArrayList<BICEPSMDSContext>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getMDSContextsFromMdib(entry.getValue(), vms, result);
					}
				}
			}
		}
		return result;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getStream(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public BICEPSStream getStream(ProxyUniqueID proxyUniqueID)
	{
		BICEPSStream cs = getProxyManager().getProxyQueryInterface().getStream(proxyUniqueID);
		if (cs == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				cs = getStreamFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID.getLocalHandle());
				if (cs != null) break;
			}
		}
		return cs;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getDevices()
	 */
	@Override
	public List<MDSDescriptor> getDevices()
	{
		List<MDSDescriptor> result = new ArrayList<MDSDescriptor>();
		for (MDIBStructure mdib : MDIBStructures.values())
		{
			for (MDSDescriptor vms : mdib.getMDSDescriptors())
			{
				result.add(vms);
			}
		}
		return result;
	}



	//	public List<MDSDescriptor> getDevices(String patientId)
	//	{
	//		List<MDSDescriptor> result = new ArrayList<MDSDescriptor>();
	//		for (MDIBStructure mdib : MDIBStructures.values())
	//		{
	//			for (MDSDescriptor vms : mdib.getMDSDescriptors())
	//			{
	//				result.addAll(getDevices(vms, patientId));
	//			}
	//		}
	//		return result;
	//	}

	//	private List<MDSDescriptor> getDevices(MDSDescriptor vms, String patientId)
	//	{
	//		List<MDSDescriptor> result = new ArrayList<MDSDescriptor>();
	//		String id = ""; 
	//		if (vms instanceof HydraMDSDescriptor)
	//		{
	//			//TODO SSch Refactor
	//			//			id = ((HydraMDSDescriptor)vms).getPatient().getPatientData().getPatientId();
	//		}
	//
	//		if(patientId.equals(id))
	//		{
	//			result.add(vms);
	//		}
	//		return result;
	//	}






	//	private static void checkRepliedMessageContext(MDPWSMessageContextMap replyMsgContextMap)
	//	{
	//		QoSMessageContext qosMsgContext;
	//		qosMsgContext = (QoSMessageContext) replyMsgContextMap.get(QoSMessageContext.class);
	//		if (qosMsgContext != null)
	//		{
	//			Iterator policyTokenIterator = qosMsgContext.getValidQoSPolicyToken();
	//			while (policyTokenIterator.hasNext())
	//			{
	//				QoSPolicyToken<?, ?> policyToken = (QoSPolicyToken<?, ?>) policyTokenIterator
	//						.next();
	//				if (policyToken instanceof DualChannelPolicyToken<?, ?>
	//				&& policyToken.getTokenState() != QoSPolicyTokenState.VALID)
	//				{
	//					Log.warn("BICEPSClient: Invoke - received not valid DualChannelPolicyToken");
	//					// TODO SSch what to do now?
	//				}
	//			}
	//		}
	//	}



	private void handleInitialStates(MDState initialStates, MDIBStructure mdib, long sequenceNumber) 
	{

		if (initialStates!=null)
		{

			if (Log.isDebug()){
				String handleOnEndpointReference=BICEPSProxyUtil.createHandleOnEndpointReference(mdib.getDeviceEndpointRef());
				Log.debug("New state retrievable method for epr:"+handleOnEndpointReference);
			}
			HashMap<ProxyUniqueID,BICEPSMDSContext> mdsContextProxyMap=new HashMap<ProxyUniqueID, BICEPSMDSContext>();
			getAllMDSContextFromMdib(mdib,mdsContextProxyMap);
			//TODO SSch extract all the lines
			for (State s:initialStates.getStates())
			{

				if ( s instanceof AbstractIdentifiableContextState)
				{
					ProxyUniqueID proxyUniqueID=ProxyUniqueID.create(s.getReferencedDescriptor(), mdib.getDeviceEndpointRef());
					BICEPSMDSContext patientProxy = mdsContextProxyMap.get(proxyUniqueID);
					if (patientProxy!=null)
					{
						BICEPSProxyUtil.updateBICEPSProxy(s,patientProxy, BigInteger.valueOf(sequenceNumber));
					}
				}
			}
		}
	}

	private BICEPSProxyManager getProxyManager() {
		return proxyManager;
	}




	private  BICEPSMetric getMetricFromMdib(MDIBStructure mdib,EndpointReference networkDeviceEndpoint, ProxyUniqueID proxyUniqueID)
	{
		BICEPSMetric cm = null;
		MetricDescriptor m = mdib.findMetric(proxyUniqueID.getLocalHandle());
		if (m != null)
		{
			//			String proxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(tProxyUniqueID, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			cm=getProxyManager().getProxyFromCache(m,proxyUniqueID,mdib, getCommunicationAdapter().createProxyCommunication(mdib,m,safetyInformationMap));//getMetricFromCache(m, proxyUniqueID, mdib);//getMetricFromCache(m, proxyUniqueID, mdib);
		}
		return cm;
	}

	//	private void getAllMetricsFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSMetric> result)
	//	{
	//		if (result!=null)
	//		{
	//			List<BICEPSMetric> allMetrics=new ArrayList<BICEPSMetric>();
	//			getMetricsFromMdib(mdib,(CodedValue)null,allMetrics);
	//
	//			result.clear();
	//			for (BICEPSMetric diceMetric : allMetrics) {
	//				result.put(diceMetric.getProxyUniqueID(), diceMetric);
	//			}
	//		}
	//	}

	private void getMetricsFromMdib(MDIBStructure mdib, 
			CodedValue code, List<BICEPSMetric> result)
	{
		List<? extends MetricDescriptor> foundMetrics = mdib.findMetrics(code);
		processMetricList(mdib, foundMetrics, result);
	}

	private  void getMetricsFromMdib(MDIBStructure mdib,
			MDSDescriptor vms, List<BICEPSMetric> result)
	{
		List<? extends MetricDescriptor> foundMetrics = mdib.findMetricsForVms(vms.getHandle());
		processMetricList(mdib,  foundMetrics, result);
	}

	private  void processMetricList(MDIBStructure mdib,
			List<? extends MetricDescriptor> metrics, List<BICEPSMetric> result)
	{
		for (MetricDescriptor m : metrics)
		{
			if (!(m instanceof RealTimeSampleArrayMetricDescriptor))
			{
				BICEPSMetric cm = null;
				//				String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(m.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(mdib.getDeviceEndpointRef()));

				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(mdib.getDeviceEndpointRef());
				cm = getProxyManager().getProxyFromCache(m,ProxyUniqueID.create(m.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,m,safetyInformationMap));//getMetricFromCache(m, proxyUniqueID, mdib);

				if (cm != null && !result.contains(cm))
				{
					result.add(cm);
				}
			}
		}
	}




	private BICEPSAlertCondition getAlertConditionFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			String deviceLocalHandle)
	{
		BICEPSAlertCondition diceAlertSignal = null;
		if (mdib!=null)
		{
			AlertConditionDescriptor alertConditionDescriptor = mdib.findAlertCondition(deviceLocalHandle);
			if (alertConditionDescriptor != null)
			{
				//				String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(deviceLocalHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
				diceAlertSignal = getProxyManager().getProxyFromCache(alertConditionDescriptor,ProxyUniqueID.create(deviceLocalHandle, mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,alertConditionDescriptor,safetyInformationMap));
			}
		}
		return diceAlertSignal;
	}


	private void getAlertConditionsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, List<BICEPSAlertCondition> result)
	{
		List<AlertConditionDescriptor> foundAlerts = mdib.findAlertConditions();
		processAlertConditionList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void getAlertsConditionsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, CodedValue code, List<BICEPSAlertCondition> result)
	{
		List<AlertConditionDescriptor> foundAlerts = mdib.findAlertConditions(code);
		processAlertConditionList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void getAlertConditionsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, MDSDescriptor vms, List<BICEPSAlertCondition> result)
	{
		List<AlertConditionDescriptor> foundAlerts = mdib.findAlertsConditionsForVms(vms.getHandle());
		processAlertConditionList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}



	private void processAlertConditionList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<AlertConditionDescriptor> alerts, List<BICEPSAlertCondition> result)
	{
		for (AlertConditionDescriptor a : alerts)
		{
			BICEPSAlertCondition alertConditionProxy = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(a.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			alertConditionProxy = getProxyManager().getProxyFromCache(a,ProxyUniqueID.create(a.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,a,safetyInformationMap)); 			//alertConditionProxy = getAlertConditionFromCache(a, proxyUniqueID, mdib);
			if (alertConditionProxy != null && !result.contains(alertConditionProxy))
			{
				result.add(alertConditionProxy);
			}
		}
	}

	//	private void getAllAlertSignalsFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSAlertSignal> result)
	//	{
	//		if (result!=null)
	//		{
	//			List<BICEPSAlertSignal> alertSignalProxies=new ArrayList<BICEPSAlertSignal>();
	//			getAlertSignalsFromMdib(mdib,mdib.getDeviceEndpointRef(),alertSignalProxies);
	//
	//			result.clear();
	//			for (BICEPSAlertSignal alertSignalProxy : alertSignalProxies) {
	//				result.put(alertSignalProxy.getProxyUniqueID(), alertSignalProxy);
	//			}
	//		}
	//	}

	//	private void getAllAlertConditionsFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSAlertCondition> result)
	//	{
	//		if (result!=null)
	//		{
	//			List<BICEPSAlertCondition> alertSignalProxies=new ArrayList<BICEPSAlertCondition>();
	//			getAlertConditionsFromMdib(mdib,mdib.getDeviceEndpointRef(),alertSignalProxies);
	//
	//			result.clear();
	//			for (BICEPSAlertCondition alertSignalProxy : alertSignalProxies) {
	//				result.put(alertSignalProxy.getProxyUniqueID(), alertSignalProxy);
	//			}
	//		}
	//	}

	private BICEPSAlertSystem getAlertSystemFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			String deviceLocalHandle)
	{
		BICEPSAlertSystem diceAlertSignal = null;
		if (mdib!=null)
		{
			AlertSystemDescriptor alertSystemDescriptor = mdib.findAlertSystem(deviceLocalHandle);
			if (alertSystemDescriptor != null)
			{
				//				String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(deviceLocalHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
				diceAlertSignal = getProxyManager().getProxyFromCache(alertSystemDescriptor, ProxyUniqueID.create(deviceLocalHandle, networkDeviceEndpoint), mdib, getCommunicationAdapter().createProxyCommunication(mdib,alertSystemDescriptor,safetyInformationMap)); //getAlertSystemFromCache(alertSystemDescriptor,proxyUniqueID, mdib);
			}
		}
		return diceAlertSignal;
	}


	private void getAlertSystemsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, List<BICEPSAlertSystem> result)
	{
		List<AlertSystemDescriptor> foundAlerts = mdib.findAlertSystems();
		processAlertSystemList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}


	private void getAlertSystemsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, MDSDescriptor vms, List<BICEPSAlertSystem> result)
	{
		List<AlertSystemDescriptor> foundAlerts = mdib.findAlertsSystemsForVms(vms.getHandle());
		processAlertSystemList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}



	private void processAlertSystemList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<AlertSystemDescriptor> alerts, List<BICEPSAlertSystem> result)
	{
		for (AlertSystemDescriptor alertSystemDescriptor : alerts)
		{
			BICEPSAlertSystem alertSystemProxy = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(alertSystemDescriptor.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			alertSystemProxy = getProxyManager().getProxyFromCache(alertSystemDescriptor, ProxyUniqueID.create(alertSystemDescriptor.getHandle(), networkDeviceEndpoint), mdib, getCommunicationAdapter().createProxyCommunication(mdib,alertSystemDescriptor, safetyInformationMap));//getAlertSystemFromCache(a, proxyUniqueID, mdib);
			if (alertSystemProxy != null && !result.contains(alertSystemProxy))
			{
				result.add(alertSystemProxy);
			}
		}
	}

	//	private void getAllManeuversFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSManeuver> result)
	//	{
	//		if (result!=null)
	//		{
	//			List<BICEPSManeuver> proxies=new ArrayList<BICEPSManeuver>();
	//			getManeuversFromMdib(mdib,mdib.getDeviceEndpointRef(),proxies);
	//
	//			result.clear();
	//			for (BICEPSManeuver proxy : proxies) {
	//				if (proxy.getProxyUniqueID()!=null) result.put(proxy.getProxyUniqueID(), proxy);
	//			}
	//		}
	//	}

	private void getAllMDSContextFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSMDSContext> result)
	{
		if (result!=null)
		{
			List<BICEPSMDSContext> mdsContextProxies=new ArrayList<BICEPSMDSContext>();
			getMDSContextsFromMdib(mdib,mdib.getDeviceEndpointRef(),mdsContextProxies);

			result.clear();
			for (BICEPSMDSContext mdsContextProxy : mdsContextProxies) {
				if (mdsContextProxy.getProxyUniqueID()!=null) result.put(mdsContextProxy.getProxyUniqueID(), mdsContextProxy);
			}
		}
	}


	//	private void getAllAlertSystemsFromMdib(MDIBStructure mdib, HashMap<ProxyUniqueID,BICEPSAlertSystem> result)
	//	{
	//		if (result!=null)
	//		{
	//			List<BICEPSAlertSystem> alertSignalProxies=new ArrayList<BICEPSAlertSystem>();
	//			getAlertSystemsFromMdib(mdib,mdib.getDeviceEndpointRef(),alertSignalProxies);
	//
	//			result.clear();
	//			for (BICEPSAlertSystem alertSignalProxy : alertSignalProxies) {
	//				result.put(alertSignalProxy.getProxyUniqueID(), alertSignalProxy);
	//			}
	//		}
	//	}

	private BICEPSAlertSignal getAlertSignalFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			String deviceLocalHandle)
	{
		BICEPSAlertSignal diceAlertSignal = null;
		if (mdib!=null)
		{
			AlertSignalDescriptor alertSignalDescriptor = mdib.findAlertSignal(deviceLocalHandle);
			if (alertSignalDescriptor != null)
			{
				//				String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(deviceLocalHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
				diceAlertSignal = getProxyManager().getProxyFromCache(alertSignalDescriptor,ProxyUniqueID.create(deviceLocalHandle, networkDeviceEndpoint),mdib, getCommunicationAdapter().createProxyCommunication(mdib,alertSignalDescriptor,safetyInformationMap));//getAlertSignalFromCache(alertSignalDescriptor,proxyUniqueID, mdib);
			}
		}
		return diceAlertSignal;
	}


	private HashMap<String, BICEPSSafetyInformationPolicyElement> getSafetyInformationForEndpoint(
			EndpointReference networkDeviceEndpoint) {
		if (safetyInformationForEPR!=null)
			return this.safetyInformationForEPR.get(networkDeviceEndpoint);
		else
			return null;
	}


	private void getAlertSignalsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, List<BICEPSAlertSignal> result)
	{
		List<AlertSignalDescriptor> foundAlerts = mdib.findAlertSignals();
		processAlertSignalList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void getAlertSignalsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, MDSDescriptor vms, List<BICEPSAlertSignal> result)
	{
		List<AlertSignalDescriptor> foundAlerts = mdib.findAlertSignalsForVms(vms.getHandle());
		processAlertSignalList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}


	private void processAlertSignalList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<AlertSignalDescriptor> alerts, List<BICEPSAlertSignal> result)
	{
		for (AlertSignalDescriptor alertSignalDescriptor : alerts)
		{
			BICEPSAlertSignal alertSignalProxy = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(alertSignalDescriptor.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			alertSignalProxy = getProxyManager().getProxyFromCache(alertSignalDescriptor,ProxyUniqueID.create(alertSignalDescriptor.getHandle(), networkDeviceEndpoint),mdib, getCommunicationAdapter().createProxyCommunication(mdib,alertSignalDescriptor,safetyInformationMap));//getAlertSignalFromCache(a, proxyUniqueID, mdib);
			if (alertSignalProxy != null && !result.contains(alertSignalProxy))
			{
				result.add(alertSignalProxy);
			}
		}
	}


	//	private void getMDSContextElemementsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
	//			List<BICEPSMDSContextElement> result)
	//	{
	//		List<AbstractContextDescriptor> foundContextDescriptors = mdib.findMDSContextElements();
	//		processAbstractContextDescriptorList(mdib, networkDeviceEndpoint, foundContextDescriptors, result);
	//	}

	//	private void processAbstractContextDescriptorList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
	//			List<AbstractContextDescriptor> contextList, List<BICEPSMDSContextElement> result)
	//	{
	//		for (AbstractContextDescriptor descriptor : contextList)
	//		{
	//			addContextElementProxyToResultset(mdib, result, descriptor);
	//		}
	//	}

	//	private void addContextElementProxyToResultset(MDIBStructure mdib,
	//			List<BICEPSMDSContextElement> result, AbstractContextDescriptor descriptor) {
	//		BICEPSMDSContextElement ctxtProxy = null;
	//		if (descriptor!=null && safetyInformationForEPR!=null && result!=null)
	//		{
	//			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = this.safetyInformationForEPR.get(mdib.getDeviceEndpointRef());
	//			ctxtProxy = getProxyManager().getProxyFromCache(descriptor,ProxyUniqueID.create(descriptor.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,descriptor,safetyInformationMap));
	//
	//			if (ctxtProxy != null && !result.contains(ctxtProxy))
	//			{
	//				result.add(ctxtProxy);
	//			}
	//		}
	//	}


	private void getMDSContextsFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<BICEPSMDSContext> result)
	{
		List<SystemContext> foundContextDescriptors = mdib.findMDSContexts();
		processSystemContextList(mdib, networkDeviceEndpoint, foundContextDescriptors, result);
	}

	private void getMDSContextsFromMdib(MDIBStructure mdib,
			MDSDescriptor vms, List<BICEPSMDSContext> result)
	{
		SystemContext foundContextDescriptors = mdib.findMDSContextForVms(vms.getHandle());
		//		processSystemContextList(mdib, networkDeviceEndpoint, foundContextDescriptors, result);
		addSystemContextProxyToResultset(mdib, result, foundContextDescriptors);
	}

	private void processSystemContextList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<SystemContext> systemContextList, List<BICEPSMDSContext> result)
	{
		for (SystemContext descriptor : systemContextList)
		{
			addSystemContextProxyToResultset(mdib, result, descriptor);
		}
	}


	private void addSystemContextProxyToResultset(MDIBStructure mdib,
			List<BICEPSMDSContext> result, SystemContext descriptor) {
		BICEPSMDSContext ctxtProxy = null;
		if (descriptor!=null &&  result!=null)
		{
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(mdib.getDeviceEndpointRef());
			ctxtProxy = getProxyManager().getProxyFromCache(descriptor,ProxyUniqueID.create(descriptor.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,descriptor,safetyInformationMap));

			if (ctxtProxy != null && !result.contains(ctxtProxy))
			{
				result.add(ctxtProxy);
			}
		}
	}

	private BICEPSStream getStreamFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, String deviceLocalHandle)
	{
		BICEPSStream cm = null;
		MetricDescriptor m = mdib.findMetric(deviceLocalHandle);
		if (m != null)
		{
			//			String proxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(deviceLocalHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			cm=getProxyManager().getProxyFromCache(m,ProxyUniqueID.create(deviceLocalHandle, networkDeviceEndpoint),mdib, getCommunicationAdapter().createProxyCommunication(mdib,m, safetyInformationMap));//getMetricFromCache(m, proxyUniqueID, mdib);//getMetricFromCache(m, proxyUniqueID, mdib);
		}
		return cm;
	}


	private  void getStreamsFromMdib(MDIBStructure mdib, MDSDescriptor vms, List<BICEPSStream> result)
	{
		List<RealTimeSampleArrayMetricDescriptor> foundMetrics = mdib.findStreamsForVms(vms.getHandle());
		processStreamMetricList(mdib,  foundMetrics, result);
	}

	private void processStreamMetricList(MDIBStructure mdib, List<RealTimeSampleArrayMetricDescriptor> streamMetrics, List<BICEPSStream> result)
	{
		for (RealTimeSampleArrayMetricDescriptor m : streamMetrics)
		{
			BICEPSStream cm = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(m.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(mdib.getDeviceEndpointRef()));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(mdib.getDeviceEndpointRef());
			cm = getProxyManager().getProxyFromCache(m,ProxyUniqueID.create(m.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,m,safetyInformationMap));//getMetricFromCache(m, proxyUniqueID, mdib);

			if (cm != null && !result.contains(cm))
			{
				result.add(cm);
			}
		}
	}







	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getStreams(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<? extends BICEPSStream> getStreams(BICEPSMedicalDeviceSystem mds) {
		List<BICEPSStream> result = new ArrayList<BICEPSStream>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getStreamsFromMdib(entry.getValue(),  vms, result);
					}
				}
			}
		}
		return result;
	}


	//ClientAdapterCallback
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback#deviceBye(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceBye(DeviceReference devRef)
	{
		if (devRef!=null)
		{
			EndpointReference networkDeviceEndpoint = devRef.getEndpointReference();

			List<BICEPSMedicalDeviceSystem> mdsList = diceDeviceProxies.remove(networkDeviceEndpoint);
			if (mdsList!=null)
			{
				// Notify listeners of the removed device.
				for (MdibListener l : mdibListeners)
				{
					if (l instanceof MdibMDSListener)
					{
						((MdibMDSListener)l).deviceRemoved(mdsList);
					}
				}

				getProxyManager().removeAllProxiesForNetworkDeviceEndpoint(networkDeviceEndpoint);

				MDIBStructure mdib = MDIBStructures.remove(networkDeviceEndpoint);
			}
		}
	}




	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback#deviceChanged(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public void deviceChanged(DeviceReference deviceRef) 
	{
		//void
		//		if (deviceRef!=null)
		//		{
		//			EndpointReference networkDeviceEndpoint = deviceRef.getEndpointReference();
		//			//			getProxyManager().getProxyQueryInterface().getMDSContexts()	
		//		}
	}

	/**
	 * Gets the MDS context.
	 *
	 * @param mds the mds
	 * @return the MDS context
	 */
	public  BICEPSMDSContext getMDSContext(BICEPSMedicalDeviceSystem mds)
	{
		BICEPSMDSContext result = null;
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						ArrayList<BICEPSMDSContext> tempResultList=new ArrayList<BICEPSMDSContext>(1);
						getMDSContextsFromMdib(entry.getValue(),  vms, tempResultList);
						if (tempResultList.size()==1){
							result=tempResultList.get(0);
						}else{
							if (Log.isError()) Log.error("One MDS must have exactly one SystemContext.");
						}
					}
				}
			}
		}
		return result;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback#modifiedDescription(org.ws4d.java.service.reference.DeviceReference, com.draeger.medical.biceps.common.model.DescriptionModificationReport)
	 */
	@Override
	public synchronized void modifiedDescription(DeviceReference devRef,
			DescriptionModificationReport report) 
	{
		if (devRef!=null && report!=null)
		{
			EndpointReference networkDeviceEndpoint = devRef.getEndpointReference();
			MDIBStructure mdibStructure = MDIBStructures.get(networkDeviceEndpoint);
			if (mdibStructure!=null)
			{
				for (DescriptionModificationReportPart reportPart : report.getReportDetails()) {
					String parentHandle=reportPart.getParentDescriptor();
					MDSDescriptor mds = mdibStructure.getParentVMSForDescriptorHandle(parentHandle);
					if (mds!=null && reportPart.getDescriptor()!=null)
					{
						mdibStructure.addDescriptor(  reportPart.getDescriptor(), parentHandle, mds.getHandle());

						if (reportPart.getModificationType().equals(DescriptionModificationType.CRT)){
							callMDIBListenersForNewVMO( reportPart.getDescriptor(), networkDeviceEndpoint);	
						}else {
							callMDIBListenersForDeletedVMO(reportPart.getDescriptor(), networkDeviceEndpoint, reportPart.getModificationType());
						}


					}
				}
			}
		}
	}

	private void callMDIBListenersForDeletedVMO(Descriptor d, EndpointReference networkDeviceEndpoint, DescriptionModificationType descriptionModificationType) {
		if (d!=null && networkDeviceEndpoint!=null)
		{
			//TODO SSch call proxies
		}
	}

	private void callMDIBListenersForNewVMO(Descriptor d, EndpointReference networkDeviceEndpoint) {
		if (d!=null && networkDeviceEndpoint!=null)
		{
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(d.getHandle(),BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			for (MdibListener l : mdibListeners)
			{
				if (l instanceof MdibChangedListener)
				{
					MdibChangedListener changedListener = (MdibChangedListener)l;
					if (d instanceof RealTimeSampleArrayMetricDescriptor){ changedListener.newStreamAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint), BICEPSProxyUtil.getCode(d));}
					else if (d instanceof MetricDescriptor){ changedListener.newMetricAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint), BICEPSProxyUtil.getCode(d));}
					else if (d instanceof ActivateOperationDescriptor){ changedListener.newManeuverAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint), BICEPSProxyUtil.getCode(d));}
					else if (d instanceof AlertConditionDescriptor){ changedListener.newAlertAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint), BICEPSProxyUtil.getCode(d));}
					else if (d instanceof AlertSignalDescriptor){ changedListener.newAlertSignalAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint));}
					else if (d instanceof AlertSystemDescriptor){ changedListener.newAlertSystemAvailable(ProxyUniqueID.create(d.getHandle(),networkDeviceEndpoint));}
					//TODO SSch implement the rest
				}

			}
		}
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback#addDevice(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public synchronized void addDevice(DeviceReference devRef)
	{
		if (isAutoDeviceAddingEnabled)
		{
			boolean hasMedicalDeviceSystemCreatedForDeviceReference=hasMedicalDeviceSystemCreatedForDeviceReference(devRef);
			if (!hasMedicalDeviceSystemCreatedForDeviceReference)
			{
				createAndAddMedicalDeviceSystem(devRef);
			}
		}
	}

	private synchronized boolean hasMedicalDeviceSystemCreatedForDeviceReference(DeviceReference devRef)
	{
		boolean hasBeenCreated=false;
		if (devRef!=null)
		{
			EndpointReference networkDeviceEndpoint = devRef.getEndpointReference();
			hasBeenCreated = (MDIBStructures.containsKey(networkDeviceEndpoint));
		}

		return hasBeenCreated;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#releaseAllProxiesForMedicalDeviceSystemProxy(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public void releaseAllProxiesForMedicalDeviceSystemProxy(BICEPSMedicalDeviceSystem mdsProxy){
		EndpointReference endpointReference = mdsProxy.getEndpointReference();
		IDeviceServiceRegistry deviceServiceRegistry = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry();

		if (deviceServiceRegistry!=null && endpointReference!=null)
		{
			DeviceReference deviceReference = deviceServiceRegistry.getStaticDeviceReference(endpointReference,false);
			this.deviceBye(deviceReference);
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#releaseDedicatedProxy(com.draeger.medical.biceps.client.proxy.BICEPSProxy)
	 */
	@Override
	public void releaseDedicatedProxy(BICEPSProxy proxy)
	{
		getProxyManager().removeProxy(proxy);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#retrieveMedicalDeviceSystemForDeviceReference(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public List<BICEPSMedicalDeviceSystem> retrieveMedicalDeviceSystemForDeviceReference(DeviceReference devRef)
	{
		return createAndAddMedicalDeviceSystem(devRef);
	}

	private synchronized List<BICEPSMedicalDeviceSystem> createAndAddMedicalDeviceSystem(DeviceReference devRef)
	{
		List<BICEPSMedicalDeviceSystem> mdsList = new ArrayList<BICEPSMedicalDeviceSystem>();
		if (devRef!=null)
		{
			EndpointReference networkDeviceEndpoint = devRef.getEndpointReference();
			if (!MDIBStructures.containsKey(networkDeviceEndpoint))
			{
				MDDescription mdibInformationForNetworkDeviceEndpoint = null;
				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap =null;

				if (!isDescriptorLazyLoadingEnabled)
				{
					try
					{
						getCommunicationAdapter().tickInterfaces(devRef);

						CommunicationContainer<MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> container = getCommunicationAdapter().retrieveMDDescription(devRef);

						mdibInformationForNetworkDeviceEndpoint = getMDDescriptionFromContainer( container);

						safetyInformationMap = getSafetyInformationFromContainer(container);
					}
					catch (Exception e)
					{
						Log.warn(e);
					}
				}else{
					try{
						try
						{
							getCommunicationAdapter().tickInterfaces(devRef);

							String handle=null;
							CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> container = getCommunicationAdapter().retrieveDescription(devRef, handle);

							mdibInformationForNetworkDeviceEndpoint = getMDDescriptionFromContainer(
									mdibInformationForNetworkDeviceEndpoint, container);

							safetyInformationMap = getSafetyInformationFromContainer(
									safetyInformationMap, container);
						}
						catch (Exception e)
						{
							Log.warn(e);
						}
					}
					catch (Exception e)
					{
						Log.warn(e);
					}
				}


				if (mdibInformationForNetworkDeviceEndpoint != null)
				{
					MDIB mdibDescriptionOnly=new MDIB();
					mdibDescriptionOnly.setDescription(mdibInformationForNetworkDeviceEndpoint);

					MDIBStructure mdibContainer = createMDIBRepresentation(networkDeviceEndpoint,
							mdibDescriptionOnly, this.isDescriptorLazyLoadingEnabled);

					this.MDIBStructures.put(networkDeviceEndpoint, mdibContainer);

					this.safetyInformationForEPR.put(networkDeviceEndpoint, safetyInformationMap);

					deviceAdded(devRef, mdibContainer);

					getCommunicationAdapter().registerMdibReports(networkDeviceEndpoint);

					try
					{
						MDState initialStates=null;
						CommunicationContainer<MDState, ?> container = getCommunicationAdapter().retrieveStates(devRef);
						if (container!=null)
						{
							initialStates=container.getMessageElem();
							handleInitialStates(initialStates, mdibContainer, container.getSequenceNumber());
						}
					}
					catch (Exception e)
					{
						Log.warn(e);
					}

					// Notify listeners of the newly found device.

					for (MDSDescriptor vms : mdibContainer.getMDSDescriptors())
					{
						if (vms instanceof HydraMDSDescriptor){
							//							String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(vms.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
							BICEPSMedicalDeviceSystem mdsProxy = getProxyManager().getProxyFromCache(vms, ProxyUniqueID.create(vms.getHandle(),networkDeviceEndpoint), mdibContainer, getCommunicationAdapter().createProxyCommunication(mdibContainer, vms, safetyInformationMap));//getProxyManager().createClientMedicalDeviceSystem(vms, devRef.getEndpointReference());
							mdsList.add(mdsProxy);
						}
					}

					diceDeviceProxies.put(networkDeviceEndpoint, mdsList);


					//Start watch dog for this device
					if (startBICEPSClientWatchDog)
					{
						this.getCommunicationAdapter().startBICEPSClientWatchDog(devRef);
					}
				}
				else
				{
					Log.warn("BICEPSClient: getMedicalDeviceSystems: Couldn't parse the GetMDIB response.");
				}

				callMDIBListenersWithMDSList(mdsList);
			}else{
				mdsList = diceDeviceProxies.get(networkDeviceEndpoint);
			}
		}



		return mdsList;
	}



	private HashMap<String, BICEPSSafetyInformationPolicyElement> getSafetyInformationFromContainer(
			CommunicationContainer<MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> container) {

		if (container!=null)
			container.getMessageContext();

		return null;
	}


	private MDDescription getMDDescriptionFromContainer(
			CommunicationContainer<MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> container) {
		if (container!=null)
			return container.getMessageElem();

		return null;
	}


	private HashMap<String, BICEPSSafetyInformationPolicyElement> getSafetyInformationFromContainer(
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap,
			CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> container) {
		if (container!=null)
		{
			safetyInformationMap=container.getMessageContext();
		}
		return safetyInformationMap;
	}


	private MDDescription getMDDescriptionFromContainer(
			MDDescription mdibInformationForNetworkDeviceEndpoint,
			CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> container) {
		if (container!=null)
		{
			//						mdibInformationForNetworkDeviceEndpoint=container.getMessageElem();
			mdibInformationForNetworkDeviceEndpoint=new MDDescription();
			List<Descriptor> messageElem = container.getMessageElem();
			for (Descriptor descriptor : messageElem) {
				if (descriptor instanceof MDSDescriptor){
					mdibInformationForNetworkDeviceEndpoint.getMDSDescriptors().add((MDSDescriptor) descriptor);
				}
			}

		}
		return mdibInformationForNetworkDeviceEndpoint;
	}


	private MdibRepresentation createMDIBRepresentation(
			EndpointReference networkDeviceEndpoint,
			MDIB mdibDescriptionOnly, boolean useDescriptorLazyLoading) {

		MdibRepresentation mdibRepresentation =null;

		if (useDescriptorLazyLoading){
			mdibRepresentation=new MdibRepresentation(mdibDescriptionOnly, networkDeviceEndpoint,getCommunicationAdapter());
		}else{
			mdibRepresentation=new MdibRepresentation(mdibDescriptionOnly, networkDeviceEndpoint);
		}

		return mdibRepresentation;
	}


	private void callMDIBListenersWithMDSList(
			List<BICEPSMedicalDeviceSystem> mdsList) {
		if (mdsList!=null && !mdsList.isEmpty())
		{
			for (MdibListener l : mdibListeners)
			{
				if (l instanceof MdibMDSListener)
				{
					((MdibMDSListener)l).newDeviceAvailable(mdsList);	
				}
			}
		}
	}



	private void deviceAdded(DeviceReference devRef,
			MDIBStructure mdibContainer) {

		getCommunicationAdapter().connectToDevice(devRef,mdibContainer);

	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback#noDevicesFound(org.ws4d.java.client.SearchParameter)
	 */
	@Override
	public void noDevicesFound(SearchParameter search) {	
		for (MdibListener l : mdibListeners)
		{
			if (l instanceof MdibMDSListener)
			{
				((MdibMDSListener)l).noDevicesFound(search);
			}

		}
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMedicalDeviceSystems(org.ws4d.java.types.EndpointReference)
	 */
	@Override
	public Set<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems(
			EndpointReference epr) {
		List<BICEPSMedicalDeviceSystem> deviceList = diceDeviceProxies.get(epr);

		if (deviceList==null || !deviceList.isEmpty())
		{
			IDeviceServiceRegistry deviceServiceRegistry = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry();
			if (deviceServiceRegistry!=null)
			{
				DeviceReference staticDeviceReference = deviceServiceRegistry.getStaticDeviceReference(epr);
				deviceList=retrieveMedicalDeviceSystemForDeviceReference(staticDeviceReference);
			}
		}

		HashSet<BICEPSMedicalDeviceSystem> retVal=new HashSet<BICEPSMedicalDeviceSystem>();
		if (deviceList!=null)
		{
			for (BICEPSMedicalDeviceSystem diceMedicalDeviceSystem : deviceList) {
				retVal.add(diceMedicalDeviceSystem);
			}
		}
		return retVal;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMedicalDeviceSystems()
	 */
	@Override
	public Set<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems() {
		HashSet<BICEPSMedicalDeviceSystem> systems=new HashSet<BICEPSMedicalDeviceSystem>();
		if (diceDeviceProxies.size()>0)
		{
			for(List<BICEPSMedicalDeviceSystem>  deviceList: diceDeviceProxies.values())
			{
				if (deviceList!=null && !deviceList.isEmpty())
				{
					for (BICEPSMedicalDeviceSystem diceMedicalDeviceSystem : deviceList) {
						systems.add(diceMedicalDeviceSystem);
					}
				}
			}
		}
		return systems;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMDIBSearchStructure(org.ws4d.java.types.EndpointReference)
	 */
	@Override
	public MDIBStructure getMDIBSearchStructure(EndpointReference epr) {
		return (epr==null ? null : MDIBStructures.get(epr));
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMDIBSearchStructure()
	 */
	@Override
	public Set<MDIBStructure> getMDIBSearchStructure() {

		HashSet<MDIBStructure> systems=new HashSet<MDIBStructure>();
		if (MDIBStructures.size()>0)
		{
			for(MDIBStructure  structure: MDIBStructures.values())
			{
				if (structure!=null)
				{
					systems.add(structure);
				}
			}
		}
		return systems;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getMetrics(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem, com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<BICEPSMetric> getMetrics(
			BICEPSMedicalDeviceSystem clientMedicalDeviceSystem,
			CodedValue code) {
		List<BICEPSMetric> result = new ArrayList<BICEPSMetric>();
		List<BICEPSMetric> cached = getProxyManager().getProxyQueryInterface().getMetrics(code);

		for (BICEPSMetric cm : cached)
		{
			if (!cm.isValid())
			{
				getProxyManager().handleInvalidProxy(cm);
			}
			else
			{
				if(clientMedicalDeviceSystem==null || clientMedicalDeviceSystem.equals(cm.getParentMedicalDeviceSystem()))
				{
					result.add(cm);
				}
			}
		}

		if (clientMedicalDeviceSystem==null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				getMetricsFromMdib(entry.getValue(),  code, result);
			}
		}else{
			MDIBStructure mdibStructure = MDIBStructures.get(clientMedicalDeviceSystem.getEndpointReference());
			if (mdibStructure!=null) getMetricsFromMdib(mdibStructure,  code, result);
		}

		return result;
	}



	//BICEPSManeuver

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getManeuvers(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<BICEPSManeuver> getManeuvers(CodedValue code)
	{
		List<BICEPSManeuver> result = new ArrayList<BICEPSManeuver>();
		List<BICEPSManeuver> cached = getProxyManager().getProxyQueryInterface().getManeuvers(code);

		for (BICEPSManeuver cm : cached)
		{
			if (!cm.isValid())
			{
				getProxyManager().handleInvalidProxy(cm);//proxyStore.removeManeuver(cm);
			}
			else
			{
				result.add(cm);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getManeuverFromMdib(entry.getValue(), entry.getKey(), code, result);
		}
		return result;
	}

	/**
	 * Gets the maneuvers.
	 *
	 * @param clientMedicalDeviceSystem the client medical device system
	 * @param code the code
	 * @return the maneuvers
	 */
	public List<BICEPSManeuver> getManeuvers(BICEPSMedicalDeviceSystem clientMedicalDeviceSystem, CodedValue code)
	{
		List<BICEPSManeuver> result = new ArrayList<BICEPSManeuver>();
		List<BICEPSManeuver> cached = getProxyManager().getProxyQueryInterface().getManeuvers(code);

		for (BICEPSManeuver cm : cached)
		{
			if (!cm.isValid())
			{
				getProxyManager().handleInvalidProxy(cm);
			}
			else
			{
				if(clientMedicalDeviceSystem==null || clientMedicalDeviceSystem.equals(cm.getParentMedicalDeviceSystem()))
				{
					result.add(cm);
				}
			}
		}

		if (clientMedicalDeviceSystem==null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				getManeuversFromMdib(entry.getValue(),  code, result);
			}
		}else{
			MDIBStructure mdibStructure = MDIBStructures.get(clientMedicalDeviceSystem.getEndpointReference());
			if (mdibStructure!=null) getManeuversFromMdib(mdibStructure,  code, result);
		}

		return result;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getManeuver(com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID)
	 */
	@Override
	public BICEPSManeuver getManeuver(ProxyUniqueID proxyUniqueID) {
		BICEPSManeuver ca = getProxyManager().getProxyQueryInterface().getManeuvers(proxyUniqueID);
		if (ca == null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures
					.entrySet())
			{
				ca = getManeuverFromMdib(entry.getValue(), entry.getKey(), proxyUniqueID.getLocalHandle());
				if (ca != null) break;
			}
		}
		return ca;
	}







	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getManeuvers()
	 */
	@Override
	public  List<? extends BICEPSManeuver> getManeuvers() {
		List<BICEPSManeuver> result = new ArrayList<BICEPSManeuver>();
		List<BICEPSManeuver> cached = getProxyManager().getProxyQueryInterface().getManeuvers();

		for (BICEPSManeuver ca : cached)
		{
			if (!ca.isValid())
			{
				getProxyManager().handleInvalidProxy(ca);//proxyStore.removeAlertSystem(ca);
			}
			else
			{
				result.add(ca);
			}
		}

		for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
		{
			getManeuversFromMdib(entry.getValue(), entry.getKey(), result);
		}
		return result;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.IBICEPSClient#getManeuvers(com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem)
	 */
	@Override
	public List<? extends BICEPSManeuver> getManeuvers(
			BICEPSMedicalDeviceSystem mds) {
		ArrayList<BICEPSManeuver> result = new ArrayList<BICEPSManeuver>();
		if (mds!=null && mds.getDescriptor()!=null)
		{
			for (Entry<EndpointReference, MDIBStructure> entry : MDIBStructures.entrySet())
			{
				String handleOnEndpointReference= BICEPSProxyUtil.createHandleOnEndpointReference(entry.getKey());
				if (handleOnEndpointReference.equals(mds.getHandleOnEndpointReference()))
				{
					MDSDescriptor vms = entry.getValue().getVMS(BICEPSProxyUtil.getHandle(mds));
					if (vms != null)
					{
						getManeuverFromMdib(entry.getValue(), entry.getKey(), vms, result);
					}
				}
			}
		}
		return result;
	}


	private BICEPSManeuver getManeuverFromMdib(MDIBStructure mdib,
			EndpointReference networkDeviceEndpoint, String deviceLocalHandle) {
		BICEPSManeuver diceManeuver = null;
		if (mdib!=null)
		{
			ActivateOperationDescriptor aod = mdib.findManeuver(deviceLocalHandle);
			if (aod != null)
			{
				//				String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(deviceLocalHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
				HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
				diceManeuver = getProxyManager().getProxyFromCache(aod, ProxyUniqueID.create(deviceLocalHandle, networkDeviceEndpoint), mdib, getCommunicationAdapter().createProxyCommunication(mdib,aod,safetyInformationMap));
			}
		}
		return diceManeuver;
	}

	private void getManeuverFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, MDSDescriptor vms, List<BICEPSManeuver> result)
	{
		List<ActivateOperationDescriptor> foundAlerts = mdib.findManeuverForVMS(vms.getHandle());
		processManeuverList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void getManeuverFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, CodedValue code, List<BICEPSManeuver> result)
	{
		List<ActivateOperationDescriptor> foundAlerts = mdib.findManeuvers(code);
		processManeuverList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void processManeuverList(MDIBStructure mdib, EndpointReference networkDeviceEndpoint,
			List<ActivateOperationDescriptor> alerts, List<BICEPSManeuver> result)
	{
		for (ActivateOperationDescriptor descriptor : alerts)
		{
			BICEPSManeuver proxy = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(descriptors.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(networkDeviceEndpoint));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(networkDeviceEndpoint);
			proxy = getProxyManager().getProxyFromCache(descriptor, ProxyUniqueID.create(descriptor.getHandle(), networkDeviceEndpoint), mdib, getCommunicationAdapter().createProxyCommunication(mdib,descriptor, safetyInformationMap));//getAlertSystemFromCache(a, proxyUniqueID, mdib);
			if (proxy != null && !result.contains(proxy))
			{
				result.add(proxy);
			}
		}
	}

	private void getManeuversFromMdib(MDIBStructure mdib, EndpointReference networkDeviceEndpoint, List<BICEPSManeuver> result)
	{
		List<ActivateOperationDescriptor> foundAlerts = mdib.findManeuvers();
		processManeuverList(mdib, networkDeviceEndpoint, foundAlerts, result);
	}

	private void getManeuversFromMdib(MDIBStructure mdib, 
			CodedValue code, List<BICEPSManeuver> result)
	{
		List<ActivateOperationDescriptor> foundDescriptors = mdib.findManeuvers(code);
		processManeuverList(mdib, foundDescriptors, result);
	}

	private  void processManeuverList(MDIBStructure mdib, List<? extends ActivateOperationDescriptor> descriptors, List<BICEPSManeuver> result)
	{
		for (ActivateOperationDescriptor m : descriptors)
		{
			BICEPSManeuver cm = null;
			//			String proxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(m.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(mdib.getDeviceEndpointRef()));
			HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformationMap = getSafetyInformationForEndpoint(mdib.getDeviceEndpointRef());
			cm = getProxyManager().getProxyFromCache(m,ProxyUniqueID.create(m.getHandle(), mdib.getDeviceEndpointRef()),mdib, getCommunicationAdapter().createProxyCommunication(mdib,m,safetyInformationMap));//getMetricFromCache(m, proxyUniqueID, mdib);

			if (cm != null && !result.contains(cm))
			{
				result.add(cm);
			}
		}
	}


}
