/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.impl;

import java.io.IOException;

import org.ws4d.java.configuration.Properties;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.IBICEPSClient;
import com.draeger.medical.mdpws.framework.MDPWSFramework;

public class DefaultBicepsClientImplTest {
	public static void main(String[] args)
	{
		MDPWSFramework.getInstance().start(args);
		try
		{
			Properties.getInstance().read(
					PlatformSupport.getInstance().getToolkit()
					.getResourceAsStream(new URI("config/TestClient.properties")));
		}
		catch (IOException e)
		{
			System.out.println("Could not read properties file.");
			MDPWSFramework.getInstance().stop();
			return;
		}

		Log.setLogLevel(Log.DEBUG_LEVEL_DEBUG);
		IBICEPSClient dc =DefaultBicepsClientImpl.getInstance();
		dc.setPnpEnabled(true);
		dc.searchDevices(null);
		try
		{
			System.in.read(new byte[1024]);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		MDPWSFramework.getInstance().stop();
	}

}
