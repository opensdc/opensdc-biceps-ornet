/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.impl;

import java.lang.reflect.Constructor;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.SubscriptionManager;
import com.draeger.medical.biceps.client.proxy.ReportProvider;

public class DefaultInitializationFactory {

	private static final String reportProviderClassname=System.getProperty("DefaultInitializationFactory.ReportProviderClass", "com.draeger.medical.biceps.client.communication.impl.DefaultReportProvider");
	
	public static final ReportProvider createReportProvider(SubscriptionManager subscriptionManager, CommunicationAdapter communicationAdapter)
	{
		ReportProvider provider=null;		
		try {
			Class<? extends ReportProvider> reportProviderClass = Class.forName(reportProviderClassname).asSubclass(ReportProvider.class);
			Constructor<? extends ReportProvider> constructor = reportProviderClass.getConstructor(SubscriptionManager.class, CommunicationAdapter.class);
			provider= constructor.newInstance(subscriptionManager,communicationAdapter);
		} catch (Exception e) {
			Log.warn(e);
		} 
		return provider;
	}
}
