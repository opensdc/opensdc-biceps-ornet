/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import org.ws4d.java.client.SearchParameter;
import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.dispatch.DefaultDeviceReference;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.dispatch.HelloData;
import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.eventing.EventingException;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.service.reference.Reference;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.ProbeScopeSet;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.CommunicationAdapterCallback;
import com.draeger.medical.biceps.client.communication.CommunicationContainer;
import com.draeger.medical.biceps.client.communication.SubscriptionManager;
import com.draeger.medical.biceps.client.communication.discovery.context.ContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.context.impl.DefaultContextDiscovery;
import com.draeger.medical.biceps.client.communication.discovery.search.NetworkSearchQuery;
import com.draeger.medical.biceps.client.communication.discovery.search.PatientSearchQuery;
import com.draeger.medical.biceps.client.communication.impl.BICEPSClientAliveWatchDog;
import com.draeger.medical.biceps.client.communication.impl.DefaultSubscriptionManager;
import com.draeger.medical.biceps.client.communication.impl.WatchDogCallback;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.ReportProvider;
import com.draeger.medical.biceps.client.proxy.impl.DefaultProxyCommunication;
import com.draeger.medical.biceps.client.utils.InvokeHelper;
import com.draeger.medical.biceps.client.utils.ServiceReferenceProxy;
import com.draeger.medical.biceps.common.context.ContextHelper;
import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.model.AbstractContextState;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.GetContainmentTree;
import com.draeger.medical.biceps.common.model.GetContainmentTreeResponse;
import com.draeger.medical.biceps.common.model.GetContextStates;
import com.draeger.medical.biceps.common.model.GetContextStatesResponse;
import com.draeger.medical.biceps.common.model.GetDescriptorResponse;
import com.draeger.medical.biceps.common.model.GetMDDescription;
import com.draeger.medical.biceps.common.model.GetMDDescriptionResponse;
import com.draeger.medical.biceps.common.model.GetMDIB;
import com.draeger.medical.biceps.common.model.GetMDIBResponse;
import com.draeger.medical.biceps.common.model.GetMDState;
import com.draeger.medical.biceps.common.model.GetMDStateResponse;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.ObjectCreatedReport;
import com.draeger.medical.biceps.common.model.ObjectDeletedReport;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyContextPolicyConverter;
import com.draeger.medical.biceps.mdib.MDIBStructure;
import com.draeger.medical.mdpws.client.MDPWSClient;
import com.draeger.medical.mdpws.dispatcher.streaming.client.IStreamFrameHandler;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandlerManager;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.management.DefaultQoSPolicyExceptionHandler;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparatorProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.Local2MDPWSConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.SafetyInformationManager;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.AbstractSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelComparatorProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelLocal2MDPWSConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelProtocolConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.InOutboundSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.InboundSafetyInformationQoSPolicy;

public class DefaultMDPWSClientAdapter extends MDPWSClient implements CommunicationAdapter {
	private static final boolean configureQoSFramework = Boolean.parseBoolean(System.getProperty("DefaultMDPWSClientAdapter.configureQoSFramework", "true"));
	private final SubscriptionManager								  subscriptionManager;
	private final ReportProvider                               		  reportProvider;
	private final IStreamFrameHandler								  streamHandler;
	private final HashMap<EndpointReference, ServiceReferenceProxy> dev2SetServiceCache    = new HashMap<EndpointReference, ServiceReferenceProxy>();
	private final HashMap<EndpointReference, ServiceReferenceProxy> dev2ContextServiceCache    = new HashMap<EndpointReference, ServiceReferenceProxy>();
	private final HashMap<EndpointReference, BICEPSClientAliveWatchDog> watchDogs				 = new HashMap<EndpointReference, BICEPSClientAliveWatchDog>();
	private final Set<CommunicationAdapterCallback>	callbacks=new HashSet<CommunicationAdapterCallback>();
	private boolean                                                   pnpEnabled;
	private boolean													  vmLocalDeviceDetectionEnabled;
	private List<String> 									  		  deviceWhitelist			= new CopyOnWriteArrayList<String>();
	private List<String> 									  		  deviceBlacklist			= new CopyOnWriteArrayList<String>();
	private ContextDiscovery										  contextDiscovery			= new DefaultContextDiscovery(this);


	public DefaultMDPWSClientAdapter() {
		super();
		this.subscriptionManager= new DefaultSubscriptionManager(this);

		ReportProvider tempReportProvider=DefaultInitializationFactory.createReportProvider(subscriptionManager, this);   // new DefaultReportProvider(this.subscriptionManager, this);

		IStreamFrameHandler tempStreamHandler=null;
		if (tempReportProvider instanceof IStreamFrameHandler)
		{
			tempStreamHandler= (IStreamFrameHandler) tempReportProvider;
		}		
		this.streamHandler=tempStreamHandler;
		this.reportProvider=tempReportProvider;
		setupMDPWSClient();
	}


	//Start Callbacks
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#add(com.draeger.medical.biceps.client.impl.ClientAdapterCallback)
	 */
	@Override
	public boolean add(CommunicationAdapterCallback arg0) {
		return callbacks.add(arg0);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#remove(com.draeger.medical.biceps.client.impl.ClientAdapterCallback)
	 */
	@Override
	public boolean remove(CommunicationAdapterCallback arg0) {
		return callbacks.remove(arg0);
	}


	private void deviceRemoved(DeviceReference devRef, boolean communicationFailed) {
		for(CommunicationAdapterCallback callback:callbacks)
		{
			callback.deviceBye(devRef);
		}

	}



	private void callAddDeviceCallback(DeviceReference devRef) {
		for(CommunicationAdapterCallback callback:callbacks)
		{
			callback.addDevice(devRef);
		}
	}
	//END Callbacks


	//START Watchdog
	private void updateAliveWatchdog(ClientSubscription subscription) {
		if (subscription!=null && subscription.getServiceReference()!=null && subscription.getServiceReference().getParentDeviceRef()!=null){
			EndpointReference devRef= subscription.getServiceReference().getParentDeviceRef().getEndpointReference();

			BICEPSClientAliveWatchDog watchDog = watchDogs.get(devRef);
			if (watchDog!=null){
				watchDog.touch();
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#startBICEPSClientWatchDog(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public BICEPSClientAliveWatchDog startBICEPSClientWatchDog(DeviceReference devRef) {
		BICEPSClientAliveWatchDog watchDog=null;
		if (devRef!=null && devRef.getLocation()!=Reference.LOCATION_LOCAL)
		{
			watchDog = createWatchDog(devRef);
			if (watchDog!=null)
			{
				watchDog.start();
			}
		}
		return watchDog;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapter#startBICEPSClientWatchDog(org.ws4d.java.service.reference.DeviceReference, int, int)
	 */
	@Override
	public BICEPSClientAliveWatchDog startBICEPSClientWatchDog(DeviceReference devRef,
			int maxWatchDogTimeout, int maxWatchDogRetries) {
		BICEPSClientAliveWatchDog watchDog=null;
		if (devRef!=null && devRef.getLocation()!=Reference.LOCATION_LOCAL)
		{
			watchDog = createWatchDog(devRef);
			if (watchDog!=null)
			{
				watchDog.setMaxWatchDogTimeout(maxWatchDogTimeout);
				watchDog.setMaxWatchDogRetries(maxWatchDogRetries);
				watchDog.start();
			}
		}
		
		return watchDog;
	}


	private BICEPSClientAliveWatchDog createWatchDog(DeviceReference devRef) {
		BICEPSClientAliveWatchDog watchDog =null;
		if (devRef!=null && devRef.getLocation()!=Reference.LOCATION_LOCAL)
		{
			WatchDogCallback callback = new WatchDogCallback(this);
			watchDog = new BICEPSClientAliveWatchDog(devRef, callback, null);
			watchDogs.put(devRef.getEndpointReference(), watchDog);
		}
		return watchDog;
	}

	//END Watchdog


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#isPnpEnabled()
	 */
	@Override
	public boolean isPnpEnabled()
	{
		return pnpEnabled;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#setPnpEnabled(boolean)
	 */
	@Override
	public void setPnpEnabled(boolean pnpEnabled)
	{
		if (pnpEnabled){
			registerHelloListening();
		}else{
			unregisterHelloListening();
		}

		this.pnpEnabled = pnpEnabled;
	}




	private void setupMDPWSClient() {

		this.setStreamFrameHandler(streamHandler);

		this.registerHelloListening();
		DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().init();


		configureQoSFramework();
	}


	private void configureQoSFramework() {
		if (configureQoSFramework)
		{
			//TODO SSch Old stuff from BICEPS Client Framework
			//			QoSPolicyExceptionHandlerManager.getInstance().add(new DefaultQoSPolicyExceptionHandler());
			//			DualChannelComparatorProvider comparatorProviderImpl = new DefaultDualChannelComparatorProvider()
			//			{
			//				DualChannelComparator comp = new DualChannelComparator()
			//				{
			//					public boolean compare(Object computedFirstChannel,
			//							DualChannel<?, ?, ?> dualChannel)
			//					{
			//						if (computedFirstChannel == null) return false;
			//						return computedFirstChannel.toString().equals(
			//								dualChannel.getFirstChannel().toString())
			//								&& dualChannel
			//								.getFirstChannel()
			//								.toString()
			//								.equals(dualChannel
			//										.getSecondChannel()
			//										.toString());
			//					}
			//				};
			//
			//				@Override
			//				public DualChannelComparator getDualChannelComparator(DualChannelPolicy policy)
			//				{
			//					return comp;
			//				}
			//			};
			//			DualChannelProtocolConverterProvider transformatorProviderImpl = new DefaultDualChannelProtocolConverterProvider();
			//			Local2MDPWSConverterProvider converterProviderImpl = new DefaultDualChannelLocal2MDPWSConverterProvider();
			//			DualChannelManager.getInstance().initializeManager(converterProviderImpl,
			//					transformatorProviderImpl, comparatorProviderImpl);

			try {
				QoSPolicyExceptionHandlerManager.getInstance().add(new DefaultQoSPolicyExceptionHandler());
				DualChannelComparatorProvider comparatorProviderImpl = new DefaultDualChannelComparatorProvider();
				DualChannelProtocolConverterProvider protocolConverterProvider = new DefaultDualChannelProtocolConverterProvider();
				Local2MDPWSConverterProvider converterProviderImpl = new DefaultDualChannelLocal2MDPWSConverterProvider();
				SafetyInformationManager.getInstance().initializeManager(converterProviderImpl, protocolConverterProvider,comparatorProviderImpl);

			} catch (NoClassDefFoundError e) {
				Log.error(e);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#connectToDevice(org.ws4d.java.service.reference.DeviceReference, com.draeger.medical.biceps.client.impl.MdibRepresentation)
	 */
	@Override
	public void connectToDevice(DeviceReference devRef,
			MDIBStructure mdibContainer) {
		devRef.addListener(this);
		//tickInterfaces(devRef);

	}

	@Override
	public void tickInterfaces(DeviceReference devRef) {
		try {
			//Build all services of this device to receive all WSDLs and policies.
			Iterator svcRefIt = devRef.getDevice().getServiceReferences();
			while(svcRefIt.hasNext()) {
				((ServiceReference)svcRefIt.next()).getService();
			}
		} catch (TimeoutException e) {
			//void -ignore if a service is not available any more
			Log.debug(e);
		}
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#stop()
	 */
	@Override
	public void stop() {
		super.stop();
		// Unsubscribe any remaining report subscriptions for this client.
		reportProvider.stop();
		try
		{
			subscriptionManager.unregisterMdibReports();
		}
		catch (EventingException e)
		{
			Log.warn(e);
		}
		catch (TimeoutException e)
		{
			Log.warn(e);
		}

		for (Entry<EndpointReference, BICEPSClientAliveWatchDog> entry : watchDogs.entrySet())
		{
			if(entry.getValue()!=null)
				entry.getValue().stopBICEPSClientWatchdog();

		}

		contextDiscovery.clear();
		watchDogs.clear();
		// ClientSubscriptionManager.getInstance().unsubscribeAll();
		//		CommunicationManagerRegistry.stopAll();
		// MessageInformer.getInstance().stop();
		// WatchDog.getInstance().stop();
	}


	@Override
	public void unsubscribeAsDeviceListener(Set<EndpointReference> set) {
		if (set!=null)
		{
			for (EndpointReference epr : set)
			{
				DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(epr).removeListener(this);
			}
		}
	}


	@Override
	public void searchDevice(NetworkSearchQuery search) 
	{
		if (search instanceof PatientSearchQuery)
		{
			PatientSearchQuery psq=(PatientSearchQuery)search;
			InstanceIdentifier[] contextMatchSet = psq.getContextMatchSet();
			URI[] scopes=null;
			if (contextMatchSet!=null && contextMatchSet.length>0)
			{
				scopes=new URI[contextMatchSet.length];
				for (int i=0; i<contextMatchSet.length;i++) {
					scopes[i]=ContextHelper.createPatientIdUri(contextMatchSet[i]);
				}
			}
			searchDevicesByPatient(scopes);
			
		}else if (search==null){
			searchDevicesByPatient(null);
		}else{
			Log.warn("Search type not supported! "+search);
		}
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#searchDevices(java.lang.String)
	 */
	private void searchDevicesByPatient(URI[] scopes) {
		searchDevicesByPatient(scopes, MDPWSConstants.MEDICAL_DEVICE_ID_QN);
		searchDevicesByPatient(scopes, MDPWSConstants.OLD_DRAEGER_MEDICAL_DEVICE_ID_QN);
	}

	private void searchDevicesByPatient(URI[] scopes, QName deviceType) {
		//TODO check if valid.
		SearchParameter search = new SearchParameter();
		if (scopes!=null){
			ProbeScopeSet scopeSet = new ProbeScopeSet();
			for (URI scopeUri : scopes) {
				scopeSet.addScope(scopeUri.toString());	
			}
			search.setScopes(scopeSet);
		}
		QNameSet devTypes = new QNameSet(deviceType);
		search.setDeviceTypes(devTypes);
		searchDevice(search);
	}

//	private String buildPatientURN(String patientId)
//	{
//		// TODO MVo implement
//		return patientId;
//	}

	@Override
	public void helloReceived(HelloData helloData)
	{
		//super.helloReceived(helloData);

		if (pnpEnabled)
		{
			DeviceReference devRef = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(helloData);
			if (devRef != null)
			{
				if (isMatchingDevice(devRef,null)){
					callAddDeviceCallback(devRef);
					contextDiscovery.updateWithEndpointInformation(devRef);
				}else
					releaseNonMatchingDeviceReference(devRef);
			}
		}
	}


	private void releaseNonMatchingDeviceReference(DeviceReference devRef) 
	{
		//TODO SSch releaseNonMatchingDeviceReference in order to release connections
	}


	private boolean isMatchingDevice(DeviceReference devRef, SearchParameter search){

		boolean matched=false;
		try{
			if (vmLocalDeviceDetectionEnabled || (!vmLocalDeviceDetectionEnabled && devRef.getLocation()!=Reference.LOCATION_LOCAL))
			{
				if (isOnWhitelist(devRef))
				{
					if (!isOnBlacklist(devRef))
					{
						Iterator iDeviceTypes = devRef.getDevicePortTypes(false);

						QName medDevType = MDPWSConstants.MEDICAL_DEVICE_ID_QN;
						QName draegerMedDevType = MDPWSConstants.OLD_DRAEGER_MEDICAL_DEVICE_ID_QN;

						while (iDeviceTypes.hasNext() && !matched)
						{
							QName devType = (QName) iDeviceTypes.next();
							matched= (medDevType.equals(devType) || draegerMedDevType.equals(devType));
						}
					}
				}
			}
		}catch(Exception e){
			if (Log.isInfo())
			{
				Log.info("Can't determine if device "+devRef+" is matching due to an exception. Reason: "+e.getMessage());
				Log.info(e);
			}

		}
		return matched;
	}

	private boolean isOnWhitelist(DeviceReference devRef) {
		return isOnUUIDList(devRef,deviceWhitelist, true);
	}


	private boolean isOnUUIDList(DeviceReference devRef, List<String> uuidList, boolean initialValue) {
		String eprString;
		boolean retVal=initialValue;
		if (!uuidList.isEmpty() && devRef.getEndpointReference()!=null && devRef.getEndpointReference().getAddress()!=null && (eprString=devRef.getEndpointReference().getAddress().toString())!=null)
		{
			retVal=uuidList.contains(eprString);
		}
		return retVal;
	}

	private boolean isOnBlacklist(DeviceReference devRef) {
		return isOnUUIDList(devRef,deviceBlacklist, false);
	}




	@Override
	public IParameterValue eventReceived(ClientSubscription subscription, URI actionURI,
			IParameterValue parameterValue)
	{
		if (Log.isDebug()) Log.debug("BICEPSClient: eventReceived() - actionURI=" + actionURI);

		if (MDPWSConstants.ACTION_OBJECT_CREATED_REPORT_URI.equalsWsdRfc3986(actionURI))
		{
			onObjectCreatedEvent(subscription, parameterValue);
		}
		else if (MDPWSConstants.ACTION_OBJECT_DELETED_REPORT_URI.equalsWsdRfc3986(actionURI))
		{
			onObjectDeletedEvent(subscription, parameterValue);
		}
		//		else if (actionURI.equalsWsdRfc3986(MDPWSConstants.ACTION_OPERATION_CREATED_REPORT_URI))
		//		{
		//			onOperationCreatedEvent(subscription, parameterValue);
		//		}
		//		else if (actionURI.equalsWsdRfc3986(MDPWSConstants.ACTION_OPERATION_DELETED_REPORT_URI))
		//		{
		//			onOperationDeletedEvent(subscription, parameterValue);
		//		}
		else
		{
			reportProvider.eventReceived(subscription, actionURI, parameterValue);
		}

		updateAliveWatchdog(subscription);

		//return super.eventReceived(subscription, actionURI, parameterValue);
		return null;
	}

	@Override
	public void deviceFound(DeviceReference devRef, SearchParameter search)
	{
		if (isMatchingDevice(devRef,search)){
			callAddDeviceCallback(devRef);
			contextDiscovery.updateWithEndpointInformation(devRef);
		}
	}

	@Override
	public void noDevicesFound(SearchParameter search) {

		for(CommunicationAdapterCallback callback:callbacks)
		{
			callback.noDevicesFound(search);
		}
	}

	@Override
	public void deviceChanged(DeviceReference deviceRef)
	{
		for(CommunicationAdapterCallback callback:callbacks)
		{
			callback.deviceChanged(deviceRef);
		}	
		contextDiscovery.updateWithEndpointInformation(deviceRef);
	}




	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#retrieveMdib(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public CommunicationContainer<MDIB, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveMdib(DeviceReference devRef) throws InvocationException,
	TimeoutException
	{
		BigInteger sequenceNumber=null;
		MDIB mdib = null;
		HashMap<String, BICEPSSafetyInformationPolicyElement> msgContext=null;
		if (devRef != null)
		{
			QNameSet mdibProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_GET_SERVICE));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
			while (iSvcRef.hasNext())
			{
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				MDIBSafetyContextFilter mdibParameterValueResponseHandler=new MDIBSafetyContextFilter();
				GetMDIBResponse response = (GetMDIBResponse) InvokeHelper.invoke(svcRef, new GetMDIB(), null,mdibParameterValueResponseHandler);

				msgContext = prepareMessageContextMap(devRef,
						mdibParameterValueResponseHandler);

				if (response != null)
				{
					sequenceNumber= response.getSequenceNumber();
					mdib = response.getMDIB();
					break;
				}
			}
		}

		CommunicationContainer<MDIB, HashMap<String, BICEPSSafetyInformationPolicyElement>> retVal=null;
		if (sequenceNumber!=null)
			retVal=new CommunicationContainer<MDIB, HashMap<String,BICEPSSafetyInformationPolicyElement>>(mdib, msgContext, sequenceNumber.longValue());

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.CommunicationAdapter#retrieveMdib(org.ws4d.java.service.reference.DeviceReference)
	 */
	@Override
	public CommunicationContainer<com.draeger.medical.biceps.common.model.MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveMDDescription(DeviceReference devRef) throws InvocationException,
	TimeoutException
	{
		BigInteger sequenceNumber=null;
		com.draeger.medical.biceps.common.model.MDDescription mdDescription = null;
		HashMap<String, BICEPSSafetyInformationPolicyElement> msgContext=null;
		if (devRef != null)
		{
			QNameSet mdibProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_GET_SERVICE));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
			while (iSvcRef.hasNext())
			{
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				MDIBSafetyContextFilter mdibParameterValueResponseHandler=new MDIBSafetyContextFilter();
				GetMDDescriptionResponse response = (GetMDDescriptionResponse) InvokeHelper.invoke(svcRef, new GetMDDescription(), null,mdibParameterValueResponseHandler);

				msgContext = prepareMessageContextMap(devRef,
						mdibParameterValueResponseHandler);

				if (response != null)
				{
					sequenceNumber= response.getSequenceNumber();
					mdDescription = response.getStaticDescription();
					break;
				}
			}
		}

		CommunicationContainer<com.draeger.medical.biceps.common.model.MDDescription, HashMap<String, BICEPSSafetyInformationPolicyElement>> retVal=null;
		if (sequenceNumber!=null)
			retVal=new CommunicationContainer<com.draeger.medical.biceps.common.model.MDDescription, HashMap<String,BICEPSSafetyInformationPolicyElement>>(mdDescription, msgContext, sequenceNumber.longValue());

		return retVal;
	}
	
	
	@Override
	public CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveDescription(
			DeviceReference devRef, String handle) throws InvocationException,
			TimeoutException {
		BigInteger sequenceNumber=null;
		List<Descriptor> descriptors = null;
		HashMap<String, BICEPSSafetyInformationPolicyElement> msgContext=null;
		if (devRef != null)
		{
			QNameSet mdibProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_GET_SERVICE));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
			while (iSvcRef.hasNext())
			{
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				MDIBSafetyContextFilter mdibParameterValueResponseHandler=new MDIBSafetyContextFilter();
				com.draeger.medical.biceps.common.model.GetDescriptor getDescriptor = new com.draeger.medical.biceps.common.model.GetDescriptor();
				
				if (handle!=null)
					getDescriptor.getHandles().add(handle);
				
				GetDescriptorResponse response = (GetDescriptorResponse) InvokeHelper.invoke(svcRef, getDescriptor, null,mdibParameterValueResponseHandler);

				msgContext = prepareMessageContextMap(devRef,
						mdibParameterValueResponseHandler);

				if (response != null)
				{
					sequenceNumber= response.getSequenceNumber();
					descriptors = response.getDescriptor();
					break;
				}
			}
		}

		CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> retVal=null;
		if (sequenceNumber!=null)
			retVal=new CommunicationContainer<List<Descriptor>, HashMap<String,BICEPSSafetyInformationPolicyElement>>(descriptors, msgContext, sequenceNumber.longValue());

		return retVal;
	}
	
	
	


	@Override
	public CommunicationContainer<ContainmentTree, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrieveContainmentTreeInfo(
			DeviceReference devRef, String handle) throws InvocationException,
			TimeoutException {
		BigInteger sequenceNumber=null;
		ContainmentTree cTree = null;
		HashMap<String, BICEPSSafetyInformationPolicyElement> msgContext=null;
		if (devRef != null)
		{
			QNameSet mdibProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_GET_SERVICE));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
			while (iSvcRef.hasNext())
			{
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				MDIBSafetyContextFilter mdibParameterValueResponseHandler=new MDIBSafetyContextFilter();
				GetContainmentTree getContainmentTree = new com.draeger.medical.biceps.common.model.GetContainmentTree();
				if (handle!=null)
					getContainmentTree.getHandles().add(handle);
				
				GetContainmentTreeResponse response = (GetContainmentTreeResponse) InvokeHelper.invoke(svcRef,getContainmentTree , null,mdibParameterValueResponseHandler);

				msgContext = prepareMessageContextMap(devRef,
						mdibParameterValueResponseHandler);

				if (response != null)
				{
					sequenceNumber= response.getSequenceNumber();
					cTree = response.getContainmentTree();
					break;
				}
			}
		}

		CommunicationContainer<ContainmentTree, HashMap<String, BICEPSSafetyInformationPolicyElement>> retVal=null;
		if (sequenceNumber!=null)
			retVal=new CommunicationContainer<ContainmentTree, HashMap<String,BICEPSSafetyInformationPolicyElement>>(cTree, msgContext, sequenceNumber.longValue());

		return retVal;
	}


	private HashMap<String, BICEPSSafetyInformationPolicyElement> prepareMessageContextMap(
			DeviceReference devRef,
			MDIBSafetyContextFilter mdibParameterValueResponseHandler) {
		HashMap<String, BICEPSSafetyInformationPolicyElement> msgContext;
		msgContext= mdibParameterValueResponseHandler.getPolicyElementMap();
		if (!msgContext.isEmpty())
		{
			Iterator qosPolicies= QoSPolicyManager.getInstance().getQoSPolicies();
			while (qosPolicies.hasNext())
			{

				QoSPolicy policy=	(QoSPolicy) qosPolicies.next();
				if (policy instanceof InboundSafetyInformationQoSPolicy || policy instanceof InOutboundSafetyInformationQoSPolicy)
				{
					Iterator subjects = policy.getSubjects();
					boolean subjectMatched = QoSPolicyUtil.containsSubjectForDevice(devRef.getEndpointReference(),subjects);
					if (subjectMatched)
					{

						AbstractSafetyInformationQoSPolicy siPolicy=(AbstractSafetyInformationQoSPolicy)policy;
						Set<Entry<String, BICEPSSafetyInformationPolicyElement>> dicePolicyEntries = msgContext.entrySet();
						for (Entry<String, BICEPSSafetyInformationPolicyElement> entry : dicePolicyEntries) {
							SafetyInformationPolicyAttributes attributes=BICEPSSafetyContextPolicyConverter.convertFromBICEPSPolicyToAttributes(entry.getValue().getSafetyInformationPolicy());
							entry.getValue().setMdpwsPolicy(siPolicy);


							if (siPolicy.getSafetyInformationPolicyAttributes()!=null)
							{
								if (attributes.getDualChannelSelectors()!=null) siPolicy.getSafetyInformationPolicyAttributes().getDualChannelSelectors().addAll(attributes.getDualChannelSelectors());
								if (attributes.getContextDefinitions()!=null) siPolicy.getSafetyInformationPolicyAttributes().getContextDefinitions().addAll(attributes.getContextDefinitions());

							}else{
								siPolicy.setSafetyInformationPolicyAttributes(attributes);
							}

						}
						//System.out.println("Updated Policy:"+policy);
					}
				}
			}

		}
		return msgContext;
	}



	//	private boolean doesPolicySubjectMatch(DeviceReference devRef, Iterator subjects) {
	//		boolean subjectMatched=false;
	//		if (subjects!=null)
	//		{
	//			while (!subjectMatched && subjects.hasNext()){
	//				//TODO SSch Was ist mit den anderen Subjects???
	//				Object subject = subjects.next();
	//				if (subject instanceof MessagePolicySubject)
	//				{
	//					MessagePolicySubject mps=(MessagePolicySubject) subject;
	//					OperationDescription proxyOP = mps.getOperation();
	//					if(proxyOP.getService()!=null && proxyOP.getService().getServiceReference()!=null && devRef.equals(proxyOP.getService().getServiceReference().getParentDeviceRef()))
	//					{
	//						subjectMatched=true;
	//					}
	//				}
	//			}
	//		}
	//		return subjectMatched;
	//	}






	private ServiceReferenceProxy getSetServiceReferenceProxy(EndpointReference devEndpointRef, String porttypeName, HashMap<EndpointReference, ServiceReferenceProxy> dev2ServiceCache)
	{
		ServiceReferenceProxy result = dev2ServiceCache.get(devEndpointRef);
		if (result == null)
		{
			DeviceReference devRef = DeviceServiceRegistryProvider.getInstance()
					.getDeviceServiceRegistry().getStaticDeviceReference(devEndpointRef);
			if (devRef != null)
			{
				QNameSet mdibProviderPTs = new QNameSet(
						QNameFactory.getInstance().getQName(porttypeName));
				try
				{
					Iterator iSvcRef;
					iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
					if (iSvcRef.hasNext())
					{
						result = new ServiceReferenceProxy((ServiceReference) iSvcRef.next());
						dev2ServiceCache.put(devEndpointRef, result);
					}
				}
				catch (TimeoutException e)
				{
					Log.warn(e);
				}
			}
		}
		return result;
	}



	@Override
	public void deviceRemovedByWatchdog(DeviceReference deviceReference) {
		handleDeviceLeftNetwork(deviceReference, true);
	}


	@Override
	public void deviceBye(DeviceReference deviceReference)
	{
		handleDeviceLeftNetwork(deviceReference, false);
	}


	private void handleDeviceLeftNetwork(final DeviceReference devRef, boolean communicationFailed) {
		if (devRef != null)
		{
			devRef.reset();
//			PlatformSupport.getInstance().getToolkit().getThreadPool().execute(new Runnable() {
//				
//				@Override
//				public void run() {
//					try{
//						if (devRef.isDeviceObjectExisting())
//						{
//							Device d=devRef.getDevice();
//							d.invalidate();
//							if (devRef instanceof DefaultDeviceReference)
//							{
//								((DefaultDeviceReference) devRef).reset();
//							}
//						}
//					} catch (TimeoutException e) {
//						if (Log.isDebug()) Log.debug(e);
//					}					
//				}
//			});

			// Cleanup cache (including notification of listeners)
			try
			{
				unregisterMdibReports(devRef,true);
				devRef.removeListener(this);

				dev2SetServiceCache.remove(devRef.getEndpointReference());
				dev2ContextServiceCache.remove(devRef.getEndpointReference());

				deviceRemoved(devRef,communicationFailed);
			}
			catch (Exception e)
			{
				if (Log.isDebug()) Log.debug(e);
			}
		}

		BICEPSClientAliveWatchDog watchDog = watchDogs.remove(devRef.getEndpointReference());
		if (watchDog!=null)
			watchDog.stopBICEPSClientWatchdog();
	}

	@Override
	public void subscriptionEndReceived(ClientSubscription subscription, URI reason)
	{
		super.subscriptionEndReceived(subscription, reason);

		if (subscription!=null)
		{
			if (Log.isDebug())
				Log.debug("BICEPSClient: subscriptionEndReceived - " + subscription.getClientSubscriptionId()+ ", " + reason);

			if (!this.subscriptionManager.removeMDIBSubscription(subscription))
			{
				reportProvider.subscriptionEndReceived(subscription, reason);
			}
			this.reportProvider.notifyClientSubscriptionInvalid(subscription);

			// Remove subscription but don't unsubscribe because it's invalid already.
			this.subscriptionManager.removeClientSubscription(subscription, false);
		}
	}

	@Override
	public void subscriptionTimeoutReceived(ClientSubscription subscription)
	{
		super.subscriptionTimeoutReceived(subscription);

		Log.debug("BICEPSClient: subscriptionTimeoutReceived - " + subscription.getClientSubscriptionId());
		// TODO SSch No expiring subscriptions implemented for now. Handle just like subscription ended. Do we need to support expiring event subscriptions or not???
		subscriptionEndReceived(subscription, new URI("/Timeout"));
	}


	private void unregisterMdibReports(DeviceReference deviceReference, boolean communicationFailed)
			throws EventingException, TimeoutException {
		if (subscriptionManager!=null && deviceReference!=null)
			this.subscriptionManager.unregisterMdibReports(deviceReference.getEndpointReference(), communicationFailed);
	}

	private void onObjectCreatedEvent(ClientSubscription subscription, IParameterValue payload)
	{
		ObjectCreatedReport msg=InvokeHelper.getObjectFromParameterValue(payload);
		

		if (msg!=null && callbacks!=null)
		{
			ServiceReference srvRef=null;
			DeviceReference devRef = null;
			if (subscription!=null && (srvRef=subscription.getServiceReference())!=null && (devRef=srvRef.getParentDeviceRef())!=null)
			{
				for (CommunicationAdapterCallback comCallback : callbacks) {
					comCallback.modifiedDescription(devRef, msg);
				}	
			}
		}
		//		ObjectCreatedReport msg = ParameterValueToSchemaMessageServiceHelper.convertToObjectCreatedReport(parameterValue);
		//		for (ObjectCreatedReport.ReportPart p : msg.getReportParts())
		//		{
		//			for (VMO o : p.getVMOs())
		//			{
		//				MdibRepresentation mdib = mdibSubscriptions.get(subscription);
		//				mdib.addVMO(o, p.getParentVMO(), p.getSourceMDS());
		//
		//				List<String> patientIds = mdib.getPatientIdsForVmo(p.getSourceMDS());
		//				if (patientIds.size() == 1)
		//				{
		//					if (o instanceof RealTimeSampleArrayMetric)
		//					{
		//						for (MdibListener l : mdibListeners)
		//						{
		//							l.newStreamAvailable(patientIds.get(0));
		//						}
		//					}
		//					else if (o instanceof Metric)
		//					{
		//						for (MdibListener l : mdibListeners)
		//						{
		//							l.newMetricAvailable(patientIds.get(0), o.getCode());
		//						}
		//					}
		//					else if (o instanceof Alert)
		//					{
		//						for (MdibListener l : mdibListeners)
		//						{
		//							l.newAlertAvailable(patientIds.get(0), o.getCode());
		//						}
		//					}
		//				}
		//			}
		//		}
	}

	private void onObjectDeletedEvent(ClientSubscription subscription, IParameterValue payload)
	{
		ObjectDeletedReport msg=InvokeHelper.getObjectFromParameterValue(payload);

		if (Log.isInfo()) Log.info("TODO Handle: "+msg);
		//TODO SSch Refactor
		//		ObjectDeletedReport msg = ParameterValueToSchemaMessageServiceHelper.convertToObjectDeletedReport(parameterValue);
		//		MdibRepresentation mdib = mdibSubscriptions.get(subscription);
		//		for (ObjectDeletedReport.ReportPart p : msg.getReportParts())
		//		{
		//			for (VMO o : p.getVMOs())
		//			{
		//				mdib.removeVMO(o.getHandle());
		//				if (o instanceof VMD)
		//				{
		//					cache.removeVMD((VMD) o);
		//				}
		//				else if (o instanceof Channel)
		//				{
		//					cache.removeChannel((Channel) o);
		//				}
		//				// This should be handled by onOperationDeletedEvent
		//				// else if(o instanceof SCO)
		//				// {
		//				// for(de.draeger.run.common.model.Operation op : ((SCO)o).getOperations()) {}
		//				// }
		//				else if (o instanceof RealTimeSampleArrayMetric)
		//				{
		//					cache.removeStream((RealTimeSampleArrayMetric) o);
		//				}
		//				else if (o instanceof Metric)
		//				{
		//					cache.removeMetric((Metric) o);
		//				}
		//				else if (o instanceof Alert)
		//				{
		//					cache.removeAlert((Alert) o);
		//				}
		//			}
		//		}
	}

	@SuppressWarnings("unused")
	private void onOperationCreatedEvent(ClientSubscription subscription,
			IParameterValue parameterValue)
	{
		//		OperationCreatedReport msg = ParameterValueToSchemaMessageServiceHelper.getInstance().convertToOperationCreatedReport(parameterValue);
		//		if (Log.isInfo()) Log.info("TODO Handle: "+msg);
		//		
		//TODO SSch Refactor
		//		for (OperationCreatedReport.ReportPart p : msg.getReportParts())
		//		{
		//			for (de.draeger.run.common.model.Operation o : p.getOperations())
		//			{
		//				MdibRepresentation mdib = mdibSubscriptions.get(subscription);
		//				mdib.addOperation(o, p.getSourceMDS());
		//
		//				List<String> patientIds = mdib.getPatientIdsForVms(p.getSourceMDS());
		//				if (patientIds.size() == 1)
		//				{
		//					if (o instanceof ActivateOperation)
		//					{
		//						for (MdibListener listener : mdibListeners)
		//						{
		//							listener.newManeuverAvailable(patientIds.get(0));
		//						}
		//					}
		//					else
		//					{
		//						// This would be a new operation for an already existing metric/alert?
		//						// Then it has to be added to the already existing Setting or
		//						// the Measure has to be replaced by a Setting?!?
		//						// ???!!! Should not happen !!!???
		//						Log.info("Unsupported new Operation for existing Metric/Alert received.");
		//					}
		//				}
		//			}
		//		}
	}

	@SuppressWarnings("unused")
	private void onOperationDeletedEvent(ClientSubscription subscription,
			IParameterValue parameterValue)
	{
		//		OperationDeletedReport msg = ParameterValueToSchemaMessageServiceHelper.getInstance() .convertToOperationDeletedReport(parameterValue);
		//		if (Log.isInfo()) Log.info("TODO Handle: "+msg);
		//MDIBStructure mdib = this.subscriptionManager.getMDIBRepForSubscription(subscription); //use subscription manager
		//TODO SSch Refactor
		//		for (OperationDeletedReport.ReportPart p : msg.getReportParts())
		//		{
		//			for (de.draeger.run.common.model.Operation o : p.getOperations())
		//			{
		//				if (mdib.removeOperation(o.getHandle()))
		//				{
		//					if (o instanceof ActivateOperation)
		//					{
		//						cache.removeManeuver((ActivateOperation) o);
		//					}
		//					else
		//					{
		//						// TODO MVo An operation was removed. Maybe a Setting has to be removed now or
		//						// something similar???
		//						//                        cache.updateCacheForRemovedOperation(o);
		//					}
		//				}
		//			}
		//		}
	}



	@Override
	public ProxyCommunication createProxyCommunication(MDIBStructure mdib, Descriptor descriptor,HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation) 
	{
		HashMap<OperationDescriptor, ServiceReferenceProxy> serviceReferencesForOperations=getNetworkInformation(mdib, descriptor);
		DefaultProxyCommunication proxyCom=new DefaultProxyCommunication(this.reportProvider, serviceReferencesForOperations, safetyInformation, this);
		return proxyCom;
	}

	private HashMap<OperationDescriptor, ServiceReferenceProxy> getNetworkInformation(MDIBStructure mdib, Descriptor descriptor) 
	{

		HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc = new HashMap<OperationDescriptor, ServiceReferenceProxy>();
		if (mdib!=null && descriptor!=null)
		{
			if (descriptor instanceof OperationDescriptor)
			{
				OperationDescriptor o = (OperationDescriptor)descriptor;
				fillNetworkInfoResult(mdib, ops2Svc, o);
			}else{
				for (OperationDescriptor o : mdib.findOperationsForOperationDescriptor(descriptor))
				{
					fillNetworkInfoResult(mdib, ops2Svc, o);
				}
			}
		}
		return ops2Svc;
	}


	private void fillNetworkInfoResult(MDIBStructure mdib,
			HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc,
			OperationDescriptor o) {
		if (o instanceof SetContextOperationDescriptor)
		{
			ops2Svc.put(o, getSetServiceReferenceProxy(mdib.getDeviceEndpointRef(),MDPWSConstants.PORTTYPE_CONTEXT_SERVICE, this.dev2ContextServiceCache));
		}else{
			ops2Svc.put(o, getSetServiceReferenceProxy(mdib.getDeviceEndpointRef(),MDPWSConstants.PORTTYPE_SET_SERVICE,this.dev2SetServiceCache));
		}
	}


	@Override
	public void registerMdibReports(EndpointReference networkDeviceEndpoint) 
	{
		this.subscriptionManager.registerMdibReports(networkDeviceEndpoint);
	}


	@Override
	public CommunicationContainer<MDState, ?> retrieveStates(
			DeviceReference devRef, Collection<String> handles)
					throws InvocationException, TimeoutException {
		MDState mdState = null;
		CommunicationContainer<MDState, Object> retVal=null;
		BigInteger sequenceNumber=null;
		if (devRef != null)
		{
			QNameSet mdibProviderPTs = new QNameSet(
					QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_GET_SERVICE));
			Iterator iSvcRef = devRef.getDevice().getServiceReferences(mdibProviderPTs);
			if (!iSvcRef.hasNext())
			{
				if (Log.isWarn())
					Log.warn("No get service available for device:"+devRef);
			}else{
				while (iSvcRef.hasNext())
				{
					ServiceReference svcRef = (ServiceReference) iSvcRef.next();
					GetMDState requestMessage = new GetMDState();
					if (handles!=null) requestMessage.getHandles().addAll(handles);

					GetMDStateResponse response = (GetMDStateResponse) InvokeHelper.invoke(svcRef, requestMessage, null);
					if (response != null)
					{
						sequenceNumber=response.getSequenceNumber();
						mdState = response.getMDState();
						break;
					}
				}


				try{
					List<AbstractContextState> contextStates = getContextStates(devRef,handles);
					if (contextStates!=null)
					{
						if (mdState==null)mdState=new MDState();
						for (AbstractContextState contextState : contextStates) {
								mdState.getStates().add(contextState);
						}
//						mdState.getStates().addAll(ContextAssociationStateValues);
					}
				}catch(Exception e){
						Log.info(e);
				}
			}
		}

		if (sequenceNumber!=null)
			retVal=new CommunicationContainer<MDState, Object>(mdState, null,sequenceNumber.longValue());

		return retVal;
	}


	@Override
	public CommunicationContainer<MDState, ?> retrieveStates(
			DeviceReference devRef, String handle) throws InvocationException,
			TimeoutException {
		ArrayList<String> handleList=new ArrayList<String>(1);
		if (handle!=null) handleList.add(handle);
		return retrieveStates(devRef, handleList);
	}

	@Override
	public CommunicationContainer<MDState,?> retrieveStates(DeviceReference devRef) throws InvocationException,
	TimeoutException
	{
		return retrieveStates(devRef,(Collection<String>)null);
	}


	private List<AbstractContextState> getContextStates(DeviceReference devRef, Collection<String> handles) throws TimeoutException, InvocationException
	{
		List<AbstractContextState> contextStateList=null;
		QNameSet phiservicePTs = new QNameSet(QNameFactory.getInstance().getQName(MDPWSConstants.PORTTYPE_CONTEXT_SERVICE));
		Iterator iSvcRef = devRef.getDevice().getServiceReferences(phiservicePTs);
		if (!iSvcRef.hasNext())
		{
			if (Log.isInfo())
				Log.info("No phi service available for device:"+devRef);
		}else{
			while (iSvcRef.hasNext())
			{
				ServiceReference svcRef = (ServiceReference) iSvcRef.next();
				GetContextStates requestMessage = new GetContextStates();
				if (handles!=null) requestMessage.getHandles().addAll(handles);

				GetContextStatesResponse response = (GetContextStatesResponse) InvokeHelper.invoke(svcRef, requestMessage, null);
				if (response != null)
				{
//					protectedHealthInformation = response.getContextStates();
					contextStateList = response.getContextStates();
//					contextStateList=new ArrayList<ContextState>();
//					for (ContextState contextStateElement : contextStates) {
//						if (contextStateElement.getContextAssociation()!=null){
//							contextStateList.add(contextStateElement.getContextAssociation());
//						}
//					}
					break;
				}
			}
		}
		return contextStateList;
	}


	@Override
	public void setDeviceReferenceWhitelist(List<String> whitelist) {
		if (whitelist!=null && whitelist!= deviceWhitelist)
		{
			deviceWhitelist=whitelist;
		}
	}


	@Override
	public void setDeviceReferenceBlacklist(List<String> blacklist) {
		if (blacklist!=null && blacklist!= deviceBlacklist)
		{
			deviceBlacklist=blacklist;
		}
	}


	@Override
	public void setVMLocalDeviceDetectionEnabled(
			boolean vmLocalDeviceDetectionEnabled) {
		this.vmLocalDeviceDetectionEnabled=vmLocalDeviceDetectionEnabled;

	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.communication.CommunicationAdapter#getContextDiscovery()
	 */
	@Override
	public ContextDiscovery getContextDiscovery() {
		return this.contextDiscovery;
	}


}
