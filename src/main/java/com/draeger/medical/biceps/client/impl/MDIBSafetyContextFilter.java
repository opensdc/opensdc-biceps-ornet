/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.communication.protocol.soap.generator.DefaultBasicTypes2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.DefaultParameterValue2SOAPSerializer;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.BicepsClientFramework;
import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.utils.InvokeHelper.RawParameterValueHandler;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSDualChannelDefinition;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyContextDefinition;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyInformationQoSPolicy;
import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.domainmodel.MDPWSOperation;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

public class MDIBSafetyContextFilter implements RawParameterValueHandler {

	private final java.util.HashMap<String, BICEPSSafetyInformationPolicyElement> policyElementMap=new java.util.HashMap<String, BICEPSSafetyInformationPolicyElement>();

	public java.util.HashMap<String, BICEPSSafetyInformationPolicyElement> getPolicyElementMap() {
		return policyElementMap;
	}


	@Override
	public void handleRawParameterResponse(MDPWSOperation mdpwsOp,IParameterValue payload) 
	{
		if (BicepsClientFramework.getInstance().isQoSFrameworkEnabled())
		{
			IParameterValue response = payload;
			try {
				//TODO SSch to not convert to a string, but use the parameterValue object
				MyByteArrayOutputStream os=new MyByteArrayOutputStream();
				convertPVToString(response, os);
				Document mdibDocument = getDocumentBuilder().parse (os.getByteArrayInputStream());
				HashMap declaredNamespaces = XPathInfo.getDeclaredNamesSpaces(mdibDocument);
				XPathNamespaceContext xPathNameCtxt = XPathInfo.createXPathSpaceContext(declaredNamespaces);
				String bicepsPrefix = xPathNameCtxt.getPrefix(SchemaHelper.NAMESPACE_MESSAGES);
				String siPrefix=xPathNameCtxt.getPrefix(SafetyInformationConstants.SI_NAMESPACE);

				if (siPrefix!=null && bicepsPrefix!=null)
				{
					XPathInfo operationFilter = getOperationFilterMDIB( bicepsPrefix, siPrefix,xPathNameCtxt);
					NodeList operationsWithSI = (NodeList) operationFilter.evaluate(mdibDocument,XPathConstants.NODESET);


					if (operationsWithSI!=null && operationsWithSI.getLength()>0)
					{
						//System.out.println("operationsWithSI:"+operationsWithSI.getLength());
						int nodeCnt=operationsWithSI.getLength();
						for(int operationNodeCnt=0;operationNodeCnt<nodeCnt;operationNodeCnt++)
						{
							Node siOperationNode=operationsWithSI.item(operationNodeCnt);
							//System.out.println("siOperationNode:"+siOperationNode);
							XPathInfo operationHandleXPath=new XPathInfo("./"+bicepsPrefix+":Handle", xPathNameCtxt);
							Node operationHandleNode=(Node) operationHandleXPath.evaluate(siOperationNode,XPathConstants.NODE );
							if (operationHandleNode!=null)
							{
								//TODO SSch get OperationHandle, get SafetyReq Node
								String opHandle=operationHandleNode.getTextContent();

								XPathInfo safetyReqXPath=new XPathInfo("./"+siPrefix+":"+SafetyInformationConstants.SI_ELEM_DEFINITION_SAFETY_REQ, xPathNameCtxt);
								Node sReqNode=(Node) safetyReqXPath.evaluate(siOperationNode,XPathConstants.NODE );	
								if (sReqNode!=null)
								{
									ArrayList<String> operationHandles=new ArrayList<String>(1);
									operationHandles.add(opHandle);
									BICEPSSafetyInformationQoSPolicy safetyInformationPolicy=new BICEPSSafetyInformationQoSPolicy(operationHandles);
									fillSafetyInformation(sReqNode,safetyInformationPolicy,xPathNameCtxt);
									BICEPSSafetyInformationPolicyElement siPolicyElement=new BICEPSSafetyInformationPolicyElement(safetyInformationPolicy);
									policyElementMap.put(opHandle, siPolicyElement);
								}
							}




							//TODO SSch How to attach the SafetyReq to the MDIB Objects??? in com.draeger.medical.biceps.client.impl.DefaultMDPWSClientAdapter.retrieveMdib(DeviceReference)

							//TODO SSch Fill SafetyInformationAttributes
						}
					}
				}

			} catch (Exception e) {
				if (Log.isWarn())
				{
					Log.warn("Could not handle ParameterValue response for op "+mdpwsOp+". PV="+response);
					Log.warn(e);
				}

			}
		}
	}


	private void fillSafetyInformation(Node sReqNode, BICEPSSafetyInformationQoSPolicy safetyInformationPolicy, XPathNamespaceContext xPathNameCtxt) throws XPathExpressionException {
		if (sReqNode!=null && safetyInformationPolicy!=null)
		{
			fillDualChannelDefinitions(sReqNode, safetyInformationPolicy,xPathNameCtxt);
			fillSafetyContextDefinitions(sReqNode, safetyInformationPolicy,xPathNameCtxt);

			if (Log.isInfo()) Log.info(safetyInformationPolicy.toString());
		}
	}


	private void fillSafetyContextDefinitions(Node sReqNode,
			BICEPSSafetyInformationQoSPolicy safetyInformationPolicy,
			XPathNamespaceContext xPathNameCtxt) throws XPathExpressionException 
			{

		String siPrefix=xPathNameCtxt.getPrefix(SafetyInformationConstants.SI_NAMESPACE);
		XPathInfo contextDefXPath=new XPathInfo("./"+siPrefix+":"+SafetyInformationConstants.SI_ELEM_DEFINITION_CONTEXT_DEF, xPathNameCtxt);
		NodeList contextDefNodes=(NodeList) contextDefXPath.evaluate(sReqNode,XPathConstants.NODESET );
		int contextDefNodeCnt=0;
		if (contextDefNodes!=null && (contextDefNodeCnt=contextDefNodes.getLength())>0)
		{
			for (int i=0;i<contextDefNodeCnt;i++)
			{
				Node contextDefNode = contextDefNodes.item(i);

				String contextTarget=null;

				NamedNodeMap contextDefAttrs = contextDefNode.getAttributes();
				int attrCnt=0;
				if (contextDefAttrs!=null && (attrCnt=contextDefAttrs.getLength())>0)
				{
					for (int a=0;a<attrCnt && contextTarget==null;a++)
					{
						Attr attrNode=(Attr) contextDefAttrs.item(a);		
						String tempValue=attrNode.getValue();
						if (SafetyInformationConstants.SI_ATTRIB_TARGET.equals(attrNode.getLocalName()))
						{
							contextTarget=tempValue;
						}
					}
				}

				if (contextTarget!=null)
				{
					ArrayList<QName> selectors=getSelectors(contextDefNode, xPathNameCtxt, siPrefix);

					BICEPSSafetyContextDefinition contextDefinition=new BICEPSSafetyContextDefinition(contextTarget);
					if (selectors!=null && selectors.size()>0) contextDefinition.getAttributeSelectors().addAll(selectors);

					safetyInformationPolicy.getSafetyContextDefinitions().add(contextDefinition);
				}else{
					if (Log.isWarn()) Log.warn("No context target for "+SafetyInformationConstants.SI_ELEM_DEFINITION_CONTEXT_DEF+" "+i);
				}
			}
		}
			}


	private void fillDualChannelDefinitions(Node sReqNode,
			BICEPSSafetyInformationQoSPolicy safetyInformationPolicy,
			XPathNamespaceContext xPathNameCtxt)
					throws XPathExpressionException {
		String siPrefix=xPathNameCtxt.getPrefix(SafetyInformationConstants.SI_NAMESPACE);
		XPathInfo dualChannelDefXPath=new XPathInfo("./"+siPrefix+":"+SafetyInformationConstants.SI_ELEM_DEFINITION_DUALCHANNEL_DEF, xPathNameCtxt);
		NodeList dualChannelDefNodes=(NodeList) dualChannelDefXPath.evaluate(sReqNode,XPathConstants.NODESET );
		int dualChannelDefNodeCnt=0;
		if (dualChannelDefNodes!=null && (dualChannelDefNodeCnt=dualChannelDefNodes.getLength())>0)
		{
			for (int i=0;i<dualChannelDefNodeCnt;i++)
			{
				Node dualChannelDefNode = dualChannelDefNodes.item(i);

				QName algorithm=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE;
				QName transform=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE;

				NamedNodeMap dualChannelDefAttr = dualChannelDefNode.getAttributes();
				int attrCnt=0;
				if (dualChannelDefAttr!=null && (attrCnt=dualChannelDefAttr.getLength())>0)
				{
					for (int a=0;a<attrCnt && (algorithm==null || transform==null);a++)
					{
						Attr attrNode=(Attr) dualChannelDefAttr.item(a);		
						String tempValue=attrNode.getValue();
						if (SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_ALGORITHM.equals(attrNode.getLocalName()))
						{
							algorithm=parsePrefixValue(xPathNameCtxt, tempValue);
						}else if (SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_TRANSFORM.equals(attrNode.getLocalName())){
							transform=parsePrefixValue(xPathNameCtxt, tempValue);
						}
					}
				}

				ArrayList<QName> selectors=getSelectors(dualChannelDefNode, xPathNameCtxt, siPrefix);

				BICEPSDualChannelDefinition dualChannelDefinition=new BICEPSDualChannelDefinition(algorithm,transform);

				if (selectors!=null && selectors.size()>0) dualChannelDefinition.getDualChannelElementSelectors().addAll(selectors);

				safetyInformationPolicy.getDualChannelDefinitions().add(dualChannelDefinition);
			}
		}
	}


	private final ArrayList<QName> getSelectors(Node sReqNode,
			XPathNamespaceContext xPathNameCtxt, String siPrefix)
					throws XPathExpressionException {
		ArrayList<QName> selectors=null;
		XPathInfo dualChannelSelectorXPath=new XPathInfo("./"+siPrefix+":"+SafetyInformationConstants.SI_ELEM_DEFINITION_SELECTOR, xPathNameCtxt);
		NodeList selectorNodes=(NodeList) dualChannelSelectorXPath.evaluate(sReqNode,XPathConstants.NODESET );
		if (selectorNodes!=null && selectorNodes.getLength()>0)
		{
			selectors=new ArrayList<QName>(selectorNodes.getLength());
			int nodeCnt=selectorNodes.getLength();
			for(int selectorNodeCnt=0;selectorNodeCnt<nodeCnt;selectorNodeCnt++)
			{
				Node selectorNode=selectorNodes.item(selectorNodeCnt);
				QName selector=parsePrefixValue(xPathNameCtxt,selectorNode.getTextContent());
				if (selector!=null) selectors.add(selector);
			}
		}
		return selectors;
	}




	private XPathInfo getOperationFilterMDIB( String bicepsPrefix,
			String siPrefix, XPathNamespaceContext xPathNameCtxt) 
	{
		XPathInfo xPathFilter=null;
		//if (safetyInformationPolicyAttr!=null && xPathNameCtxt!=null  && safetyInformationPolicyAttr.getMessageFilter()!=null)
		{


			if (bicepsPrefix!=null)
			{
				//CMDM:SCO/CMDM:Operation[CMDM:Handle='a' or CMDM:Handle='b']
				xPathFilter=new XPathInfo("//"+bicepsPrefix+":SCO/"+bicepsPrefix+":Operation["+siPrefix+":"+SafetyInformationConstants.SI_ELEM_DEFINITION_SAFETY_REQ+"]", xPathNameCtxt);
			}
		}
		return xPathFilter;
	}


	//TODO SSch Extract SIHelper, used also in GetMDIB in Device
	private static final DocumentBuilderFactory docBuilderFactory;
	static{
		docBuilderFactory = DocumentBuilderFactory.newInstance();
		docBuilderFactory.setNamespaceAware(true);
	}
	private DocumentBuilder builder;
	private synchronized DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
	{
		if (this.builder==null){
			if (Log.isDebug())
				Log.debug("Create new builder in Thread: "+Thread.currentThread());

			this.builder = docBuilderFactory.newDocumentBuilder();
		}

		return this.builder;
	}

	//SSch Extract to helper, needed also in JAXBUtilInstance
	private synchronized static void convertPVToString(IParameterValue pv, OutputStream os) throws IOException
	{
		DefaultBasicTypes2SOAPConverter btc = new DefaultBasicTypes2SOAPConverter();
		DefaultParameterValue2SOAPSerializer converter = new DefaultParameterValue2SOAPSerializer(btc);
		converter.serializeParameterValue(pv, os, null);
	}

	private final static int XML_PREFIX_LENGTH = 0;//38;
	private static class MyByteArrayOutputStream extends ByteArrayOutputStream {
		final private MyByteArrayInputStream is=new MyByteArrayInputStream(new byte[1]); 

		MyByteArrayOutputStream()
		{
			super(100000);
		}

		public byte[] getInternalBytes(){
			return buf;
		}

		public int getCount()
		{
			return count;
		}

		public ByteArrayInputStream getByteArrayInputStream()
		{
			is.setBytes(getInternalBytes(), getCount());
			return is;
		}

		@Override
		public synchronized String toString() {
			try {
				return new String(buf,XML_PREFIX_LENGTH,count-XML_PREFIX_LENGTH,XMLConstants.ENCODING);
			} catch (UnsupportedEncodingException e) {
				Log.warn(e);

			}
			return super.toString();
		}
	}

	private static class MyByteArrayInputStream extends ByteArrayInputStream{
		MyByteArrayInputStream(byte[] buf) {
			super(buf);
		}

		public void setBytes(byte[] bytes, int count)
		{
			this.buf=bytes;
			this.count=count;
			this.mark = 0;
			this.pos = 0;
		}
	}

	//TODO SSch extract to Util????
	protected QName parsePrefixValue(XPathNamespaceContext declaredNameSpaces, String prefixedString) {
		QName qNameValue=null;
		if (prefixedString!=null)
		{
			StringTokenizer tokenizer=new StringTokenizer(prefixedString,":");
			if (tokenizer.countTokens()==2)
			{
				String prefix=tokenizer.nextToken();
				String namespace=findNameSpaceForPrefix(declaredNameSpaces,prefix);
				if (namespace!=null)
				{
					qNameValue=QNameFactory.getInstance().getQName(tokenizer.nextToken(), namespace);
				}else{
					qNameValue=QNameFactory.getInstance().getQName(prefixedString);	
				}

			}else{
				qNameValue=QNameFactory.getInstance().getQName(prefixedString);
			}
		}

		return qNameValue;
	}

	private String findNameSpaceForPrefix(XPathNamespaceContext declaredNameSpaces,String prefix) {
		if (declaredNameSpaces!=null)
		{
			return declaredNameSpaces.getNamespaceURI(prefix);
		}
		return null;
	}
}
