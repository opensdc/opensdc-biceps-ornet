/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import java.util.List;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;

interface BICEPSLifecycleListener {
	public abstract void removed();
	public abstract void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps);
	public abstract void requestStateChanged(long transactionId, long sequenceNumber, InvocationState newState, InvocationError errorCode, String errorMsg);
	public abstract void subscriptionEnded(SubscriptionEndCodeType reason);	
}
