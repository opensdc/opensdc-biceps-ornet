/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.Descriptor;

public interface BICEPSProxy extends BICEPSLifecycleListener{
    public abstract boolean isValid();
    public abstract ProxyUniqueID getProxyUniqueID();
    
    public abstract BICEPSMedicalDeviceSystem getParentMedicalDeviceSystem();
    public abstract EndpointReference getEndpointReference();
    public abstract String getHandleOnEndpointReference();
    
    
    
    public abstract boolean isSubscribed(BICEPSClientCallback callback);
    //public abstract void setReportProvider(ReportProvider provider);
    
    public abstract Descriptor getDescriptor();
    public abstract boolean isStateAvailable();
    public abstract boolean isStateValid();
    public abstract BICEPSStateProxy getStateProxy();
}
