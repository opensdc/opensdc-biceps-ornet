/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.mdib.MDIBStructure;

public interface BICEPSProxyManager {	
	public abstract BICEPSProxyQueryInterface getProxyQueryInterface();
	public abstract <T extends BICEPSProxy> T getProxyFromCache(Descriptor descriptor, ProxyUniqueID proxyUniqueId, MDIBStructure mdib, ProxyCommunication communicationInterface);
	public abstract void handleInvalidProxy(BICEPSProxy proxy); 
	public abstract <T extends BICEPSProxy> T removeProxy(T proxyToRemove);
	public abstract void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkDeviceEndpoint);
}
