/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.CodedValue;

public interface BICEPSProxyQueryInterface {

	public abstract BICEPSProxy getProxy(ProxyUniqueID proxyUniqueId);
	
	public abstract BICEPSMedicalDeviceSystem getMedicalDeviceSystem(ProxyUniqueID proxyUniqueId);
	
	public abstract List<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems();
	
	public abstract BICEPSMedicalDeviceSystem getMedicalDeviceSystems(CodedValue code);
	
	public abstract BICEPSMetric getMetric(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSMetric> getMetrics();

	public abstract List<BICEPSMetric> getMetrics(CodedValue code);

	public abstract BICEPSAlertSignal getAlertSignal(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSAlertSignal> getAlertSignals();

	public abstract BICEPSAlertCondition getAlertCondition(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSAlertCondition> getAlertConditions();

	public abstract List<BICEPSAlertCondition> getAlertConditions(CodedValue code);

	public abstract BICEPSAlertSystem getAlertSystem(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSAlertSystem> getAlertSystems();

	public abstract List<BICEPSMDSContext> getMDSContexts();

	public abstract BICEPSMDSContext getMDSContext(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSManeuver> getManeuvers();

	public abstract List<BICEPSManeuver> getManeuvers(CodedValue code);

	public abstract BICEPSManeuver getManeuvers(ProxyUniqueID proxyUniqueId);

	public abstract List<BICEPSStream> getStreams();
	
	public abstract List<BICEPSStream> getStreams(CodedValue code);
	
	public abstract BICEPSStream getStream(ProxyUniqueID proxyUniqueId);

}
