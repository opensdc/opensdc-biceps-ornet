/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;

public interface BICEPSProxyStore extends BICEPSProxyQueryInterface{
	
	/* NEW */
	public abstract boolean containsProxy(ProxyUniqueID proxyUniqueId);
	
	public abstract void addProxy(BICEPSProxy proxyToAdd);
	
	public abstract <T extends BICEPSProxy> T removeProxy(T proxyToRemove);
	
	public abstract void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkDeviceEndpoint);
	
//	// Current 
//	
//	public abstract void addMedicalDeviceSystem(BICEPSMedicalDeviceSystem mds);
//
//	public abstract void removeMedicalDeviceSystem(BICEPSMedicalDeviceSystem mds);
//
//	//public abstract void removeMedicalDeviceSystem(MDSDescriptor metric);
//
//	public abstract boolean containsMedicalDeviceSystem(String proxyUniqueID);
//	
//	public abstract void addMetric(BICEPSMetric metric);
//
//	public abstract void removeMetric(BICEPSMetric metric);
//
//	//public abstract void removeMetric(MetricDescriptor metric);
//
//	public abstract boolean containsMetric(String proxyUniqueID);
//
//	public abstract void addAlertSignal(BICEPSAlertSignal alert);
//
//	public abstract void removeAlertSignal(BICEPSAlertSignal alert);
//
//	//public abstract void removeAlertSignal(AlertSignalDescriptor alert);
//
//	public abstract boolean containsAlertSignal(String proxyUniqueID);
//
//	public abstract void addAlertCondition(BICEPSAlertCondition alert);
//
//	public abstract void removeAlertCondition(BICEPSAlertCondition alert);
//
//	//public abstract void removeAlertCondition(AlertConditionDescriptor alert);
//
//	public abstract boolean containsAlertCondition(String proxyUniqueID);
//	
//	public abstract void addAlertSystem(BICEPSAlertSystem alert);
//
//	public abstract void removeAlertSystem(BICEPSAlertSystem alert);
//
//	//public abstract void removeAlertSystem(AlertSystemDescriptor alert);
//
//	public abstract boolean containsAlertSystem(String proxyUniqueID);
//
//	public abstract void addContext(BICEPSMDSContext context);
//
//	public abstract void removeContext(BICEPSMDSContext context);
//
//	//public abstract void removePatient(PatientAssociationDescriptor patient);
//
//	public abstract boolean containsContext(String proxyUniqueID);
//
//	public abstract void addManeuver(BICEPSManeuver maneuver);
//
//	public abstract void removeManeuver(BICEPSManeuver maneuver);
//
//	//public abstract void removeManeuver(ActivateOperationDescriptor operation);
//
//	public abstract boolean containsManeuver(String proxyUniqueID);
//
//	public abstract void addStream(BICEPSStream stream);
//
//	public abstract void removeStream(BICEPSStream stream);
//
//	//public abstract void removeStream(RealTimeSampleArrayMetricDescriptor streamMetric);
//
//	public abstract boolean containsStream(String proxyUniqueID);
//		
//	//public abstract void removeChannel(ChannelDescriptor channel);
//	//public abstract void removeVMD(VMDDescriptor vmd);
//	public abstract void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkDeviceEndpoint);

}
