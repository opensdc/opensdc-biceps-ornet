/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import java.util.HashMap;
import java.util.Set;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.OperationDescriptor;

public interface ProxyCommunication {
	public abstract ReportProvider getReportProvider();
	public abstract boolean hasInvokeInformation();
	public abstract <T extends OperationDescriptor> Set<T> getOperationDescriptors();
	public abstract AbstractSetResponse invokeOperation(OperationDescriptor op, AbstractSet params, BICEPSClientTransmissionInformationContainer transmissionInformation);
	public abstract HashMap<String, BICEPSSafetyInformationPolicyElement> getSafetyInformation();
	public abstract CommunicationAdapter getCommunicationAdapter();
}
