/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy;

import org.ws4d.java.eventing.ClientSubscription;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.URI;


public interface ReportProvider
{
	public void subscribeReport(BICEPSProxy proxy);

    public void unsubscribeReport(BICEPSProxy proxy);

    public boolean isReportSubscribed(BICEPSProxy proxy);

	public void stop();

	//TODO SSch refactor extract from this interface
	public void eventReceived(ClientSubscription subscription, URI actionURI, IParameterValue IParameterValue);

	public void subscriptionEndReceived(ClientSubscription subscription, URI reason);

	public void notifyClientSubscriptionInvalid(ClientSubscription subscription);
}
