/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.callbacks;

import java.util.List;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertConditionControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;

public interface AlertConditionListener extends BICEPSClientCallback
{
    public void changedAlertCondition(BICEPSAlertCondition source, List<ChangedProperty> changedProps);

    public void invokationStateChanged(BICEPSAlertConditionControl source, int transactionId, InvocationState newState, InvocationError errorCode, String errorMsg);

    public void removedAlertCondition(BICEPSAlertCondition source);

    public void subscriptionEnded(BICEPSAlertCondition source, SubscriptionEndCodeType reason);
}
