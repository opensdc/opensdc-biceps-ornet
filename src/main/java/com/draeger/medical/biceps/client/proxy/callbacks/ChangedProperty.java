/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.callbacks;

public enum ChangedProperty
{
		 OBJECT_STATE, OPERATION_STATE
		
//    //Top
//    General_Code,
//    //VMO
//    General_Type, General_Handle, General_LabelString,
//
//    //Metric
//    Metric_UnitCode, Metric_State,
//    //MetricSpec
//    MetricSpec_UpdatePeriod, MetricSpec_Category, MetricSpec_Availability, MetricSpec_Structure, MetricSpec_Relevance,
//    //NumericMetric
//    Numeric_Value, Numeric_Range, Numeric_Resolution, Numeric_Timestamp,
//    //StringMetric
//    String_Value, String_Timestamp,
//    //RealTimeSampleArrayMetric
//    RealTime_Value, RealTime_SamplePeriod,
//    //ComplexMetric
//    Complex_Value,
//
//    //Alert
//    Alert_SupervisedVMO,
//    //AlertCondition
//    AlertCondition_SupervisedVMO, AlertCondition_SupervisedType, AlertCondition_State, AlertCondition_Flags, AlertCondition_Code, AlertCondition_Type, AlertCondition_Priority,
//    //InfoType
//    AlertInfo_Code, AlertInfo_Info,
//    //LimitSpecification
//    AlertLimit_SupervisedVMO, AlertLimit_SupervisedType, AlertLimit_LimitAlertState, AlertLimit_LimitAlertValue,
//
//    //PatientContextState
//    Patient_Handle, Patient_State, Patient_PatientId, Patient_Firstname, Patient_Middlename, Patient_Lastname, Patient_Gender, Patient_Type, Patient_Birthday, Patient_Age, Patient_Height, Patient_Weight, Patient_Location
}
