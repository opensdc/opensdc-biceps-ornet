package com.draeger.medical.biceps.client.proxy.callbacks;

import com.draeger.medical.biceps.client.proxy.state.BICEPSEnsembleState;

public interface EnsembleListener extends MDSContextElementListener<BICEPSEnsembleState> {
	//void
}
