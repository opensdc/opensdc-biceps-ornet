/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.callbacks;

import java.util.List;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;

public interface  MDSContextElementListener<T extends BICEPSMDSContextElementState<?,?>> extends BICEPSClientCallback{    
	public void changedContext(T source, List<ChangedProperty> changedProps);
	public void changedContextAssociation(T source, List<ChangedProperty> changedProps);
	public void subscriptionEnded(T source, SubscriptionEndCodeType reason);
	public void removedContext(T source);
	public void changeRequestStateChanged(T source, long transactionId, InvocationState newState, InvocationError errorCode, String errorMsg);
}
