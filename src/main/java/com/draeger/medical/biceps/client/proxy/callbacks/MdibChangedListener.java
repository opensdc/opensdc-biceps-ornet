package com.draeger.medical.biceps.client.proxy.callbacks;

import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.CodedValue;

public interface MdibChangedListener extends MdibListener{
    public void newMetricAvailable(ProxyUniqueID proxyUniqueId, CodedValue code);

    public void newStreamAvailable(ProxyUniqueID proxyUniqueId, CodedValue code);
    
    public void newManeuverAvailable(ProxyUniqueID proxyUniqueId, CodedValue code);
    
    public void newAlertAvailable(ProxyUniqueID proxyUniqueId, CodedValue code);

    public void newAlertSystemAvailable(ProxyUniqueID proxyUniqueId);
    
    public void newAlertSignalAvailable(ProxyUniqueID proxyUniqueId);
}
