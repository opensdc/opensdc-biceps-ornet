/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.callbacks;

import java.util.List;

import org.ws4d.java.client.SearchParameter;

import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;


public interface MdibMDSListener extends MdibListener
{
	public void noDevicesFound(SearchParameter search); 
	
    public  void newDeviceAvailable(List<? extends BICEPSMedicalDeviceSystem> deviceMdsList);

    public  void deviceRemoved(List<? extends BICEPSMedicalDeviceSystem> deviceMdsList);

}
