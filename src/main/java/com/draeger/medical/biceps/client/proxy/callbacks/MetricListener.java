/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.callbacks;

import java.util.List;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;

public interface MetricListener extends BICEPSClientCallback
{
    public void changedMetric(BICEPSMetric source, List<? extends ChangedProperty> changedProps);

    public void changeRequestStateChanged(BICEPSMetricControl source, long transactionId,
            InvocationState newState, InvocationError errorCode, String errorMsg);

    public void removedMetric(BICEPSMetric source);

    public void subscriptionEnded(BICEPSMetric source, SubscriptionEndCodeType reason);
}
