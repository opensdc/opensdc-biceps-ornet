package com.draeger.medical.biceps.client.proxy.callbacks;

import com.draeger.medical.biceps.client.proxy.state.BICEPSWorkflowState;

public interface WorkflowListener extends MDSContextElementListener<BICEPSWorkflowState> {
	// void
}
