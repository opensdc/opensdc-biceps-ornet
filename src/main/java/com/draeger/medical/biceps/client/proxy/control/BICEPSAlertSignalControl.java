/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import com.draeger.medical.biceps.client.proxy.BICEPSControlProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSignalState;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertSignal;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.SetAlertStateResponse;

public interface BICEPSAlertSignalControl extends BICEPSControlProxy{
	@Override
	public abstract BICEPSAlertSignalState getStateProxy();
	@Override
	public abstract BICEPSAlertSignalControl getControlProxy();
	public abstract OperationState getOperationState();
	public abstract CurrentAlertSignal getState();
	@Override
	public abstract AlertSignalDescriptor getDescriptor();
	public abstract boolean isSetAlertSignalStateSupported();
	public abstract SetAlertStateResponse setAlertSignalState(CurrentAlertSignal alertSignalState, BICEPSClientTransmissionInformationContainer transmissionInformation);
}
