/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import java.util.HashMap;

import org.ws4d.java.types.QName;

public class BICEPSClientDualChannel {
	private final HashMap<QName,BICEPSClientDualChannelValuePair> dualChannelValues=new HashMap<QName, BICEPSClientDualChannelValuePair>();

	public HashMap<QName, BICEPSClientDualChannelValuePair> getDualChannelValues() {
		return dualChannelValues;
	} 

}
