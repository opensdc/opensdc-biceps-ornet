/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import org.ws4d.java.types.QName;

public class BICEPSClientDualChannelValuePair {
	private final float firstChannel;
	private final float secondChannel;
	private final QName valueElementSelector;
	
	public BICEPSClientDualChannelValuePair(QName valueElementSelector, float firstChannel,float secondChannel)
	{
		this.valueElementSelector=valueElementSelector;
		this.firstChannel=firstChannel;
		this.secondChannel=secondChannel;
	}

	public float getFirstChannel() {
		return firstChannel;
	}

	public float getSecondChannel() {
		return secondChannel;
	}

	public QName getValueElementSelector() {
		return valueElementSelector;
	}
}
