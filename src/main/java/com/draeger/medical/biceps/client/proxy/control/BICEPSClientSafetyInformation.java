/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

public class BICEPSClientSafetyInformation implements BICEPSClientTransmissionInfoItem{
	private final BICEPSClientSafetyContext sCtxt;
	private final BICEPSClientDualChannel dualChannel;

	public BICEPSClientSafetyInformation(BICEPSClientSafetyContext sCtxt, BICEPSClientDualChannel dualChannel) {
		this.sCtxt=sCtxt;
		this.dualChannel=dualChannel;
	}


	public BICEPSClientSafetyContext getSafetyContext() {
		return sCtxt;
	}

	public BICEPSClientDualChannel getDualChannel() {
		return dualChannel;
	}
	
	

}
