/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import java.util.HashSet;
import java.util.Iterator;

public class BICEPSClientTransmissionInformationContainer {
	
	private final HashSet<BICEPSClientTransmissionInfoItem> elements=new HashSet<BICEPSClientTransmissionInfoItem>();
	
	public void add(BICEPSClientTransmissionInfoItem transmissionInfo) 
	{
		elements.add(transmissionInfo);
	}

	public void clear() {
		elements.clear();
	}

	public boolean isEmpty() {
		return elements.isEmpty();
	}

	public boolean remove(Object arg0) {
		return elements.remove(arg0);
	}

	public int size() {
		return elements.size();
	}

	public Iterator<BICEPSClientTransmissionInfoItem> iterator() {
		return elements.iterator();
	}
	
}
