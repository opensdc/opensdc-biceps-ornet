package com.draeger.medical.biceps.client.proxy.control;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.EnsembleListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSEnsembleState;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.State;

public interface BICEPSEnsembleControl extends BICEPSMDSContextElementControl<BICEPSEnsembleState,EnsembleListener> {
	@Override
	public BICEPSEnsembleState getStateProxy();
	@Override
	public BICEPSEnsembleControl getControlProxy();
	
    @Override
	public abstract void subscribe(EnsembleListener callback);
    @Override
	public abstract void unsubscribe(EnsembleListener callback);

	@Override
	public abstract void changed(State s, long sequenceNumber, List<ChangedProperty> changedProps);
		
	@Override
	public abstract EnsembleContextDescriptor getDescriptor();

}
