/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSControlProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.ManeuverListener;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.ActivateResponse;
import com.draeger.medical.biceps.common.model.ArgumentType;
import com.draeger.medical.biceps.common.model.OperationState;

public interface BICEPSManeuver extends BICEPSControlProxy
{
    public abstract void subscribe(ManeuverListener callback);
    public abstract void unsubscribe(ManeuverListener callback);
	public abstract OperationState getState();
	@Override
	public abstract ActivateOperationDescriptor getDescriptor();
	@Override
	public abstract BICEPSManeuver getControlProxy();
	@Override
	public abstract BICEPSManeuver getStateProxy();
	public abstract void changed(OperationState s, List<ChangedProperty> changedProps);

    public ActivateResponse activate(List<ArgumentType> args, BICEPSClientTransmissionInformationContainer transmissionInformation);
}
