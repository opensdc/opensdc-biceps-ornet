/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.SetValueResponse;

public interface BICEPSNumericMetricControl extends BICEPSMetricControl{
	@Override
	public BICEPSNumericMetricState getStateProxy();
	@Override
	public BICEPSNumericMetricControl getControlProxy();
	
	@Override
	public abstract NumericMetricDescriptor getDescriptor();
	public abstract boolean isSetNumericSupported();
	
	//public abstract SetValueResponse setNumeric(float value, CodedValue unit);
	public abstract SetValueResponse setNumeric(float value, BICEPSClientTransmissionInformationContainer transmissionInformation);

}
