/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.control;

import com.draeger.medical.biceps.client.proxy.state.BICEPSMetricState;
import com.draeger.medical.biceps.common.model.SetStringResponse;

public interface BICEPSStringMetricControl extends BICEPSMetricControl{
	@Override
	public BICEPSMetricState getStateProxy();
	@Override
	public BICEPSStringMetricControl getControlProxy();
	
	public abstract boolean isSetStringSupported();
	public abstract SetStringResponse setString(String value);
	public abstract SetStringResponse setString(String value, BICEPSClientTransmissionInformationContainer transmissionInformation);
}
