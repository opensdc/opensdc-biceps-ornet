package com.draeger.medical.biceps.client.proxy.control;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.WorkflowListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSWorkflowState;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextState;

public interface BICEPSWorkflowControl extends BICEPSMDSContextElementControl<BICEPSWorkflowState,WorkflowListener> {
	@Override
	public BICEPSWorkflowState getStateProxy();
	@Override
	public BICEPSWorkflowControl getControlProxy();
	
    @Override
	public abstract void subscribe(WorkflowListener callback);
    @Override
	public abstract void unsubscribe(WorkflowListener callback);

	@Override
	public abstract void changed(State s, long sequenceNumber, List<ChangedProperty> changedProps);
		
	@Override
	public abstract WorkflowContextDescriptor getDescriptor();

}
