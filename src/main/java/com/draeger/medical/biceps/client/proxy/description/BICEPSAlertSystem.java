/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.description;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSystemState;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;


public interface BICEPSAlertSystem extends BICEPSProxy{
    @Override
	public abstract AlertSystemDescriptor getDescriptor();
    @Override
	public abstract BICEPSAlertSystemState getStateProxy();
}
