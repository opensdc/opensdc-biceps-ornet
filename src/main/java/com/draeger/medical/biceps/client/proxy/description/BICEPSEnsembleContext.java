package com.draeger.medical.biceps.client.proxy.description;

import com.draeger.medical.biceps.client.proxy.callbacks.EnsembleListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSEnsembleState;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;

public interface BICEPSEnsembleContext extends BICEPSMDSContextElement<BICEPSEnsembleState,EnsembleListener> {
	@Override
	public abstract BICEPSEnsembleState getStateProxy();
    @Override
	public abstract EnsembleContextDescriptor getDescriptor();
}
