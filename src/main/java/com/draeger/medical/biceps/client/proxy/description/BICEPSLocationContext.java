/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.description;

import com.draeger.medical.biceps.client.proxy.callbacks.LocationListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSLocationState;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;

public interface BICEPSLocationContext extends BICEPSMDSContextElement<BICEPSLocationState,LocationListener>
{
	@Override
	public abstract BICEPSLocationState getStateProxy();
    @Override
	public abstract LocationContextDescriptor getDescriptor();
}
