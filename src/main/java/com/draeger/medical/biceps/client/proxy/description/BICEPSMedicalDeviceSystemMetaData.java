package com.draeger.medical.biceps.client.proxy.description;

public interface BICEPSMedicalDeviceSystemMetaData {
	public abstract String getManufacturer(String lang);
	public abstract String getModelName(String lang);
	public abstract String getModelNumber();
	public abstract String getFirmwareVersion();
	public abstract String getSerialNumber();
	public abstract String getUniqueDeviceIdentifier();
	public abstract String getDefaultLanguage();	
}
