/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.description;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.StreamListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStreamState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;


public interface BICEPSStream extends BICEPSProxy
{   
    public abstract void subscribe(StreamListener callback);
    public abstract void unsubscribe(StreamListener callback);
    @Override
	public abstract BICEPSStreamState getStateProxy();
    @Override
	public abstract RealTimeSampleArrayMetricDescriptor getDescriptor();
}
