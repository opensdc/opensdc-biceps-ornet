package com.draeger.medical.biceps.client.proxy.description;

import com.draeger.medical.biceps.client.proxy.callbacks.WorkflowListener;
import com.draeger.medical.biceps.client.proxy.state.BICEPSWorkflowState;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;

public interface BICEPSWorkflowContext extends BICEPSMDSContextElement<BICEPSWorkflowState,WorkflowListener> {
	@Override
	public abstract BICEPSWorkflowState getStateProxy();
    @Override
	public abstract WorkflowContextDescriptor getDescriptor();
}
