/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.State;

public abstract class  DefaultAbstractBICEPSProxy<T extends BICEPSClientCallback> implements BICEPSProxy {

	private final EndpointReference deviceEndpointReference;
	private final ProxyCommunication proxyCom;
	private final List<T> callbacks=new CopyOnWriteArrayList<T>();
	private final BICEPSMedicalDeviceSystem medicalDeviceSystem;
	private boolean isValid;
	
	protected DefaultAbstractBICEPSProxy(EndpointReference deviceEndpointReference, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS)
	{
		this.deviceEndpointReference=deviceEndpointReference;
		this.proxyCom=proxyCom;
		this.medicalDeviceSystem=parentMDS;
	}

	
	protected void setInitialValidity(Descriptor d)
	{
		setValid(d!=null && getEndpointReference()!=null && d.getHandle()!=null);
		if (!isValid()){
			Log.warn("Proxy of type "+this.getClass()+" with EPR "+this.getHandleOnEndpointReference()+" and descriptor handle "+(d!=null?d.getHandle():"")+ " is not valid during creation.");
		}
	}

	@Override
	public EndpointReference getEndpointReference() {
		return deviceEndpointReference;
	}
	
	@Override
	public boolean isValid() {
		return isValid;
	}
	
	protected void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
	protected ProxyCommunication getProxyCom() {
		return proxyCom;
	}

	public void subscribe(T callback) {
		if (!callbacks.contains(callback))
		{
			callbacks.add(callback);
		}
		if (getProxyCom()!=null){
			getProxyCom().getReportProvider().subscribeReport(this);
		}

	}

	public void unsubscribe(T callback) {
		if (callbacks.remove(callback) && callbacks.isEmpty())
		{
			if (getProxyCom()!=null){
				proxyCom.getReportProvider().unsubscribeReport(this);
			}
		}
	}


	protected  List<T> getListeners() {
		return callbacks;
	}

	@Override
	public boolean isSubscribed(BICEPSClientCallback callback)
	{
		boolean isSubscribed=callbacks.contains(callback);

		if (isSubscribed)
		{
			if (getProxyCom()!=null){
				isSubscribed=getProxyCom().getReportProvider().isReportSubscribed(this);
			}
		}
		return isSubscribed;
	}
	
	protected void handleProxyRemove()
	{
		callbacks.clear();	
		if (getProxyCom()!=null){
			getProxyCom().getReportProvider().unsubscribeReport(this);
		}
	}


	@Override
	public BICEPSMedicalDeviceSystem getParentMedicalDeviceSystem() {
		return this.medicalDeviceSystem;
	}


	@Override
	public void removed() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void changed(State newState, long sequenceNumber,
			List<ChangedProperty> changedProps) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean isStateAvailable() {
		return (getStateProxy()!=null);
	}
	
	
	
}
