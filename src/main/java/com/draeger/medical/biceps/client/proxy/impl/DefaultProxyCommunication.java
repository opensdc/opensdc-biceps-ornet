/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.ReportProvider;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.utils.InvokeHelper;
import com.draeger.medical.biceps.client.utils.ServiceReferenceProxy;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.OperationDescriptor;

public class DefaultProxyCommunication implements ProxyCommunication 
{

	private final ReportProvider reportProvider;
	//TODO SSch Refactor Hashmap maybe BICEPSOperationInvokeInformqation?
	private final HashMap<OperationDescriptor, ServiceReferenceProxy> serviceReferencesForOperations;
	private final HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation;
	private final CommunicationAdapter communicationAdapter;

	public DefaultProxyCommunication(ReportProvider rp, HashMap<OperationDescriptor, ServiceReferenceProxy> serviceReferencesForOperations, HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation, CommunicationAdapter communicationAdapter)
	{
		this.communicationAdapter=communicationAdapter;
		this.reportProvider=rp;
		this.serviceReferencesForOperations=serviceReferencesForOperations;
		this.safetyInformation=safetyInformation;
	}
	
	
	@Override
	public CommunicationAdapter getCommunicationAdapter() {
		return communicationAdapter;
	}

	@Override
	public HashMap<String, BICEPSSafetyInformationPolicyElement> getSafetyInformation() {
		return safetyInformation;
	}
	
	@Override
	public ReportProvider getReportProvider() {
		return reportProvider;
	}

	@Override
	public boolean hasInvokeInformation() {
		boolean hasInvokeInformation=false;

		if (this.serviceReferencesForOperations!=null)
			hasInvokeInformation=!this.serviceReferencesForOperations.isEmpty();

		return hasInvokeInformation;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<? extends OperationDescriptor> getOperationDescriptors() {
		Set<OperationDescriptor> retVal=new HashSet<OperationDescriptor>();
		
		if (this.serviceReferencesForOperations!=null)
			retVal= this.serviceReferencesForOperations.keySet();
		
		return retVal;
	}

	@Override
	public AbstractSetResponse invokeOperation(OperationDescriptor op,
			AbstractSet params, BICEPSClientTransmissionInformationContainer transmissionInformation) {
		AbstractSetResponse response=null;
		try
		{
			ServiceReferenceProxy servProxy = getServiceReferenceProxyForDescriptor(op);
			if (servProxy!=null && servProxy.getServiceReference()!=null){
				BICEPSSafetyInformationPolicyElement safetyInformationPolicyElement=null;
				if (getSafetyInformation()!=null)
				{
					safetyInformationPolicyElement= getSafetyInformation().get(op.getHandle());
				}
				response =  InvokeHelper.invoke(servProxy.getServiceReference(), params, transmissionInformation, safetyInformationPolicyElement);
			}
		}
		catch (CommunicationException e)
		{
			//TODO SSch maybe create a failure response object or throw an exception
			Log.warn(e);
		}
		catch (InvocationException e)
		{
			Log.warn(e);
		}
		return response;
	}

	private ServiceReferenceProxy getServiceReferenceProxyForDescriptor(
			OperationDescriptor op) {
		ServiceReferenceProxy retVal=null;
		if (this.serviceReferencesForOperations!=null)
		{
			retVal=this.serviceReferencesForOperations.get(op);
		}
		return retVal;
	}

}
