/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.BICEPSProxyManager;
import com.draeger.medical.biceps.client.proxy.BICEPSProxyQueryInterface;
import com.draeger.medical.biceps.client.proxy.BICEPSProxyStore;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertConditionState;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertSignalState;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSAlertSystemState;
import com.draeger.medical.biceps.client.proxy.impl.alarm.DefaultBICEPSLimitAlertConditionState;
import com.draeger.medical.biceps.client.proxy.impl.context.DefaultBICEPSEnsembleContext;
import com.draeger.medical.biceps.client.proxy.impl.context.DefaultBICEPSLocationContext;
import com.draeger.medical.biceps.client.proxy.impl.context.DefaultBICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.impl.context.DefaultBICEPSPatientContext;
import com.draeger.medical.biceps.client.proxy.impl.context.DefaultBICEPSWorkflowContext;
import com.draeger.medical.biceps.client.proxy.impl.maneuver.DefaultBICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.impl.mds.DefaultBICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSMetric;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSMetricState;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSNumericMetricState;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSStringMetricControl;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSStringMetricState;
import com.draeger.medical.biceps.client.proxy.impl.stream.DefaultBICEPSStream;
import com.draeger.medical.biceps.client.proxy.impl.stream.DefaultBICEPSStreamState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.LimitAlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.NonGenericOperationDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;
import com.draeger.medical.biceps.common.model.util.MetricDescriptorUtil;
import com.draeger.medical.biceps.mdib.MDIBStructure;

public class DefaultProxyFactory implements BICEPSProxyManager {

	private final BICEPSProxyStore proxyStore;

	public DefaultProxyFactory(BICEPSProxyStore store) 
	{
		this.proxyStore=store;
	}

	@Override
	public void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkDeviceEndpoint) {
		if (this.proxyStore!=null && networkDeviceEndpoint!=null)
		{
			this.proxyStore.removeAllProxiesForNetworkDeviceEndpoint(networkDeviceEndpoint);
		}
	}
	
	@Override
	public <T extends BICEPSProxy> T removeProxy(T proxyToRemove) {
		return proxyStore.removeProxy(proxyToRemove);
	}

	private  BICEPSMetric createClientMetric(
			MetricDescriptor m, EndpointReference devEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMetric retVal=null;
		if (m!=null)
		{			
			if (comInterface==null || !comInterface.hasInvokeInformation())
			{
				boolean stateAvailable = isMetricStateAvailable(m);

				if (stateAvailable)
				{
					retVal = createMetricState(m, devEndpointRef,comInterface, parentMDS);
				}else{
					retVal=new DefaultBICEPSMetric<MetricDescriptor>(m,devEndpointRef,comInterface, parentMDS);
				}
			}
			else
			{
				retVal = createMetricContol(m, devEndpointRef, comInterface, parentMDS);
			}
		}

		return retVal;
	}

	private BICEPSMetric createMetricState(MetricDescriptor m,
			EndpointReference devEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMetric retVal=null;
		if (m instanceof NumericMetricDescriptor){
			retVal=new DefaultBICEPSNumericMetricState((NumericMetricDescriptor) m, devEndpointRef, comInterface, parentMDS);
		}else if (m instanceof StringMetricDescriptor){
			retVal=new DefaultBICEPSStringMetricState((StringMetricDescriptor) m, devEndpointRef, comInterface, parentMDS);
		}else{
			retVal=new DefaultBICEPSMetricState<MetricDescriptor>(m, devEndpointRef, comInterface, parentMDS);
		}
		return retVal;
	}

	private BICEPSMetric createMetricContol(MetricDescriptor metric,
			EndpointReference devEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMetric retVal=null;
		if (metric instanceof NumericMetricDescriptor){
			retVal=new DefaultBICEPSNumericMetricControl((NumericMetricDescriptor) metric,devEndpointRef,comInterface, parentMDS);
		}else	if (metric instanceof StringMetricDescriptor){
			retVal=new DefaultBICEPSStringMetricControl((StringMetricDescriptor) metric,devEndpointRef,comInterface, parentMDS);
		}else{
			retVal= new DefaultBICEPSMetricControl<MetricDescriptor>(metric,devEndpointRef,comInterface, parentMDS);
		}
		return retVal;
	}

	private  BICEPSStream createClientStream(
			RealTimeSampleArrayMetricDescriptor m, EndpointReference devEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSStream retVal=null;
		if (m!=null)
		{			
			if (comInterface==null ||!comInterface.hasInvokeInformation())
			{
				boolean stateAvailable = isStreamStateAvailable(m);

				if (stateAvailable)
				{
					retVal = createClientStreamState(m, devEndpointRef,comInterface, parentMDS);
				}else{
					retVal=new DefaultBICEPSStream(m,devEndpointRef,comInterface, parentMDS);
				}
			}
			else
			{
				retVal = createClientStreamControl(m, devEndpointRef, comInterface, parentMDS);
			}
		}

		return retVal;
	}

	private BICEPSStream createClientStreamState(RealTimeSampleArrayMetricDescriptor m,
			EndpointReference devEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSStream retVal=null;
		if (m!=null)
		{
			retVal=new DefaultBICEPSStreamState(m, devEndpointRef, comInterface, parentMDS);
		}
		return retVal;
	}

	private BICEPSStream createClientStreamControl(RealTimeSampleArrayMetricDescriptor m,
			EndpointReference devEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSStream retVal=null;
		if (m!=null)
		{
			retVal= new DefaultBICEPSStreamState(m,devEndpointRef,comInterface, parentMDS);
		}
		return retVal;
	}

	private boolean isStreamStateAvailable(RealTimeSampleArrayMetricDescriptor m) {
		boolean stateAvailable=false;
		boolean streamAvailable=false;
		List<MetricRetrievability> retrieveOptions = MetricDescriptorUtil.getRetrievabilityOptions(m);
		for (MetricRetrievability metricRetrievability : retrieveOptions) {
			if (MetricRetrievability.STREAM.equals(metricRetrievability))
			{
				streamAvailable=true;
			}
		}

		stateAvailable=streamAvailable;
		return stateAvailable;
	}

	private boolean isMetricStateAvailable(MetricDescriptor m) {
		boolean stateAvailable=false;
		boolean getAvailable=false;
		boolean episodicAvailable=false;
		boolean periodicAvailable=false;
		List<MetricRetrievability> retrieveOptions = MetricDescriptorUtil.getRetrievabilityOptions(m);
		for (MetricRetrievability metricRetrievability : retrieveOptions) {
			if (MetricRetrievability.GET.equals(metricRetrievability))
			{
				getAvailable=true;
			}else if (MetricRetrievability.EPISODIC.equals(metricRetrievability))
			{
				episodicAvailable=true;
			}else if (MetricRetrievability.PERIODIC.equals(metricRetrievability))
			{
				periodicAvailable=true;
			}
		}

		stateAvailable=getAvailable||episodicAvailable||periodicAvailable;
		return stateAvailable;
	}

	private BICEPSMedicalDeviceSystem createClientMedicalDeviceSystem(
			MDSDescriptor mds, EndpointReference deviceEndpointReference, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMedicalDeviceSystem retVal = new DefaultBICEPSMedicalDeviceSystem((HydraMDSDescriptor)mds, deviceEndpointReference, proxyCom,parentMDS);
		return retVal;
	}

	private BICEPSAlertSignal createAlertSignal(AlertSignalDescriptor a,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSignal retVal=null;
		if (a!=null)
		{			
			if (proxyCom==null || !proxyCom.hasInvokeInformation())
			{
				boolean stateAvailable = isAlertStateAvailable(a);

				if (stateAvailable)
				{
					retVal = createAlertSignalState(a, deviceEndpointRef, proxyCom, parentMDS);
				}else{
					retVal=new DefaultBICEPSAlertSignal(a,deviceEndpointRef,proxyCom,parentMDS);
				}
			}
			else
			{
				retVal = createAlertSignalContol(a, deviceEndpointRef, proxyCom, parentMDS);
			}
		}

		return retVal;
	}

	private BICEPSAlertSignal createAlertSignalState(AlertSignalDescriptor a,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSignal retVal=null;
		if (a !=null){
			retVal=new DefaultBICEPSAlertSignalState(a, deviceEndpointRef, proxyCom,parentMDS);
		}
		return retVal;
	}

	private BICEPSAlertSignal createAlertSignalContol(AlertSignalDescriptor a,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSignal retVal=null;
		if (a !=null){
			retVal=new DefaultBICEPSAlertSignalState(a, deviceEndpointRef, proxyCom, parentMDS);
		}
		return retVal;
	}

	private boolean isAlertStateAvailable(Descriptor a) {
		return true;
	}

	private BICEPSAlertCondition createAlertCondition(
			AlertConditionDescriptor alertConditionDescriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem dmds) {
		BICEPSAlertCondition retVal=null;
		if (alertConditionDescriptor!=null)
		{			
			if (comInterface==null || !comInterface.hasInvokeInformation())
			{
				boolean stateAvailable = isAlertStateAvailable(alertConditionDescriptor);

				if (stateAvailable)
				{
					retVal = createAlertConditionState(alertConditionDescriptor, deviceEndpointRef, comInterface, dmds);
				}else{
					retVal=new DefaultBICEPSAlertCondition(alertConditionDescriptor,deviceEndpointRef,comInterface, dmds);
				}
			}
			else
			{
				retVal = createAlertConditionContol(alertConditionDescriptor, deviceEndpointRef, comInterface, dmds);
			}
		}

		return retVal;
	}

	private BICEPSAlertCondition createAlertConditionState(
			AlertConditionDescriptor alertConditionDescriptor,
			EndpointReference deviceEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem dmds) {
		BICEPSAlertCondition retVal=null;
		if (alertConditionDescriptor instanceof LimitAlertConditionDescriptor){
			retVal=new DefaultBICEPSLimitAlertConditionState((LimitAlertConditionDescriptor)alertConditionDescriptor, deviceEndpointRef, comInterface, dmds);
		}else if (alertConditionDescriptor!=null){
			retVal=new DefaultBICEPSAlertConditionState(alertConditionDescriptor, deviceEndpointRef, comInterface, dmds);
		}
		return retVal;
	}

	private BICEPSAlertCondition createAlertConditionContol(
			AlertConditionDescriptor alertConditionDescriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem dmds) {
		BICEPSAlertCondition retVal=null;
		if (alertConditionDescriptor instanceof LimitAlertConditionDescriptor)
		{
			retVal=new DefaultBICEPSLimitAlertConditionState((LimitAlertConditionDescriptor)alertConditionDescriptor, deviceEndpointRef, proxyCom, dmds);
		}else if (alertConditionDescriptor !=null){
			//TODO SSch replace with control
			retVal=new DefaultBICEPSAlertConditionState(alertConditionDescriptor, deviceEndpointRef, proxyCom, dmds);
		}
		return retVal;
	}

	private BICEPSAlertSystem createAlertSystem(
			AlertSystemDescriptor alertSystemDescriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSystem retVal=null;
		if (alertSystemDescriptor!=null)
		{			
			if (proxyCom==null || !proxyCom.hasInvokeInformation())
			{
				boolean stateAvailable = isAlertStateAvailable(alertSystemDescriptor);

				if (stateAvailable)
				{
					retVal = createAlertSystemState(alertSystemDescriptor, deviceEndpointRef, proxyCom,parentMDS);
				}else{
					retVal=new DefaultBICEPSAlertSystem(alertSystemDescriptor,deviceEndpointRef,proxyCom, parentMDS);
				}
			}
			else
			{
				retVal = createAlertConditionContol(alertSystemDescriptor, deviceEndpointRef, proxyCom, parentMDS);
			}
		}

		return retVal;
	}

	private BICEPSAlertSystem createAlertSystemState(
			AlertSystemDescriptor alertSystemDescriptor,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSystem retVal=null;
		if (alertSystemDescriptor!=null){
			retVal=new DefaultBICEPSAlertSystemState(alertSystemDescriptor, deviceEndpointRef, proxyCom, parentMDS);
		}
		return retVal;
	}

	private BICEPSAlertSystem createAlertConditionContol(
			AlertSystemDescriptor alertSystemDescriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSystem retVal=null;
		if (alertSystemDescriptor !=null){
			//TODO SSch replace with control
			retVal=new DefaultBICEPSAlertSystemState(alertSystemDescriptor, deviceEndpointRef, proxyCom, parentMDS);
		}
		return retVal;
	}

	//TODO SSch Unchecked casts
	@Override
	@SuppressWarnings("unchecked")
	public <T extends BICEPSProxy> T getProxyFromCache(Descriptor descriptor,ProxyUniqueID proxyUniqueID, MDIBStructure mdib, ProxyCommunication communicationInterface) {
		T t=null;
		BICEPSMedicalDeviceSystem parentMDS=null;

		if (descriptor!=null){
			MDSDescriptor parentMDSDescriptor=mdib.getParentVMSForDescriptorHandle(descriptor.getHandle());
			if (parentMDSDescriptor!=null){
				//				String parentProxyUniqueID = BICEPSProxyUtil.createProxyUniqueID(parentMDSDescriptor.getHandle(), BICEPSProxyUtil.createHandleOnEndpointReference(mdib.getDeviceEndpointRef()));
				ProxyUniqueID parentProxyUniqueID=ProxyUniqueID.create(parentMDSDescriptor.getHandle(),mdib.getDeviceEndpointRef());
				parentMDS = getMedicalDeviceSystemFromCache(parentMDSDescriptor,parentProxyUniqueID , mdib,communicationInterface,null);//proxyStore.getMedicalDeviceSystem(parentProxyUniqueID);
			}
		}

		if (descriptor instanceof RealTimeSampleArrayMetricDescriptor)
		{
			t = (T) getStreamProxyFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		}
		else if (descriptor instanceof MetricDescriptor)
		{
			t = (T) getMetricProxyFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		} else if (descriptor instanceof AlertConditionDescriptor)
		{
			t = (T) getAlertConditionFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		} else if (descriptor instanceof AlertSystemDescriptor)
		{
			t = (T) getAlertSystemFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		} else if (descriptor instanceof AlertSignalDescriptor)
		{
			t = (T) getAlertSignalFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		}else if (descriptor instanceof MDSDescriptor)
		{
			MDSDescriptor mdsD= (MDSDescriptor)descriptor;
			t = (T) getMedicalDeviceSystemFromCache(descriptor, proxyUniqueID,mdib,communicationInterface,null);
		}else if (descriptor instanceof SystemContext)
		{
			t = (T) getMDSContextFromCache((SystemContext)descriptor,proxyUniqueID, mdib,communicationInterface,parentMDS);
		}
		else if (descriptor instanceof AbstractContextDescriptor)
		{
			t = (T) getMDSContextElementFromCache(descriptor,proxyUniqueID, mdib,communicationInterface,parentMDS);
		}else if (descriptor instanceof ActivateOperationDescriptor)
		{
			t= (T) getManeuverFromCache(descriptor, proxyUniqueID, mdib,communicationInterface,parentMDS);
		}else if (descriptor instanceof NonGenericOperationDescriptor){
			//TODO create an NonBICEPSInvokeProxy
		}
		return t;
	}

	private BICEPSManeuver getManeuverFromCache(Descriptor descriptor, ProxyUniqueID proxyUniqueId,
			MDIBStructure mdib, ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS)  {
		BICEPSManeuver proxy=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			proxy= createManeuver(descriptor, mdib.getDeviceEndpointRef(), communicationInterface,parentMDS); 
			if (proxy!=null) proxyStore.addProxy(proxy);
		}
		else
		{
			proxy = proxyStore.getManeuvers(proxyUniqueId);
		}


		return proxy;
	}

	private BICEPSManeuver createManeuver(Descriptor descriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSManeuver retVal=null;
		if (descriptor instanceof ActivateOperationDescriptor)
		{
			retVal = new DefaultBICEPSManeuver((ActivateOperationDescriptor)descriptor,deviceEndpointRef,communicationInterface,parentMDS);	
		}

		return retVal;
	}

	private BICEPSMDSContext getMDSContextFromCache(SystemContext descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMDSContext proxy=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
				SystemContext systemContext=(SystemContext)descriptor;
				proxy= createMDSContext(systemContext, mdib.getDeviceEndpointRef(), communicationInterface,parentMDS); 

				if (proxy!=null)
				{
					proxyStore.addProxy(proxy);

					
					createContextElementState(mdib, communicationInterface,
							parentMDS, proxy, systemContext.getPatientContext());
					createContextElementState(mdib, communicationInterface,
							parentMDS, proxy, systemContext.getLocationContext());
					createContextElementState(mdib, communicationInterface,
							parentMDS, proxy, systemContext.getEnsembleContext());
					createContextElementState(mdib, communicationInterface,
							parentMDS, proxy, systemContext.getOperatorContext());
					createContextElementState(mdib, communicationInterface,
							parentMDS, proxy, systemContext.getWorkflowContext());
				}
		}
		else
		{
			proxy = proxyStore.getMDSContext(proxyUniqueId);
		}


		return proxy;
	}

	private void createContextElementState(MDIBStructure mdib,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS, BICEPSMDSContext proxy,
			AbstractContextDescriptor aContextDescriptor) {
		if (aContextDescriptor!=null)
		{
			ProxyUniqueID patientContextProxyUniqueId=ProxyUniqueID.create(aContextDescriptor.getHandle(),mdib.getDeviceEndpointRef());
			ProxyCommunication padComInterface = communicationInterface.getCommunicationAdapter().createProxyCommunication(mdib, aContextDescriptor, communicationInterface.getSafetyInformation());
			BICEPSMDSContextElementState<?,?> e=getMDSContextElementFromCache(aContextDescriptor, patientContextProxyUniqueId, mdib,padComInterface , parentMDS);
			if (e !=null) proxy.getMDSContextElements().add(e);
		}
	}

	private BICEPSMDSContextElementState<?,?> getMDSContextElementFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMDSContextElementState<?,?> proxy=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			proxy= createMDSContextElement(descriptor, mdib.getDeviceEndpointRef(), communicationInterface,parentMDS); //TODO 
			if (proxy!=null) proxyStore.addProxy(proxy);
		}
		else
		{

			BICEPSProxy tProxy = proxyStore.getProxy(proxyUniqueId);
			if (tProxy instanceof BICEPSMDSContextElementState )
				proxy = (BICEPSMDSContextElementState<?,?>) tProxy;
		}


		return proxy;
	}

	private BICEPSMDSContextElementState<?,?> createMDSContextElement(Descriptor descriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMDSContextElementState<?,?> retVal=null;
		if (descriptor instanceof PatientAssociationDescriptor)
		{
			retVal = new DefaultBICEPSPatientContext((PatientAssociationDescriptor)descriptor,deviceEndpointRef,communicationInterface,parentMDS);	
		}else if (descriptor instanceof LocationContextDescriptor)
		{
			retVal = new DefaultBICEPSLocationContext((LocationContextDescriptor)descriptor,deviceEndpointRef,communicationInterface,parentMDS);
		}else if (descriptor instanceof EnsembleContextDescriptor)
		{
			retVal = new DefaultBICEPSEnsembleContext((EnsembleContextDescriptor)descriptor,deviceEndpointRef,communicationInterface,parentMDS);
		} else if (descriptor instanceof WorkflowContextDescriptor) {
			retVal = new DefaultBICEPSWorkflowContext((WorkflowContextDescriptor)descriptor, deviceEndpointRef, communicationInterface, parentMDS);
		}
		

		return retVal;
	}

	private BICEPSMDSContext createMDSContext(SystemContext descriptor,
			EndpointReference deviceEndpointRef,
			ProxyCommunication communicationInterface,
			BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMDSContext retVal = new DefaultBICEPSMDSContext(descriptor,deviceEndpointRef,communicationInterface,parentMDS,null);	

		return retVal;
	}

	private BICEPSMedicalDeviceSystem getMedicalDeviceSystemFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSMedicalDeviceSystem cm=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			//			HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc = getNetworkInformation(mdib);
			cm= createClientMedicalDeviceSystem((MDSDescriptor)descriptor, mdib.getDeviceEndpointRef(), communicationInterface,parentMDS); //TODO 
			if (cm!=null)
				proxyStore.addProxy(cm);
		}
		else
		{
			cm = proxyStore.getMedicalDeviceSystem(proxyUniqueId);
		}


		return cm;
	}

	private BICEPSAlertSignal getAlertSignalFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem parentMDS) 
	{
		BICEPSAlertSignal cm=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			//			HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc = getNetworkInformation(mdib);
			cm= createAlertSignal((AlertSignalDescriptor)descriptor, mdib.getDeviceEndpointRef(), communicationInterface, parentMDS); //TODO 
			if (cm!=null)
				proxyStore.addProxy(cm);
		}
		else
		{
			cm = proxyStore.getAlertSignal(proxyUniqueId);
		}


		return cm;
	}

	private BICEPSAlertSystem getAlertSystemFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem parentMDS) {
		BICEPSAlertSystem cm=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			//			HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc = getNetworkInformation(mdib);
			cm= createAlertSystem((AlertSystemDescriptor)descriptor, mdib.getDeviceEndpointRef(), communicationInterface, parentMDS); //TODO 
			if (cm!=null)
				proxyStore.addProxy(cm);
		}
		else
		{
			cm = proxyStore.getAlertSystem(proxyUniqueId);
		}


		return cm;
	}

	private BICEPSMetric getMetricProxyFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem parentMDS) {

		BICEPSMetric cm=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			cm= createClientMetric((MetricDescriptor)descriptor, mdib.getDeviceEndpointRef(), communicationInterface, parentMDS); //TODO 
			if (cm!=null)
				proxyStore.addProxy(cm);
		}
		else
		{
			cm = proxyStore.getMetric(proxyUniqueId);
		}


		return cm;
	}

	private BICEPSStream getStreamProxyFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem parentMDS) {

		BICEPSStream cm=null;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			cm= createClientStream((RealTimeSampleArrayMetricDescriptor) descriptor, mdib.getDeviceEndpointRef(), communicationInterface,parentMDS); //TODO 
			if (cm!=null)
				proxyStore.addProxy(cm);
		}
		else
		{
			cm = proxyStore.getStream(proxyUniqueId);
		}


		return cm;
	}


	private BICEPSAlertCondition getAlertConditionFromCache(Descriptor descriptor,
			ProxyUniqueID proxyUniqueId, MDIBStructure mdib,
			ProxyCommunication communicationInterface, BICEPSMedicalDeviceSystem dmds)
	{
		BICEPSAlertCondition alertConditionProxy;
		if (!proxyStore.containsProxy(proxyUniqueId))
		{
			//			HashMap<OperationDescriptor, ServiceReferenceProxy> operationDescriptor2Serviceproxies = new HashMap<OperationDescriptor, ServiceReferenceProxy>();
			//			for (OperationDescriptor o : mdib.findOperations(descriptor))
			//			{
			//				operationDescriptor2Serviceproxies.put(o, getSetServiceReferenceProxy(mdib.getDeviceEndpointRef()));
			//			}
			alertConditionProxy=createAlertCondition((AlertConditionDescriptor)descriptor, mdib.getDeviceEndpointRef(), communicationInterface, dmds); //TODO

			if (alertConditionProxy!=null)
				proxyStore.addProxy(alertConditionProxy);
		}
		else
		{
			alertConditionProxy = proxyStore.getAlertCondition(proxyUniqueId);
		}
		return alertConditionProxy;
	}

	//	private BICEPSAlertSystem getAlertSystemFromCache(
	//			AlertSystemDescriptor alertConditionDescriptor, String proxyUniqueID,
	//			MDIBStructure mdib) {
	//		BICEPSAlertSystem alertSystemProxy;
	//		if (!proxyStore.containsAlertSystem(proxyUniqueID))
	//		{
	//			HashMap<OperationDescriptor, ServiceReferenceProxy> operationDescriptor2Serviceproxies = new HashMap<OperationDescriptor, ServiceReferenceProxy>();
	//			for (OperationDescriptor o : mdib.findOperations(alertConditionDescriptor))
	//			{
	//				operationDescriptor2Serviceproxies.put(o, getSetServiceReferenceProxy(mdib.getDeviceEndpointRef()));
	//			}
	//			alertSystemProxy=getProxyManager().createAlertSystem(alertConditionDescriptor, mdib.getDeviceEndpointRef(), this.reportProvider, operationDescriptor2Serviceproxies);
	//			if (alertSystemProxy!=null)
	//				proxyStore.addAlertSystem(alertSystemProxy);
	//		}
	//		else
	//		{
	//			alertSystemProxy = proxyStore.getAlertSystem(proxyUniqueID);
	//		}
	//		return alertSystemProxy;
	//	}


	@Override
	public BICEPSProxyQueryInterface getProxyQueryInterface() {
		return this.proxyStore;
	}

	@Override
	public void handleInvalidProxy(BICEPSProxy proxy) {
		// TODO SSch implement
		//proxyStore.removeMetric(cm);
		//proxyStore.removeAlertCondition(ca);
		//proxyStore.removeAlertSystem(ca);
		//proxyStore.removePatient(cp);
		//proxyStore.removeManeuver(cm);
		//proxyStore.removeStream(cs);

	}




	//	private HashMap<OperationDescriptor, ServiceReferenceProxy> getNetworkInformation(
	//			MdibRepresentation mdib) {
	//		HashMap<OperationDescriptor, ServiceReferenceProxy> ops2Svc = new HashMap<OperationDescriptor, ServiceReferenceProxy>();
	//		for (OperationDescriptor o : mdib.findOperations(m))
	//		{
	//			ops2Svc.put(o, getSetServiceReferenceProxy(mdib.getDeviceEndpointRef()));
	//		}
	//		return ops2Svc;
	//	}
}
