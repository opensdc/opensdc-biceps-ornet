/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.BICEPSProxyStore;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.util.ModelComparer;

public class DefaultProxyStore implements BICEPSProxyStore
{
	Map<EndpointReference,EndpointProxyStoreElement>   				proxyStoresPerEndpoint   			= new ConcurrentHashMap<EndpointReference,EndpointProxyStoreElement>();

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getMetric(java.lang.String)
	 */
	@Override
	public  BICEPSMetric getMetric(ProxyUniqueID proxyUniqueId)
	{
		return getProxyOfType(BICEPSMetric.class, proxyUniqueId);
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getMetrics()
	 */
	@Override
	public  List<BICEPSMetric> getMetrics()
	{
		return getAllProxiesOfAType(BICEPSMetric.class,null);

	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getMetrics(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public  List<BICEPSMetric>  getMetrics(CodedValue code)
	{
		List<BICEPSMetric> result = new ArrayList<BICEPSMetric>();
		List<BICEPSMetric> allProxiesOfAType = getAllProxiesOfAType(BICEPSMetric.class,null);
		if (allProxiesOfAType!=null)
		{
			for (BICEPSMetric proxy : allProxiesOfAType) {
				if (proxy.getDescriptor()!=null)
				{
					if (ModelComparer.equals(proxy.getDescriptor().getType(), code))
					{
						result.add(proxy);
					}
				}
			}
		}
		return result;
		//		List<BICEPSMetric> result = new ArrayList<BICEPSMetric>();
		//		for (BICEPSMetric cm : metrics.values())
		//		{
		//			if (cm.getDescriptor()!=null)
		//			{
		//				if (checkProxy(cm) && ModelComparer.equals(cm.getDescriptor().getType(), code))
		//				{
		//					result.add(cm);
		//				}
		//			}
		//		}
		//		return result;
	}
	//
	//
	//
	//	//	/* (non-Javadoc)
	//	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeMetric(com.draeger.medical.biceps.common.model.MetricDescriptor)
	//	//	 */
	//	//	private void removeMetric(MetricDescriptor metric)
	//	//	{
	//	//		//		for (BICEPSMetric c : metrics)
	//	//		//		{
	//	//
	//	//		//			//            if (c.metric == metric)
	//	//		//			//            {
	//	//		//			//                removeMetric(c);
	//	//		//			//                break;
	//	//		//			//            }
	//	//		//		}
	//	//	}
	//
	//
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#addAlertSignal(com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal)
	//	 */
	//	public void addAlertSignal(BICEPSAlertSignal alert)
	//	{
	//		if (alert!=null)
	//		{
	//			String clientUniqueID=alert.getProxyUniqueID();
	//			if (!alertSignals.containsKey(clientUniqueID))
	//			{
	//				alertSignals.put(clientUniqueID,alert);
	//			}
	//		}
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeAlertSignal(com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal)
	//	 */
	//	public void removeAlertSignal(BICEPSAlertSignal alert)
	//	{
	//		if (alert!=null)
	//		{
	//			if (alertSignals.remove(alert.getProxyUniqueID())!=null)
	//			{
	//				alert.removed();
	//			}
	//		}
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#containsAlertSignal(java.lang.String)
	//	 */
	//	public boolean containsAlertSignal(String proxyUniqueID)
	//	{
	//		return alertSignals.containsKey(proxyUniqueID);
	//	}
	//
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getAlertSignal(java.lang.String)
	 */
	@Override
	public BICEPSAlertSignal getAlertSignal(ProxyUniqueID proxyUniqueId)
	{
		return getProxyOfType(BICEPSAlertSignal.class, proxyUniqueId);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getAlertSignals()
	 */
	@Override
	public List<BICEPSAlertSignal> getAlertSignals()
	{
		return getAllProxiesOfAType(BICEPSAlertSignal.class,null);
	}
	//
	//
	//
	//	//	/* (non-Javadoc)
	//	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeAlertSignal(com.draeger.medical.biceps.common.model.AlertSignalDescriptor)
	//	//	 */
	//	//	private void removeAlertSignal(AlertSignalDescriptor alert)
	//	//	{
	//	//		if (alert!=null)
	//	//		{
	//	//			for (BICEPSAlertSignal c : alertSignals.values())
	//	//			{
	//	//				if (alert.equals(c.getDescriptor()))
	//	//				{
	//	//					removeAlertSignal(c);
	//	//					break;
	//	//				}
	//	//			}
	//	//		}
	//	//	}
	//
	//
	//
	//
	//
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#addAlertCondition(com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition)
	//	 */
	//	public void addAlertCondition(BICEPSAlertCondition alert)
	//	{
	//		if (alert!=null)
	//		{
	//			String clientUniqueID=alert.getProxyUniqueID();
	//			if (!alertConditions.containsKey(clientUniqueID))
	//			{
	//				alertConditions.put(clientUniqueID,alert);
	//			}
	//		}
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeAlertCondition(com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition)
	//	 */
	//	public void removeAlertCondition(BICEPSAlertCondition alert)
	//	{
	//		if (alert!=null)
	//		{
	//			if (alertConditions.remove(alert.getProxyUniqueID())!=null)
	//			{
	//				alert.removed();
	//			}
	//		}
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#containsAlertCondition(java.lang.String)
	//	 */
	//	public boolean containsAlertCondition(String proxyUniqueID)
	//	{
	//		return alertConditions.containsKey(proxyUniqueID);
	//	}
	//
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getAlertCondition(java.lang.String)
	 */
	@Override
	public BICEPSAlertCondition getAlertCondition(ProxyUniqueID proxyUniqueId)
	{		
		return getProxyOfType(BICEPSAlertCondition.class, proxyUniqueId);
	}

	@Override
	public BICEPSAlertSystem getAlertSystem(ProxyUniqueID proxyUniqueId)
	{
		return getProxyOfType(BICEPSAlertSystem.class, proxyUniqueId);
	}

	@Override
	public List<BICEPSAlertSystem> getAlertSystems()
	{
		return getAllProxiesOfAType(BICEPSAlertSystem.class,null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getAlertConditions()
	 */
	@Override
	public List<BICEPSAlertCondition> getAlertConditions()
	{
		return getAllProxiesOfAType(BICEPSAlertCondition.class,null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getAlertConditions(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<BICEPSAlertCondition> getAlertConditions(CodedValue code)
	{
		return getProxyOfClassWithCode(BICEPSAlertCondition.class, code);
	}
	//
	//
	//	public void addAlertSystem(BICEPSAlertSystem alert)
	//	{
	//		if (alert!=null)
	//		{
	//			String clientUniqueID=alert.getProxyUniqueID();
	//			if (!alertSystems.containsKey(clientUniqueID))
	//			{
	//				alertSystems.put(clientUniqueID,alert);
	//			}
	//		}
	//	}
	//
	//	public void removeAlertSystem(BICEPSAlertSystem alert)
	//	{
	//		if (alert!=null)
	//		{
	//			if (alertSystems.remove(alert.getProxyUniqueID())!=null)
	//			{
	//				alert.removed();
	//			}
	//		}
	//	}
	//
	//
	//	public boolean containsAlertSystem(String proxyUniqueID)
	//	{
	//		return alertSystems.containsKey(proxyUniqueID);
	//	}
	//
	//	//	/* (non-Javadoc)
	//	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeAlertCondition(com.draeger.medical.biceps.common.model.AlertConditionDescriptor)
	//	//	 */
	//	//	private void removeAlertCondition(AlertConditionDescriptor alert)
	//	//	{
	//	//		if (alert!=null)
	//	//		{
	//	//			for (BICEPSAlertCondition c : alertConditions.values())
	//	//			{
	//	//				if (alert.equals(c.getDescriptor()))
	//	//				{
	//	//					removeAlertCondition(c);
	//	//					break;
	//	//				}
	//	//			}
	//	//		}
	//	//	}
	//
	//	//	private void removeAlertSystem(AlertSystemDescriptor alert)
	//	//	{
	//	//		if (alert!=null)
	//	//		{
	//	//			for (BICEPSAlertSystem c : alertSystems.values())
	//	//			{
	//	//				if (alert.equals(c.getDescriptor()))
	//	//				{
	//	//					removeAlertSystem(c);
	//	//					break;
	//	//				}
	//	//			}
	//	//		}
	//	//	}
	//
	//
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#addPatient(com.draeger.medical.biceps.client.proxy.description.BICEPSPatient)
	//	 */
	//	public void addContext(BICEPSMDSContext context)
	//	{
	//		if (context!=null)
	//		{
	//			String clientUniqueID=context.getProxyUniqueID();
	//			if (clientUniqueID!=null && !contexts.containsKey(clientUniqueID))
	//			{
	//				contexts.put(clientUniqueID,context);
	//			}
	//		}
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removePatient(com.draeger.medical.biceps.client.proxy.description.BICEPSPatient)
	//	 */
	//	public void removeContext(BICEPSMDSContext context)
	//	{
	//		if (context!=null)
	//		{
	//			String clientUniqueID=context.getProxyUniqueID();
	//			if (clientUniqueID!=null && contexts.remove(clientUniqueID)!=null)
	//			{
	//				context.removed();
	//			}
	//		}
	//	}
	//
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getPatients()
	 */
	@Override
	public List<BICEPSMDSContext> getMDSContexts()
	{
		return getAllProxiesOfAType(BICEPSMDSContext.class,null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getPatient(java.lang.String)
	 */
	@Override
	public BICEPSMDSContext getMDSContext(ProxyUniqueID proxyUniqueId)
	{
		return getProxyOfType(BICEPSMDSContext.class, proxyUniqueId);
	}
	//
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#containsPatient(java.lang.String)
	//	 */
	//	public boolean containsContext(String proxyUniqueID)
	//	{
	//		return proxyUniqueID!=null?contexts.containsKey(proxyUniqueID):false;
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#addManeuver(com.draeger.medical.biceps.client.proxy.description.BICEPSManeuver)
	//	 */
	//	public void addManeuver(BICEPSManeuver maneuver)
	//	{
	//		if (maneuvers!=null)
	//		{
	//			String clientUniqueID=maneuver.getProxyUniqueID();
	//			if (!maneuvers.containsKey(clientUniqueID))
	//			{
	//				maneuvers.put(clientUniqueID,maneuver);
	//			}
	//		}
	//	}
	//
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getManeuvers()
	 */
	@Override
	public List<BICEPSManeuver> getManeuvers()
	{
		return getAllProxiesOfAType(BICEPSManeuver.class,null);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getManeuvers(com.draeger.medical.biceps.common.model.CodedValue)
	 */
	@Override
	public List<BICEPSManeuver> getManeuvers(CodedValue code)
	{
		return getProxyOfClassWithCode(BICEPSManeuver.class, code);
	}

	@Override
	public BICEPSManeuver getManeuvers(ProxyUniqueID proxyUniqueId) {
		return getProxyOfType(BICEPSManeuver.class,proxyUniqueId);
	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#removeManeuver(com.draeger.medical.biceps.client.proxy.description.BICEPSManeuver)
	//	 */
	//	public void removeManeuver(BICEPSManeuver maneuver)
	//	{
	//		if (maneuver!=null)
	//		{
	//			if (maneuvers.remove(maneuver.getProxyUniqueID())!=null)
	//			{
	//				maneuver.removed();
	//			}
	//		}
	//	}
	//
	//
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#containsManeuver(java.lang.String)
	//	 */
	//	public boolean containsManeuver(String proxyUniqueID)
	//	{
	//		return maneuvers.containsKey(proxyUniqueID);
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#addStream(com.draeger.medical.biceps.client.proxy.description.BICEPSStream)
	//	 */
	//	public void addStream(BICEPSStream stream)
	//	{
	//		if (stream!=null)
	//		{
	//			String clientUniqueID=stream.getProxyUniqueID();
	//			if (!streams.containsKey(clientUniqueID))
	//			{
	//				streams.put(clientUniqueID,stream);
	//			}
	//		}
	//	}
	//	
	//
	//	public void removeStream(BICEPSStream stream)
	//	{
	//		if (stream!=null)
	//		{
	//			if (streams.remove(stream.getProxyUniqueID())!=null)
	//			{
	//				stream.removed();
	//			}
	//		}
	//	}
	//
	//
	//	public boolean containsStream(String proxyUniqueID)
	//	{
	//		return streams.containsKey(proxyUniqueID);
	//	}
	//
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getStream(java.lang.String)
	 */
	@Override
	public BICEPSStream getStream(ProxyUniqueID proxyUniqueId) {
		return getProxyOfType(BICEPSStream.class, proxyUniqueId);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.impl.ProxyStore#getStreams()
	 */
	@Override
	public List<BICEPSStream> getStreams()
	{
		return getAllProxiesOfAType(BICEPSStream.class,null);
	}

	@Override
	public List<BICEPSStream> getStreams(CodedValue code) {
		return getProxyOfClassWithCode(BICEPSStream.class, code);
	}

	//
	//
	//
	//	public void removeChannel(ChannelDescriptor channel)
	//	{
	//		//TODO SSch Refactor
	//		//        for (Alert alert : channel.getAlerts())
	//		//        {
	//		//            removeAlert(alert);
	//		//        }
	//		//        for (Metric metric : channel.getMetrics())
	//		//        {
	//		//            removeMetric(metric);
	//		//        }
	//	}
	//
	//	public void removeVMD(VMDDescriptor vmd)
	//	{
	//		//TODO SSch Refactor
	//		//        for (Channel c : vmd.getChannels())
	//		//        {
	//		//            removeChannel(c);
	//		//        }
	//		//        for (Alert a : vmd.getAlerts())
	//		//        {
	//		//            removeAlert(a);
	//		//        }
	//	}
	//
	//	
	//
	//
	//
	//
	//
	//	public void addMedicalDeviceSystem(BICEPSMedicalDeviceSystem mds) 
	//	{
	//		if (mds!=null)
	//		{
	//			String proxyUniqueID = mds.getProxyUniqueID();
	//			if (!medicalDeviceSystems.containsKey(proxyUniqueID) && checkProxy(mds))
	//			{
	//				medicalDeviceSystems.put(proxyUniqueID,mds);
	//			}
	//		}
	//	}
	//
	//
	//	public void removeMedicalDeviceSystem(BICEPSMedicalDeviceSystem mds) {
	//		if (mds!=null)
	//		{
	//			BICEPSMedicalDeviceSystem removedMDS=medicalDeviceSystems.remove(mds.getProxyUniqueID());
	//			if (removedMDS!=null)
	//			{
	//				removedMDS.removed();
	//			}
	//		}
	//
	//	}
	//
	//
	//	public boolean containsMedicalDeviceSystem(String proxyUniqueID) {
	//		return medicalDeviceSystems.containsKey(proxyUniqueID);
	//	}
	//	
	@Override
	public BICEPSMedicalDeviceSystem getMedicalDeviceSystem(ProxyUniqueID proxyUniqueId) {
		return getProxyOfType(BICEPSMedicalDeviceSystem.class,proxyUniqueId);
	}


	@Override
	public List<BICEPSMedicalDeviceSystem> getMedicalDeviceSystems() {
		return getAllProxiesOfAType(BICEPSMedicalDeviceSystem.class,null);
	}


	@Override
	public BICEPSMedicalDeviceSystem getMedicalDeviceSystems(CodedValue code) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkEndpoint)
	{
		if (networkEndpoint==null) return;

		EndpointProxyStoreElement removeProxyStoreElementForEndpoint = removeProxyStoreElementForEndpoint(networkEndpoint);
		
		if (removeProxyStoreElementForEndpoint!=null)
			removeProxyStoreElementForEndpoint.removeAllProxiesForNetworkDeviceEndpoint(networkEndpoint);
	}

	private EndpointProxyStoreElement getProxyStoreElementForEndpoint(EndpointReference networkEndpoint) {
		return proxyStoresPerEndpoint.get(networkEndpoint);
	}

	private EndpointProxyStoreElement getOrCreateProxyStoreElementForEndpoint(EndpointReference networkDeviceEndpoint) {
		EndpointProxyStoreElement endpointProxyStoreElement = proxyStoresPerEndpoint.get(networkDeviceEndpoint);
		if (endpointProxyStoreElement==null)
		{
			endpointProxyStoreElement=new EndpointProxyStoreElement();
			proxyStoresPerEndpoint.put(networkDeviceEndpoint, endpointProxyStoreElement);
		}
		return endpointProxyStoreElement;
	}

	private EndpointProxyStoreElement removeProxyStoreElementForEndpoint(EndpointReference networkDeviceEndpoint) {
		return proxyStoresPerEndpoint.remove(networkDeviceEndpoint);
	}


	@Override
	public boolean containsProxy(ProxyUniqueID proxyUniqueID) {
		boolean retVal=false;
		if (proxyUniqueID!=null)
		{
			EndpointProxyStoreElement proxyStoreElementForEndpoint = getProxyStoreElementForEndpoint(proxyUniqueID.getNetworkEndpoint());
			if (proxyStoreElementForEndpoint!=null)
			{
				retVal=(proxyStoreElementForEndpoint.getProxy(proxyUniqueID.getLocalHandle())!=null);
			}
		}
		return retVal;
	}


	@Override
	public void addProxy(BICEPSProxy proxyToAdd) {
		if (proxyToAdd !=null && proxyToAdd.getEndpointReference()!=null)
		{
			EndpointReference networkEndpoint = proxyToAdd.getEndpointReference();
			EndpointProxyStoreElement proxyStoreElementForEndpoint = getOrCreateProxyStoreElementForEndpoint(networkEndpoint);
			proxyStoreElementForEndpoint.addProxy(proxyToAdd);
		}

	}


	@SuppressWarnings("unchecked")
	@Override
	public <T extends BICEPSProxy> T removeProxy(T proxyToRemove) {
		T retVal=null;
		if (proxyToRemove!=null)
		{
			EndpointReference networkEndpoint = proxyToRemove.getEndpointReference();
			EndpointProxyStoreElement proxyStoreElementForEndpoint = getProxyStoreElementForEndpoint(networkEndpoint);
			if (proxyStoreElementForEndpoint!=null && proxyToRemove.getDescriptor()!=null)
			{

				BICEPSProxy tempRetVal = proxyStoreElementForEndpoint.removeProxy(BICEPSProxyUtil.getHandle(proxyToRemove));
				if (tempRetVal!=null && proxyToRemove.getClass().isAssignableFrom(tempRetVal.getClass()))
				{
					retVal=(T) tempRetVal;
				}

			}
		}
		return retVal;
	}


	@Override
	public BICEPSProxy getProxy(ProxyUniqueID proxyUniqueId) {
		BICEPSProxy retVal=null;
		if (proxyUniqueId!=null)
		{
			EndpointProxyStoreElement proxyStoreElementForEndpoint = getProxyStoreElementForEndpoint(proxyUniqueId.getNetworkEndpoint());
			if (proxyStoreElementForEndpoint!=null)
			{
				retVal=proxyStoreElementForEndpoint.getProxy(proxyUniqueId.getLocalHandle());
			}
		}
		return retVal;
	}


	public <T extends BICEPSProxy> List<T> getAllProxiesOfAType(Class<T> proxyType, List<T> proxyList)
	{
		if (proxyType!=null)
		{
			if (proxyList==null)proxyList=new ArrayList<T>();
			Collection<EndpointProxyStoreElement> proxyStoresPerEndpointList = proxyStoresPerEndpoint.values();
			if (proxyStoresPerEndpointList!=null)
			{
				for (EndpointProxyStoreElement endpointProxyStoreElement : proxyStoresPerEndpointList) {
					endpointProxyStoreElement.getAllProxiesOfAType(proxyType,proxyList);
				}
			}
		}
		return proxyList;
	}


	private <T extends BICEPSProxy> T getProxyOfType(Class<T> proxyType, ProxyUniqueID proxyUniqueId)
	{
		T retVal=null;
		if (proxyUniqueId!=null)
		{
			BICEPSProxy proxy =getProxy(proxyUniqueId);
			if (proxy !=null && proxyType.isAssignableFrom(proxy.getClass()))
			{
				retVal=proxyType.cast(proxy);
			}
		}
		return retVal;
	}


	private <T extends BICEPSProxy> List<T> getProxyOfClassWithCode(Class<T> proxyClass, CodedValue code){
		List<T> result = new ArrayList<T>();
		List<T> allProxiesOfAType = getAllProxiesOfAType(proxyClass,null);
		if (allProxiesOfAType!=null)
		{
			for (T proxy : allProxiesOfAType) {
				if (proxy.getDescriptor()!=null)
				{
					if (ModelComparer.equals(BICEPSProxyUtil.getCode(proxy.getDescriptor()), code))
					{
						result.add(proxy);
					}
				}
			}
		}
		return result;
	}
}
