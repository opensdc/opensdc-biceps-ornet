package com.draeger.medical.biceps.client.proxy.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;

public class EndpointProxyStoreElement {


	Map<String, BICEPSProxy>								proxyMap						= new ConcurrentHashMap<String,BICEPSProxy>();


	EndpointProxyStoreElement()
	{
		//void
	}

	public void removeAllProxiesForNetworkDeviceEndpoint(EndpointReference networkDeviceEndpoint)
	{
		if (networkDeviceEndpoint==null) return;

		this.removeObjects(networkDeviceEndpoint, proxyMap);
	}

	private <T extends BICEPSProxy> void  removeObjects(EndpointReference networkDeviceEndpoint, Map<String,T>  clientProxyMap) {
		if (clientProxyMap!=null && networkDeviceEndpoint!=null)
		{
			ArrayList<BICEPSProxy> proxiesToRemove=new ArrayList<BICEPSProxy>();
			for (T proxy : clientProxyMap.values()) 
			{
				if (proxy!=null && proxy.getEndpointReference() == networkDeviceEndpoint)
				{
					proxiesToRemove.add(proxy);
					proxy.removed();
				}
			}

			for (BICEPSProxy proxy : proxiesToRemove) {
				clientProxyMap.remove(proxy.getProxyUniqueID());
			}
		}
	}

	public BICEPSProxy removeProxy(String localHandle) 
	{
		BICEPSProxy retVal=proxyMap.remove(localHandle);
		return retVal;
	}

	//	private <T extends BICEPSProxy> T  getObject(String handle, Map<String,T>  clientProxyMap) {
	//		T retVal=null;
	//		if (clientProxyMap!=null && handle!=null)
	//		{
	//			retVal= clientProxyMap.get(handle);
	//		}
	//		return retVal;
	//	}


	@SuppressWarnings("unchecked")
	public <T extends BICEPSProxy> T getProxy(String handle)
	{
		T retVal=null;
		BICEPSProxy tempObject =proxyMap.get(handle);
		try{
			if (tempObject!=null && checkProxy(tempObject)){
				retVal= (T) tempObject;
			}
		}catch(ClassCastException cce){
			if (Log.isInfo())Log.info("GetProxy called with wrong class for handle "+handle+"! "+cce.getMessage());
		}
		//		BICEPSProxy tempObject =null;
		//		for (Map map : lookupMaps.values()) {
		//			tempObject= getObject(handle, map);
		//			if (tempObject!=null) break;
		//		}
		//		try{
		//			if (tempObject!=null)
		//				retVal= (T) tempObject;
		//		}catch(ClassCastException cce){
		//			if (Log.isInfo())Log.info("GetProxy called with wrong class for handle "+handle+"! "+cce.getMessage());
		//		}

		return retVal;
	}

	public void addProxy(BICEPSProxy proxyToAdd) {
		if (proxyToAdd!=null && BICEPSProxyUtil.getHandle(proxyToAdd)!=null)
		{
			proxyMap.put(BICEPSProxyUtil.getHandle(proxyToAdd), proxyToAdd);
		}
	}

	public <T extends BICEPSProxy> List<T> getAllProxiesOfAType(Class<T> proxyType, List<T> proxyList)
	{
		if (proxyType!=null)
		{
			if (proxyList==null)proxyList=new ArrayList<T>();
			Collection<BICEPSProxy> allProxies = proxyMap.values();
			for (BICEPSProxy bicepsProxy : allProxies) {
				if (bicepsProxy!=null && proxyType.isAssignableFrom(bicepsProxy.getClass()))
				{
					T proxy = proxyType.cast(bicepsProxy);
					if (checkProxy(proxy)) proxyList.add(proxy);
				}
			}
		}
		return proxyList;
	}

	private boolean checkProxy(BICEPSProxy proxy) 
	{
		boolean retVal=true;
		if (proxy!=null && !proxy.isValid())
		{
			removeProxy(BICEPSProxyUtil.getHandle(proxy));
			retVal=false;
		}
		return retVal;
	}

}
