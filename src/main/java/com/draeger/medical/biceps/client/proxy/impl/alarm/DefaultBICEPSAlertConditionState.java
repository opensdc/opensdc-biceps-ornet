/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertConditionListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertConditionControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertConditionState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertCondition;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSAlertConditionState extends DefaultBICEPSAlertCondition implements BICEPSAlertConditionState {

	private CurrentAlertCondition currentState=null;

	public DefaultBICEPSAlertConditionState(AlertConditionDescriptor metric,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, proxyCom, parentMDS);
	}

	@Override
	public CurrentAlertCondition getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				CurrentAlertCondition newState = BICEPSProxyUtil.getCurrentStateFromDevice(
							this,
							getProxyCom().getCommunicationAdapter(), CurrentAlertCondition.class);
				if (this.currentState!=newState)
					this.currentState=newState;
			}
		}
		return currentState;
	}

	
	protected void setAlertSignalState(CurrentAlertCondition state){
		this.currentState=state;
	}


	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public BICEPSAlertConditionControl getControlProxy() {
		return null;
	}

	@Override
	public BICEPSAlertConditionState getStateProxy() {
		return this;
	}

	@Override
	public void changed(CurrentAlertCondition s,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setAlertSignalState(s);
			for (AlertConditionListener l : getListeners())
			{
				l.changedAlertCondition(this, changedProps);
			}
		}
	}
	
	

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof CurrentAlertCondition)
		{
			this.changed((CurrentAlertCondition)newState,sequenceNumber, changedProps);
		}
		super.changed(newState, sequenceNumber, changedProps);

	}

	@Override
	public void removed() {
		setAlertSignalState(null);
		super.removed();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		CurrentAlertCondition latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), CurrentAlertCondition.class);
		return (latestStateFromDevice!=null);
		
	}



}
