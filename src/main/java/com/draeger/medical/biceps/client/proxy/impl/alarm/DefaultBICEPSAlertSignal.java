/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSignalListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSignalState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSAlertSignal extends DefaultAbstractBICEPSProxy<AlertSignalListener> implements BICEPSAlertSignal {

	private final AlertSignalDescriptor          descriptor;
	private final ProxyUniqueID clientProxyUniqueID;

	public DefaultBICEPSAlertSignal(AlertSignalDescriptor descriptor, EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS)
	{
		super(deviceEndpointRef,proxyCom, parentMDS);
		this.descriptor = descriptor;
		setInitialValidity(descriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid())
		{
//			tClientProxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(getDescriptor().getHandle(), getHandleOnEndpointReference());
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
	}


	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return this.clientProxyUniqueID;
	}


	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}


	@Override
	public AlertSignalDescriptor getDescriptor() {
		return this.descriptor;
	}


	@Override
	public boolean isStateValid() {
		return false;
	}

	@Override
	public BICEPSAlertSignalState getStateProxy() {
		return null;
	}


	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (AlertSignalListener listener : getListeners())
			{
				listener.removedAlertSignal(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) 
	{
		if (isValid())
		{
			for (AlertSignalListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}

	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		//void
	}



	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) 
	{
		//void
	}







}
