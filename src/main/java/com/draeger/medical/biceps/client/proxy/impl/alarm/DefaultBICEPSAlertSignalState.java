/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;
import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSignalListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSignalControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSignalState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertSignal;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.SetAlertState;
import com.draeger.medical.biceps.common.model.SetAlertStateOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetAlertStateResponse;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSAlertSignalState extends DefaultBICEPSAlertSignal implements BICEPSAlertSignalState, BICEPSAlertSignalControl {

	private CurrentAlertSignal currentState=null;
	private SetAlertStateOperationDescriptor setAlertStateOperationDescriptor;
	private OperationState operationState=null;

	public DefaultBICEPSAlertSignalState(AlertSignalDescriptor metric,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, proxyCom, parentMDS);
		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetAlertStateOperationDescriptor) 
			{
				this.setAlertStateOperationDescriptor= (SetAlertStateOperationDescriptor)o;
			}
		}
	}

	@Override
	public CurrentAlertSignal getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				CurrentAlertSignal newState = BICEPSProxyUtil.getCurrentStateFromDevice(
							this,
							getProxyCom().getCommunicationAdapter(), CurrentAlertSignal.class);
					this.currentState=newState;
			}
		}
		return currentState;
	}

	protected void setAlertSignalState(CurrentAlertSignal state){
		this.currentState=state;
	}
	
	@Override
	public OperationState getOperationState() {
		return operationState;
	}
	
	protected void setOperationState(OperationState newState)
	{
		this.operationState=newState;
	}


	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}



	@Override
	public BICEPSAlertSignalState getStateProxy() {
		return this;
	}

	@Override
	public void changed(CurrentAlertSignal newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setAlertSignalState(newState);
			for (AlertSignalListener l : getListeners())
			{
				l.changedAlertSignal(this, changedProps);
			}
		}
	}
	
	public void changed(OperationState newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setOperationState(newState);
			for (AlertSignalListener l : getListeners())
			{
				l.changedAlertSignal(this, changedProps);
			}
		}
	}
	
	
	

	@Override
	public void changed(State newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof CurrentAlertSignal)
		{
			this.changed((CurrentAlertSignal)newState,sequenceNumber, changedProps);
		} else if (newState instanceof OperationState && setAlertStateOperationDescriptor!=null)
		{
			if (setAlertStateOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
			{
				this.changed((OperationState)newState,sequenceNumber, changedProps);
			}
		}
		super.changed(newState,sequenceNumber, changedProps);

	}

	@Override
	public void removed() {
		setAlertSignalState(null);
		super.removed();
	}

	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;

		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();

		return retVal;
	}

	@Override
	public BICEPSAlertSignalControl getControlProxy() {
		return this;
	}

	private boolean isOperationActive() 
	{
		boolean retVal=false;
		if (operationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(operationState.getState()));
		}
		return retVal;
	}


	@Override
	public SetAlertStateResponse setAlertSignalState(
			CurrentAlertSignal alertSignalState,
			BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetAlertStateResponse response = null;
		if (isSetAlertSignalStateSupported() && alertSignalState!=null)
		{

			SetAlertState params = new SetAlertState();
			params.setOperationHandle(this.setAlertStateOperationDescriptor.getHandle());

			params.setState(alertSignalState);

			if (getProxyCom()!=null){
				response = (SetAlertStateResponse) getProxyCom().invokeOperation(this.setAlertStateOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}

	@Override
	public boolean isSetAlertSignalStateSupported() {
		 return (this.setAlertStateOperationDescriptor!=null && isOperationActive());
	}

	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
        for (AlertSignalListener l : getListeners())
        {
            l.invokationStateChanged( this, transactionId, newState, errorCode,errorMsg);
        }
        
		super.requestStateChanged(transactionId, sequenceNumber, newState, errorCode, errorMsg);
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		CurrentAlertSignal latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), CurrentAlertSignal.class);
		
		return (latestStateFromDevice!=null);
	}



}
