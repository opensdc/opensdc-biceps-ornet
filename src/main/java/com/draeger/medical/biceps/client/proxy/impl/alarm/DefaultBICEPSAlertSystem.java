/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSystemListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSystemState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSAlertSystem extends DefaultAbstractBICEPSProxy<AlertSystemListener> implements BICEPSAlertSystem {

	private final AlertSystemDescriptor          descriptor;
	private final ProxyUniqueID clientProxyUniqueID;

	public DefaultBICEPSAlertSystem(AlertSystemDescriptor descriptor, EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS)
	{
		super(deviceEndpointRef,proxyCom, parentMDS);
		this.descriptor = descriptor;
		setInitialValidity(descriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid())
		{
//			tClientProxyUniqueID=BICEPSProxyUtil.createProxyUniqueID(getDescriptor().getHandle(), getHandleOnEndpointReference());
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
	}


	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return this.clientProxyUniqueID;
	}



	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}



	@Override
	public AlertSystemDescriptor getDescriptor() {
		return this.descriptor;
	}


	@Override
	public boolean isStateValid() {
		return false;
	}

	@Override
	public BICEPSAlertSystemState getStateProxy() {
		return null;
	}


	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (AlertSystemListener listener : getListeners())
			{
				listener.removedAlertSystem(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) 
	{
		if (isValid())
		{
			for (AlertSystemListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}
	
	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		//void
	}



	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) 
	{
		//void
	}







}
