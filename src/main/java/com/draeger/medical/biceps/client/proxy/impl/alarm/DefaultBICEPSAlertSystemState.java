/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSystemListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSystemControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSAlertSystemState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertSystem;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSAlertSystemState extends DefaultBICEPSAlertSystem implements BICEPSAlertSystemState {

	private CurrentAlertSystem currentState=null;

	public DefaultBICEPSAlertSystemState(AlertSystemDescriptor metric,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, proxyCom, parentMDS);
	}

	@Override
	public CurrentAlertSystem getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				CurrentAlertSystem newState = BICEPSProxyUtil.getCurrentStateFromDevice(
							this,
							getProxyCom().getCommunicationAdapter(), CurrentAlertSystem.class);
				if (this.currentState!=newState)
					this.currentState=newState;
			}
		}
		return currentState;
	}

	protected void setAlertSignalState(CurrentAlertSystem state){
		this.currentState=state;
	}


	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public BICEPSAlertSystemControl getControlProxy() {
		return null;
	}

	@Override
	public BICEPSAlertSystemState getStateProxy() {
		return this;
	}

	@Override
	public void changed(CurrentAlertSystem s,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setAlertSignalState(s);
			for (AlertSystemListener l : getListeners())
			{
				l.changedAlertSystem(this, changedProps);
			}
		}
	}

	@Override
	public void changed(State newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof CurrentAlertSystem)
		{
			this.changed((CurrentAlertSystem)newState,sequenceNumber, changedProps);
		}
		super.changed(newState,sequenceNumber, changedProps);

	}

	@Override
	public void removed() {
		setAlertSignalState(null);
		super.removed();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		CurrentAlertSystem latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), CurrentAlertSystem.class);
		 return (latestStateFromDevice!=null);
	}



}
