/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.alarm;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertConditionListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSLimitAlertConditionControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSLimitAlertConditionState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.CurrentAlertCondition;
import com.draeger.medical.biceps.common.model.CurrentLimitAlertCondition;
import com.draeger.medical.biceps.common.model.LimitAlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSLimitAlertConditionState extends DefaultBICEPSAlertCondition implements BICEPSLimitAlertConditionState {

	private CurrentLimitAlertCondition currentState=null;

	public DefaultBICEPSLimitAlertConditionState(LimitAlertConditionDescriptor metric,
			EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem dmds) {
		super(metric, deviceEndpointRef, proxyCom,dmds);
	}

	@Override
	public CurrentLimitAlertCondition getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				CurrentLimitAlertCondition newState = BICEPSProxyUtil.getCurrentStateFromDevice(
							this,
							getProxyCom().getCommunicationAdapter(), CurrentLimitAlertCondition.class);
				
				if (this.currentState!=newState)
					this.currentState=newState;
			}
		}
		return currentState;
	}

	protected void setAlertSignalState(CurrentLimitAlertCondition state){
		this.currentState=state;
	}


	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public BICEPSLimitAlertConditionControl getControlProxy() {
		return null;
	}

	@Override
	public BICEPSLimitAlertConditionState getStateProxy() {
		return this;
	}

	@Override
	public void changed(CurrentLimitAlertCondition s,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setAlertSignalState(s);
			for (AlertConditionListener l : getListeners())
			{
				l.changedAlertCondition(this, changedProps);
			}
		}
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof CurrentLimitAlertCondition)
		{
			this.changed((CurrentAlertCondition)newState, sequenceNumber, changedProps);
		}
		super.changed(newState,sequenceNumber, changedProps);

	}

	@Override
	public void removed() {
		setAlertSignalState(null);
		super.removed();
	}

	@Override
	public void changed(CurrentAlertCondition newState, long sequenceNumber,
			List<ChangedProperty> changedProps) {
		if (newState instanceof CurrentLimitAlertCondition)
		{
			this.changed((CurrentLimitAlertCondition)newState, sequenceNumber, changedProps);
		}
		super.changed(newState, sequenceNumber, changedProps);
	}

	@Override
	public LimitAlertConditionDescriptor getDescriptor() {
		return (LimitAlertConditionDescriptor) super.getDescriptor();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		CurrentLimitAlertCondition latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), CurrentLimitAlertCondition.class);
		return (latestStateFromDevice!=null);
	}

}
