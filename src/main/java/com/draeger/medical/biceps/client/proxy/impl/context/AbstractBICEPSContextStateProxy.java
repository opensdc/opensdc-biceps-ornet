/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.context;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;

public abstract class AbstractBICEPSContextStateProxy<T extends BICEPSClientCallback, S extends AbstractIdentifiableContextState> extends DefaultAbstractBICEPSProxy<T>{
	protected final Map<String,S> multiStateMap=new ConcurrentHashMap<String,S>();
	protected final Map<S,BigInteger> mdibVersionChanges=new WeakHashMap<S,BigInteger>();
	protected BigInteger latestMdibVersionChange=BigInteger.valueOf(-1L);
	protected final Class<S> stateClass;
	
	/**
	 * @param deviceEndpointReference
	 * @param proxyCom
	 * @param parentMDS
	 */
	protected AbstractBICEPSContextStateProxy(
			EndpointReference deviceEndpointReference,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS, Class<S> stateClass) {
		super(deviceEndpointReference, proxyCom, parentMDS);
		this.stateClass=stateClass;
	}

	
	@Override
	public boolean isStateValid() {
		return (!this.multiStateMap.values().isEmpty());
	}
	
	
	protected boolean addState(S newState, long sequenceNumber) 
	{
		boolean modified=false;
		if (newState!=null) 
		{
			S pState=newState;
			 if (pState.getHandle()==null)
			 {
				 pState.setHandle(pState.getReferencedDescriptor());
			 }
			 
			 S pContextState = multiStateMap.get(pState.getHandle());
			 
			if (pContextState==null || 
				!pContextState.getHandle().equals(pState.getHandle()) || 
					pContextState.getStateVersion() ==null || // some devices may not have been properly initiated and a null value would keep the notification from happening
					(pContextState.getStateVersion() !=null &&
					!pContextState.getStateVersion().equals(pState.getStateVersion())))
			{
				modified=true;
				multiStateMap.put(pState.getHandle(), pState);
				BigInteger mdibVersionChanged = BigInteger.valueOf(sequenceNumber);
				mdibVersionChanges.put(pState, mdibVersionChanged);
				latestMdibVersionChange=mdibVersionChanged.max(latestMdibVersionChange);
			}
		}
		return modified;
	}
	
	public List<AbstractIdentifiableContextState> getAllContextElementItems() {
		List<AbstractIdentifiableContextState> retVal=new ArrayList<AbstractIdentifiableContextState>();
		Collection<S> values = multiStateMap.values();
		if (values!=null && !values.isEmpty())
			retVal.addAll(values);

		return retVal;
	}
	
	public boolean touch() {
		List<S> latestStatesFromDevice = BICEPSProxyUtil.getCurrentStatesFromDevice(this, getProxyCom().getCommunicationAdapter(), stateClass);
		return (latestStatesFromDevice!=null);
	}
	
}
