package com.draeger.medical.biceps.client.proxy.impl.context;

import java.util.List;
import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.EnsembleListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSEnsembleControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSEnsembleContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSEnsembleState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractContextState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.EnsembleContextState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextState;
import com.draeger.medical.biceps.common.model.SetContextStateResponse;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSEnsembleContext extends AbstractBICEPSContextStateProxy<EnsembleListener,EnsembleContextState> implements BICEPSEnsembleContext, BICEPSEnsembleState, BICEPSEnsembleControl {

	private final EnsembleContextDescriptor descriptor;
	private final ProxyUniqueID clientProxyUniqueID;
	
	private SetContextOperationDescriptor setContextOperationDescriptor;
	private OperationState setContextOperationState=null;
	
	public DefaultBICEPSEnsembleContext(
			EnsembleContextDescriptor descriptor,
			EndpointReference deviceEndpointReference,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(deviceEndpointReference, proxyCom, parentMDS, EnsembleContextState.class);
		this.descriptor=descriptor;
		setInitialValidity(descriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;

		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetContextOperationDescriptor) 
			{
				this.setContextOperationDescriptor= (SetContextOperationDescriptor)o;
			}
		}
	}
	
	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return clientProxyUniqueID;
	}

	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}

	@Override
	public BICEPSEnsembleState getStateProxy() {
		return this;
	}

	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (EnsembleListener listener : getListeners())
			{
				listener.removedContext(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			if (newState instanceof OperationState && setContextOperationDescriptor!=null)
			{
				if (setContextOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
				{
					this.setContextOperationState=(OperationState)newState;
				}
			}
			if (newState instanceof AbstractIdentifiableContextState)
				changed((AbstractIdentifiableContextState)newState, sequenceNumber, changedProps);
		}
	}



	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		List<EnsembleListener> listeners = this.getListeners();
		if (listeners!=null)
		{
			for (EnsembleListener listener : listeners) {
				listener.changeRequestStateChanged(this, transactionId, newState, errorCode, errorMsg);
			}
		}
	}


	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		if (isValid())
		{
			for (EnsembleListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}

	@Override
	public EnsembleContextDescriptor getDescriptor() {
		return this.descriptor;
	}

	@Override
	public boolean isModifiable() {
		return (this.setContextOperationDescriptor!=null);
	}


	@Override
	public BICEPSEnsembleControl getControlProxy() {
		return this;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState#changed(com.draeger.medical.biceps.common.model.ContextStateElement, long, java.util.List)
	 */
	@Override
	public void changed(AbstractIdentifiableContextState newState, long sequenceNumber,
			List<ChangedProperty> changedProps) {
		if (newState instanceof EnsembleContextState){
			if (addState((EnsembleContextState)newState, sequenceNumber)){
				for (EnsembleListener l : getListeners())
				{

					l.changedContext(this, changedProps);
				}
			}
		}
	}

//	/* (non-Javadoc)
//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSPatientState#getContextElementAssociation()
//	 */
//	@Override
//	public ContextAssociationStateValue getContextElementAssociation() {
//		if (ensembleAssociationState==null )
//		{
//			if (getDescriptor()!=null)
//			{				
////				ContextAssociationStateValue newState = BICEPSProxyUtil.getCurrentStateFromDevice(
////							getEndpointReference(),
////							getDescriptor().getHandle(),
////							getProxyCom().getCommunicationAdapter(),ContextAssociationStateValue.class);
////				this.ensembleAssociationState=newState;
//			}
//		}
//		return ensembleAssociationState;
//	}

//	/* (non-Javadoc)
//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSPatientState#getContextElementItem()
//	 */
//	@Override
//	public EnsembleContextState getContextElementItem() {
//		if (ensembleContextState==null )
//		{
//			if (getDescriptor()!=null)
//			{				
//				EnsembleContextState newState = BICEPSProxyUtil.getCurrentStateFromDevice(
//							this,
//							getProxyCom().getCommunicationAdapter(), EnsembleContextState.class);
//				
//				if (this.ensembleContextState!=newState)
//					this.ensembleContextState=newState;
//			}
//		}
//		return ensembleContextState;
//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSControlProxy#getOperationDescriptors()
	 */
	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;

		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();

		return retVal;
	}

	@Override
	public SetContextStateResponse setContextElement(
			AbstractIdentifiableContextState contextState,
			BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetContextStateResponse response = null;
		if (isModifiable())
		{

			SetContextState params = new SetContextState();
			params.setOperationHandle(this.setContextOperationDescriptor.getHandle());
			List<AbstractContextState> proposedContextStates = params.getProposedContextStates();

			proposedContextStates.add(contextState);


			if (getProxyCom()!=null){
				response = (SetContextStateResponse) getProxyCom().invokeOperation(this.setContextOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}


	public boolean isOperationActive() 
	{
		boolean retVal=false;
		if (setContextOperationDescriptor!=null && setContextOperationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(setContextOperationState.getState()));
		}
		return retVal;
	}


}
