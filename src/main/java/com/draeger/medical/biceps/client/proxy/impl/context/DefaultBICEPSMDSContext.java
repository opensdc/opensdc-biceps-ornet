/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.context;

import java.util.ArrayList;
import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSStateProxy;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.MDSContextListener;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.SystemContext;

public class DefaultBICEPSMDSContext extends DefaultAbstractBICEPSProxy<MDSContextListener> implements
BICEPSMDSContext {

	private final SystemContext systemContextDescriptor;
	private final ProxyUniqueID clientProxyUniqueID;
	public  final List<BICEPSMDSContextElementState<?,?>> mdsContextElements;

	public DefaultBICEPSMDSContext(SystemContext systemContextDescriptor, EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentDMDS, List<BICEPSMDSContextElementState<?,?>> mdsContextElements)
	{
		super(deviceEndpointRef,  proxyCom, parentDMDS);
		this.systemContextDescriptor=systemContextDescriptor;
		
		setInitialValidity(systemContextDescriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
		List<BICEPSMDSContextElementState<?,?>> tContextElements=null;	
		if (mdsContextElements!=null){
			tContextElements=mdsContextElements;
		}else{
			tContextElements=new ArrayList<BICEPSMDSContextElementState<?,?>>(3);
		}
		
		this.mdsContextElements=tContextElements;
	}
	
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.description.BICEPSMDSContext#getMDSContextElements()
	 */
	@Override
	public List<BICEPSMDSContextElementState<?,?>> getMDSContextElements() {
		return mdsContextElements;
	}
	
	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return this.clientProxyUniqueID;
	}

	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}

	@Override
	public SystemContext getDescriptor() {
		return this.systemContextDescriptor;
	}

	@Override
	public boolean isStateValid() {
		return false;
	}

	@Override
	public void removed() {
		setValid(false);
	}

	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		//void
	}
	
	@Override
	public BICEPSStateProxy getStateProxy() {
		return null;
	}

	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		//void
	}
	
	public void changed(AbstractIdentifiableContextState s, long sequenceNumber,List<ChangedProperty> changedProps) {
		if (isValid())
		{
			for (MDSContextListener l : getListeners())
			{
				l.changedContext(this, changedProps);
			}
		}
	}
	
	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) 
	{
		if (newState instanceof AbstractIdentifiableContextState)
		{
			System.err.println("Handle Context State Change");
			this.changed((AbstractIdentifiableContextState)newState,sequenceNumber, changedProps);
		}
		super.changed(newState, sequenceNumber, changedProps);
	}

}
