/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.context;

import java.util.List;
import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.PatientListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSPatientControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSPatientContext;
import com.draeger.medical.biceps.client.proxy.state.BICEPSPatientState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractContextState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.PatientContextState;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextState;
import com.draeger.medical.biceps.common.model.SetContextStateResponse;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSPatientContext extends AbstractBICEPSContextStateProxy<PatientListener, PatientContextState> implements BICEPSPatientContext, BICEPSPatientState, BICEPSPatientControl {

	private final PatientAssociationDescriptor descriptor;
	private final ProxyUniqueID clientProxyUniqueID;
	private SetContextOperationDescriptor setContextOperationDescriptor;
	private OperationState setContextOperationState=null;

	public DefaultBICEPSPatientContext(PatientAssociationDescriptor descriptor,
			EndpointReference deviceEndpointReference,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(deviceEndpointReference, proxyCom, parentMDS, PatientContextState.class);
		this.descriptor=descriptor;
		setInitialValidity(descriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;

		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetContextOperationDescriptor) 
			{
				this.setContextOperationDescriptor= (SetContextOperationDescriptor)o;
			}
		}
	}

	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return clientProxyUniqueID;
	}

	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}


	//	@Override
	//	public boolean isStateValid() {
	//		return (!multiStateMap.values().isEmpty());
	//	}

	@Override
	public BICEPSPatientState getStateProxy() {
		return this;
	}

	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (PatientListener listener : getListeners())
			{
				listener.removedContext(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			if (newState instanceof OperationState && setContextOperationDescriptor!=null)
			{
				if (setContextOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
				{
					this.setContextOperationState=(OperationState)newState;
				}
			}
			if (newState instanceof AbstractIdentifiableContextState)
				changed((AbstractIdentifiableContextState)newState, sequenceNumber, changedProps);
		}
	}

	//	protected boolean addState(AbstractIdentifiableContextState newState, long sequenceNumber) 
	//	{
	//		boolean modified=false;
	//		if (newState instanceof PatientContextState) {
	//			PatientContextState pState=(PatientContextState)newState;
	//			 if (pState.getHandle()==null)
	//			 {
	//				 pState.setHandle(pState.getReferencedDescriptor());
	//			 }
	//			 
	//			PatientContextState pContextState = multiStateMap.get(pState.getHandle());
	//			 
	//			if (pContextState==null || 
	//				!pContextState.getHandle().equals(pState.getHandle()) || 
	//					(pContextState.getStateVersion() !=null &&
	//					!pContextState.getStateVersion().equals(pState.getStateVersion())))
	//			{
	//				modified=true;
	//				multiStateMap.put(pState.getHandle(), pState);
	////				patientContextState=pState;
	//				BigInteger mdibVersionChanged = BigInteger.valueOf(sequenceNumber);
	//				mdibVersionChanges.put(pState, mdibVersionChanged);
	//				latestMdibVersionChange=mdibVersionChanged.max(latestMdibVersionChange);
	//			}
	//		}
	//		return modified;
	//	}


	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		List<PatientListener> listeners = this.getListeners();
		if (listeners!=null)
		{
			for (PatientListener patientListener : listeners) {
				patientListener.changeRequestStateChanged(this, transactionId, newState, errorCode, errorMsg);
			}
		}
	}


	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		if (isValid())
		{
			for (PatientListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}

	@Override
	public PatientAssociationDescriptor getDescriptor() {
		return this.descriptor;
	}

	@Override
	public boolean isModifiable() {
		return (this.setContextOperationDescriptor!=null);
	}


	@Override
	public BICEPSPatientControl getControlProxy() {
		return this;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState#changed(com.draeger.medical.biceps.common.model.ContextStateElement, long, java.util.List)
	 */
	@Override
	public void changed(AbstractIdentifiableContextState newState, long sequenceNumber,
			List<ChangedProperty> changedProps) {
		if (newState instanceof PatientContextState){
			if (addState((PatientContextState)newState,sequenceNumber)){
				for (PatientListener l : getListeners())
				{
					l.changedContext(this, changedProps);
				}
			}
		}
	}

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSPatientState#getContextElementAssociation()
	//	 */
	//	@Override
	//	public ContextAssociationStateValue getContextElementAssociation() {
	//		ContextAssociationStateValue retVal=null;
	//		if (patientContextState==null )
	//		{
	//			
	//		}
	//		
	//		return retVal;
	//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSPatientState#getContextElementItem()
	 */
	//	@Override
	//	public PatientContextState getContextElementItem() {
	//		PatientContextState patientContextState=null;
	//		if (multiStateMap.isEmpty())
	//		{
	//			if (getDescriptor()!=null)
	//			{				
	//				PatientContextState newState = BICEPSProxyUtil.getCurrentStateFromDevice(this, getProxyCom().getCommunicationAdapter(), PatientContextState.class);
	//				if (this.patientContextState!=newState)
	//					this.patientContextState=newState;
	//			}
	//		}
	//		return patientContextState;
	//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSControlProxy#getOperationDescriptors()
	 */
	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;

		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.control.BICEPSPatientControl#setContextElement(com.draeger.medical.biceps.common.model.ContextStateElement, com.draeger.medical.biceps.common.model.ContextStateElement, com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer)
	 */
	@Override
	public SetContextStateResponse setContextElement(
			AbstractIdentifiableContextState contextState,
			BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetContextStateResponse response = null;
		if (isModifiable())
		{

			SetContextState params = new SetContextState();
			params.setOperationHandle(this.setContextOperationDescriptor.getHandle());
			List<AbstractContextState> proposedContextStates = params.getProposedContextStates();

			proposedContextStates.add(contextState);


			if (getProxyCom()!=null){
				response = (SetContextStateResponse) getProxyCom().invokeOperation(this.setContextOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}


	public boolean isOperationActive() 
	{
		boolean retVal=false;
		if (setContextOperationDescriptor!=null && setContextOperationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(setContextOperationState.getState()));
		}
		return retVal;
	}

//	/* (non-Javadoc)
//	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
//	 */
//	@Override
//	public void touch() {
//		BICEPSProxyUtil.getCurrentStateFromDevice(this, getProxyCom().getCommunicationAdapter(), PatientContextState.class);
//	}

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState#getAllContextElementItems()
	//	 */
	//	@Override
	//	public List<AbstractIdentifiableContextState> getAllContextElementItems() {
	//		List<AbstractIdentifiableContextState> retVal=new ArrayList<AbstractIdentifiableContextState>();
	//		Collection<PatientContextState> values = multiStateMap.values();
	//		if (values!=null && !values.isEmpty())
	//			retVal.addAll(values);
	//		
	//		return retVal;
	//	}


}
