package com.draeger.medical.biceps.client.proxy.impl.context;

import java.util.List;
import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.WorkflowListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSWorkflowControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSWorkflowContext;
import com.draeger.medical.biceps.client.proxy.state.BICEPSWorkflowState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractContextState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextState;
import com.draeger.medical.biceps.common.model.SetContextStateResponse;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextState;

public class DefaultBICEPSWorkflowContext extends AbstractBICEPSContextStateProxy<WorkflowListener,WorkflowContextState> implements BICEPSWorkflowContext, BICEPSWorkflowState, BICEPSWorkflowControl {

	private final WorkflowContextDescriptor descriptor;
	private final ProxyUniqueID clientProxyUniqueID;
	//	private WorkflowContextState workflowContextState;
	//	private ContextAssociationStateValue associationState;
//	private Map<String,WorkflowContextState> multiStateMap=new ConcurrentHashMap<String,WorkflowContextState>();
	private SetContextOperationDescriptor setContextOperationDescriptor;
	private OperationState setContextOperationState=null;

	public DefaultBICEPSWorkflowContext(
			WorkflowContextDescriptor descriptor,
			EndpointReference deviceEndpointReference,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(deviceEndpointReference, proxyCom, parentMDS, WorkflowContextState.class);
		this.descriptor = descriptor;
		setInitialValidity(descriptor);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;

		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetContextOperationDescriptor) 
			{
				this.setContextOperationDescriptor= (SetContextOperationDescriptor)o;
			}
		}
	}

	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return clientProxyUniqueID;
	}

	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}

	@Override
	public BICEPSWorkflowState getStateProxy() {
		return this;
	}

	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (WorkflowListener listener : getListeners())
			{
				listener.removedContext(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			if (newState instanceof OperationState && setContextOperationDescriptor!=null)
			{
				if (setContextOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
				{
					this.setContextOperationState=(OperationState)newState;
				}
			}
			if (newState instanceof AbstractIdentifiableContextState)
				changed((AbstractIdentifiableContextState)newState, sequenceNumber, changedProps);
		}
	}

	//	protected boolean setState(AbstractIdentifiableContextState newState) 
	//	{
	//		boolean modified=false;
	//		//		if (newState instanceof ContextAssociationStateValue){ 
	//		//			if (associationState==null || 
	//		//					(
	//		//							associationState.getStateVersion() !=null &&
	//		//					!associationState.getStateVersion().equals(newState.getStateVersion())))
	//		//			{
	//		//				modified= true; 
	//		//				associationState=(ContextAssociationStateValue) newState;
	//		//			}
	//		//		}
	//		//		else 
	//		if (newState instanceof WorkflowContextState) {
	////			if (workflowContextState==null || 
	////					(workflowContextState.getStateVersion() !=null &&
	////					!workflowContextState.getStateVersion().equals(newState.getStateVersion())))
	////			{
	////				modified=true; 
	////				workflowContextState=(WorkflowContextState) newState;
	////			}
	//		}
	//		return modified;
	//	}


	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		List<WorkflowListener> listeners = this.getListeners();
		if (listeners!=null)
		{
			for (WorkflowListener workflowListener : listeners) {
				workflowListener.changeRequestStateChanged(this, transactionId, newState, errorCode, errorMsg);
			}
		}
	}


	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		if (isValid())
		{
			for (WorkflowListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}

	@Override
	public WorkflowContextDescriptor getDescriptor() {
		return this.descriptor;
	}

	@Override
	public boolean isModifiable() {
		return (this.setContextOperationDescriptor!=null);
	}


	@Override
	public BICEPSWorkflowControl getControlProxy() {
		return this;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState#changed(com.draeger.medical.biceps.common.model.ContextStateElement, long, java.util.List)
	 */
	@Override
	public void changed(AbstractIdentifiableContextState newState, long sequenceNumber,
			List<ChangedProperty> changedProps) {
		if (newState instanceof WorkflowContextState){
			if (addState((WorkflowContextState)newState, sequenceNumber)){
				for (WorkflowListener l : getListeners())
				{

					l.changedContext(this, changedProps);
				}
			}
		}
	}

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSLocationState#getContextElementAssociation()
	//	 */
	//	@Override
	//	public ContextAssociationStateValue getContextElementAssociation() {
	//		if (associationState==null )
	//		{
	//			if (getDescriptor()!=null)
	//			{				
	////				ContextAssociationStateValue newState = BICEPSProxyUtil.getCurrentStateFromDevice(
	////						getEndpointReference(),
	////						getDescriptor().getHandle(),
	////						getProxyCom().getCommunicationAdapter(),ContextAssociationStateValue.class);
	////				this.associationState=newState;
	//			}
	//		}
	//		return associationState;
	//	}

	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSLocationState#getContextElementItem()
	//	 */
	//	@Override
	//	public WorkflowContextState getContextElementItem() {
	//		if (workflowContextState==null )
	//		{
	//			if (getDescriptor()!=null)
	//			{				
	//				WorkflowContextState newState = BICEPSProxyUtil.getCurrentStateFromDevice(
	//						this,
	//						getProxyCom().getCommunicationAdapter(), WorkflowContextState.class);
	//				
	//				if (this.workflowContextState!=newState)
	//					this.workflowContextState=newState;
	//			}
	//		}
	//		return workflowContextState;
	//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSControlProxy#getOperationDescriptors()
	 */
	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;

		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();

		return retVal;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.control.BICEPSLocationControl#setContextElement(com.draeger.medical.biceps.common.model.ContextStateElement, com.draeger.medical.biceps.common.model.ContextStateElement, com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer)
	 */
	@Override
	public SetContextStateResponse setContextElement(
			AbstractIdentifiableContextState contextState,
			BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetContextStateResponse response = null;
		if (isModifiable())
		{

			SetContextState params = new SetContextState();
			params.setOperationHandle(this.setContextOperationDescriptor.getHandle());
			List<AbstractContextState> proposedContextStates = params.getProposedContextStates();

			proposedContextStates.add(contextState);


			if (getProxyCom()!=null){
				response = (SetContextStateResponse) getProxyCom().invokeOperation(this.setContextOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}


	public boolean isOperationActive() 
	{
		boolean retVal=false;
		if (setContextOperationDescriptor!=null && setContextOperationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(setContextOperationState.getState()));
		}
		return retVal;
	}

//	/* (non-Javadoc)
//	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
//	 */
//	@Override
//	public void touch() {
//		BICEPSProxyUtil.getCurrentStateFromDevice(
//				this,
//				getProxyCom().getCommunicationAdapter(), WorkflowContextState.class);
//
//	}
	//
	//	/* (non-Javadoc)
	//	 * @see com.draeger.medical.biceps.client.proxy.state.BICEPSMDSContextElementState#getAllContextElementItems()
	//	 */
	//	@Override
	//	public List<AbstractIdentifiableContextState> getAllContextElementItems() {
	//		List<AbstractIdentifiableContextState> retVal=new ArrayList<AbstractIdentifiableContextState>();
	//		Collection<WorkflowContextState> values = multiStateMap.values();
	//		if (values!=null && !values.isEmpty())
	//			retVal.addAll(values);
	//		
	//		return retVal;
	//	}

}
