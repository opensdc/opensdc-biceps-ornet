/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.maneuver;

import java.util.List;
import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.ManeuverListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.Activate;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.ActivateResponse;
import com.draeger.medical.biceps.common.model.ArgumentType;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.LocalizedText;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSManeuver  extends DefaultAbstractBICEPSProxy<ManeuverListener> implements BICEPSManeuver{

	private final ActivateOperationDescriptor opDesc;
	private final ProxyUniqueID clientProxyUniqueID;
	private OperationState currentState;

	public DefaultBICEPSManeuver(ActivateOperationDescriptor opDesc, EndpointReference deviceEndpointReference,
			ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentMDS) {
		super(deviceEndpointReference, proxyCom, parentMDS);
		this.opDesc=opDesc;
		
		setInitialValidity(opDesc);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
	}

	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return clientProxyUniqueID;
	}

	
	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}


	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;
		
		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();
		
		return retVal;
	}

	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public void removed() {
		setActivateOperationState(null);
	}

	@Override
	public void changed(OperationState newState, List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setActivateOperationState(newState);
			for (ManeuverListener l : getListeners())
			{
				l.changedManeuver(this, changedProps);
			}
		}
	}
	
	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
        for (ManeuverListener l : getListeners())
        {
            l.requestStateChanged( this, transactionId, newState, errorCode,errorMsg);
        }
		
	}

	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		if (isValid())
		{
			for (ManeuverListener l : getListeners())
			{
				l.subscriptionEnded(this, reason);
			}
		}
	}

	@Override
	public OperationState getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				OperationState newState = BICEPSProxyUtil.getCurrentStateFromDevice(
						this,
						getProxyCom().getCommunicationAdapter(), OperationState.class);

					this.currentState=newState;
			}
		}
		return currentState;
	}

	@Override
	public ActivateOperationDescriptor getDescriptor() {
		return this.opDesc;
	}

	@Override
	public BICEPSManeuver getControlProxy() {
		return this;
	}

	@Override
	public BICEPSManeuver getStateProxy() {
		return this;
	}

	@Override
	public ActivateResponse activate(List<ArgumentType> args, BICEPSClientTransmissionInformationContainer transmissionInformation) {
		ActivateResponse response = null;
		//if (isSetNumericSupported() && !Float.isInfinite(value) && !Float.isNaN(value))
		{

			Activate params = new Activate();
			params.setOperationHandle(this.opDesc.getHandle());
			if (args!=null)
				params.setArguments(args);
			
			if (getProxyCom()!=null){
				response = (ActivateResponse) getProxyCom().invokeOperation(this.opDesc, params, transmissionInformation);
			}
		}
		return response;
	}
	
	protected void setActivateOperationState(OperationState state){
		this.currentState=state;
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof OperationState)
		{
			this.changed((OperationState)newState, changedProps);
		}
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		OperationState latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), OperationState.class);

		return (latestStateFromDevice!=null);
	}

	
}
