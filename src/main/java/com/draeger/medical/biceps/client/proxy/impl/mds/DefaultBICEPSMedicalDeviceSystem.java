/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.mds;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.BICEPSClientCallback;
import com.draeger.medical.biceps.client.proxy.BICEPSStateProxy;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystemMetaData;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSMedicalDeviceSystem extends DefaultAbstractBICEPSProxy<BICEPSClientCallback> implements
BICEPSMedicalDeviceSystem {
	//TODO implement state of mds

	private final HydraMDSDescriptor mds;
	private final ProxyUniqueID clientProxyUniqueID;

	public DefaultBICEPSMedicalDeviceSystem(HydraMDSDescriptor mds, EndpointReference deviceEndpointRef, ProxyCommunication proxyCom, BICEPSMedicalDeviceSystem parentDMDS)
	{
		super(deviceEndpointRef,  proxyCom, parentDMDS);
		this.mds=mds;
		
		setInitialValidity(mds);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid()){
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
	}
	
	

	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return this.clientProxyUniqueID;
	}

	
	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}


	@Override
	public HydraMDSDescriptor getDescriptor() {
		return this.mds;
	}

	@Override
	public boolean isStateValid() {
		return false;
	}

	@Override
	public void removed() {
		setValid(false);
	}


	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) {
		//void
	}

	@Override
	public BICEPSStateProxy getStateProxy() {
		return null;
	}

	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		//void
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) 
	{
		//void
	}



	@Override
	public BICEPSMedicalDeviceSystemMetaData getMetaData() {
		return new DefaultBICEPSMedicalDeviceSystemMetaData(this.getEndpointReference());
	}
}
