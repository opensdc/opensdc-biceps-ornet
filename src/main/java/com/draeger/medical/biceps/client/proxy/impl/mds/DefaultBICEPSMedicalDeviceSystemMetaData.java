package com.draeger.medical.biceps.client.proxy.impl.mds;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.dispatch.IDeviceServiceRegistry;
import org.ws4d.java.service.Device;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystemMetaData;

public class DefaultBICEPSMedicalDeviceSystemMetaData implements
BICEPSMedicalDeviceSystemMetaData {

	private final EndpointReference deviceEPR;
	private final String defaultLang="en-US";

	public DefaultBICEPSMedicalDeviceSystemMetaData(EndpointReference deviceEPR )
	{
		this.deviceEPR=deviceEPR;

	}
	
	

	@Override
	public String getManufacturer(String lang) {
		String retVal=null;
		
		if (lang==null || lang.trim().length()==0)
		{
			lang=getDefaultLanguage();
		}
		
		Device device = getDevice(this.deviceEPR);
		
		if (device!=null)
		{
			retVal=device.getManufacturer(lang);	
		}

		return retVal;
	}



	private Device getDevice(EndpointReference deviceEPR) {
		Device device=null;
		IDeviceServiceRegistry deviceServiceRegistry = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry();
		if (deviceServiceRegistry!=null)
		{
			DeviceReference devRef = deviceServiceRegistry.getStaticDeviceReference(deviceEPR);
			try {
				device = devRef.getDevice();
			} catch (TimeoutException e) {
				Log.warn(e);
			}

		}
		return device;
	}

	@Override
	public String getModelName(String lang) {
		String retVal=null;
		
		if (lang==null || lang.trim().length()==0)
		{
			lang=getDefaultLanguage();
		}
		
		Device device = getDevice(this.deviceEPR);
		
		if (device!=null)
		{
			retVal=device.getModelName(lang);	
		}

		return retVal;
	}

	@Override
	public String getFirmwareVersion() {
		String retVal=null;
		
		Device device = getDevice(this.deviceEPR);
		
		if (device!=null)
		{
			retVal=device.getFirmwareVersion();	
		}

		return retVal;
	}
	

	@Override
	public String getSerialNumber() {
		String retVal=null;
		
		Device device = getDevice(this.deviceEPR);
		
		if (device!=null)
		{
			retVal=device.getSerialNumber();	
		}

		return retVal;
	}



	@Override
	public String getDefaultLanguage() {
		return defaultLang;
	}



	@Override
	public String getModelNumber() {
		String retVal=null;
		
		Device device = getDevice(this.deviceEPR);
		
		if (device!=null)
		{
			retVal=device.getModelNumber();	
		}

		return retVal;
	}


	@Override
	public String getUniqueDeviceIdentifier() {
		return this.deviceEPR.getAddress().toString();
	}

}
