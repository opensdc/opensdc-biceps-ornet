/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import java.util.Set;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.MetricListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.OperationDescriptor;

public class DefaultBICEPSMetricControl<D extends MetricDescriptor> extends DefaultBICEPSMetricState<D> implements
		BICEPSMetricControl {


	
	public DefaultBICEPSMetricControl(D metric,
			EndpointReference deviceEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
		
	}
	
	@Override
	public <T extends OperationDescriptor> Set<T> getOperationDescriptors() {
		Set<T> retVal=null;
		
		if (getProxyCom()!=null)
			retVal=getProxyCom().getOperationDescriptors();
		
		return retVal;
	}


	
	@Override
	public D getDescriptor() {
		return super.getDescriptor();
	}

	@Override
	public BICEPSMetricControl getControlProxy() {
		return this;
	}

	@Override
	public void removed() 
	{
		//this.serviceReferencesForOperations.clear();
		super.removed();
	}

	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
        for (MetricListener l : getListeners())
        {
            l.changeRequestStateChanged( this, transactionId, newState, errorCode,errorMsg);
        }
	}








	
}
