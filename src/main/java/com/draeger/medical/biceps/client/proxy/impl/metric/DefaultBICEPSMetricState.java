/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.MetricListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSMetricState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSMetricState<D extends MetricDescriptor> extends DefaultBICEPSMetric<D> implements BICEPSMetricState {

	private AbstractMetricState currentState=null;

	public DefaultBICEPSMetricState(D metric,
			EndpointReference deviceEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
	}

	@Override
	public AbstractMetricState getState() {
		if (currentState==null )
		{
			if (getDescriptor()!=null)
			{				
				AbstractMetricState newState = BICEPSProxyUtil.getCurrentStateFromDevice(
							this,
							getProxyCom().getCommunicationAdapter(),AbstractMetricState.class);
				if (this.currentState!=newState)
					this.currentState=newState;
			}
		}
		return currentState;
	}

	protected void setMetricState(AbstractMetricState state){
		this.currentState=state;
	}


	@SuppressWarnings("unchecked")
	@Override
	public D getDescriptor() {
		return (D)super.getDescriptor();
	}

	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public BICEPSMetricControl getControlProxy() {
		return null;
	}

	@Override
	public BICEPSMetricState getStateProxy() {
		return this;
	}

	@Override
	public void changed(AbstractMetricState s, long sequenceNumber,List<ChangedProperty> changedProps) {
		if (isValid())
		{
			setMetricState(s);
			for (MetricListener l : getListeners())
			{
				l.changedMetric(this, changedProps);
			}
		}
	}

	@Override
	public void changed(State newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof AbstractMetricState)
		{
			this.changed((AbstractMetricState)newState,sequenceNumber, changedProps);
		}
		super.changed(newState,sequenceNumber, changedProps);

	}

	@Override
	public void removed() {
		setMetricState(null);
		super.removed();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		AbstractMetricState latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(),AbstractMetricState.class);
		return (latestStateFromDevice!=null);
	}



}
