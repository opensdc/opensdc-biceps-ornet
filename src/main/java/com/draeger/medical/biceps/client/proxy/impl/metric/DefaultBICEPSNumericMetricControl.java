/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import java.math.BigDecimal;
import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.SetValue;
import com.draeger.medical.biceps.common.model.SetValueOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetValueResponse;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSNumericMetricControl extends DefaultBICEPSMetricControl<NumericMetricDescriptor> implements BICEPSNumericMetricControl,BICEPSNumericMetricState {

	private SetValueOperationDescriptor setValueOperationDescriptor=null;
	private OperationState setValueOperationState=null;


	public DefaultBICEPSNumericMetricControl(
			NumericMetricDescriptor metric,
			EndpointReference deviceEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetValueOperationDescriptor) 
			{
				this.setValueOperationDescriptor= (SetValueOperationDescriptor)o;
			}
		}
	}

	@Override
	public BICEPSNumericMetricControl getControlProxy() {
		return this;
	}

	@Override
	public boolean isSetNumericSupported() {
		if (BICEPSProxyUtil.isNumericMetric(this))
		{
			return (this.setValueOperationDescriptor!=null && isOperationActive());
		}
		return false;
	}

	private boolean isOperationActive() 
	{
		boolean retVal=false;
		if (setValueOperationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(setValueOperationState.getState()));
		}
		return retVal;
	}

//	public SetValueResponse setNumeric(float value, CodedValue unit) {
//		BICEPSClientTransmissionInformationContainer ti=new BICEPSClientTransmissionInformationContainer();
//		BICEPSClientSafetyContext sCtxt=new BICEPSClientSafetyContext(getDescriptor().getHandle());
//		BICEPSClientSafetyInformation clSI=new BICEPSClientSafetyInformation(sCtxt,null);
//		ti.add(clSI);
//		return setNumeric(value, ti);
//	}

	@Override
	public SetValueResponse setNumeric(float value,BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetValueResponse response = null;
		if (isSetNumericSupported() && !Float.isInfinite(value) && !Float.isNaN(value))
		{

			SetValue params = new SetValue();
			params.setOperationHandle(this.setValueOperationDescriptor.getHandle());
			params.setValue(BigDecimal.valueOf(value));

			if (getProxyCom()!=null){
				response = (SetValueResponse) getProxyCom().invokeOperation(this.setValueOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}

	@Override
	public void changed(State newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof OperationState && setValueOperationDescriptor!=null)
		{
			if (setValueOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
			{
				this.setValueOperationState=(OperationState)newState;
			}
		}
		super.changed(newState,sequenceNumber, changedProps);
	}

	@Override
	public BICEPSNumericMetricState getStateProxy() {
		return this;
	}

	@Override
	public NumericMetricState getState() {
		NumericMetricState retVal=null;
		AbstractMetricState s= super.getState();
		if (s instanceof NumericMetricState)
			retVal=(NumericMetricState)s;

		return retVal;
	}


}
