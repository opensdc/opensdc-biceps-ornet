/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;

public class DefaultBICEPSNumericMetricState extends DefaultBICEPSMetricState<NumericMetricDescriptor> implements BICEPSNumericMetricState {



	public DefaultBICEPSNumericMetricState(
			NumericMetricDescriptor metric,
			EndpointReference deviceEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
	}

	@Override
	public BICEPSNumericMetricControl getControlProxy() {
		return null;
	}
	
	@Override
	public BICEPSNumericMetricState getStateProxy() {
		return this;
	}

	@Override
	public NumericMetricState getState() {
		NumericMetricState retVal=null;
		AbstractMetricState s= super.getState();
		if (s instanceof NumericMetricState)
			retVal=(NumericMetricState)s;
		
		return retVal;
	}




}
