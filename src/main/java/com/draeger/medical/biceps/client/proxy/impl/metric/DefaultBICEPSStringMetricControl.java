/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientSafetyInformation;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSStringMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStringMetricState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.SetString;
import com.draeger.medical.biceps.common.model.SetStringOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetStringResponse;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.StringMetricState;

public class DefaultBICEPSStringMetricControl extends DefaultBICEPSMetricControl<StringMetricDescriptor> implements BICEPSStringMetricControl,BICEPSStringMetricState {

	private SetStringOperationDescriptor setStringOperationDescriptor=null;
	private OperationState setValueOperationState=null;


	public DefaultBICEPSStringMetricControl(
			StringMetricDescriptor metric,
			EndpointReference deviceEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
		for (OperationDescriptor o : getOperationDescriptors())
		{
			if (o instanceof SetStringOperationDescriptor) 
			{
				this.setStringOperationDescriptor= (SetStringOperationDescriptor)o;
			}
		}
	}

	@Override
	public BICEPSStringMetricControl getControlProxy() {
		return this;
	}

	@Override
	public boolean isSetStringSupported() {
		if (BICEPSProxyUtil.isStringMetric(this))
		{
			return (this.setStringOperationDescriptor!=null && isOperationActive());
		}
		return false;
	}

	private boolean isOperationActive() 
	{
		boolean retVal=false;
		if (setValueOperationState!=null)
		{
			retVal=(OperationalState.ENABLED.equals(setValueOperationState.getState()));
		}
		return retVal;
	}

	@Override
	public SetStringResponse setString(String value,BICEPSClientTransmissionInformationContainer transmissionInformation) {
		SetStringResponse response = null;
		if (isSetStringSupported() && value!=null)
		{

			SetString params = new SetString();
			params.setOperationHandle(this.setStringOperationDescriptor.getHandle());

//			com.draeger.medical.biceps.common.model.String requestedValue=new com.draeger.medical.biceps.common.model.String();
//			requestedValue.setValue(value);
			params.setString(value);

			if (getProxyCom()!=null){
				response = (SetStringResponse) getProxyCom().invokeOperation(this.setStringOperationDescriptor, params, transmissionInformation);
			}
		}
		return response;
	}

	@Override
	public void changed(State newState,long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof OperationState && setStringOperationDescriptor!=null)
		{
			if (setStringOperationDescriptor.getHandle().equals(newState.getReferencedDescriptor()))
			{
				this.setValueOperationState=(OperationState)newState;
			}
		}
		super.changed(newState,sequenceNumber, changedProps);
	}

	@Override
	public BICEPSStringMetricState getStateProxy() {
		return this;
	}

	@Override
	public StringMetricState getState() {
		StringMetricState retVal=null;
		AbstractMetricState s= super.getState();
		if (s instanceof StringMetricState)
			retVal=(StringMetricState)s;

		return retVal;
	}

	@Override
	public SetStringResponse setString(String value) {
		BICEPSClientTransmissionInformationContainer ti=new BICEPSClientTransmissionInformationContainer();
		BICEPSClientSafetyInformation clSI=new BICEPSClientSafetyInformation(null,null);
		ti.add(clSI);
		return setString(value,ti);
	}


}
