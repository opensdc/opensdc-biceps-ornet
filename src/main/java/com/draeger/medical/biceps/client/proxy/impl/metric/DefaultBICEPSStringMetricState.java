/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.metric;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.control.BICEPSStringMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStringMetricState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.StringMetricState;

public class DefaultBICEPSStringMetricState extends DefaultBICEPSMetricState<StringMetricDescriptor> implements BICEPSStringMetricState {



	public DefaultBICEPSStringMetricState(
			StringMetricDescriptor metric,
			EndpointReference deviceEndpointRef,
			ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
	}

	@Override
	public BICEPSStringMetricControl getControlProxy() {
		return null;
	}
	
	@Override
	public BICEPSStringMetricState getStateProxy() {
		return this;
	}

	@Override
	public StringMetricState getState() {
		StringMetricState retVal=null;
		AbstractMetricState s= super.getState();
		if (s instanceof StringMetricState) retVal=(StringMetricState)s;
		return retVal;
	}




}
