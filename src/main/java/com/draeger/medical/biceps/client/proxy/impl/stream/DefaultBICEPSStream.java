/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.stream;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.StreamListener;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.impl.DefaultAbstractBICEPSProxy;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStreamState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.State;

public class DefaultBICEPSStream extends DefaultAbstractBICEPSProxy<StreamListener> implements BICEPSStream {

	private final RealTimeSampleArrayMetricDescriptor          metric;
	private final ProxyUniqueID clientProxyUniqueID;


	public DefaultBICEPSStream(RealTimeSampleArrayMetricDescriptor metric, EndpointReference deviceEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS)
	{
		super(deviceEndpointRef,comInterface, parentMDS);
		this.metric = metric;
		setInitialValidity(metric);
		ProxyUniqueID tClientProxyUniqueID=null;
		if (isValid())
		{
			tClientProxyUniqueID=ProxyUniqueID.create(getDescriptor().getHandle(), getEndpointReference());
		}
		this.clientProxyUniqueID=tClientProxyUniqueID;
	}



	@Override
	public ProxyUniqueID getProxyUniqueID() {
		return this.clientProxyUniqueID;
	}

	@Override
	public String getHandleOnEndpointReference() {
		return BICEPSProxyUtil.createHandleOnEndpointReference( getEndpointReference());
	}


	@Override
	public RealTimeSampleArrayMetricDescriptor getDescriptor() {
		return this.metric;
	}

	@Override
	public boolean isStateValid() {
		return false;
	}

	@Override
	public BICEPSStreamState getStateProxy() {
		return null;
	}


	@Override
	public void removed() {
		if (isValid())
		{
			setValid(false);
			for (StreamListener listener : getListeners())
			{
				listener.removedStream(this);
			}
			handleProxyRemove();
		}
	}

	@Override
	public void subscriptionEnded(SubscriptionEndCodeType reason) 
	{
		//void
	}
	
	@Override
	public void requestStateChanged(long transactionId, long sequenceNumber,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		//void
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) 
	{
		//void
	}

}
