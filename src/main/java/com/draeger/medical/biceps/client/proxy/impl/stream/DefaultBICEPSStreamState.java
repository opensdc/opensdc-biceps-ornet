/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.impl.stream;

import java.util.ArrayList;
import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.proxy.ProxyCommunication;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.StreamListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStreamState;
import com.draeger.medical.biceps.client.proxy.utils.BICEPSProxyUtil;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.common.utils.RingBufferModel;

public class DefaultBICEPSStreamState extends DefaultBICEPSStream implements BICEPSStreamState {

	private final RingBufferModel<RealTimeSampleArrayMetricState> stateBuffer=new RingBufferModel<RealTimeSampleArrayMetricState>(20);

	public DefaultBICEPSStreamState(RealTimeSampleArrayMetricDescriptor metric,
			EndpointReference deviceEndpointRef, ProxyCommunication comInterface, BICEPSMedicalDeviceSystem parentMDS) {
		super(metric, deviceEndpointRef, comInterface, parentMDS);
	}

	@Override
	public RealTimeSampleArrayMetricState getState() {
		//return currentState;
		synchronized(this.stateBuffer)
		{
			return stateBuffer.head();
		}
	}

	protected void addStreamState(RealTimeSampleArrayMetricState state){
		//this.currentState=state;
		synchronized(this.stateBuffer)
		{
			this.stateBuffer.add(state);
		}
	}

	protected void clearStateBuffer()
	{
		synchronized(this.stateBuffer)
		{
			this.stateBuffer.clear();
		}
	}
	
	@Override
	public List<RealTimeSampleArrayMetricState> getBuffer()
	{
		List<RealTimeSampleArrayMetricState> retVal=new ArrayList<RealTimeSampleArrayMetricState>();
		
		synchronized(this.stateBuffer)
		{
			Object[] bufferContent= this.stateBuffer.toArray();
			if (bufferContent!=null){
				//System.out.println(bufferContent.length+" "+Arrays.toString(bufferContent));
				for (Object object : bufferContent) {
					retVal.add((RealTimeSampleArrayMetricState) object);
				}
			}
			
		}
		return retVal;
	}


	@Override
	public boolean isStateValid() {
		return (getState()!=null);
	}

	@Override
	public boolean isModifiable() {
		return (getControlProxy()!=null);
	}

	@Override
	public BICEPSMetricControl getControlProxy() {
		return null;
	}

	@Override
	public BICEPSStreamState getStateProxy() {
		return this;
	}

	public void changed(RealTimeSampleArrayMetricState s) {
		if (isValid())
		{
			addStreamState(s);
			for (StreamListener l : getListeners())
			{
				l.receivedStreamData(this, s.getObservedValue());
			}
		}
	}

	@Override
	public void changed(State newState, long sequenceNumber, List<ChangedProperty> changedProps) {
		if (newState instanceof RealTimeSampleArrayMetricState)
		{
			this.changed((RealTimeSampleArrayMetricState)newState);
		}
		super.changed(newState,sequenceNumber, changedProps);
	}

	@Override
	public void removed() {
		addStreamState(null);
		super.removed();
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.BICEPSStateProxy#touch()
	 */
	@Override
	public boolean touch() {
		RealTimeSampleArrayMetricState latestStateFromDevice = BICEPSProxyUtil.getCurrentStateFromDevice(
				this,
				getProxyCom().getCommunicationAdapter(), RealTimeSampleArrayMetricState.class);
		
		return (latestStateFromDevice!=null);
	}



}
