package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSSingleStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertConditionListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertConditionControl;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertCondition;

public interface BICEPSAlertConditionState extends BICEPSSingleStateProxy {
	public abstract void subscribe(AlertConditionListener callback);
    public abstract void unsubscribe(AlertConditionListener callback);
	@Override
	public abstract CurrentAlertCondition getState();
	public abstract void changed(CurrentAlertCondition s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSAlertConditionState getStateProxy();
	@Override
	public abstract  BICEPSAlertConditionControl getControlProxy();
	@Override
	public abstract AlertConditionDescriptor getDescriptor();
}
