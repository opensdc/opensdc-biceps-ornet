package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSSingleStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSignalListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSignalControl;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertSignal;

public interface BICEPSAlertSignalState extends BICEPSSingleStateProxy {
	public abstract void subscribe(AlertSignalListener callback);
    public abstract void unsubscribe(AlertSignalListener callback);
	@Override
	public abstract CurrentAlertSignal getState();
	@Override
	public abstract AlertSignalDescriptor getDescriptor();
	public abstract void changed(CurrentAlertSignal s,long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSAlertSignalState getStateProxy();
	@Override
	public abstract BICEPSAlertSignalControl getControlProxy();
}
