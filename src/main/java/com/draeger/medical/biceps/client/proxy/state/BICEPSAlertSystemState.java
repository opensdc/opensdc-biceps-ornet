/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSSingleStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSystemListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSystemControl;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertSystem;

public interface BICEPSAlertSystemState extends BICEPSSingleStateProxy {
	public abstract void subscribe(AlertSystemListener callback);
    public abstract void unsubscribe(AlertSystemListener callback);
	@Override
	public abstract CurrentAlertSystem getState();
	@Override
	public abstract AlertSystemDescriptor getDescriptor();
	public abstract void changed(CurrentAlertSystem s,long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public BICEPSAlertSystemState getStateProxy();
	@Override
	public BICEPSAlertSystemControl getControlProxy();
}
