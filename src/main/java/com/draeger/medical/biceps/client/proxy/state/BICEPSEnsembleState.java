package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.EnsembleListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSEnsembleControl;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;

public interface BICEPSEnsembleState extends BICEPSMDSContextElementState<BICEPSEnsembleState, EnsembleListener> {
	
	@Override
	public abstract void changed(AbstractIdentifiableContextState s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSEnsembleState getStateProxy();
	@Override
	public abstract BICEPSEnsembleControl getControlProxy();
		
	@Override
	public abstract EnsembleContextDescriptor getDescriptor();

	
}
