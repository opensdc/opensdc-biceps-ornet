/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.AlertConditionListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.control.BICEPSLimitAlertConditionControl;
import com.draeger.medical.biceps.common.model.CurrentLimitAlertCondition;
import com.draeger.medical.biceps.common.model.LimitAlertConditionDescriptor;

public interface BICEPSLimitAlertConditionState extends BICEPSAlertConditionState {
	@Override
	public abstract void subscribe(AlertConditionListener callback);
    @Override
	public abstract void unsubscribe(AlertConditionListener callback);
	@Override
	public abstract CurrentLimitAlertCondition getState();
	@Override
	public abstract LimitAlertConditionDescriptor getDescriptor();
	public abstract void changed(CurrentLimitAlertCondition s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSLimitAlertConditionState getStateProxy();
	@Override
	public abstract BICEPSLimitAlertConditionControl getControlProxy();
}
