/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.LocationListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSLocationControl;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;

public interface BICEPSLocationState extends BICEPSMDSContextElementState<BICEPSLocationState,LocationListener> {

	@Override
	public abstract void changed(AbstractIdentifiableContextState s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSLocationState getStateProxy();
	@Override
	public abstract BICEPSLocationControl getControlProxy();
		
	@Override
	public abstract LocationContextDescriptor getDescriptor();

}
