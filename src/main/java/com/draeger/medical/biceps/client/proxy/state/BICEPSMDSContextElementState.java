/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.MDSContextElementListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMDSContextElementControl;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;

public interface BICEPSMDSContextElementState<E extends BICEPSMDSContextElementState<?,?>,T extends MDSContextElementListener<E>> extends BICEPSStateProxy {
	
    public abstract void subscribe(T callback);
    public abstract void unsubscribe(T callback);
	public abstract void changed(AbstractIdentifiableContextState s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public E getStateProxy();
	@Override
	public boolean isModifiable();
	@Override
	public BICEPSMDSContextElementControl<E,T> getControlProxy();
	
	@Override
	public abstract AbstractContextDescriptor getDescriptor();
//	public abstract ContextAssociationStateValue getContextElementAssociation();
//	public abstract AbstractIdentifiableContextState getContextElementItem();
	public abstract List<AbstractIdentifiableContextState> getAllContextElementItems();
	
	
}
