/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSSingleStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.MetricListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;

public interface BICEPSMetricState extends BICEPSSingleStateProxy 
{
    public abstract void subscribe(MetricListener callback);
    public abstract void unsubscribe(MetricListener callback);
	@Override
	public abstract AbstractMetricState getState();
	@Override
	public abstract MetricDescriptor getDescriptor();
	public abstract void changed(AbstractMetricState s, long sequenceNumber,List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSMetricState getStateProxy();
	@Override
	public abstract BICEPSMetricControl getControlProxy();
}
