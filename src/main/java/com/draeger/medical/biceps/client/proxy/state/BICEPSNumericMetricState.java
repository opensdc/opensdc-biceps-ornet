/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;

public interface BICEPSNumericMetricState extends BICEPSMetricState 
{
	@Override
	public abstract NumericMetricDescriptor getDescriptor();
	@Override
	public abstract NumericMetricState getState();
	@Override
	public abstract BICEPSNumericMetricState getStateProxy();
	@Override
	public abstract BICEPSNumericMetricControl getControlProxy();
}
