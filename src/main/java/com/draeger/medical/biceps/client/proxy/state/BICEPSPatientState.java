/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.PatientListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSPatientControl;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;

public interface BICEPSPatientState extends BICEPSMDSContextElementState<BICEPSPatientState,PatientListener> {

	@Override
	public abstract void changed(AbstractIdentifiableContextState s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSPatientState getStateProxy();
	@Override
	public abstract BICEPSPatientControl getControlProxy();
		
	@Override
	public abstract PatientAssociationDescriptor getDescriptor();
//	@Override
//	public abstract ContextAssociationStateValue getContextElementAssociation();
//	@Override
//	public abstract PatientContextState getContextElementItem();
	@Override
	public abstract List<AbstractIdentifiableContextState> getAllContextElementItems();
}
