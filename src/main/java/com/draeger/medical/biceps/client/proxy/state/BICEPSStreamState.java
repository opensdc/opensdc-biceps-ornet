/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSSingleStateProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.StreamListener;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;

public interface BICEPSStreamState extends BICEPSSingleStateProxy{
    public abstract void subscribe(StreamListener callback);
    public abstract void unsubscribe(StreamListener callback);
    
    @Override
	public abstract RealTimeSampleArrayMetricState getState();
    public abstract List<RealTimeSampleArrayMetricState> getBuffer();
    @Override
	public abstract RealTimeSampleArrayMetricDescriptor getDescriptor();
    
    @Override
	public abstract BICEPSStreamState getStateProxy();
}
