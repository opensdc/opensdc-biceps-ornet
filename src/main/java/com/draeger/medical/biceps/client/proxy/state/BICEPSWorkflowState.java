package com.draeger.medical.biceps.client.proxy.state;

import java.util.List;

import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.WorkflowListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSWorkflowControl;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;

public interface BICEPSWorkflowState extends BICEPSMDSContextElementState<BICEPSWorkflowState, WorkflowListener> {
	@Override
	public abstract void changed(AbstractIdentifiableContextState s, long sequenceNumber, List<ChangedProperty> changedProps);
	@Override
	public abstract BICEPSWorkflowState getStateProxy();
	@Override
	public abstract BICEPSWorkflowControl getControlProxy();
		
	@Override
	public abstract WorkflowContextDescriptor getDescriptor();

}
