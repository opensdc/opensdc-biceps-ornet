/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.utils;

import com.draeger.medical.biceps.client.proxy.BICEPSProxyManager;
import com.draeger.medical.biceps.client.proxy.impl.DefaultProxyFactory;


public class BICEPSProxyManagerProvider {

	private static final BICEPSProxyManagerProvider instance=new BICEPSProxyManagerProvider();
	
	private BICEPSProxyManager factory=new DefaultProxyFactory(BICEPSProxyStoreProvider.getInstance().getStore()); //TODO Ssch resolve dependency
	
	private BICEPSProxyManagerProvider()
	{
		//void
	}
	
	public static BICEPSProxyManagerProvider getInstance() {
		return instance;
	}
	
	public BICEPSProxyManager getFactory()
	{
		return factory;
	}


}
