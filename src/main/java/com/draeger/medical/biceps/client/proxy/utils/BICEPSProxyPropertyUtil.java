/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.utils;

import java.util.ArrayList;
import java.util.List;

import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.PatientContextState;
import com.draeger.medical.biceps.common.model.State;

public class BICEPSProxyPropertyUtil {

	public static List<ChangedProperty> findPropertyChanged(
			BICEPSMetric relatedBICEPSClientMetric,
			State state) {
		return null;
	}
	
	public static List<ChangedProperty> findPropertyChanged(
			BICEPSProxy diceProxy,
			State state) {

		List<ChangedProperty> changedProps=new ArrayList<ChangedProperty>(1);
		if (state instanceof OperationState)
		{
			changedProps.add(ChangedProperty.OPERATION_STATE);	
		}else if (state instanceof AbstractMetricState){
			changedProps.add(ChangedProperty.OBJECT_STATE);
		}else if (state instanceof AbstractAlertState){
			changedProps.add(ChangedProperty.OBJECT_STATE);
		}else if (state instanceof PatientContextState){
			changedProps.add(ChangedProperty.OBJECT_STATE);
		}else if (state instanceof AbstractIdentifiableContextState){
			changedProps.add(ChangedProperty.OBJECT_STATE);
		}
		return changedProps;
	}

}
