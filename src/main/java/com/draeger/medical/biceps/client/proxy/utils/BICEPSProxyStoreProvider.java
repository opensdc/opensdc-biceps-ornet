/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.utils;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.BICEPSProxyStore;
import com.draeger.medical.biceps.client.proxy.ProxyStoreProvider;
import com.draeger.medical.biceps.client.proxy.impl.DefaultProxyStore;


public class BICEPSProxyStoreProvider implements ProxyStoreProvider {

	private static final ProxyStoreProvider instance=new BICEPSProxyStoreProvider();

	private final BICEPSProxyStore proxyStore;

	private BICEPSProxyStoreProvider()
	{
		String factoryClassName=System.getProperty("BICEPS.Client.BICEPSProxyStore");

		BICEPSProxyStore tProxyStore=null;
		if (factoryClassName == null) {
			tProxyStore=createDefault();
		}else{
			try {
				Class<?> factoryClass = Class.forName(factoryClassName);
				tProxyStore = (BICEPSProxyStore) factoryClass.newInstance();
				if (Log.isDebug()) {
					Log.debug("Using BICEPSProxyStore [" + factoryClassName + "]");
				}
			} catch (ClassNotFoundException e) {
				if (Log.isError())
				{
					Log.error("BICEPSProxyStore: Configured BICEPSProxyStore class [" + factoryClassName + "] not found, falling back to default implementation");
					Log.error(e);
				}
				tProxyStore = createDefault();
			} catch (Exception e) {
				if (Log.isError())
				{
					Log.error("BICEPSProxyStore: Unable to create instance of configured BICEPSProxyStore class [" + factoryClassName + "], falling back to default implementation");
					Log.error(e);
				}
				tProxyStore = createDefault();
			}
		}

		proxyStore=tProxyStore;
		//void
	}

	private BICEPSProxyStore createDefault() {
		return new DefaultProxyStore(); //TODO SSch remove dependency
	}

	public static ProxyStoreProvider getInstance() {
		return instance;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.client.proxy.ProxyStoreProvider#getStore()
	 */
	@Override
	public BICEPSProxyStore getStore()
	{
		return proxyStore;
	}


}
