/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.proxy.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.AttributedURI;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.CommunicationContainer;
import com.draeger.medical.biceps.client.proxy.BICEPSProxy;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.VMDDescriptor;

public class BICEPSProxyUtil {







	public static String createHandleOnEndpointReference(
			EndpointReference deviceEndpointReference) {
		String retVal=null;
		if (deviceEndpointReference!=null && deviceEndpointReference.getAddress()!=null)
			retVal=deviceEndpointReference.getAddress().toString();

		return retVal;
	}
	
	public static <T extends State> List<T> getCurrentStatesFromDevice(BICEPSProxy proxy,CommunicationAdapter comAdapter, Class<T> stateClass) {

		//	public static <T> T getCurrentStateFromDevice(EndpointReference deviceEPR, String descriptorHandle, CommunicationAdapter comAdapter, Class<T> stateClass) {
		List<T> newStates=null;
		BigInteger mdibSequenceNumber=null;

		if (proxy!=null)
		{
			String descriptorHandle=proxy.getDescriptor().getHandle();
			
			newStates=new ArrayList<T>();
			mdibSequenceNumber = retrieveStatesInternal(proxy, comAdapter,
					stateClass, newStates, mdibSequenceNumber, descriptorHandle);
			if (mdibSequenceNumber!=null)
			{
				for (T newState : newStates) {
					 if (newState!=null)
						 updateBICEPSProxy(newState, proxy, mdibSequenceNumber);	
				}
			}
		}
		return newStates;
	}
	
	public static <T extends State> T getCurrentStateFromDevice(BICEPSProxy proxy,CommunicationAdapter comAdapter, Class<T> stateClass) {
		List<T> newStates=new ArrayList<T>();
		BigInteger mdibSequenceNumber=null;

		T newState=null;
		if (proxy!=null)
		{
			String descriptorHandle=proxy.getDescriptor().getHandle();
			
			mdibSequenceNumber = retrieveStatesInternal(proxy, comAdapter,
					stateClass, newStates, mdibSequenceNumber, descriptorHandle);
			
				for (T nState : newStates) {
					if (nState!=null){
						newState=nState;
						updateBICEPSProxy(newState, proxy, mdibSequenceNumber);
						break;
					}
				}
		}
		return newState;
	}

	public static <T extends State> BigInteger retrieveStatesInternal(
			BICEPSProxy proxy, CommunicationAdapter comAdapter,
			Class<T> stateClass, List<T> newStates,
			BigInteger mdibSequenceNumber, String descriptorHandle) {
		if (comAdapter!=null && descriptorHandle!=null)
		{

			try {
				CommunicationContainer<MDState, ?> retrievedStates = comAdapter.retrieveStates(getDeviceReferenceFromEPR(proxy.getEndpointReference()),descriptorHandle);

				mdibSequenceNumber=BigInteger.valueOf(retrievedStates.getSequenceNumber());
				
				MDState mdStates = retrievedStates.getMessageElem();
				if (mdStates!=null)
				{
					
					List<State> states = mdStates.getStates();
					if (states!=null)
					{
						for (State state : states) {
							if (state !=null)
								if (descriptorHandle.equals(state.getReferencedDescriptor()))
								{
									try{
										if (stateClass.isAssignableFrom(state.getClass())){
											T newState=stateClass.cast(state);
											newStates.add(newState);
										}
									}catch(ClassCastException cce){
										//void
									}
								}
						}
					}
				}
			} catch (Exception e) {
				if (Log.isInfo())
				{
					Log.info("Communition exception while retrieving state information for handle "+proxy.getProxyUniqueID()+" on device "+proxy.getEndpointReference()+":"+e.getMessage());
					Log.info(e);
				}

			} 
		}
		return mdibSequenceNumber;
	}



//	public static <T extends State> T getCurrentStateFromDevice(BICEPSProxy proxy,CommunicationAdapter comAdapter, Class<T> stateClass) {
//
//		//	public static <T> T getCurrentStateFromDevice(EndpointReference deviceEPR, String descriptorHandle, CommunicationAdapter comAdapter, Class<T> stateClass) {
//		T newState=null;
//		BigInteger mdibSequenceNumber=null;
//
//		if (proxy!=null)
//		{
//			String descriptorHandle=proxy.getDescriptor().getHandle();
//			
//			if (comAdapter!=null && descriptorHandle!=null)
//			{
//
//				try {
//					CommunicationContainer<MDState, ?> retrievedStates = comAdapter.retrieveStates(getDeviceReferenceFromEPR(proxy.getEndpointReference()),descriptorHandle);
//
//					mdibSequenceNumber=BigInteger.valueOf(retrievedStates.getSequenceNumber());
//					
//					MDState mdStates = retrievedStates.getMessageElem();
//					if (mdStates!=null)
//					{
//						
//						List<State> states = mdStates.getStates();
//						if (states!=null)
//						{
//							for (State state : states) {
//								if (state !=null)
//									if (descriptorHandle.equals(state.getReferencedDescriptor()))
//									{
//										try{
//											if (stateClass.isAssignableFrom(state.getClass())){
//												newState=stateClass.cast(state);
//												break;
//											}
//										}catch(ClassCastException cce){
//											//void
//										}
//									}
//							}
//						}
//					}
//				} catch (Exception e) {
//					if (Log.isInfo())
//					{
//						Log.info("Communition exception while retrieving state information for handle "+proxy.getProxyUniqueID()+" on device "+proxy.getEndpointReference()+":"+e.getMessage());
//						Log.info(e);
//					}
//
//				} 
//			}
//			
//			if (newState!=null)
//			{
//				
//				updateBICEPSProxy(newState, proxy, mdibSequenceNumber);
//			}
//		}
//		return newState;
//	}

	private static final DeviceReference getDeviceReferenceFromEPR(
			EndpointReference networkDeviceEndpoint) {
		return DeviceServiceRegistryProvider.getInstance()
				.getDeviceServiceRegistry()
				.getStaticDeviceReference(networkDeviceEndpoint);
	}


	public static void updateBICEPSProxy(State newState, BICEPSProxy diceProxy, BigInteger sequenceNumber) 
	{
		if (newState!=null)
		{
			//Get changed properties
			List<ChangedProperty> changedProps = BICEPSProxyPropertyUtil.findPropertyChanged(diceProxy,newState);

			// notify, the metric will change itself

			diceProxy.changed(newState,sequenceNumber!=null?sequenceNumber.longValue():-1, changedProps);
		}	
	}

	public static boolean isNumericMetric(BICEPSMetric metric) {
		boolean retVal=false;
		if (metric!=null)
			retVal=(metric.getDescriptor() instanceof NumericMetricDescriptor);

		return retVal;
	}

	public static boolean isStringMetric(BICEPSMetric metric) {
		boolean retVal=false;
		if (metric!=null)
			retVal=(metric.getDescriptor() instanceof StringMetricDescriptor);

		return retVal;
	}

	public static boolean isWaveformMetric(BICEPSMetric metric) {
		boolean retVal=false;
		if (metric!=null)
			retVal=(metric.getDescriptor() instanceof RealTimeSampleArrayMetricDescriptor);

		return retVal;
	}



	public static CodedValue getCode(Descriptor descriptor) {
		CodedValue retVal=null;
		if (descriptor instanceof  MetricDescriptor)
		{
			retVal= ((MetricDescriptor) descriptor).getType();
		}else if (descriptor instanceof  AlertConditionDescriptor)
		{
			retVal= ((AlertConditionDescriptor) descriptor).getType();
		}else if (descriptor instanceof  ActivateOperationDescriptor){
			retVal= ((ActivateOperationDescriptor) descriptor).getType();
		}else if (descriptor instanceof MDSDescriptor){
			retVal= ((MDSDescriptor) descriptor).getType();
		}else if (descriptor instanceof VMDDescriptor){
			retVal= ((VMDDescriptor) descriptor).getType();
		}else if (descriptor instanceof ChannelDescriptor){
			retVal= ((ChannelDescriptor) descriptor).getType();
		}
		if (retVal==null && descriptor!=null)
		{
			if (Log.isWarn()) Log.warn("Descriptor of class "+descriptor.getClass()+" does not provide a type code.");
		}
		return retVal;
	}

	public static String getHandle(BICEPSProxy proxy) {
		String proxyHandle=null;
		if (proxy!=null && proxy.getDescriptor()!=null)
			proxyHandle=proxy.getDescriptor().getHandle();

		return proxyHandle;
	}

	public static EndpointReference getEndpointReferenceFromURIString(
			String endpointStr) 
	{
		return new EndpointReference(new AttributedURI(endpointStr));
	}
}
