package com.draeger.medical.biceps.client.proxy.utils;

import java.util.StringTokenizer;

import org.ws4d.java.types.EndpointReference;

public class ProxyUniqueID {
	
	private static final String DELIM="@";
	
	private final String localHandle;
	private final EndpointReference networkEndpoint;
	
	private ProxyUniqueID(String localHandle, EndpointReference networkEndpoint) {
		super();
		this.localHandle = localHandle;
		this.networkEndpoint = networkEndpoint;
	}
	
	
	public String getLocalHandle() {
		return localHandle;
	}

	public EndpointReference getNetworkEndpoint() {
		return networkEndpoint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((localHandle == null) ? 0 : localHandle.hashCode());
		result = prime * result
				+ ((networkEndpoint == null) ? 0 : networkEndpoint.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProxyUniqueID other = (ProxyUniqueID) obj;
		if (localHandle == null) {
			if (other.localHandle != null)
				return false;
		} else if (!localHandle.equals(other.localHandle))
			return false;
		if (networkEndpoint == null) {
			if (other.networkEndpoint != null)
				return false;
		} else if (!networkEndpoint.equals(other.networkEndpoint))
			return false;
		return true;
	}
	
	public String getStringRepresentation()
	{
		String retVal=null;
		
		if (localHandle!=null && networkEndpoint!=null)
		{
			retVal=createProxyUniqueID(localHandle, BICEPSProxyUtil.createHandleOnEndpointReference(networkEndpoint));
		}
		return retVal;
	}
	
	public static ProxyUniqueID valueOf(String proxyUniqueId)
	{
		ProxyUniqueID retVal=null;
		if (proxyUniqueId!=null)
		{
			StringTokenizer tokenizer=new StringTokenizer(proxyUniqueId, DELIM);
			if (tokenizer.countTokens()==2)
			{
				String handleStr = tokenizer.nextToken();
				String endpointStr = tokenizer.nextToken();
				retVal=ProxyUniqueID.create(handleStr, BICEPSProxyUtil.getEndpointReferenceFromURIString(endpointStr));
			}
		}
		return retVal; 
	}
	
	public static ProxyUniqueID create(String localHandle, EndpointReference networkEndpoint)
	{
		ProxyUniqueID retVal=null;
		if (localHandle!=null && networkEndpoint!=null)
		{
			retVal= new ProxyUniqueID(localHandle, networkEndpoint);	
		}
		return retVal;
	}
	
	public static String getProxyLocalHandle(String proxyUniqueID) {
		String retVal=null;
		if (proxyUniqueID!=null)
		{
			StringTokenizer tokenizer=new StringTokenizer(proxyUniqueID, DELIM);
			if (tokenizer.countTokens()==2)
			{
				retVal=tokenizer.nextToken();
			}
		}
		return retVal;
	}
	
	private static String createProxyUniqueID(String handle,String handleOnEndpointReference){
		return handle+DELIM+handleOnEndpointReference;
	}
	
	@Override
	public String toString() {
		return "ProxyUniqueID [localHandle=" + localHandle
				+ ", networkEndpoint=" + networkEndpoint + "]";
	}
	
	
	
	
}
