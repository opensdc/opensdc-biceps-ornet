/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.reference.ServiceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientDualChannelValuePair;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientSafetyInformation;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInfoItem;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractGet;
import com.draeger.medical.biceps.common.model.AbstractGetResponse;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.Activate;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.GetContainmentTree;
import com.draeger.medical.biceps.common.model.GetContextStates;
import com.draeger.medical.biceps.common.model.GetDescriptor;
import com.draeger.medical.biceps.common.model.GetMDDescription;
import com.draeger.medical.biceps.common.model.GetMDIB;
import com.draeger.medical.biceps.common.model.GetMDState;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.SetAlertState;
import com.draeger.medical.biceps.common.model.SetAlertStateOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextState;
import com.draeger.medical.biceps.common.model.SetPatientState;
import com.draeger.medical.biceps.common.model.SetRange;
import com.draeger.medical.biceps.common.model.SetRangeOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetString;
import com.draeger.medical.biceps.common.model.SetStringOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetValue;
import com.draeger.medical.biceps.common.model.SetValueOperationDescriptor;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSMultiOperationsQoSPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSingleOperationQoSPolicy;
import com.draeger.medical.mdpws.domainmodel.MDPWSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyInterceptionDirection;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.DualChannel;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyContext;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyContextValue;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyInformation;
public class InvokeHelper 
{

	@Deprecated
	public static AbstractSetResponse invoke(ServiceReference svcRef, AbstractSet params, BICEPSClientTransmissionInformationContainer transmissionInfo) throws TimeoutException, InvocationException
	{
		return invoke(svcRef,params,transmissionInfo,null);
	}

	public static AbstractSetResponse invoke(ServiceReference svcRef, AbstractSet params, BICEPSClientTransmissionInformationContainer transmissionInfo, BICEPSSafetyInformationPolicyElement safetyInformationPolicyElement) throws TimeoutException, InvocationException
	{
		AbstractSetResponse result = null;

		String inputAction = getInputAction(params);
		if (inputAction == null)
		{
			if (Log.isWarn())Log.warn("BICEPSClient: Invoke unsupported parameter type " + params.getClass());
			return null;
		}



		Operation op = svcRef.getService().getOperation(inputAction);
		if (op != null)
		{			
			IParameterValue requestParams = getParameterValuefromObject(params);

			if (requestParams != null)
			{
				if (op instanceof MDPWSOperation)
				{
					MDPWSOperation mdpwsOp = (MDPWSOperation) op;

					MDPWSMessageContextMap msgContextMap=convertTransmissionInformation2MessageContextMap(transmissionInfo, safetyInformationPolicyElement);
					MDPWSMessageContextMap replyMsgContextMap= new MDPWSMessageContextMap();

					result = invokeMDPWSOperation(requestParams, mdpwsOp, msgContextMap, replyMsgContextMap);
					if (result != null && result.getInvocationState() != InvocationState.FAILED && !replyMsgContextMap.isEmpty())
					{
						checkRepliedMessageContext(replyMsgContextMap);
					}

				}else
				{
					IParameterValue resultPayload = op.invoke(requestParams);
					result = getObjectFromParameterValue(resultPayload);
				}
			}
		}
		return result;
	}

	


	@SuppressWarnings({ "rawtypes" })
	private static MDPWSMessageContextMap convertTransmissionInformation2MessageContextMap(BICEPSClientTransmissionInformationContainer transmissionInfo, BICEPSSafetyInformationPolicyElement safetyInformationPolicyElement) 
	{
		MDPWSMessageContextMap retVal=null;
		if (transmissionInfo!=null && safetyInformationPolicyElement!=null)
		{
			java.util.Iterator<BICEPSClientTransmissionInfoItem> transmissionInfoItems= transmissionInfo.iterator();

			QoSMessageContext qosCtxt=null;
			while (transmissionInfoItems.hasNext())
			{
				BICEPSClientTransmissionInfoItem transmissionInfoItem = transmissionInfoItems.next();

				if (transmissionInfoItem instanceof BICEPSClientSafetyInformation)
				{
					BICEPSClientSafetyInformation clientSafetyInformation=(BICEPSClientSafetyInformation)transmissionInfoItem;
					SafetyInformation safetyInformationValue=new SafetyInformation();

					if ( clientSafetyInformation.getDualChannel()!=null && clientSafetyInformation.getDualChannel().getDualChannelValues()!=null )
					{
						HashMap<QName, BICEPSClientDualChannelValuePair> clientDualChannelValues = clientSafetyInformation.getDualChannel().getDualChannelValues();

						if (clientDualChannelValues.size()>0)
						{
							List<DualChannel> dualChannels=new ArrayList<DualChannel>(clientDualChannelValues.size());
							for (BICEPSClientDualChannelValuePair dualChannel : clientDualChannelValues.values()) {
								dualChannels.add(new DualChannel<Float,Float,QName>(dualChannel.getFirstChannel(),dualChannel.getSecondChannel(),dualChannel.getValueElementSelector()));
							}

							safetyInformationValue.setDualChannels(dualChannels);
						}

					} 

					if (clientSafetyInformation.getSafetyContext()!=null && !clientSafetyInformation.getSafetyContext().getAttributeValues().isEmpty())
					{
						HashMap<QName, Object> attrValues = clientSafetyInformation.getSafetyContext().getAttributeValues();
						ArrayList<SafetyContext> safetyContexts=new ArrayList<SafetyContext>();
						for (Entry<QName, Object> attributeEntry : attrValues.entrySet()) {
							QName attributeQname = attributeEntry.getKey();
							
							IParameterValue ctxtParameterValue= getParameterValuefromObject(attributeEntry.getValue(), attributeQname);
							
							if (ctxtParameterValue!=null)
							{
								SafetyContext safetyContext=new SafetyContext(clientSafetyInformation.getSafetyContext().getContextObjectHandle());
								SafetyContextValue safetyContextValue=new SafetyContextValue(attributeQname, ctxtParameterValue);
								safetyContext.addContextValue(safetyContextValue.getReferencedElementQName(), safetyContextValue);
								safetyContexts.add(safetyContext);
							}else{
								if (Log.isWarn())Log.warn("Could not add safety context. Could not convert value ["+attributeEntry+"] to parameter value.");
							}
						}
						if (!safetyContexts.isEmpty()) safetyInformationValue.setSafetyContexts(safetyContexts);
					}

					SafetyInformationPolicy policy=safetyInformationPolicyElement.getMdpwsPolicy();

					SafetyInformationPolicyToken<SafetyInformation,SafetyInformationPolicy> siToken = new SafetyInformationPolicyToken<SafetyInformation,SafetyInformationPolicy>(policy, safetyInformationValue,QoSPolicyInterceptionDirection.INOUTBOUND);

					if (retVal==null) retVal=new MDPWSMessageContextMap();
					if (qosCtxt==null){
						qosCtxt=new QoSMessageContext();
						retVal.put(qosCtxt.getClass(), qosCtxt);
					}
					qosCtxt.addQoSPolicyToken(siToken);	
				}
			}

		}

		return retVal;
	}


	private static AbstractSetResponse invokeMDPWSOperation(
			IParameterValue in, MDPWSOperation mdpwsOp,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMsgContextMap)
					throws InvocationException, TimeoutException {
		AbstractSetResponse result=null;
		
		IParameterValue response = mdpwsOp.invoke(in,msgContextMap, replyMsgContextMap);

		result = getObjectFromParameterValue(response);
		return result;
	}


	public static AbstractGetResponse invoke(ServiceReference svcRef, AbstractGet params, BICEPSClientTransmissionInformationContainer transmissionInfo) throws TimeoutException, InvocationException
	{
		return invoke(svcRef,params,transmissionInfo,null);
	}

	public static AbstractGetResponse invoke(ServiceReference svcRef, AbstractGet params, BICEPSClientTransmissionInformationContainer transmissionInfo, RawParameterValueHandler handler) throws TimeoutException, InvocationException
	{
		AbstractGetResponse result = null;

		String inputAction = getInputAction(params);
		if (inputAction == null)
		{
			Log.warn("BICEPSClient: Invoke unsupported parameter type " + params.getClass());
			return null;
		}

		Operation op = svcRef.getService().getOperation(inputAction);
		if (op != null)
		{
			IParameterValue in = getParameterValuefromObject(params);
			if (in != null)
			{
				if (op instanceof MDPWSOperation)
				{
					MDPWSOperation mdpwsOp = (MDPWSOperation) op;
					MDPWSMessageContextMap msgContextMap = new MDPWSMessageContextMap();
					MDPWSMessageContextMap replyMsgContextMap = new MDPWSMessageContextMap();
					IParameterValue response = mdpwsOp.invoke(in, msgContextMap,replyMsgContextMap);

					if (handler!=null) handler.handleRawParameterResponse(mdpwsOp, response);

					result = getObjectFromParameterValue(response);

					// TODO SSch Maybe check replied message context map.
					if (result != null && !replyMsgContextMap.isEmpty())
					{
						checkRepliedMessageContext(replyMsgContextMap);
					}
				}
				else
				{
					if (Log.isDebug()) Log.debug("Not a MDPWS Operation: "+inputAction+" "+svcRef.getService().getServiceId()+" "+svcRef.getService().getClass());

					IParameterValue invokeResult = op.invoke(in);

					result = getObjectFromParameterValue(invokeResult);

				}
			}
		}

		return result;
	}


	//TODO SSch SafetyInformation 
	//	public static List<DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>> createPolicyTokens(String inputAction, AbstractSet params, BICEPSClientTransmissionInformation transmissionInfo){
	//		List<DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>> policyTokens = new ArrayList<DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>>();
	//		final String handleExpression = ":OperationHandle/text()";
	//		List<String> valueExpressions = new ArrayList<String>();
	//		List<Object> values = new ArrayList<Object>();
	//		if (params instanceof SetValue)
	//		{
	//			valueExpressions.add(":Value/text()");
	//			values.add(((SetValue) params).getValue());
	//		}
	//		else if (params instanceof SetString)
	//		{
	//			valueExpressions.add(":String/text()");
	//			values.add(((SetString) params).getString());
	//		}
	//		else if (params instanceof SetRange)
	//		{
	//			valueExpressions.add(":Range/text()");
	//			values.add(((SetRange) params).getRange());
	//		}
	//		else if (params instanceof SetCurrentAlertConditionState)
	//		{
	//			//TODO SSch
	//			valueExpressions.add(":State/text()");
	//			values.add(((SetCurrentAlertConditionState) params).getState());
	//		}
	//		else if (params instanceof Activate)
	//		{
	//			// TODO SSch
	//		}
	//		else if (params instanceof SetPatient)
	//		{
	//			valueExpressions.add(":PatientData/text()");
	//			values.add(((SetPatient) params).getPatientData());
	//		}
	//		else if (params instanceof SetPatientState)
	//		{
	//			//TODO SSch refactor
	//			valueExpressions.add(":PatientAssociationState/text()");
	//			values.add(((SetPatientState) params).getPatientAssociationState());
	//		}
	//		else
	//		{
	//			Log.warn("BICEPSClient: createPolicyTokens: Unsupported parameter type "
	//					+ params.getClass());
	//		}
	//
	//		if (!valueExpressions.isEmpty() && valueExpressions.size() < secondChannel.length)
	//		{
	//			Log.warn("BICEPSClient: createPolicyTokens: Unexpected amount of second channel values. Expected "
	//					+ valueExpressions.size() + " but specified " + secondChannel.length);
	//			return policyTokens;
	//		}
	//
	//		org.ws4d.java.structures.ArrayList policyList = QoSPolicyUtil
	//		.getApplicablePoliciesForAction(inputAction);
	//		for (int i = 0; i < policyList.size(); ++i)
	//		{
	//			QoSPolicy policy = (QoSPolicy) policyList.get(i);
	//			if (policy instanceof DualChannelPolicy)
	//			{
	//				DualChannelPolicy dualChannelPolicy = (DualChannelPolicy) policy;
	//				String xPathExpression = dualChannelPolicy.getDualChannelPolicyAttributes()
	//				.getXPath().getExpression();
	//				if (xPathExpression.contains(handleExpression))
	//				{
	//					if(xPathExpression.contains(params.getOperationHandle()))
	//					{
	//						// TODO SSch get second channel value for operation handle
	//						Object secondChannelValue = params.getOperationHandle(); 
	//						String idInfo = "idInfo_" + handleExpression;
	//						policyTokens.add(createPolicyToken(dualChannelPolicy,
	//								params.getOperationHandle(), secondChannelValue, idInfo));
	//					}
	//				}
	//				else
	//				{
	//					for (int j = 0; j < valueExpressions.size(); ++j)
	//					{
	//						if (xPathExpression.contains(valueExpressions.get(j)))
	//						{
	//							String idInfo = "idInfo_" + valueExpressions.get(j);
	//							policyTokens.add(createPolicyToken(dualChannelPolicy, values.get(j),
	//									secondChannel[j], idInfo));
	//						}
	//					}
	//				}
	//			}
	//		}
	//		return policyTokens;
	//	}


	//	public static <C1, C2, I> DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy> createPolicyToken(
	//			DualChannelPolicy dualChannelPolicy, C1 firstChannelValue, C2 secondChannelValue,
	//			I idInfo)
	//			{
	//		DualChannel<?, ?, ?> dualChannelValue;
	//		dualChannelValue = new DualChannel<C1, C2, I>(firstChannelValue, secondChannelValue, idInfo);
	//		DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy> qosPolicyToken;
	//		qosPolicyToken = new DualChannelPolicyToken<DualChannel<?, ?, ?>, DualChannelPolicy>(
	//				dualChannelPolicy, dualChannelValue, QoSPolicyInterceptionDirection.OUTBOUND);
	//		return qosPolicyToken;
	//			}

	private static void checkRepliedMessageContext(MDPWSMessageContextMap replyMsgContextMap)
	{
		//TODO SSch SafetyInformation ????
		//		QoSMessageContext qosMsgContext;
		//		qosMsgContext = (QoSMessageContext) replyMsgContextMap.get(QoSMessageContext.class);
		//		if (qosMsgContext != null)
		//		{
		//			Iterator policyTokenIterator = qosMsgContext.getValidQoSPolicyToken();
		//			while (policyTokenIterator.hasNext())
		//			{
		//				QoSPolicyToken<?, ?> policyToken = (QoSPolicyToken<?, ?>) policyTokenIterator
		//				.next();
		//				if (policyToken instanceof DualChannelPolicyToken<?, ?>
		//				&& policyToken.getTokenState() != QoSPolicyTokenState.VALID)
		//				{
		//					Log.warn("BICEPSClient: Invoke - received not valid DualChannelPolicyToken");
		//					// TODO SSch what to do now?
		//				}
		//			}
		//		}
	}

	public static String getInputAction(AbstractSet params)
	{
		//TODO SSch if-then-else
		String inputAction = null;
		if (params instanceof SetValue)
			inputAction = MDPWSConstants.ACTION_SET_VALUE;
		else if (params instanceof SetString)
			inputAction = MDPWSConstants.ACTION_SET_STRING;
		else if (params instanceof SetRange)
			inputAction = MDPWSConstants.ACTION_SET_RANGE;
		else if (params instanceof SetAlertState)
			inputAction = MDPWSConstants.ACTION_SET_ALERT_STATE;
		else if (params instanceof Activate)
			inputAction = MDPWSConstants.ACTION_ACTIVATE;
		else if (params instanceof SetContextState)
			inputAction = MDPWSConstants.ACTION_SET_CONTEXT_STATE;
		else if (params instanceof SetPatientState)
			inputAction = MDPWSConstants.ACTION_SET_ID_CONTEXT;
		else {
			if (Log.isError()) Log.error("BICEPSClient: getInputAction for AbstractSet: Unsupported parameter type " + params.getClass());
		}
		return inputAction;
	}

	public static String getInputAction(AbstractGet params)
	{
		//TODO SSch if-then-else
		String inputAction = null;
		if (params instanceof GetMDIB)
			inputAction = MDPWSConstants.ACTION_GET_MDIB;
		else if (params instanceof GetMDState)
			inputAction = MDPWSConstants.ACTION_GET_MDSTATE;
		else if (params instanceof GetMDDescription)
			inputAction = MDPWSConstants.ACTION_GET_MDDESCRIPTION;
		else if (params instanceof GetDescriptor)
			inputAction = MDPWSConstants.ACTION_GET_DESCRIPTOR;
		else if (params instanceof GetContainmentTree)
			inputAction = MDPWSConstants.ACTION_GET_CONTAINMENTTREE;
		//		else if (params instanceof GetMetrics)
		//			inputAction = MDPWSConstants.ACTION_GET_METRICS;
		//		else if (params instanceof GetAlerts)
		//			inputAction = MDPWSConstants.ACTION_GET_ALERTS;
		//		else if (params instanceof GetMetricStates)
		//			inputAction = MDPWSConstants.ACTION_GET_METRIC_STATES;
		//
		//		else if (params instanceof GetAlertStates)
		//			inputAction = MDPWSConstants.ACTION_GET_ALERT_STATES;
		else if (params instanceof GetContextStates)
			inputAction = MDPWSConstants.ACTION_GET_CONTEXT_STATES;
//		else if (params instanceof GetContextStates)
//			inputAction = MDPWSConstants.ACTION_GET_ID_CONTEXT_STATES;
		else{ 
			if (Log.isError()) Log.error("BICEPSClient: getInputAction for AbstractGet: Unsupported parameter type " + params.getClass());
		}
		return inputAction;
	}

	public static String getInputAction(OperationDescriptor op)
	{
		//TODO SSch if-then-else
		String inputAction = null;
		if (op instanceof SetValueOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_VALUE;
		else if (op instanceof SetStringOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_STRING;
		else if (op instanceof SetRangeOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_RANGE;
		else if (op instanceof SetAlertStateOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_ALERT_STATE;
		else if (op instanceof ActivateOperationDescriptor) inputAction = MDPWSConstants.ACTION_ACTIVATE;
		else if (op instanceof SetContextOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_CONTEXT_STATE;
		//		else if (op instanceof SetIdentifiableContextOperationDescriptor) inputAction = MDPWSConstants.ACTION_SET_ID_CONTEXT;
		else Log.warn("BICEPSClient: Unsupported operationDescriptor type " + op.getClass());
		return inputAction;
	}

	public static interface RawParameterValueHandler
	{

		void handleRawParameterResponse(MDPWSOperation mdpwsOp, IParameterValue response);

	}

	public static Map<Class<?>,BICEPSQoSPolicy> getApplicablePolicies(EndpointReference deviceEPR, OperationDescriptor operationDescriptor, HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation, Class<?> policyClassFilter) 
	{
		HashMap<Class<?>,BICEPSQoSPolicy> applicablePolicies=new HashMap<Class<?>,BICEPSQoSPolicy>();

		String inputAction = getInputAction(operationDescriptor);
		Set<BICEPSQoSPolicy> policiesForAction=getPoliciesForAction(deviceEPR,inputAction,safetyInformation, operationDescriptor);
		if (policiesForAction!=null && policiesForAction.size()>0)
		{
			for (BICEPSQoSPolicy diceQoSPolicy : policiesForAction) {
				if (policyClassFilter==null || policyClassFilter.isAssignableFrom(diceQoSPolicy.getClass()))
				{
					if (diceQoSPolicy instanceof BICEPSMultiOperationsQoSPolicy)
					{
						BICEPSMultiOperationsQoSPolicy p=(BICEPSMultiOperationsQoSPolicy)diceQoSPolicy;
						if (p.getOperationHandles()!=null && p.getOperationHandles().contains(operationDescriptor.getHandle()))
						{
							applicablePolicies.put(p.getClass(), p);
						}
					}else if (diceQoSPolicy instanceof BICEPSSingleOperationQoSPolicy){
						BICEPSSingleOperationQoSPolicy s=(BICEPSSingleOperationQoSPolicy)diceQoSPolicy;
						if (operationDescriptor.getHandle().equals(s.getOperationHandle()))
						{
							applicablePolicies.put(s.getClass(), s);
						}
					}
				}
			}
		}

		return applicablePolicies;
	}


	private static Set<BICEPSQoSPolicy> getPoliciesForAction(EndpointReference deviceEPR,String inputAction, HashMap<String, BICEPSSafetyInformationPolicyElement> safetyInformation, OperationDescriptor operationDescriptor) {

		HashSet<BICEPSQoSPolicy> retVal=new  HashSet<BICEPSQoSPolicy>(); 

		if (safetyInformation!=null)
		{
			BICEPSSafetyInformationPolicyElement sInformation = safetyInformation.get(operationDescriptor.getHandle());
			if (sInformation!=null && sInformation.getMdpwsPolicy()!=null && sInformation.getSafetyInformationPolicy()!=null){
				SafetyInformationPolicy safetyPolicy = sInformation.getMdpwsPolicy();
				if ( QoSPolicyUtil.containsSubjectForDevice(deviceEPR, safetyPolicy.getSubjects()) && QoSPolicyUtil.isPolicyApplicableForThisMessage(inputAction, safetyPolicy))
					retVal.add(sInformation.getSafetyInformationPolicy());
			}
		}
		return retVal;
	}


	private static String getSchemaElementNameFromClass(
			Class<?> clazz) {
		String schemaElementName=getXmlRootElementQName(clazz);

		return schemaElementName;
	}


	private static String getXmlRootElementQName(Class<?> clazz) {

		// See if the object represents a root element
		XmlRootElement root = (XmlRootElement)
				getAnnotation(clazz,XmlRootElement.class);
		if (root == null) {
			return null;
		}

		return root.name();
		//String namespace = root.namespace();

		//return name;
	}


	private static Annotation getAnnotation(final AnnotatedElement element, final Class<XmlRootElement> annotation) {
		return  AccessController.doPrivileged(new PrivilegedAction<Annotation>() {
			@Override
			public Annotation run() {
				return element.getAnnotation(annotation);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public static <T> T getObjectFromParameterValue(
			IParameterValue payload) {
		T input=null;

		ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
		ParameterValueObjectUtil util=null;

		try{
			util = pool.borrowObject();
			input = (T) util.unmarshallParameterValue(payload);
		} catch (Exception e) {
			if (Log.isWarn())
				Log.warn(e);
		}finally{
			try {
				pool.returnObject(util);
			} catch (Exception e) {
				if (Log.isDebug())
					Log.debug(e);
			}
		}
		return input;
	}
	
	
	public static IParameterValue getParameterValuefromObject(
			Object params) {
		ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
		ParameterValueObjectUtil util=null;
		IParameterValue requestParams = null;
		try
		{
			util = pool.borrowObject();
			Element elem = SchemaHelper.getInstance().getSchemaElement(getSchemaElementNameFromClass(params.getClass()));
			requestParams=util.convertObject(params, elem);
		}
		catch (Exception e)
		{
			Log.warn(e);
		}finally{
			if (util!=null){
				try {
					pool.returnObject(util);
				} catch (Exception e) 
				{
					Log.warn(e);
				}
			}
		}
		return requestParams;
	}

	/**
	 * @param value
	 * @param attributeQname 
	 * @return
	 */
	private static IParameterValue getParameterValuefromObject(Object value, QName attributeQname) {
		// TODO Auto-generated method stub
		return null;
	}

}
