/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.client.utils;

import org.ws4d.java.service.reference.ServiceReference;

public class ServiceReferenceProxy
{
    //TODO MVo Maybe it's better not to hold the service reference itself, but the device/endpoint reference and service id. 
    private final ServiceReference svcRef;

    public ServiceReferenceProxy(ServiceReference svcRef)
    {
        this.svcRef = svcRef;
    }

    public ServiceReference getServiceReference()
    {
        return svcRef;
    }
}
