/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common;

import com.draeger.medical.biceps.common.impl.replicator.DefaultReplicator;

public abstract class AbstractParameterValueObjectUtil implements
		ParameterValueObjectUtil {
	private final ObjectReplicator replicator=new DefaultReplicator();
	

	@Override
	public <V, T> T replicateTo(V original, Class<T> to) throws Exception {
		return getReplicator().replicateTo(original, to);
	}

	/* (non-Javadoc)
	 * @see de.draeger.run.dpws.common.ParameterValueObjectConverter#populate(V, T)
	 */
	@Override
	public synchronized <V,T> T populate(V from,T to)
	{
		return getReplicator().populate(from, to);
	}

	/* (non-Javadoc)
	 * @see de.draeger.run.dpws.common.ParameterValueObjectConverter#deepcopy(V)
	 */
	@Override
	public synchronized <V> V deepcopy(V original) throws Exception
	{
		return getReplicator().deepcopy(original);
	}
	
	protected ObjectReplicator getReplicator() {
		return replicator;
	}
	
	@Override
	public <V, T> T replicateTo(V original, Class<T> to,
			boolean restrictToPackage) throws Exception {
		return getReplicator().replicateTo(original, to, restrictToPackage);
	}

}
