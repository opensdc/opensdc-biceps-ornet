/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common;

public interface ObjectReplicator {
	public abstract <V, T> T populate(V from, T to);
	
	public abstract <V,T> T replicateTo(V original,Class<T> to) throws Exception;
	public abstract <V,T> T replicateTo(V original, Class<T> to, boolean restrictToPackage) throws Exception;
	
	public abstract <V> V deepcopy(V original) throws Exception;
}
