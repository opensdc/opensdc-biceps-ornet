/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common;

import java.util.ArrayList;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.ws4d.java.util.Log;


public class PVObjectUtilPoolProvider {
	private static final PVObjectUtilPoolProvider instance=new PVObjectUtilPoolProvider();


	private final ParameterValueObjectUtilPool pool;

	public static PVObjectUtilPoolProvider getInstance() {
		return instance;
	}

	private final int 			maxProvider=Integer.parseInt(System.getProperty("BICEPS.PVObjectUtilPoolSize", "5"));
	private final int 			maxBlockTime=Integer.parseInt(System.getProperty("BICEPS.PVObjectUtilPool.maxBlockTime", "1000"));
	private final byte 			whenExhaustedAction=(byte)Integer.parseInt(System.getProperty("BICEPS.PVObjectUtilPool.whenExhaustedAction", String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));


	private final String factoryClassName=System.getProperty("BICEPS.PVObjectUtilFactoryClz", "com.draeger.medical.biceps.common.impl.jaxb.JAXBBuilderFactory");

	private PVObjectUtilPoolProvider()
	{

		PoolableObjectFactory factory=createPoolableObjectFactory();
		ParameterValueObjectUtilPool tPool=null;
		try {
			if (factory!=null)
			{
				tPool=new ParameterValueObjectUtilPool(factory);
				ArrayList<ParameterValueObjectUtil> borrowedObjects=new ArrayList<ParameterValueObjectUtil>();

				//Initialize all provider beforehand by borrowing and returning them to the object factory
				for (int i=0;i<maxProvider;i++)
					borrowedObjects.add(tPool.borrowObject());

				for (int i=0;i<borrowedObjects.size();i++)
					tPool.returnObject(borrowedObjects.get(i));
			}
			
		} catch (Exception e) {
			Log.error(e);
		}finally{
		pool=tPool;
		}
	}

	public ParameterValueObjectUtilPool getPool() 
	{
		if (pool==null) throw new IllegalStateException("No ParameterValueObjectUtilPool available for factory class "+factoryClassName+". Use -DBICEPS.PVObjectUtilFactoryClz as VM arg.");
		return pool;
	}

	private PoolableObjectFactory createPoolableObjectFactory() {
		PoolableObjectFactory factory=null;

		try {
			if (Log.isInfo())
				Log.info("PVObjectUtilFactory: "+factoryClassName);
			
			Class<?> factoryClass = Class.forName(factoryClassName);
			factory= (PoolableObjectFactory) factoryClass.newInstance();
		} catch (Exception e) {
			Log.error(e);
		}

		return factory;
	}



	public class ParameterValueObjectUtilPool extends GenericObjectPool{

		public ParameterValueObjectUtilPool(PoolableObjectFactory factory) {
			super(factory, maxProvider,whenExhaustedAction,maxBlockTime);
		}

		@Override
		public ParameterValueObjectUtil borrowObject() throws Exception {
			return (ParameterValueObjectUtil) super.borrowObject();
		}

		public void returnObject(ParameterValueObjectUtil arg0) throws Exception {
			super.returnObject(arg0);
		}
	}
}
