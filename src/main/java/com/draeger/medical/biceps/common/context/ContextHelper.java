/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.context;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.ws4d.java.types.URI;

import com.draeger.medical.biceps.common.messages.BICEPSConstants;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;
import com.draeger.medical.biceps.common.model.OperatorContextDescriptor;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;

/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
public class ContextHelper {


	/**
	 * Creates the interpretable instance identifier.
	 *
	 * @param root the root
	 * @param interpretationParticles the interpretation particles
	 * @return the instance identifier
	 */
	public static InstanceIdentifier createInterpretableInstanceIdentifier(String root, Map<String,String> interpretationParticles){
		return createInterpretableInstanceIdentifier(root, interpretationParticles,BICEPSConstants.LOCATION_ID_KEYS);
	}

	/**
	 * Creates an interpretable instance identifier.
	 * The interpretation particles may be ordered based on the list of known keys in the created instance identifier.
	 *
	 * @param root the mandatory value for a root 
	 * @param interpretationParticles the particles that will form the extension value with interpretation keys.
	 * @param knownKeys the ordered list of known keys 
	 * @return the instance identifier
	 */
	public static InstanceIdentifier createInterpretableInstanceIdentifier(String root, Map<String,String> interpretationParticles, List<String> knownKeys)
	{

		InstanceIdentifier retVal=new InstanceIdentifier();
		retVal.setRoot(root);

		StringBuilder extensionVal=new StringBuilder();
		StringBuilder extensionInterpretation=new StringBuilder();
		for (String extensionCode : knownKeys) {
			addValue(interpretationParticles, extensionVal, extensionInterpretation, extensionCode);	
		}

		Set<String> keySet = new  HashSet<String>(interpretationParticles.keySet());
		for (String knownExtensionCode : knownKeys) {
			keySet.remove(knownExtensionCode);
		}

		for (String extensionCode : keySet) {
			if (!BICEPSConstants.ROOT_PARAM_TYPE_NAME.equals(extensionCode))
				addValue(interpretationParticles, extensionVal, extensionInterpretation, extensionCode);	
		}

		if (extensionInterpretation.length()>0){
			extensionVal.append(BICEPSConstants.URI_QUERY_DELIM);
			extensionVal.append(BICEPSConstants.INTERPRT_PARAM_TYPE_NAME);
			extensionVal.append(BICEPSConstants.QUERY_KEY_DELIM);
			extensionVal.append(extensionInterpretation);
		}

		retVal.setExt(extensionVal.toString());

		return retVal;
	}

	private static void addValue(Map<String, String> extension,
			StringBuilder extensionvalue, StringBuilder extensionInterpretation, String extensionCode) {
		String extensionIDValue = extension.get(extensionCode);
		if (extensionIDValue!=null && !extensionIDValue.isEmpty())
		{
			if (extensionvalue.length()>0){
				extensionvalue.append(BICEPSConstants.URI_PATH_DELIM);
			}
			extensionvalue.append(extensionIDValue);

			//interpretation
			if (extensionInterpretation.length()>0){
				extensionInterpretation.append(BICEPSConstants.URI_PATH_DELIM);
			}
			if (extensionCode!=null && !extensionCode.isEmpty())
			{
				extensionInterpretation.append(extensionCode);
			}else{
				extensionInterpretation.append(BICEPSConstants.CONTEXT_ID_NULL_FLAVOR_UNKNOWN);
			}
		}
	}

	/**
	 * Creates the {@link URI} representation of an {@link InstanceIdentifier} that identifies a location context.
	 *
	 * @param instanceIdentifier the instance identifier
	 * @return the uri
	 */
	public static URI createLocationIdUri(InstanceIdentifier instanceIdentifier) {

		//		return new URI(BICEPSConstants.LOCATION_ID_URI+ii.getExtension());
		return createURI(BICEPSConstants.LOCATION_ID_URI, instanceIdentifier);
	}

	/**
	 * Creates the {@link URI} representation of a location association state.
	 *
	 * @param handleQuery the handle query
	 * @param stateValue the state value
	 * @return the uri
	 */
	public static URI createLocationAssociationUri(String handleQuery, ContextAssociationStateValue stateValue){
		return new URI(BICEPSConstants.LOCATION_ASSOCIATION_URI+BICEPSConstants.URI_PATH_DELIM+handleQuery+BICEPSConstants.URI_PATH_DELIM+stateValue.value());
	}

	/**
	 * Creates the {@link URI} representation of an {@link InstanceIdentifier} that identifies a patient context.
	 *
	 * @param instanceIdentifier the instance identifier
	 * @return the uri
	 */
	public static URI createPatientIdUri(InstanceIdentifier instanceIdentifier){
		//		return new URI(BICEPSConstants.PATIENT_ID_URI+BICEPSConstants.URI_PATH_DELIM+instanceIdentifier.getRoot()+BICEPSConstants.URI_PATH_DELIM+instanceIdentifier.getExtension());
		return createURI(BICEPSConstants.PATIENT_ID_URI, instanceIdentifier);
	}

	/**
	 * Creates the {@link URI} representation of a patient association state.
	 *
	 * @param handleQuery the handle query
	 * @param stateValue the state value
	 * @return the uri
	 */
	public static URI createPatientAssociationUri(String handleQuery, ContextAssociationStateValue stateValue){
		return new URI(BICEPSConstants.PATIENT_ASSOCIATION_URI+BICEPSConstants.URI_PATH_DELIM+handleQuery+BICEPSConstants.URI_PATH_DELIM+stateValue.value());
	}
	
	public static URI createEnsembleIdUri(InstanceIdentifier instanceIdentifier) {
		return createURI(BICEPSConstants.ENSEMBLE_ID_URI, instanceIdentifier);
	}
	
	public static URI createEnsembleAssociationUri(String handleQuery, ContextAssociationStateValue stateValue) {
		return new URI(BICEPSConstants.ENSEMBLE_ASSOCIATION_URI+BICEPSConstants.URI_PATH_DELIM+handleQuery+BICEPSConstants.URI_PATH_DELIM+stateValue.value());
	}
	
	public static URI createWorkflowIdUri(InstanceIdentifier instanceIdentifier) {
		return createURI(BICEPSConstants.WORKFLOW_ID_URI, instanceIdentifier);
	}
	
	public static URI createWorkflowAssociationUri(String handleQuery, ContextAssociationStateValue stateValue) {
		return new URI(BICEPSConstants.WORKFLOW_ASSOCIATION_URI+BICEPSConstants.URI_PATH_DELIM+handleQuery+BICEPSConstants.URI_PATH_DELIM+stateValue.value());
	}
	
	public static URI createOperatorAssociationUri(String handleQuery, ContextAssociationStateValue stateValue) {
		return new URI(BICEPSConstants.WORKFLOW_ASSOCIATION_URI+BICEPSConstants.URI_PATH_DELIM+handleQuery+BICEPSConstants.URI_PATH_DELIM+stateValue.value());
	}
	

	private static URI createURI(String prefix, InstanceIdentifier instanceIdentifier){

		StringBuilder sb=new StringBuilder(prefix);

		if (instanceIdentifier.getExtension()!=null)
		{
			sb.append(BICEPSConstants.URI_PATH_DELIM);
			sb.append(instanceIdentifier.getExtension());
		}
		if (instanceIdentifier.getRoot()!=null)
		{
			if (instanceIdentifier.getExt()==null || instanceIdentifier.getExt().isEmpty())
			{
				sb.append(BICEPSConstants.URI_QUERY_DELIM);
			}else{
				sb.append(BICEPSConstants.QUERY_PARAM_DELIM);
			}
			sb.append(BICEPSConstants.ROOT_PARAM_TYPE_NAME);
			sb.append(BICEPSConstants.QUERY_KEY_DELIM);
			sb.append(instanceIdentifier.getRoot());
		}
		return new URI(sb.toString());
	}

	/**
	 * Creates the typed instance identifier.
	 *
	 * @param uriRepresentation the uri representation
	 * @return the instance identifier with type
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static InstanceIdentifierWithType<AbstractContextDescriptor> createTypedInstanceIdentifier(URI uriRepresentation){
		InstanceIdentifierWithType<AbstractContextDescriptor> retVal=null;

		InstanceIdentifier ii = createInstanceIdentifierFromUri(uriRepresentation);
		if (ii!=null)
		{
			if (BICEPSConstants.LOCATION_CONTEXT_URI_SCHEMA.equals(uriRepresentation.getSchema())){
				retVal=new InstanceIdentifierWithType(LocationContextDescriptor.class, ii);
			}else if (BICEPSConstants.PATIENT_CONTEXT_URI_SCHEMA.equals(uriRepresentation.getSchema())){
				retVal=new InstanceIdentifierWithType(PatientAssociationDescriptor.class, ii);
			} else if (BICEPSConstants.ENSEMBLE_CONTEXT_URI_SCHEMA.equals(uriRepresentation.getSchema())) {
				retVal=new InstanceIdentifierWithType(EnsembleContextDescriptor.class, ii);
			} else if (BICEPSConstants.WORKFLOW_CONTEXT_URI_SCHEMA.equals(uriRepresentation.getSchema())) {
				retVal=new InstanceIdentifierWithType(WorkflowContextDescriptor.class, ii);
			} else{
				retVal=new InstanceIdentifierWithType<AbstractContextDescriptor>(null, ii);
			}
		}
		return retVal;
	}

	/**
	 * Creates the instance identifier from uri.
	 *
	 * @param uriRepresentation the uri representation
	 * @return the instance identifier
	 */
	public static InstanceIdentifier createInstanceIdentifierFromUri(URI uriRepresentation)
	{

		InstanceIdentifier retVal=null;
		if (uriRepresentation!=null){

			String path = uriRepresentation.getPath();
			String query = uriRepresentation.getQuery();
			if (path!=null)
			{
				if (path.startsWith(BICEPSConstants.CONTEXT_ID_URI_PATH)){
					path=path.replaceFirst(BICEPSConstants.CONTEXT_ID_URI_PATH, "");
					HashMap<String, String> parsedParticles=new HashMap<String, String>();
					retrieveParticles(parsedParticles, path, query);
					String rootId = parsedParticles.remove(BICEPSConstants.ROOT_PARAM_TYPE_NAME);
					if (rootId!=null)
					{
						retVal=createInterpretableInstanceIdentifier(rootId, parsedParticles);
					}
				}
			}else{
				//void
			}
		}

		return retVal;
	}


	private static String[] parsePath(String pathString) {
		String[] pathElements=null;
		StringTokenizer pathTokenizer=new StringTokenizer(pathString, BICEPSConstants.URI_PATH_DELIM);

		int tokenCnt=pathTokenizer.countTokens();
		if (tokenCnt>0){

			pathElements=new String[tokenCnt];
			for (int i=0;i<tokenCnt;i++)
			{
				pathElements[i]= pathTokenizer.nextToken();
			}
		}
		return pathElements;
	}

	/**
	 * Gets the interpretable particles.
	 *
	 * @param locationId the location id
	 * @return the interpretable particles
	 */
	public static  Map<String,String> getInterpretableParticles(InstanceIdentifier locationId)
	{
		HashMap<String,String> parsedParticles=new HashMap<String, String>();

		if (locationId!=null && locationId.getExtension()!=null)
		{
			StringTokenizer extensionTokenizer=new StringTokenizer(locationId.getExt(),BICEPSConstants.URI_QUERY_DELIM);
			if(extensionTokenizer.countTokens()==2){
				String pathToken = extensionTokenizer.nextToken();
				String queryToken = extensionTokenizer.nextToken();
				retrieveParticles(parsedParticles, pathToken, queryToken);

			}
		}
		return parsedParticles;
	}

	private static void retrieveParticles(
			HashMap<String, String> parsedParticles, String pathToken,
			String queryToken) {
		if (queryToken!=null)
		{
			StringTokenizer queryTokenizer=new StringTokenizer(queryToken,BICEPSConstants.QUERY_PARAM_DELIM);
			String[] interPretationPath=null;
			String rootId=null;
			while (queryTokenizer.hasMoreTokens())
			{
				String queryParam=queryTokenizer.nextToken();
				StringTokenizer queryParamTokenizer=new StringTokenizer(queryParam,BICEPSConstants.QUERY_KEY_DELIM);
				if (queryParamTokenizer.countTokens()==2)
				{
					String key=queryParamTokenizer.nextToken();
					if (BICEPSConstants.INTERPRT_PARAM_TYPE_NAME.equals(key)){
						interPretationPath=parsePath(queryParamTokenizer.nextToken());
					}else if (BICEPSConstants.ROOT_PARAM_TYPE_NAME.equals(key)){
						rootId=queryParamTokenizer.nextToken();
					}
				}
			}
			if (interPretationPath!=null){
				String[] idPath=parsePath(pathToken);
				if (idPath!=null && idPath.length==interPretationPath.length){
					for(int i=0;i<idPath.length;i++){
						parsedParticles.put( interPretationPath[i],idPath[i]);
					}
				}
			}
			if (rootId!=null){
				parsedParticles.put(BICEPSConstants.ROOT_PARAM_TYPE_NAME, rootId);
			}
		}
	}
	
	/**
	 * Gets the {@link URI} representation of an {@link InstanceIdentifier} of a defined {@link Class}.
	 *
	 * @param ctxtDescriptorClass the ctxt descriptor class
	 * @param instanceIdentifier the instance identifier
	 * @return the id uri
	 */
	public static URI getIdUri(Class<? extends AbstractContextDescriptor> ctxtDescriptorClass,
			InstanceIdentifier instanceIdentifier) {
		URI idUri = null;
		
		if (ctxtDescriptorClass!=null)
		{
			if (PatientAssociationDescriptor.class.isAssignableFrom(ctxtDescriptorClass))
			{
				idUri=ContextHelper.createPatientIdUri(instanceIdentifier);
			}else if  (LocationContextDescriptor.class.isAssignableFrom(ctxtDescriptorClass)){
				idUri=ContextHelper.createLocationIdUri(instanceIdentifier);
			} else if (EnsembleContextDescriptor.class.isAssignableFrom(ctxtDescriptorClass)) {
				idUri=ContextHelper.createEnsembleIdUri(instanceIdentifier);
			} else if (WorkflowContextDescriptor.class.isAssignableFrom(ctxtDescriptorClass)) {
				idUri=ContextHelper.createWorkflowIdUri(instanceIdentifier);
			}
		}
		return idUri;
	}


	/**
	 * Gets the association uri.
	 *
	 * @param ctxtDescriptor the ctxt descriptor
	 * @param pas the pas
	 * @param stateValue the state value
	 * @return the association uri
	 */
	public static URI getAssociationUri(AbstractContextDescriptor ctxtDescriptor,
			AbstractIdentifiableContextState stateValue) {
		URI assocationURI =null;
		if (ctxtDescriptor instanceof PatientAssociationDescriptor){
			assocationURI=ContextHelper.createPatientAssociationUri(stateValue.getHandle(), stateValue.getContextAssociation());
		}else if  (ctxtDescriptor instanceof LocationContextDescriptor){
			assocationURI=ContextHelper.createLocationAssociationUri(stateValue.getHandle(), stateValue.getContextAssociation());
		} else if (ctxtDescriptor instanceof EnsembleContextDescriptor) {
			assocationURI=ContextHelper.createEnsembleAssociationUri(stateValue.getHandle(), stateValue.getContextAssociation());
		} else if (ctxtDescriptor instanceof WorkflowContextDescriptor) {
			assocationURI=ContextHelper.createWorkflowAssociationUri(stateValue.getHandle(), stateValue.getContextAssociation());
		} else if (ctxtDescriptor instanceof OperatorContextDescriptor) {
			assocationURI=ContextHelper.createOperatorAssociationUri(stateValue.getHandle(), stateValue.getContextAssociation());
		}
		return assocationURI;
	}

	/**
	 * The Class InstanceIdentifierWithType.
	 *
	 * @param <T> the generic type
	 */
	public static class InstanceIdentifierWithType <T extends AbstractContextDescriptor>{
		private final Class<T> identifierType;
		private final InstanceIdentifier instanceIdentifier;
		
		/**
		 * Instantiates a new instance identifier with type.
		 *
		 * @param identifierType the identifier type
		 * @param instanceIdentifier the instance identifier
		 */
		public InstanceIdentifierWithType(Class<T> identifierType,InstanceIdentifier instanceIdentifier)
		{
			this.identifierType=identifierType;
			this.instanceIdentifier=instanceIdentifier;
		}
		
		/**
		 * Gets the identifier type.
		 *
		 * @return the identifier type
		 */
		public Class<T> getIdentifierType() {
			return identifierType;
		}
		
		/**
		 * Gets the instance identifier.
		 *
		 * @return the instance identifier
		 */
		public InstanceIdentifier getInstanceIdentifier() {
			return instanceIdentifier;
		}
	}
}


