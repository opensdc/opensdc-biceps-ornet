/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.impl.jaxb;

import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;
import com.sun.xml.internal.bind.v2.WellKnownNamespace;

@SuppressWarnings({ "deprecation", "restriction" })
final class BICEPSNamespacePrefixMapper1_6 extends
		NamespacePrefixMapper {
	@Override
	public String[] getPreDeclaredNamespaceUris() {
		return new String[] { WellKnownNamespace.XML_SCHEMA_INSTANCE,SchemaHelper.NAMESPACE_MESSAGES };
	}

	public String getPropertyName() {
		return "com.sun.xml.internal.bind.namespacePrefixMapper";
	}

	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		if (namespaceUri.equals(WellKnownNamespace.XML_SCHEMA_INSTANCE))
			return "x";
		if (namespaceUri.equals(WellKnownNamespace.XML_SCHEMA))
			return "xs";
		if (namespaceUri.equals(WellKnownNamespace.XML_MIME_URI))
			return "xmime";
		if (SchemaHelper.NAMESPACE_MESSAGES.equals(namespaceUri))
			return SchemaHelper.NAMESPACE_MESSAGES_PREFIX;
		return suggestion;
	}
}
