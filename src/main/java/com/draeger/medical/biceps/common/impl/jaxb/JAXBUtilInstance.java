/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.impl.jaxb;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.MarshalException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.ws4d.java.communication.protocol.soap.generator.DefaultBasicTypes2SOAPConverter;
import org.ws4d.java.communication.protocol.soap.generator.DefaultParameterValue2SOAPSerializer;
import org.ws4d.java.communication.protocol.soap.generator.DefaultSOAP2BasicTypesConverter;
import org.ws4d.java.communication.protocol.soap.generator.DefaultSOAP2ParameterValueConverter;
import org.ws4d.java.io.xml.ElementParser;
import org.ws4d.java.schema.Element;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.util.Log;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.draeger.medical.biceps.common.AbstractParameterValueObjectUtil;
import com.draeger.medical.biceps.common.messages.SchemaLoaderProvider;
import com.draeger.medical.biceps.common.model.ObjectFactory;
import com.draeger.medical.mdpws.utils.InputStreamUtil;

public class JAXBUtilInstance extends AbstractParameterValueObjectUtil{


	//	TODO SSch -Dcom.sun.xml.bind.v2.runtime.JAXBContextImpl.fastBoot=true
	private final static boolean useOverride=Boolean.parseBoolean(System.getProperty("BICEPS.JAXBBuilder.UseOverride", "true"));

	private final boolean validateDuringConversion = Boolean.parseBoolean(System.getProperty("BICEPS.Commons.JAXBUtilInstance.validateDuringConversion", "false"));
	private final boolean validatingUnmarshaller = Boolean.parseBoolean(System.getProperty("BICEPS.Commons.JAXBUtilInstance.validatingUnmarshaller", "true"));
	private final String dedicatedSerializers = System.getProperty("BICEPS.Commons.JAXBUtilInstance.DedicatedSerializers", "").trim();
	
	private final XmlPullParserFactory XPP_FACTORY;

	private final HashMap<String,Unmarshaller> unmarshallerMap=new HashMap<String, Unmarshaller>();
	private final HashMap<String,Marshaller> marshallerMap=new HashMap<String, Marshaller>();
	private final StringWriter sw = new StringWriter();
	private final XmlPullParser parser;

//	private final static String MSG_PACKAGE_NAME= "com.draeger.medical.biceps.common.messages.v2";
	private final static String MODEL_PACKAGE_NAME= "com.draeger.medical.biceps.common.model";

	public JAXBUtilInstance()
	{
		XmlPullParserFactory factory = null;
		try
		{
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
		}
		catch (XmlPullParserException e)
		{
			if (Log.isError())
			{
				Log.error("Could not create XmlPullParserFactory: " + e);
				Log.error(e);
			}
			throw new RuntimeException("Could not create XmlPullParserFactory: " + e);
		}

		XPP_FACTORY = factory;
		InputStream[] streams=null;
		try
		{		 			
			if (Log.isDebug())Log.debug("Create Marshaller and Unmarshaller for "+MODEL_PACKAGE_NAME);

			streams=SchemaLoaderProvider.getInstance().getSchemaLoader().getSchemaModelInputStream();
			createMarshallerAndUnmarshallerForPackage(MODEL_PACKAGE_NAME,streams);
			for (InputStream inputStream : streams) {
				inputStream.close();	
			}

		}
		catch (Exception e)
		{
			if (Log.isError())
			{
				Log.error("Could not create marshaller/unmarshaller: " + e);
				Log.error(e);
			}


			throw new RuntimeException(e);
		}finally{
			if (stream!=null)
				try {
					stream.close();
				} catch (IOException e) {
					if (Log.isError()){
						Log.error("Could not close schema stream. "+e);
						Log.error(e);
					}
				}
		}

		XmlPullParser tParser=null;
		if (!useOverride) //Do not create parser if not needed later
		{
			try {
				tParser= XPP_FACTORY.newPullParser();
			} catch (XmlPullParserException e) {
				Log.error("Could not create XPP! " + e);
				Log.error(e);
			}
		}
		parser=tParser;

		if (dedicatedSerializers!=null && dedicatedSerializers.length()>0)
		{
			setupDedicatedSerializers();
		}

		if (Log.isDebug())Log.debug("JAXBUtilInstance created");
	}


	private void createMarshallerAndUnmarshallerForPackage(String packageName,InputStream... stream)
			throws SAXException, JAXBException {
		javax.xml.validation.Schema schema = loadSchema(stream);
		JAXBContext jaxbCtxt = initContext(packageName);
		createUnmarshaller(jaxbCtxt,schema,packageName);
		createMarshaller(jaxbCtxt,schema,packageName);
	}


	private void createMarshaller(JAXBContext jaxbCtxt,Schema schema, String packageName) throws JAXBException {
		Marshaller marshaller = jaxbCtxt.createMarshaller();
		if (schema!=null) setupMarshaller(marshaller,schema);
		marshallerMap.put(packageName, marshaller);
	}


	private void createUnmarshaller(JAXBContext jaxbCtxt,Schema schema, String packageName) throws JAXBException {
		Unmarshaller unmarshaller = jaxbCtxt.createUnmarshaller();
		if (schema!=null) setupUnmarshaller(unmarshaller,schema);

		unmarshallerMap.put(packageName, unmarshaller);

	}


	private void setupUnmarshaller(Unmarshaller unmarshaller, Schema schema) {
		if (validatingUnmarshaller)
			unmarshaller.setSchema(schema);
		else 
			unmarshaller.setSchema(null);
	}


	private javax.xml.validation.Schema loadSchema(InputStream... stream)
			throws SAXException {
		Source[] sources = new Source[stream.length];
		for (int i=0;i<stream.length;i++) {
			sources[i]= new StreamSource(stream[i]);
		}
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		javax.xml.validation.Schema schema = schemaFactory.newSchema(sources);

		if (Float.parseFloat(System.getProperty("java.version").substring(0, 3)) < 1.6)
		{
			if (Log.isDebug())
				Log.debug("Java < 1.6: Add BugFix code for JAXB!");

			Thread.currentThread().setContextClassLoader(JAXBContext.class.getClassLoader());
		}
		return schema;
	}


	private void setupDedicatedSerializers() {
		StringTokenizer tokenizer=new StringTokenizer(dedicatedSerializers, ";");
		while(tokenizer.hasMoreTokens())
		{
			StringTokenizer tokenizer2=new StringTokenizer(tokenizer.nextToken(), ":");
			if (tokenizer2.countTokens()==2)
			{
				try {
					Class<?> messageClass= Class.forName(tokenizer2.nextToken());

					Class<?> contentCreatorClass= Class.forName(tokenizer2.nextToken());
					Object contentCreator=contentCreatorClass.newInstance();
					if (contentCreator instanceof ContentCreator)
					{
						contentCreators.put(messageClass, (ContentCreator) contentCreator);
						if (Log.isInfo())
						{
							Log.info("Added ContentCreator"+contentCreator+" for messages of class "+messageClass);
						}
					}

				} catch (Exception e) {
					Log.warn(e);
				} 
			}
		}
	}


	private void setupMarshaller(Marshaller marshaller,javax.xml.validation.Schema schema)
			throws PropertyException {
		if (validateDuringConversion)
			marshaller.setSchema(schema);
		else 
			marshaller.setSchema(null);

		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		setNamespacePrefixMapper(marshaller);
	}


	private void setNamespacePrefixMapper(Marshaller marshaller)
			throws PropertyException {
		Object prefixMapperO=null;

		String className=System.getProperty("BICEPS.Commons.JAXBUtilInstance.NamespacePrefixMapperClass", "com.draeger.medical.biceps.common.impl.jaxb.BICEPSNamespacePrefixMapper1_6");
		//java 1.5 com.sun.xml.bind.namespacePrefixMapper
		//java 1.6 com.sun.xml.internal.bind.namespacePrefixMapper
		String namespacePrefixPropertyName=System.getProperty("BICEPS.Commons.JAXBUtilInstance.NamespacePrefixPropertyName", "com.sun.xml.internal.bind.namespacePrefixMapper");//
		boolean activateNameSpacePrefixMapper=Boolean.parseBoolean(System.getProperty("BICEPS.Commons.JAXBUtilInstance.ActivateNameSpacePrefixMapper", "false"));
		if (activateNameSpacePrefixMapper)
		{
			//			//see http://stackoverflow.com/questions/2326107/what-happened-to-jaxbs-namespaceprefixmapper-in-jdk6u18
			//http://j2stuff.blogspot.de/2011/05/jaxb-replacing-namespace-prefixes.html
			try{
				tryToSetProperty(marshaller, prefixMapperO, className, namespacePrefixPropertyName);
			}catch(PropertyException propEx){
				if (Log.isWarn())Log.warn("Could not set namespace prefix mapper property["+namespacePrefixPropertyName+"] to "+className+"! Exception message is '"+propEx.getMessage()+"'");
			}
		}
	}


	private void tryToSetProperty(Marshaller marshaller, Object prefixMapperO,
			String className, String namespacePrefixPropertyName)
					throws PropertyException {
		try {
			Class<?> namespacePrefixMapperClz= Class.forName(className);
			prefixMapperO=namespacePrefixMapperClz.newInstance();
		} catch (Exception e) {
			if (Log.isWarn())
			{
				Log.warn("Could not create namespace mapper class: "+e.getMessage()+" className:"+className+" namespacePrefixPropertyName"+namespacePrefixPropertyName);
				Log.warn(e);
			}

		}

		if (prefixMapperO!=null){
			marshaller.setProperty(namespacePrefixPropertyName,prefixMapperO );
		}else{
			if (Log.isWarn()){
				Log.warn("No namespace prefix mapper defined! className:"+className+" namespacePrefixPropertyName"+namespacePrefixPropertyName);
			}
		}
	}

	private static JAXBContext initContext(String packageName) {
		// FIXME SSc ClassNotFoundException on Java5 ???
		try
		{
			return  JAXBContext.newInstance(packageName);
		}
		catch (Exception e)
		{
			if(Log.isError())
			{
				Log.error("Could not create JAXB context:"+e.getMessage());
				Log.error(e);
			}

		}
		return null;
	}



	@Override
	public synchronized <T> IParameterValue convertObject(T object, Element elem) throws Exception
	{
		Object content=createContent(object, elem.getName());
		if (useOverride)
		{
			IParameterValue pv=ParameterValue.createElementValue(elem);
			pv.overrideSerialization(content.toString());			

			return pv; 

		}
		return convertJAXBXMLString2PV(new InputStreamReader(contentStream.getByteArrayInputStream()), elem);

	}

	private final HashMap<Class<?>,ContentCreator> contentCreators=new HashMap<Class<?>,ContentCreator>();
	private final MyByteArrayOutputStream contentStream=new MyByteArrayOutputStream();
	private Object createContent(Object o, org.ws4d.java.types.QName elemName) throws Exception
	{
		Object retVal=null;
		contentStream.reset();
		if (o!=null)
		{
			ContentCreator cc= contentCreators.get(o.getClass());
			if (cc!=null)
			{
				retVal=cc.createContent(o,contentStream, elemName);
			}else{

				createContentJAXB(o, elemName);
				retVal=contentStream;
			}
		}

		return retVal;
	}


	private Object createContentJAXB(Object o, org.ws4d.java.types.QName elemName)
			throws MarshalException, NoSuchMethodException, JAXBException {

		Marshaller marshaller=getMarshallerForObject(o);
		if (marshaller!=null)
		{
			try
			{
				marshaller.marshal(o, contentStream);
			}
			catch (Exception me)
			{
				//sw.close();
				// Maybe it was not a root element
				Annotation[] an = o.getClass().getAnnotations();
				if (an == null) throw new MarshalException(me.getMessage());

				for (Annotation annotation : an)
				{
					if (annotation.annotationType().getName()
							.equals("javax.xml.bind.annotation.XmlRootElement"))
					{
						// it is a root element --> throw an exception
						throw new MarshalException(me.getMessage());
					}
				}

				String methodName = "create" + o.getClass().getSimpleName();
				Method method = ObjectFactory.class.getMethod(methodName);

				if (method != null)
				{
					resetStringWriter();
					@SuppressWarnings({ "unchecked", "rawtypes" })
					JAXBElement<?> jaxbElem = new JAXBElement(QName.valueOf(elemName.toString()), o.getClass(), o);// (JAXBElement<?>) method.invoke(modelObjectFactory);
					marshaller.marshal(jaxbElem, contentStream);
				}
				else
				{
					throw new MarshalException(me.getMessage());
				}
			}
		}
		return contentStream;
	}

	private Marshaller getMarshallerForObject(Object object) {
		Marshaller m=null;
		if (object == null || (m=marshallerMap.get(object.getClass().getPackage().getName()))==null)
			throw new IllegalArgumentException("Parameter " + object + " invalid! No Marshaller found.");

		return m;
	}

	private Unmarshaller getUnmarshallerForObject(Class<?> classType) {
		Unmarshaller m=null;
		if (classType == null)
		{
			m= unmarshallerMap.get(MODEL_PACKAGE_NAME);
		}else{
			m=unmarshallerMap.get(classType.getPackage().getName());
		}
		return m;
	}


	private void resetStringWriter() {
		if (sw!=null)
		{
			sw.getBuffer().setLength(0);
			//			sw.getBuffer().trimToSize(); //bad idea due to performance penalties
		}
	}

	//	private XMLStreamReader getWSXMLReader(InputStream input) throws XMLStreamException{
	//		return xmlif.createXMLStreamReader(input);
	//	}
	//	
	//	private XMLStreamWriter getWSXMLWriter(OutputStream output) throws XMLStreamException{
	//		return xmlof.createXMLStreamWriter(output);
	//	}
	//
	//	private static XMLStreamReader getFIXMLReader(InputStream input)
	//	{
	//		// Create the StAX XMLStreamReader using the input stream
	//		XMLStreamReader streamReader = new StAXDocumentParser(input);
	//
	//		return streamReader;
	//	}
	//
	//	private static XMLStreamWriter getFIXMLWriter(OutputStream output) {
	//
	//		// Create the StAX document serializer
	//		StAXDocumentSerializer staxDocumentSerializer = new StAXDocumentSerializer();
	//		staxDocumentSerializer.setOutputStream(output);
	//
	//		return  staxDocumentSerializer;
	//	}

	private IParameterValue convertJAXBXMLString2PV(InputStreamReader ir, Element element)
			throws IOException, XmlPullParserException
			{
		if (!useOverride)
		{
			parser.setInput(ir); //resets the parser
			parser.nextTag(); // Skip <xml...>

			DefaultSOAP2BasicTypesConverter btc = new DefaultSOAP2BasicTypesConverter();
			DefaultSOAP2ParameterValueConverter defaultSOAP2ParameterValueConverter = new DefaultSOAP2ParameterValueConverter(btc);

			return defaultSOAP2ParameterValueConverter.parseParameterValue(new ElementParser(parser,null),element);
		}else{
			IParameterValue pv=ParameterValue.createElementValue(element);
			pv.overrideSerialization(new String(contentStream.getInternalBytes(),0,contentStream.getCount(),org.ws4d.java.constants.XMLConstants.ENCODING));
			return pv; 
		}
			}

	@Override
	@SuppressWarnings({"unchecked"})
	public synchronized <T> T unmarshallParameterValue(IParameterValue parameterValue) throws Exception
	{
		stream.reset();
		convertPV2JAXBXMLString(parameterValue);
		return (T)createObject(stream);
	}

	@SuppressWarnings("unchecked")
	private <T> T createObject(MyByteArrayOutputStream stream) throws JAXBException {
		if (Log.isDebug()) Log.debug("From Unmarshalling\r\n" + sw);

		//StringReader reader = new StringReader(sw.toString());
		return (T)unmarshalFromReader(stream,null);
	}

	@SuppressWarnings("unchecked")
	public <T> T unmarshalFromReader(MyByteArrayOutputStream reader, Class<T> knownType)
			throws JAXBException {
		
		Object unmarshalledObject = null;
		try{
			Unmarshaller unmarshaller = getUnmarshallerForObject(knownType);
			if (unmarshaller!=null)
			{
				if (knownType==null)
					unmarshalledObject=unmarshaller.unmarshal(reader.getByteArrayInputStream());
				else
					unmarshalledObject=unmarshaller.unmarshal(new StreamSource(reader.getByteArrayInputStream()), knownType);

				if (unmarshalledObject instanceof JAXBElement<?>)
				{
					return (T)((JAXBElement<?>) unmarshalledObject).getValue();
				}
			}
		}catch (JAXBException jaxb)
		{

			if (Log.isDebug()){
				try {
					Log.debug(jaxb.getMessage()+": "+InputStreamUtil.convertStreamToString(reader.getByteArrayInputStream()));
				} catch (IOException e) {
					Log.warn(e);
				}
			}
			throw jaxb;
		}
		return (T)unmarshalledObject;
	}

	private final MyByteArrayOutputStream stream = new MyByteArrayOutputStream();
	private void convertPV2JAXBXMLString(IParameterValue pv) throws IOException
	{

		serializePV2OutputStream(pv, stream);
	}

	private void serializePV2OutputStream(IParameterValue pv, OutputStream os) throws IOException
	{
		DefaultBasicTypes2SOAPConverter btc = new DefaultBasicTypes2SOAPConverter();
		DefaultParameterValue2SOAPSerializer converter = new DefaultParameterValue2SOAPSerializer(btc);
		converter.serializeParameterValue(pv, os, null);
	}


	private final static int XML_PREFIX_LENGTH = 0;//38;
	private static class MyByteArrayOutputStream extends ByteArrayOutputStream {
		final private MyByteArrayInputStream is=new MyByteArrayInputStream(new byte[1]); 

		MyByteArrayOutputStream()
		{
			super(100000);
		}

		public byte[] getInternalBytes(){
			return buf;
		}

		public int getCount()
		{
			return count;
		}

		public ByteArrayInputStream getByteArrayInputStream()
		{
			is.setBytes(getInternalBytes(), getCount());
			return is;
		}

		@Override
		public synchronized String toString() {
			try {
				return new String(buf,XML_PREFIX_LENGTH,count-XML_PREFIX_LENGTH,org.ws4d.java.constants.XMLConstants.ENCODING);
			} catch (UnsupportedEncodingException e) {
				Log.warn(e);
			}
			return super.toString();
		}


	}

	private static class MyByteArrayInputStream extends ByteArrayInputStream{
		MyByteArrayInputStream(byte[] buf) {
			super(buf);
		}

		public void setBytes(byte[] bytes, int count)
		{
			this.buf=bytes;
			this.count=count;
			this.mark = 0;
			this.pos = 0;
		}
	}

	//TODO SSch: Maybe it is better to use methods like http://kickjava.com/src/java/io/DataOutputStream.java.htm
}
