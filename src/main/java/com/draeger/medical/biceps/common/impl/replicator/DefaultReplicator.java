/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.impl.replicator;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import net.sf.beanlib.PropertyInfo;
import net.sf.beanlib.provider.BeanPopulator;
import net.sf.beanlib.provider.BeanTransformer;
import net.sf.beanlib.provider.replicator.BeanReplicator;
import net.sf.beanlib.spi.BeanTransformerSpi;
import net.sf.beanlib.spi.CustomBeanTransformerSpi;
import net.sf.beanlib.spi.DetailedPropertyFilter;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.ObjectReplicator;

public class DefaultReplicator implements ObjectReplicator{

	private void releaseReplicator(BeanReplicator replicator) 
	{
		//void
	}

	private BeanReplicator getReplicator() {
		BeanReplicator replicator=new BeanReplicator(customTransformer());
		return replicator;
	}

	private BeanPopulator getPopulator(Object from, Object to) {
		BeanPopulator p= new BeanPopulator(from, to);
		p.initDetailedPropertyFilter(DetailedPropertyFilter.ALWAYS_PROPAGATE);
//		BeanTransformer poptransformer = new BeanTransformer();
//		poptransformer=poptransformer.initImmutableReplicatableFactory(HeteroImmutableReplicator.factory);
//		p.initTransformer(poptransformer);
//		BeanTransformer poptransformer = new BeanTransformer();
//		HeteroImmutableReplicator hir=HeteroImmutableReplicator.newImmutableReplicatable(customTransformer());
		p.initTransformer(customTransformer());
		return p;
	}

	private void releasePopulator(BeanPopulator populator) 
	{
		//void
	}

	@Override
	public <V, T> T replicateTo(V original, Class<T> to) throws Exception {
		return replicateTo(original,to,false);
	}

	@Override
	public <V, T> T replicateTo(V original, Class<T> to, boolean restrictToPackage) throws Exception {
		T replic=null;
		if (original!=null)
		{		
			//			BeanReplicator replicator=getReplicator();
			//			replic=replicator.replicateBean(original, to);
			//			releaseReplicator(replicator);
			//			BeanPopulator populator=getPopulator(original, replic);
			//			replic=populator.populate();
			//			releasePopulator(populator);
		}
		return replic;
	}

	/* (non-Javadoc)
	 * @see de.draeger.run.dpws.common.ParameterValueObjectConverter#populate(V, T)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public synchronized <V,T> T populate(V from,T to)
	{
		return (T)getPopulator(from,to).populate();
	}

	/* (non-Javadoc)
	 * @see de.draeger.run.dpws.common.ParameterValueObjectConverter#deepcopy(V)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public synchronized <V> V deepcopy(V original) throws Exception
	{
		V copy=null;
		if (original!=null)
		{
			BeanReplicator replicator=getReplicator();
			copy=replicator.replicateBean(original);
			releaseReplicator(replicator);
			BeanPopulator populator=getPopulator(original, copy);
			copy=(V)populator.populate();
			releasePopulator(populator);
		}
		return copy;

	}

	private BeanTransformerSpi customTransformer() {
		BeanTransformerSpi beanTransformer = new BeanTransformer();
		try {
			return beanTransformer.initCustomTransformerFactory(new CustomBeanTransformerSpi.Factory() {
				DatatypeFactory dtFactory = DatatypeFactory.newInstance();

				@Override
				public CustomBeanTransformerSpi newCustomBeanTransformer(BeanTransformerSpi beanTransformer) {
					return new CustomBeanTransformerSpi() {



						@Override
						public boolean isTransformable(Object from, Class<?> toClass, PropertyInfo propertyInfo) {
							
							boolean isTransformable= 
									"javax.xml.datatype.XMLGregorianCalendar".equals(toClass.getName()) 
									|| "javax.xml.datatype.Duration".equals(toClass.getName());
							
							
							return isTransformable;
						}

						@Override
						@SuppressWarnings("unchecked")
						public <T> T transform(Object in, Class<T> toClass, PropertyInfo propertyInfo) {
							T retVal=null;
							if (in instanceof Duration)
							{
								Duration inVal=(Duration)in;

								synchronized(dtFactory)
								{
									retVal=(T)dtFactory.newDuration(inVal.toString());
								}
							}else if (in instanceof XMLGregorianCalendar)
							{
								XMLGregorianCalendar inVal=(XMLGregorianCalendar)in;
								synchronized(dtFactory)
								{
									retVal=(T)dtFactory.newXMLGregorianCalendar(inVal.toString());
								}
							}
							return retVal;
						}
					};
				}
			});
		} catch (DatatypeConfigurationException e) {
			if (Log.isWarn())
				Log.warn(e);
		}
		return null;
	}



}
