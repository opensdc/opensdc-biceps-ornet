/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.messages;

import java.util.ArrayList;
import java.util.List;


/**
 * The Class BICEPSConstants.
 */
public class BICEPSConstants {
	
	/** The Constant URN_PREFIX_DELIM. */
	public final static String URN_PREFIX_DELIM							=	":".intern();
	
	/** The Constant URI_PATH_DELIM. */
	public final static String URI_PATH_DELIM							=	"/".intern();
	
	/** The Constant URI_QUERY_DELIM. */
	public final static String URI_QUERY_DELIM							=	"?".intern();
	
    /*PatientData */
    //TODO SSch review constants
    /** The Constant URI_SCHEMA. */
    public final static String URI_SCHEMA							= "biceps".intern();
    
    /** The Constant URI_SCHEMA_DELIM. */
    public final static String URI_SCHEMA_DELIM						= ".".intern();
    
    /** The Constant URI_SCHEMA_END. */
    public final static String URI_SCHEMA_END						= ":".intern();
    
    /* Context */
    /** The Constant CONTEXT_URI_SCHEMA_PART. */
    public final static String CONTEXT_URI_SCHEMA_PART				= "ctxt".intern();
    
    /** The Constant CONTEXT_URI_SCHEMA. */
    public final static String CONTEXT_URI_SCHEMA					= (URI_SCHEMA+URI_SCHEMA_DELIM+CONTEXT_URI_SCHEMA_PART).intern();
    
    /** The Constant URI_STANDARD_AUTHORITY. */
    public final static String URI_STANDARD_AUTHORITY				= "".intern();

    /** The Constant CONTEXT_ID_URI_PATH_PART. */
    public final static String CONTEXT_ID_URI_PATH_PART					=  "id".intern();
    
    /** The Constant CONTEXT_ID_URI_PATH. */
    public final static String CONTEXT_ID_URI_PATH					 	=  (URI_PATH_DELIM+CONTEXT_ID_URI_PATH_PART).intern();
    
    /** The Constant CONTEXT_ID_URI. */
    public final static String CONTEXT_ID_URI							=  (CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+CONTEXT_ID_URI_PATH).intern();
    
    /** The Constant CONTEXT_ID_NULL_FLAVOR_UNKNOWN. */
    public final static String CONTEXT_ID_NULL_FLAVOR_UNKNOWN			=  "UNK".intern();
    
    /** The Constant CONTEXT_ID_NULL_FLAVOR_MASKED. */
    public final static String CONTEXT_ID_NULL_FLAVOR_MASKED			=  "MSK".intern();
    
    /** The Constant CONTEXT_ASSOCIATION_URI_PATH_PART. */
    public final static String CONTEXT_ASSOCIATION_URI_PATH_PART		=  "assoc".intern();
    
    /** The Constant CONTEXT_ASSOCIATION_URI_PATH. */
    public final static String CONTEXT_ASSOCIATION_URI_PATH				=  (URI_PATH_DELIM+CONTEXT_ASSOCIATION_URI_PATH_PART).intern();
    
    /** The Constant CONTEXT_ASSOCIATION_URI. */
    public final static String CONTEXT_ASSOCIATION_URI					=  (CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+CONTEXT_ASSOCIATION_URI_PATH).intern();
    
    /* Patient */
    /** The Constant PATIENT_CONTEXT_URI_SCHEMA_PART. */
    public final static String PATIENT_CONTEXT_URI_SCHEMA_PART			=  "patient".intern();
    
    /** The Constant PATIENT_CONTEXT_URI_SCHEMA. */
    public final static String PATIENT_CONTEXT_URI_SCHEMA				=  (CONTEXT_URI_SCHEMA+URI_SCHEMA_DELIM+PATIENT_CONTEXT_URI_SCHEMA_PART).intern();

    /** The Constant PATIENT_ID_URI_PATH_PART. */
    public final static String PATIENT_ID_URI_PATH_PART					=  CONTEXT_ID_URI_PATH_PART;
    
    /** The Constant PATIENT_ID_URI_PATH. */
    public final static String PATIENT_ID_URI_PATH					 	=  CONTEXT_ID_URI_PATH;
    
    /** The Constant PATIENT_ID_URI. */
    public final static String PATIENT_ID_URI							=  (PATIENT_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+PATIENT_ID_URI_PATH).intern();
    
    /** The Constant PATIENT_ASSOCIATION_URI_PATH_PART. */
    public final static String PATIENT_ASSOCIATION_URI_PATH_PART		=  CONTEXT_ASSOCIATION_URI_PATH_PART;
    
    /** The Constant PATIENT_ASSOCIATION_URI_PATH. */
    public final static String PATIENT_ASSOCIATION_URI_PATH				=  CONTEXT_ASSOCIATION_URI_PATH;
    
    /** The Constant PATIENT_ASSOCIATION_URI. */
    public final static String PATIENT_ASSOCIATION_URI					=  (PATIENT_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+PATIENT_ASSOCIATION_URI_PATH).intern();
    
    /** The Constant PATIENT_UNKOWN_ID_URI. */
    public final static String PATIENT_UNKOWN_ID_URI					=  (PATIENT_ID_URI+URI_PATH_DELIM+CONTEXT_ID_NULL_FLAVOR_UNKNOWN).intern();
    
    /** The Constant PATIENT_MASKED_ID_URI. */
    public final static String PATIENT_MASKED_ID_URI					=  (PATIENT_ID_URI+URI_PATH_DELIM+CONTEXT_ID_NULL_FLAVOR_MASKED).intern();
    
    /* Location */
    /** The Constant LOCATION_CONTEXT_URI_SCHEMA_PART. */
    public final static String LOCATION_CONTEXT_URI_SCHEMA_PART			=  "location".intern();
    
    /** The Constant LOCATION_CONTEXT_URI_SCHEMA. */
    public final static String LOCATION_CONTEXT_URI_SCHEMA				=  (CONTEXT_URI_SCHEMA+URI_SCHEMA_DELIM+LOCATION_CONTEXT_URI_SCHEMA_PART).intern();

    /** The Constant LOCATION_ID_URI_PATH_PART. */
    public final static String LOCATION_ID_URI_PATH_PART				=  CONTEXT_ID_URI_PATH_PART;
    
    /** The Constant LOCATION_ID_URI_PATH. */
    public final static String LOCATION_ID_URI_PATH					 	=  CONTEXT_ID_URI_PATH;
    
    /** The Constant LOCATION_ID_URI. */
    public final static String LOCATION_ID_URI							=  (LOCATION_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+LOCATION_ID_URI_PATH).intern();
    
    /** The Constant LOCATION_MASKED_ID_URI. */
    public final static String LOCATION_MASKED_ID_URI					=  (LOCATION_ID_URI+URI_PATH_DELIM+CONTEXT_ID_NULL_FLAVOR_MASKED).intern();
    
    /** The Constant LOCATION_ASSOCIATION_URI_PATH_PART. */
    public final static String LOCATION_ASSOCIATION_URI_PATH_PART		=  CONTEXT_ASSOCIATION_URI_PATH_PART;
    
    /** The Constant LOCATION_ASSOCIATION_URI_PATH. */
    public final static String LOCATION_ASSOCIATION_URI_PATH			=  CONTEXT_ASSOCIATION_URI_PATH;
    
    /** The Constant LOCATION_ASSOCIATION_URI. */
    public final static String LOCATION_ASSOCIATION_URI					=  (LOCATION_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+LOCATION_ASSOCIATION_URI_PATH).intern();
    
    /* Ensemble */
    public final static String ENSEMBLE_CONTEXT_URI_SCHEMA_PART 		= "ensemble".intern();
    public final static String ENSEMBLE_CONTEXT_URI_SCHEMA 				= (CONTEXT_URI_SCHEMA+URI_SCHEMA_DELIM+ENSEMBLE_CONTEXT_URI_SCHEMA_PART).intern();
    public final static String ENSEMBLE_ID_URI_PATH_PART 				= CONTEXT_ID_URI_PATH_PART;
    public final static String ENSEMBLE_ID_URI_PATH 					= CONTEXT_ID_URI_PATH;
    public final static String ENSEMBLE_ID_URI 							= (ENSEMBLE_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+ENSEMBLE_ID_URI_PATH).intern();
    public final static String ENSEMBLE_MASKED_ID_URI					=  (ENSEMBLE_ID_URI+URI_PATH_DELIM+CONTEXT_ID_NULL_FLAVOR_MASKED).intern();
    public final static String ENSEMBLE_ASSOCIATION_URI_PATH_PART		=  CONTEXT_ASSOCIATION_URI_PATH_PART;
    public final static String ENSEMBLE_ASSOCIATION_URI_PATH			=  CONTEXT_ASSOCIATION_URI_PATH;
    public final static String ENSEMBLE_ASSOCIATION_URI					=  (ENSEMBLE_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+ENSEMBLE_ASSOCIATION_URI_PATH).intern();
    
    /* Workflow */
    public final static String WORKFLOW_CONTEXT_URI_SCHEMA_PART 		= "workflow".intern();
    public final static String WORKFLOW_CONTEXT_URI_SCHEMA 				= (CONTEXT_URI_SCHEMA+URI_SCHEMA_DELIM+WORKFLOW_CONTEXT_URI_SCHEMA_PART).intern();
    public final static String WORKFLOW_ID_URI_PATH_PART 				= CONTEXT_ID_URI_PATH_PART;
    public final static String WORKFLOW_ID_URI_PATH 					= CONTEXT_ID_URI_PATH;
    public final static String WORKFLOW_ID_URI 							= (WORKFLOW_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+WORKFLOW_ID_URI_PATH).intern();
    public final static String WORKFLOW_MASKED_ID_URI					=  (WORKFLOW_ID_URI+URI_PATH_DELIM+CONTEXT_ID_NULL_FLAVOR_MASKED).intern();
    public final static String WORKFLOW_ASSOCIATION_URI_PATH_PART		=  CONTEXT_ASSOCIATION_URI_PATH_PART;
    public final static String WORKFLOW_ASSOCIATION_URI_PATH			=  CONTEXT_ASSOCIATION_URI_PATH;
    public final static String WORKFLOW_ASSOCIATION_URI					=  (WORKFLOW_CONTEXT_URI_SCHEMA+URI_SCHEMA_END+URI_STANDARD_AUTHORITY+WORKFLOW_ASSOCIATION_URI_PATH).intern();
    

    //http://doriantaylor.com/policy/http-url-path-parameter-syntax
    /** The Constant QUERY_PARAM_DELIM. */
    public static final String QUERY_PARAM_DELIM					= ";".intern();
    
    /** The Constant QUERY_KEY_DELIM. */
    public static final String QUERY_KEY_DELIM						= "=".intern();
    
    /** The Constant INTERPRT_PARAM_TYPE_NAME. */
    public static final String INTERPRT_PARAM_TYPE_NAME 			= "I".intern();
    
    /** The Constant ROOT_PARAM_TYPE_NAME. */
    public static final String ROOT_PARAM_TYPE_NAME 				= "RT".intern();
    
    /** The Constant LOCATION_POC_ID_KEY. */
    public static final String LOCATION_POC_ID_KEY					= "poc".intern();
    
    /** The Constant LOCATION_ROOM_ID_KEY. */
    public static final String LOCATION_ROOM_ID_KEY					= "ro".intern();
    
    /** The Constant LOCATION_BED_ID_KEY. */
    public static final String LOCATION_BED_ID_KEY					= "bed".intern();
    
    /** The Constant LOCATION_BUILDING_ID_KEY. */
    public static final String LOCATION_BUILDING_ID_KEY				= "bdg".intern();
    
    /** The Constant LOCATION_FLOOR_ID_KEY. */
    public static final String LOCATION_FLOOR_ID_KEY				= "flr".intern();
    
    /** The Constant LOCATION_FACILITY_ID_KEY. */
    public static final String LOCATION_FACILITY_ID_KEY				= "fac".intern();
    
    /** The Constant PATIENT_PID_ID_KEY. */
    public static final String PATIENT_PID_ID_KEY					= "PID".intern(); //Patient identifier
    
    /** The Constant PATIENT_CID_ID_KEY. */
    public static final String PATIENT_CID_ID_KEY					= "CID".intern(); //Case Identifier
    
    
    
	/** The Constant LOCATION_ID_KEYS. */
	public static final List<String> LOCATION_ID_KEYS=new ArrayList<String>();
	static{
		//facility/building/point of care/floor/room/bed
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_FACILITY_ID_KEY);
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_BUILDING_ID_KEY);
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_POC_ID_KEY);
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_FLOOR_ID_KEY);
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_ROOM_ID_KEY);
		LOCATION_ID_KEYS.add(BICEPSConstants.LOCATION_BED_ID_KEY);
	}
}
