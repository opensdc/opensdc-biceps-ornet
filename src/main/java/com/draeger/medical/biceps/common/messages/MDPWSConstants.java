/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.messages;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;

import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;

public class MDPWSConstants
{
    public final static String MEDICAL_DEVICE_ID                         = SchemaHelper.NAMESPACE_SERVICES
                                                                                 + "/MedicalDevice".intern();
    public final static String OLD_DRAEGER_MEDICAL_DEVICE_ID             = SchemaHelper.OLD_DRAEGER_NAMESPACE_SERVICES+ "/MedicalDevice".intern();
    
    public final static QName  MEDICAL_DEVICE_ID_QN                      = QNameFactory.getInstance().getQName(MEDICAL_DEVICE_ID);
    public final static QName  OLD_DRAEGER_MEDICAL_DEVICE_ID_QN          = QNameFactory.getInstance().getQName(OLD_DRAEGER_MEDICAL_DEVICE_ID);

    /* PortType strings of services */
    public final static String PORTTYPE_GET_SERVICE                      = SchemaHelper.NAMESPACE_SERVICES
                                                                                 + "/GetService".intern();
    public final static String PORTTYPE_SET_SERVICE                      = SchemaHelper.NAMESPACE_SERVICES
                                                                                 + "/SetService".intern();
    public final static String PORTTYPE_REPORT_SERVICE                   = SchemaHelper.NAMESPACE_SERVICES
                                                                                 + "/ReportService".intern();
    public final static String PORTTYPE_STREAM_SERVICE                   = SchemaHelper.NAMESPACE_SERVICES
                                                                                 + "/WaveformStreamService".intern();
    
//    public final static String PORTTYPE_CONTEXT_SERVICE                   	= SchemaHelper.NAMESPACE_SERVICES+ "/PHIService".intern();
    
    public final static String PORTTYPE_CONTEXT_SERVICE                  = SchemaHelper.NAMESPACE_SERVICES+ "/ContextService".intern();

    /* Input action strings for get operations */
    public final static String ACTION_GET_MDIB                           = (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_MDIB_ELEMENT_NAME).intern();
    
	public static final String ACTION_GET_MDSTATE 						= (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_MDSTATE_ELEMENT_NAME).intern();
	
	public static final String ACTION_GET_MDDESCRIPTION 				= (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_MDDESCRIPTION_ELEMENT_NAME).intern();
	
	public static final String ACTION_GET_CONTAINMENTTREE 				= (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_CONTAINMENTTREE_ELEMENT_NAME).intern();
	
	public static final String ACTION_GET_DESCRIPTOR 					= (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_DESCRIPTOR_ELEMENT_NAME).intern();

    public final static String ACTION_GET_ALERT_STATES                   = (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_ALERT_STATES_ELEMENT_NAME).intern();
        
    public final static String ACTION_GET_METRIC_STATES                  = (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_METRIC_STATES_ELEMENT_NAME).intern();
    
//  public final static String ACTION_GET_METRICS                        = (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_METRICS_ELEMENT_NAME).intern();
//  public final static String ACTION_GET_ALERTS                         = (PORTTYPE_GET_SERVICE+ "/"+ SchemaHelper.GET_ALERTS_ELEMENT_NAME).intern();

    
    /* Input action strings for secured get operations */
    public final static String ACTION_GET_ID_CONTEXT_STATES                       = (PORTTYPE_CONTEXT_SERVICE+ "/"+ SchemaHelper.GET_ID_CONTEXT_STATES_ELEMENT_NAME).intern();
    
    public final static String ACTION_GET_CONTEXT_STATES     = (PORTTYPE_CONTEXT_SERVICE+ "/"+ SchemaHelper.GET_CONTEXT_STATES_ELEMENT_NAME).intern();

    /* Input action strings for set operations */
    public final static String ACTION_SET_RANGE                          = (PORTTYPE_SET_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.SET_RANGE_ELEMENT_NAME).intern();
    public final static String ACTION_SET_VALUE                          = (PORTTYPE_SET_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.SET_VALUE_ELEMENT_NAME).intern();
    public final static String ACTION_SET_STRING                         = (PORTTYPE_SET_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.SET_STRING_ELEMENT_NAME).intern();
    public final static String ACTION_SET_ALERT_STATE                     = (PORTTYPE_SET_SERVICE+ "/"+ SchemaHelper.SET_ALERT_STATE_ELEMENT_NAME).intern();
    
    public final static String ACTION_ACTIVATE                           = (PORTTYPE_SET_SERVICE+ "/"+ SchemaHelper.ACTIVATE_ELEMENT_NAME).intern();
    
    public final static String ACTION_SET_CONTEXT_STATE                  = (PORTTYPE_CONTEXT_SERVICE+ "/"+ SchemaHelper.SET_CONTEXT_STATE_ELEMENT_NAME).intern();
    
    /* Input action strings for secured set operations */
    public final static String ACTION_SET_ID_CONTEXT                        = (PORTTYPE_CONTEXT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.SET_CONTEXTID_STATE_ELEMENT_NAME).intern();

    /* Output action strings for reports */
    public final static String ACTION_PERIODIC_ALERT_REPORT              = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.PERIODIC_ALERT_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_EPISODIC_ALERT_REPORT              = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.EPISODIC_ALERT_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_PERIODIC_METRIC_REPORT             = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.PERIODIC_METRIC_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_EPISODIC_METRIC_REPORT             = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.EPISODIC_METRIC_REPORT_ELEMENT_NAME).intern();
//    public final static String ACTION_PERIODIC_PATIENT_REPORT            = (PORTTYPE_CONTEXT_SERVICE
//                                                                                 + "/"
//                                                                                 + SchemaHelper.PERIODIC_PATIENT_DATA_REPORT_ELEMENT_NAME).intern();
//    public final static String ACTION_EPISODIC_PATIENT_REPORT            = (PORTTYPE_CONTEXT_SERVICE
//                                                                                 + "/"
//                                                                                 + SchemaHelper.EPISODIC_PATIENT_DATA_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_PERIODIC_CONTEXT_CHANGED_REPORT      = (PORTTYPE_CONTEXT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_EPISODIC_CONTEXT_CHANGED_REPORT      = (PORTTYPE_CONTEXT_SERVICE
    																			+ "/"
    																			+ SchemaHelper.EPISODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_OPERATION_INVOKED_REPORT           = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.OPERATION_INVOKED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_OPERATIONAL_STATE_CHANGED_REPORT   = (PORTTYPE_REPORT_SERVICE+ "/"+ SchemaHelper.OPERATIONAL_STATE_CHANGED_REPORT_ELEMENT_NAME).intern();
    
    public final static String ACTION_OPERATION_CREATED_REPORT           = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.OPERATION_CREATED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_OPERATION_DELETED_REPORT           = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.OPERATION_DELETED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_OBJECT_CREATED_REPORT              = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.OBJECT_CREATED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_OBJECT_DELETED_REPORT              = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.OBJECT_DELETED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_SYSTEM_ERROR_REPORT                = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.SYSTEM_ERROR_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_MDS_CREATED_REPORT                 = (PORTTYPE_REPORT_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.MDS_CREATED_REPORT_ELEMENT_NAME).intern();
    public final static String ACTION_MDS_DELETED_REPORT                 = (PORTTYPE_REPORT_SERVICE+ "/"+ SchemaHelper.MDS_DELETED_REPORT_ELEMENT_NAME).intern();

    
    public final static URI    ACTION_PERIODIC_ALERT_REPORT_URI          = new URI(
                                                                                 ACTION_PERIODIC_ALERT_REPORT);
    public final static URI    ACTION_EPISODIC_ALERT_REPORT_URI          = new URI(
                                                                                 ACTION_EPISODIC_ALERT_REPORT);
    public final static URI    ACTION_PERIODIC_METRIC_REPORT_URI         = new URI(
                                                                                 ACTION_PERIODIC_METRIC_REPORT);
    public final static URI    ACTION_EPISODIC_METRIC_REPORT_URI         = new URI(
                                                                                 ACTION_EPISODIC_METRIC_REPORT);
//    public final static URI    ACTION_PERIODIC_PATIENT_REPORT_URI        = new URI(
//                                                                                 ACTION_PERIODIC_PATIENT_REPORT);
//    public final static URI    ACTION_EPISODIC_PATIENT_REPORT_URI        = new URI(
//                                                                                 ACTION_EPISODIC_PATIENT_REPORT);
    public final static URI    ACTION_PERIODIC_CONTEXT_CHANGED_REPORT_URI  = new URI(ACTION_PERIODIC_CONTEXT_CHANGED_REPORT);
    public final static URI    ACTION_EPISODIC_CONTEXT_CHANGED_REPORT_URI  = new URI(ACTION_EPISODIC_CONTEXT_CHANGED_REPORT);
    
    public final static URI    ACTION_OPERATION_INVOKED_REPORT_URI       = new URI(
                                                                                 ACTION_OPERATION_INVOKED_REPORT);
//    public final static URI    ACTION_OPERATION_CREATED_REPORT_URI       = new URI(
//                                                                                 ACTION_OPERATION_CREATED_REPORT);
//    public final static URI    ACTION_OPERATION_DELETED_REPORT_URI       = new URI(
//                                                                                 ACTION_OPERATION_DELETED_REPORT);
    public final static URI    ACTION_OBJECT_CREATED_REPORT_URI          = new URI(
                                                                                 ACTION_OBJECT_CREATED_REPORT);
    public final static URI    ACTION_OBJECT_DELETED_REPORT_URI          = new URI(
                                                                                 ACTION_OBJECT_DELETED_REPORT);

    public final static URI    ACTION_SYSTEM_ERROR_REPORT_URI            = new URI(
                                                                                 ACTION_SYSTEM_ERROR_REPORT);
    public final static URI    ACTION_MDS_CREATED_REPORT_URI             = new URI(
                                                                                 ACTION_MDS_CREATED_REPORT);
    
    public final static URI    ACTION_OPERATIONAL_STATE_CHANGED_REPORT_URI  = new URI(ACTION_OPERATIONAL_STATE_CHANGED_REPORT);

    /* Output action strings for streams */
    public final static String ACTION_WAVEFORM_STREAM                    = (PORTTYPE_STREAM_SERVICE
                                                                                 + "/"
                                                                                 + SchemaHelper.WAVEFORM_STREAM_ELEMENT_NAME).intern();

    /* Locales */
//    public final static String LOCALE_DE                                 = "de-DE".intern();
    
    /* OID */
//    public final static String URN_PREFIX_DELIM							=	":".intern();
//    public final static String OID_URN									= "urn:oid".intern();
    /* Context */
    
//    public final static String PATIENT_ASSOCIATION_URN					= (OID_URN+URN_PREFIX_DELIM).intern();
//    
//    public final static String LOCATION_TYPE_OID						="2.16.840.1.113883.4.642.2.336"; //http://fhir.azurewebsites.net/valueset-location-physical-type.html
//    public final static String LOCATION_TYPE_URN						= (OID_URN+URN_PREFIX_DELIM+LOCATION_TYPE_OID).intern();
//    
//    public final static String SERVICE_DELIVERY_LOCATION_TYPE_ROLE_OID	="2.16.840.1.113883.1.11.17660".intern(); //http://www.hl7.org/implement/standards/fhir/v3/vs/ServiceDeliveryLocationRoleType/index.html
//    public final static String SERVICE_DELIVERY_LOCATION_TYPE_ROLE_URN	= (OID_URN+URN_PREFIX_DELIM+SERVICE_DELIVERY_LOCATION_TYPE_ROLE_OID).intern();
    
// public final static String PATIENT_ASSOCIATION_URI					=  (SchemaHelper.NAMESPACE_SERVICES+"/"+"PatientAssociation").intern();
//    
//    
//    /*PatientData */
//    //TODO SSch review constants
//    public final static String PATIENTID_URN					 		=  "urn:draeger:com:DSC:CMDM:2012:05:patid".intern();//http://www.itm.uni-luebeck.de/projects/tekomed/CMDS/2011/04
//    public final static String BEDID_URN								=  "urn:draeger:com:DSC:CMDM:2012:05:bedid".intern();
//    
//    
//    public final static String PATIENTID_URN_PREFIX						=  	(PATIENTID_URN+URN_PREFIX_DELIM).intern();//http://www.itm.uni-luebeck.de/projects/tekomed/CMDS/2011/04
//    public final static String BEDID_URN_PREFIX							=  	(BEDID_URN+URN_PREFIX_DELIM).intern();
//    
//    public static final String PATIENT_UNKNOWN_URN 						= (PATIENTID_URN_PREFIX+"unknown").intern();
//    public static final String PATIENT_ANONYMOUS_URN					= (PATIENTID_URN_PREFIX+"anonymous").intern();
//    public static final String PATIENT_NO_URN							= (PATIENTID_URN_PREFIX+"nopatient").intern();
//    public static final String PATIENTS_MULTIPLE_URN					= (PATIENTID_URN_PREFIX+"multiplepatients").intern();

}
