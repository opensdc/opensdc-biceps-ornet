@javax.xml.bind.annotation.XmlSchema(
		namespace = "http://domain-model-uri/15/04",
		xmlns = {
				@javax.xml.bind.annotation.XmlNs(prefix = "d",namespaceURI = "http://domain-model-uri/15/04"),
				@javax.xml.bind.annotation.XmlNs(prefix="xs", namespaceURI="http://www.w3.org/2001/XMLSchema"),
				@javax.xml.bind.annotation.XmlNs(prefix="xmime", namespaceURI="http://www.w3.org/2005/05/xmlmime"),
				@javax.xml.bind.annotation.XmlNs(prefix="x", namespaceURI="http://www.w3.org/2001/XMLSchema-instance")
				},
		elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.draeger.medical.biceps.common.messages.v2;
