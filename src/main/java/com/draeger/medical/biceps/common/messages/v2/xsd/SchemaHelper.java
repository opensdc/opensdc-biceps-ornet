/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.messages.v2.xsd;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;

import org.ws4d.java.constants.SchemaConstants;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.Schema;
import org.ws4d.java.schema.SchemaException;
import org.ws4d.java.schema.Type;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.draeger.medical.biceps.common.messages.SchemaLoaderProvider;

public class SchemaHelper
{
	public final static String                NAMESPACE_MESSAGES                          = "http://message-model-uri/15/04".intern(); //TODO SSch Change to http://ornet.org/sm/14/06 when xsd schema is splitted
	public final static String 				  NAMESPACE_MESSAGES_PREFIX 				  = "d";
	public final static String                OLD_DRAEGER_NAMESPACE_SERVICES              = "http://www.draeger.com/projects/DSC/CMDM/2012/05".intern();
	public final static String                NAMESPACE_SERVICES                          = "http://message-model-uri/15/04".intern(); //TODO MK changed "http://ornet.org/sm/14/06".intern(); 
	public final static String 				  NAMESPACE_SERVICES_PREFIX 				  = "s";

	// ------------------------------------------------------------------------------------------------    

	public final static String                GET_MDIB_ELEMENT_NAME                       		= "GetMDIB".intern();
	public final static String                GET_MDIB_RESPONSE_ELEMENT_NAME              		= "GetMDIBResponse".intern();
	public final static String                GET_MDDESCRIPTION_ELEMENT_NAME              		= "GetMDDescription".intern();
	public final static String                GET_MDDESCRIPTION_RESPONSE_ELEMENT_NAME     		= "GetMDDescriptionResponse".intern();
	public final static String                GET_CONTAINMENTTREE_ELEMENT_NAME              	= "GetContainmentTree".intern();
	public final static String                GET_CONTAINMENTTREE_RESPONSE_ELEMENT_NAME     	= "GetContainmentTreeResponse".intern();
	public final static String                GET_DESCRIPTOR_ELEMENT_NAME              			= "GetDescriptor".intern();
	public final static String                GET_DESCRIPTOR_RESPONSE_ELEMENT_NAME     			= "GetDescriptorResponse".intern();
	public final static String                GET_MDSTATE_ELEMENT_NAME             		  		= "GetMDState".intern();
	public final static String                GET_MDSTATE_RESPONSE_ELEMENT_NAME     	  		= "GetMDStateResponse".intern();
	//    public final static String                GET_ALERTS_ELEMENT_NAME                     		= "GetAlertDescriptors".intern();
	//    public final static String                GET_ALERTS_RESPONSE_ELEMENT_NAME            		= "GetAlertDescriptorsResponse".intern();
	public final static String                GET_ALERT_STATES_ELEMENT_NAME               		= "GetAlertStates".intern();
	public final static String                GET_ALERT_STATES_RESPONSE_ELEMENT_NAME      		= "GetAlertStatesResponse".intern();
	//    public final static String                GET_METRICS_ELEMENT_NAME                    		= "GetMetricDescriptors".intern();
	//    public final static String                GET_METRICS_RESPONSE_ELEMENT_NAME           		= "GetMetricDescriptorsResponse".intern();
	public final static String                GET_METRIC_STATES_ELEMENT_NAME              		= "GetMetricStates".intern();
	public final static String                GET_METRIC_STATES_RESPONSE_ELEMENT_NAME     		= "GetMetricStatesResponse".intern();
	public final static String                GET_ID_CONTEXT_STATES_ELEMENT_NAME                = "GetIdentifiableContextStates".intern();
	public final static String                GET_ID_CONTEXT_STATES_RESPONSE_ELEMENT_NAME       = "GetIdentifiableContextStatesResponse".intern();
	public final static String                GET_CONTEXT_STATES_ELEMENT_NAME             		= "GetContextStates".intern();
	public final static String                GET_CONTEXT_STATES_RESPONSE_ELEMENT_NAME   		= "GetContextStatesResponse".intern();
	public final static String                PERIODIC_ALERT_REPORT_ELEMENT_NAME          		= "PeriodicAlertReport".intern();
	public final static String                EPISODIC_ALERT_REPORT_ELEMENT_NAME          		= "EpisodicAlertReport".intern();
	public final static String                PERIODIC_METRIC_REPORT_ELEMENT_NAME         		= "PeriodicMetricReport".intern();
	public final static String                EPISODIC_METRIC_REPORT_ELEMENT_NAME         		= "EpisodicMetricReport".intern();
//	public final static String                PERIODIC_PATIENT_DATA_REPORT_ELEMENT_NAME        	= "PeriodicPatientContextReport".intern();
//	public final static String                EPISODIC_PATIENT_DATA_REPORT_ELEMENT_NAME        	= "EpisodicPatientContextReport".intern();
	public final static String                PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME        = "PeriodicContextChangedReport".intern();
	public final static String                EPISODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME     	= "EpisodicContextChangedReport".intern();
	public final static String                OPERATION_INVOKED_REPORT_ELEMENT_NAME       		= "OperationInvokedReport".intern();
	public final static String                OPERATIONAL_STATE_CHANGED_REPORT_ELEMENT_NAME   	= "OperationalStateChangedReport".intern();
	public final static String                OPERATION_CREATED_REPORT_ELEMENT_NAME       		= "OperationCreatedReport".intern();
	public final static String                OPERATION_DELETED_REPORT_ELEMENT_NAME       		= "OperationDeletedReport".intern();
	public final static String                OBJECT_CREATED_REPORT_ELEMENT_NAME          		= "ObjectCreatedReport".intern();
	public final static String                OBJECT_DELETED_REPORT_ELEMENT_NAME         		= "ObjectDeletedReport".intern();
	public final static String                SYSTEM_ERROR_REPORT_ELEMENT_NAME            		= "SystemErrorReport".intern();
	public final static String                MDS_CREATED_REPORT_ELEMENT_NAME             		= "MDSCreatedReport".intern();
	public final static String                MDS_DELETED_REPORT_ELEMENT_NAME             		= "MDSDeletedReport".intern();
	public final static String                WAVEFORM_STREAM_ELEMENT_NAME                		= "WaveformStream".intern();
	public final static String                SET_RANGE_ELEMENT_NAME                      		= "SetRange".intern();
	public final static String                SET_RANGE_RESPONSE_ELEMENT_NAME             		= "SetRangeResponse".intern();
	public final static String                SET_VALUE_ELEMENT_NAME                      		= "SetValue".intern();
	public final static String                SET_VALUE_RESPONSE_ELEMENT_NAME             		= "SetValueResponse".intern();
	public final static String                SET_STRING_ELEMENT_NAME                     		= "SetString".intern();
	public final static String                SET_STRING_RESPONSE_ELEMENT_NAME            		= "SetStringResponse".intern();
	public final static String                SET_ALERT_STATE_ELEMENT_NAME   					= "SetAlertState".intern();
	public final static String                SET_ALERT_STATE_RESPONSE_ELEMENT_NAME  			= "SetAlertStateResponse".intern();
	public final static String                ACTIVATE_ELEMENT_NAME                       		= "Activate".intern();
	public final static String                ACTIVATE_RESPONSE_ELEMENT_NAME              		= "ActivateResponse".intern();
	public final static String                SET_CONTEXTID_STATE_ELEMENT_NAME                  = "SetIdentifiableContext".intern();
	public final static String                SET_CONTEXTID_STATE_RESPONSE_ELEMENT_NAME         = "SetIdentifiableContextResponse".intern();
	public final static String                SET_CONTEXT_STATE_ELEMENT_NAME             	= "SetContextState".intern();
	public final static String                SET_CONTEXT_STATE_RESPONSE_ELEMENT_NAME     	= "SetContextStateResponse".intern();
//	public final static String                SET_PATIENT_ELEMENT_NAME                    		= "SetPatientContextStateState".intern();
//	public final static String                SET_PATIENT_RESPONSE_ELEMENT_NAME           		= "SetPatientContextStateStateResponse".intern();
//	public final static String                SET_PATIENT_STATE_ELEMENT_NAME              		= "SetPatientAssociationState".intern();
//	public final static String                SET_PATIENT_STATE_RESPONSE_ELEMENT_NAME     		= "SetPatientAssociationStateResponse".intern();
	public final static String                HANDLE_REF_ELEMENT_NAME     						= "HandleRef".intern();

	// ------------------------------------------------------------------------------------------------    

	public final static QName                 GET_MDIB_ELEMENT_QN                         = QNameFactory.getInstance().getQName(
			GET_MDIB_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_MDIB_RESPONSE_ELEMENT_QN                = QNameFactory.getInstance().getQName(
			GET_MDIB_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_MDDESCRIPTION_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_MDDESCRIPTION_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_MDDESCRIPTION_RESPONSE_ELEMENT_QN        = QNameFactory.getInstance().getQName(
			GET_MDDESCRIPTION_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_CONTAINMENTTREE_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_CONTAINMENTTREE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_CONTAINMENTTREE_RESPONSE_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_CONTAINMENTTREE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_DESCRIPTOR_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_DESCRIPTOR_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_DESCRIPTOR_RESPONSE_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_DESCRIPTOR_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_MDSTATE_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_MDSTATE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_MDSTATE_RESPONSE_ELEMENT_QN        = QNameFactory.getInstance().getQName(
			GET_MDSTATE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);    

	//    public final static QName                 GET_ALERTS_ELEMENT_QN                       = QNameFactory.getInstance().getQName(
	//                                                                                                  GET_ALERTS_ELEMENT_NAME,
	//                                                                                                  SchemaHelper.NAMESPACE_MESSAGES);
	//    public final static QName                 GET_ALERTS_RESPONSE_ELEMENT_QN              = QNameFactory.getInstance().getQName(
	//                                                                                                  GET_ALERTS_RESPONSE_ELEMENT_NAME,
	//                                                                                                  SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_ALERT_STATES_ELEMENT_QN                 = QNameFactory.getInstance().getQName(
			GET_ALERT_STATES_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_ALERT_STATES_RESPONSE_ELEMENT_QN        = QNameFactory.getInstance().getQName(
			GET_ALERT_STATES_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	//    public final static QName                 GET_METRICS_ELEMENT_QN                      = QNameFactory.getInstance().getQName(
	//                                                                                                  GET_METRICS_ELEMENT_NAME,
	//                                                                                                  SchemaHelper.NAMESPACE_MESSAGES);
	//    public final static QName                 GET_METRICS_RESPONSE_ELEMENT_QN             = QNameFactory.getInstance().getQName(
	//                                                                                                  GET_METRICS_RESPONSE_ELEMENT_NAME,
	//                                                                                                  SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_METRIC_STATES_ELEMENT_QN                = QNameFactory.getInstance().getQName(
			GET_METRIC_STATES_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_METRIC_STATES_RESPONSE_ELEMENT_QN       = QNameFactory.getInstance().getQName(
			GET_METRIC_STATES_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);

	public final static QName                 GET_ID_CONTEXT_STATES_ELEMENT_QN                     = QNameFactory.getInstance().getQName(
			GET_ID_CONTEXT_STATES_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_ID_CONTEXT_STATES_RESPONSE_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			GET_ID_CONTEXT_STATES_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_CONTEXT_STATES_ELEMENT_QN                     = QNameFactory.getInstance().getQName(
			GET_CONTEXT_STATES_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 GET_CONTEXT_STATES_RESPONSE_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			GET_CONTEXT_STATES_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);


	public final static QName                 PERIODIC_ALERT_REPORT_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			PERIODIC_ALERT_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 EPISODIC_ALERT_REPORT_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			EPISODIC_ALERT_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 PERIODIC_METRIC_REPORT_ELEMENT_QN           = QNameFactory.getInstance().getQName(
			PERIODIC_METRIC_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 EPISODIC_METRIC_REPORT_ELEMENT_QN           = QNameFactory.getInstance().getQName(
			EPISODIC_METRIC_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
//	public final static QName                 PERIODIC_PATIENT_DATA_REPORT_ELEMENT_QN          = QNameFactory.getInstance().getQName(
//			PERIODIC_PATIENT_DATA_REPORT_ELEMENT_NAME,
//			SchemaHelper.NAMESPACE_MESSAGES);
//	public final static QName                 EPISODIC_PATIENT_DATA_REPORT_ELEMENT_QN          = QNameFactory.getInstance().getQName(
//			EPISODIC_PATIENT_DATA_REPORT_ELEMENT_NAME,
//			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_QN             = QNameFactory.getInstance().getQName(
			PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 EPISODIC_PATIENT_STATE_REPORT_ELEMENT_QN             = QNameFactory.getInstance().getQName(
			EPISODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);

	public final static QName                 OPERATION_INVOKED_REPORT_ELEMENT_QN         = QNameFactory.getInstance().getQName(
			OPERATION_INVOKED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 OPERATIONAL_STATE_CHANGED_REPORT_ELEMENT_QN         = QNameFactory.getInstance().getQName(
			OPERATIONAL_STATE_CHANGED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);


	public final static QName                 OPERATION_CREATED_REPORT_ELEMENT_QN         = QNameFactory.getInstance().getQName(
			OPERATION_CREATED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 OPERATION_DELETED_REPORT_ELEMENT_QN         = QNameFactory.getInstance().getQName(
			OPERATION_DELETED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 OBJECT_CREATED_REPORT_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			OBJECT_CREATED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 OBJECT_DELETED_REPORT_ELEMENT_QN            = QNameFactory.getInstance().getQName(
			OBJECT_DELETED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SYSTEM_ERROR_REPORT_ELEMENT_QN              = QNameFactory.getInstance().getQName(
			SYSTEM_ERROR_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 MDS_CREATED_REPORT_ELEMENT_QN               = QNameFactory.getInstance().getQName(
			MDS_CREATED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 MDS_DELETED_REPORT_ELEMENT_QN               = QNameFactory.getInstance().getQName(
			MDS_DELETED_REPORT_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);

	public final static QName                 WAVEFORM_STREAM_ELEMENT_QN                  = QNameFactory.getInstance().getQName(
			WAVEFORM_STREAM_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_RANGE_ELEMENT_QN                        = QNameFactory.getInstance().getQName(
			SET_RANGE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_RANGE_RESPONSE_ELEMENT_QN               = QNameFactory.getInstance().getQName(
			SET_RANGE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_VALUE_ELEMENT_QN                        = QNameFactory.getInstance().getQName(
			SET_VALUE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_VALUE_RESPONSE_ELEMENT_QN               = QNameFactory.getInstance().getQName(
			SET_VALUE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_STRING_ELEMENT_QN                       = QNameFactory.getInstance().getQName(
			SET_STRING_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_STRING_RESPONSE_ELEMENT_QN              = QNameFactory.getInstance().getQName(
			SET_STRING_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_ALERT_STATE_ELEMENT_QN                      = QNameFactory.getInstance().getQName(
			SET_ALERT_STATE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_ALERT_STATE_RESPONSE_ELEMENT_QN             = QNameFactory.getInstance().getQName(
			SET_ALERT_STATE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 ACTIVATE_ELEMENT_QN                         = QNameFactory.getInstance().getQName(
			ACTIVATE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 ACTIVATE_RESPONSE_ELEMENT_QN                = QNameFactory.getInstance().getQName(
			ACTIVATE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_CONTEXTID_STATE_ELEMENT_QN               = QNameFactory.getInstance().getQName(
			SET_CONTEXTID_STATE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_CONTEXTID_STATE_RESPONSE_ELEMENT_QN             = QNameFactory.getInstance().getQName(
			SET_CONTEXTID_STATE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_CONTEXT_STATE_ELEMENT_QN                = QNameFactory.getInstance().getQName(
			SET_CONTEXT_STATE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);
	public final static QName                 SET_CONTEXT_STATE_RESPONSE_ELEMENT_QN       = QNameFactory.getInstance().getQName(
			SET_CONTEXT_STATE_RESPONSE_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);

	public final static QName                 HANDLEREF_ELEMENT_QN       = QNameFactory.getInstance().getQName(
			HANDLE_REF_ELEMENT_NAME,
			SchemaHelper.NAMESPACE_MESSAGES);

	// ------------------------------------------------------------------------------------------------

	private final static SchemaHelper         instance;
	private final static XmlPullParserFactory XPP_FACTORY;


	private final Schema                      schemaWS4D;

	static
	{
		XmlPullParserFactory factory = null;
		try
		{
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			// the next is the default
			// factory.setValidating(false);
		}
		catch (XmlPullParserException e)
		{
			Log.error("Could not create XmlPullParserFactory: " + e);
			e.printStackTrace();
			throw new RuntimeException("Could not create XmlPullParserFactory: " + e);
		}
		XPP_FACTORY = factory;

		instance = new SchemaHelper();
	}

	public static SchemaHelper getInstance()
	{
		return instance;
	}

	private SchemaHelper()
	{
		//        String schemaTypesFile = System.getProperty("BICEPS.SchemaTypesFile",
		//        		"de/draeger/run/dice/common/messages/v2/xsd/BICEPSModel.xsd");
		//        InputStream stream = this.getClass().getResourceAsStream(schemaTypesFile);
		//        if (stream == null)
		//            throw new RuntimeException("VM Arg: MDPWS.ISOTypeFile=" + schemaTypesFile
		//                    + " is not valid!");
		InputStream[] streams=SchemaLoaderProvider.getInstance().getSchemaLoader().getSchemaInputStream();
		URL schemaBaseURL = SchemaLoaderProvider.getInstance().getSchemaLoader().getSchemaBaseURL();
		// parse the schema file and create a ws4d schema object
		int streamIndex = streams.length-1;
		schemaWS4D = parseSchema(streams[streamIndex], schemaBaseURL);
		try {
			for (InputStream inputStream : streams) {
				inputStream.close();
			}
			
		} catch (IOException e) {
			Log.error(e);
		}
	}

	private Schema parseSchema(InputStream stream, URL schemaBaseURL)
	{
		try
		{
			XmlPullParser parser = XPP_FACTORY.newPullParser();

			parser.setInput(stream, null);

			parser.nextTag(); // skip <?xml version="1.0" encoding="UTF-8" standalone="no"?>
			String schemaTNS = parser.getAttributeValue(null,
					SchemaConstants.SCHEMA_TARGETNAMESPACE);
			if (schemaTNS == null)
			{
				/*
				 * no explicit namespace set? use the targetNamespace from the
				 * WSDL?
				 */
				// TODO Check
			}

			Schema schema =null;
			try {
				URI baseURI=new URI(schemaBaseURL.toURI().toString());
				schema = Schema.parse(parser, baseURI, schemaTNS);
			} catch (URISyntaxException e) {
				Log.printStackTrace(e);
			}

			return schema;
		}
		catch (SchemaException e)
		{
			Log.error(e.getMessage());
		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	// ------------------------------------------------------------------------------------------------    

	public QName getSchemaName(String className)
	{
		QName retVal = QNameFactory.getInstance().getQName(className, SchemaHelper.NAMESPACE_MESSAGES);
		Element e = schemaWS4D.getElement(retVal);
		if (e != null) return e.getName();
		Type t = schemaWS4D.getType(retVal);
		if (t != null) return t.getName();
		return retVal;
	}

	public Element getSchemaElement(QName qname)
	{
		return schemaWS4D.getElement(qname);
	}

	public Element getSchemaElement(String className)
	{
		return schemaWS4D.getElement(QNameFactory.getInstance().getQName(className, SchemaHelper.NAMESPACE_MESSAGES));
	}

	public Element getGetMDIBElement()
	{
		return schemaWS4D.getElement(GET_MDIB_ELEMENT_QN);
	}

	public Element getGetMDIBResponseElement()
	{
		return schemaWS4D.getElement(GET_MDIB_RESPONSE_ELEMENT_QN);
	}

	public Element getGetMDDescriptionElement()
	{
		return schemaWS4D.getElement(GET_MDDESCRIPTION_ELEMENT_QN);
	}

	public Element getGetMDDescriptionResponseElement()
	{
		return schemaWS4D.getElement(GET_MDDESCRIPTION_RESPONSE_ELEMENT_QN);
	}
	
	public Element getGetDescriptorElement()
	{
		return schemaWS4D.getElement(GET_DESCRIPTOR_ELEMENT_QN);
	}

	public Element getGetDescriptorResponseElement()
	{
		return schemaWS4D.getElement(GET_DESCRIPTOR_RESPONSE_ELEMENT_QN);
	}
	
	public Element getGetContainmentTreeElement()
	{
		return schemaWS4D.getElement(GET_CONTAINMENTTREE_ELEMENT_QN);
	}

	public Element getGetContainmentTreeResponseElement()
	{
		return schemaWS4D.getElement(GET_CONTAINMENTTREE_RESPONSE_ELEMENT_QN);
	}

	public Element getGetMDStateElement()
	{
		return schemaWS4D.getElement(GET_MDSTATE_ELEMENT_QN);
	}

	public Element getGetMDStateResponseElement()
	{
		return schemaWS4D.getElement(GET_MDSTATE_RESPONSE_ELEMENT_QN);
	}

	//    public Element getGetAlertsElement()
	//    {
		//        return schemaWS4D.getElement(GET_ALERTS_ELEMENT_QN);
		//    }
	//
	//    public Element getGetAlertsResponseElement()
	//    {
	//        return schemaWS4D.getElement(GET_ALERTS_RESPONSE_ELEMENT_QN);
	//    }

	public Element getGetAlertStatesElement()
	{
		return schemaWS4D.getElement(GET_ALERT_STATES_ELEMENT_QN);
	}

	public Element getGetAlertStatesResponseElement()
	{
		return schemaWS4D.getElement(GET_ALERT_STATES_RESPONSE_ELEMENT_QN);
	}

	//    public Element getGetMetricsElement()
	//    {
	//        return schemaWS4D.getElement(GET_METRICS_ELEMENT_QN);
	//    }
	//
	//    public Element getGetMetricsResponseElement()
	//    {
	//        return schemaWS4D.getElement(GET_METRICS_RESPONSE_ELEMENT_QN);
	//    }
	//    
	public Element getGetMetricStatesElement()
	{
		return schemaWS4D.getElement(GET_METRIC_STATES_ELEMENT_QN);
	}

	public Element getGetMetricStatesResponseElement()
	{
		return schemaWS4D.getElement(GET_METRIC_STATES_RESPONSE_ELEMENT_QN);
	}


	public Element getGetIdContextStateElement()
	{
		return schemaWS4D.getElement(GET_ID_CONTEXT_STATES_ELEMENT_QN);
	}

	public Element getGetIdContextStateResponseElement()
	{
		return schemaWS4D.getElement(GET_ID_CONTEXT_STATES_RESPONSE_ELEMENT_QN);
	}

	public Element getGetContextStatesElement()
	{
		return schemaWS4D.getElement(GET_CONTEXT_STATES_ELEMENT_QN);
	}

	public Element getGetContextStatesResponseElement()
	{
		return schemaWS4D.getElement(GET_CONTEXT_STATES_RESPONSE_ELEMENT_QN);
	}

	public Element getPeriodicAlertReportElement()
	{
		return schemaWS4D.getElement(PERIODIC_ALERT_REPORT_ELEMENT_QN);
	}

	public Element getEpisodicAlertReportElement()
	{
		return schemaWS4D.getElement(EPISODIC_ALERT_REPORT_ELEMENT_QN);
	}

	public Element getPeriodicMetricReportElement()
	{
		return schemaWS4D.getElement(PERIODIC_METRIC_REPORT_ELEMENT_QN);
	}

	public Element getEpisodicMetricReportElement()
	{
		return schemaWS4D.getElement(EPISODIC_METRIC_REPORT_ELEMENT_QN);
	}

//	public Element getPeriodicPatientDataReportElement()
//	{
//		return schemaWS4D.getElement(PERIODIC_PATIENT_DATA_REPORT_ELEMENT_QN);
//	}
//
//	public Element getEpisodicPatientDataReportElement()
//	{
//		return schemaWS4D.getElement(EPISODIC_PATIENT_DATA_REPORT_ELEMENT_QN);
//	}
//
	public Element getPeriodicContextChangedReportElement()
	{
		return schemaWS4D.getElement(PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_QN);
	}

	public Element getEpisodicPatientStateReportElement()
	{
		return schemaWS4D.getElement(EPISODIC_PATIENT_STATE_REPORT_ELEMENT_QN);
	}

	public Element getOperationInvokedReportElement()
	{
		return schemaWS4D.getElement(OPERATION_INVOKED_REPORT_ELEMENT_QN);
	}

	public Element getOperationalStateChangedReportElement()
	{
		return schemaWS4D.getElement(OPERATIONAL_STATE_CHANGED_REPORT_ELEMENT_QN);
	}

	public Element getOperationCreatedReportElement()
	{
		return schemaWS4D.getElement(OPERATION_CREATED_REPORT_ELEMENT_QN);
	}

	public Element getOperationDeletedReportElement()
	{
		return schemaWS4D.getElement(OPERATION_DELETED_REPORT_ELEMENT_QN);
	}

	public Element getObjectCreatedReportElement()
	{
		return schemaWS4D.getElement(OBJECT_CREATED_REPORT_ELEMENT_QN);
	}

	public Element getObjectDeletedReportElement()
	{
		return schemaWS4D.getElement(OBJECT_DELETED_REPORT_ELEMENT_QN);
	}


	public Element getSystemErrorReportElement()
	{
		return schemaWS4D.getElement(SYSTEM_ERROR_REPORT_ELEMENT_QN);
	}

	public Element getMDSCreatedReportElement()
	{
		return schemaWS4D.getElement(MDS_CREATED_REPORT_ELEMENT_QN);
	}

	public Element getMDSDeletedReportElement()
	{
		return schemaWS4D.getElement(MDS_DELETED_REPORT_ELEMENT_QN);
	}

	public Element getWaveformStreamElement()
	{
		return schemaWS4D.getElement(WAVEFORM_STREAM_ELEMENT_QN);
	}

	public Element getSetRangeElement()
	{
		return schemaWS4D.getElement(SET_RANGE_ELEMENT_QN);
	}

	public Element getSetRangeResponseElement()
	{
		return schemaWS4D.getElement(SET_RANGE_RESPONSE_ELEMENT_QN);
	}

	public Element getSetValueElement()
	{
		return schemaWS4D.getElement(SET_VALUE_ELEMENT_QN);
	}

	public Element getSetValueResponseElement()
	{
		return schemaWS4D.getElement(SET_VALUE_RESPONSE_ELEMENT_QN);
	}

	public Element getSetStringElement()
	{
		return schemaWS4D.getElement(SET_STRING_ELEMENT_QN);
	}

	public Element getSetStringResponseElement()
	{
		return schemaWS4D.getElement(SET_STRING_RESPONSE_ELEMENT_QN);
	}

	public Element getSetAlertStateElement()
	{
		return schemaWS4D.getElement(SET_ALERT_STATE_ELEMENT_QN);
	}

	public Element getSetAlertStateResponseElement()
	{
		return schemaWS4D.getElement(SET_ALERT_STATE_RESPONSE_ELEMENT_QN);
	}

	public Element getActivateElement()
	{
		return schemaWS4D.getElement(ACTIVATE_ELEMENT_QN);
	}

	public Element getActivateResponseElement()
	{
		return schemaWS4D.getElement(ACTIVATE_RESPONSE_ELEMENT_QN);
	}

	public Element getSetContextIdElement()
	{
		return schemaWS4D.getElement(SET_CONTEXTID_STATE_ELEMENT_QN);
	}

	public Element getSetContextIdResponseElement()
	{
		return schemaWS4D.getElement(SET_CONTEXTID_STATE_RESPONSE_ELEMENT_QN);
	}

	public Element getSetContextStateElement()
	{
		return schemaWS4D.getElement(SET_CONTEXT_STATE_ELEMENT_QN);
	}

	public Element getSetContextStateResponseElement()
	{
		return schemaWS4D.getElement(SET_CONTEXT_STATE_RESPONSE_ELEMENT_QN);
	}



}
