/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.messages.v2.xsd;

import java.io.InputStream;
import java.net.URL;

import com.draeger.medical.biceps.common.messages.SchemaLoader;

public class V2SchemaLoader implements SchemaLoader {

	String schemaTypesFileName = System.getProperty("BICEPS.SchemaTypesFile","BICEPS_Model.xsd");
	
    @SuppressWarnings("resource")
	@Override
	public InputStream[] getSchemaInputStream()
    {
//    	String schemaTypesFileName = System.getProperty("BICEPS.SchemaTypesFile",
//		"BICEPS_MessagesV2.xsd");
		InputStream messageModel = createInputStream(schemaTypesFileName);
		InputStream extensionModel=createInputStream("ExtensionPoint.xsd");
		InputStream bicepsModel=createInputStream("BICEPS_DomainModel.xsd");
		
		
		return new InputStream[]{
				extensionModel,
				bicepsModel,
				messageModel
			};
    }
    
    @SuppressWarnings("resource")
	@Override
	public InputStream[] getSchemaModelInputStream()
    {
//    	String schemaTypesFileName = System.getProperty("BICEPS.SchemaTypesModelFile",
//		"BICEPS_Model.xsd");
		InputStream messageModel = createInputStream(schemaTypesFileName);
		InputStream extensionModel=createInputStream("ExtensionPoint.xsd");
		InputStream bicepsModel=createInputStream("BICEPS_DomainModel.xsd");
		
		return new InputStream[]{
				extensionModel,
				bicepsModel,
				messageModel
			};
    }

	private InputStream createInputStream(String schemaTypesFileName) {
		InputStream stream = V2SchemaLoader.class.getResourceAsStream(schemaTypesFileName);
		if (stream == null)
			throw new RuntimeException("VM Arg: BICEPS.SchemaTypesModelFile|BICEPS.SchemaTypesFile=" + schemaTypesFileName
					+ " is not valid!");
		return stream;
	}

	@Override
	public String getHandledPackageName() {
		return "com.draeger.medical.biceps.common.model";
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.common.messages.SchemaLoader#getSchemaBaseURI()
	 */
	@Override
	public URL getSchemaBaseURL() {
		URL resource = V2SchemaLoader.class.getResource(schemaTypesFileName);
		return resource;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.common.messages.SchemaLoader#getModelSchemaBaseURI()
	 */
	@Override
	public URL getModelSchemaBaseURI() {
		URL resource = V2SchemaLoader.class.getResource(schemaTypesFileName);
		return resource;
	}

}
