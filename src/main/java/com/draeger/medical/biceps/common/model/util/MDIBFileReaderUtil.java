/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.model.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;
import org.xml.sax.SAXException;

import com.draeger.medical.biceps.common.messages.SchemaLoaderProvider;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDIB;

/**
 * The Class MDIBFileReaderUtil is a utility that retrieves the description of an {@link MDDescription} from files.
 */
public class MDIBFileReaderUtil 
{

	/** The Constant MODEL_PACKAGE_NAME. */
	private final static String MODEL_PACKAGE_NAME= "com.draeger.medical.biceps.common.model";
	
	/** The unmarshaller. */
	private Unmarshaller unmarshaller;
	

	/**
	 * Instantiates a new MDIB file reader util.
	 */
	public MDIBFileReaderUtil()
	{
		InputStream[] stream=null;
		try
		{		 
			stream=SchemaLoaderProvider.getInstance().getSchemaLoader().getSchemaModelInputStream();
			createMarshallerAndUnmarshallerForPackage(MODEL_PACKAGE_NAME,stream);

		}
		catch (Exception e)
		{
			if (Log.isError()){
				Log.error("Could not create marshaller/unmarshaller: " + e);
				Log.error(e);
			}


			throw new RuntimeException(e);
		}finally{
			if (stream!=null)
				try {
					for (InputStream inputStream : stream) {
						inputStream.close();	
					}
				} catch (IOException e) {
					if (Log.isError()){
						Log.error("Could not close schema stream. "+e);
						Log.error(e);
					}

				}
		}
	}

	/**
	 * Gets the {@link MDDescription} from a file.
	 *
	 * @param file the file to read the description from
	 * @return the description from file
	 */
	public MDDescription getDescriptionFromFile(File file){

		if (file ==null)
		{
			IllegalArgumentException e=new IllegalArgumentException("Could not load  MDDescription from File. The file is null.");
			if (Log.isError())
			{
				Log.error(e);
			}
			throw e;
		}

		MDDescription retVal=null;
		try{
			Object unmarshalledObject = unmarshaller.unmarshal(file);
			if (unmarshalledObject instanceof JAXBElement<?>)
			{
				MDIB t= (MDIB)((JAXBElement<?>) unmarshalledObject).getValue();
				retVal=t.getDescription();
			}

		}catch(Exception e){
			Log.error(e);
			Log.error(e.getCause());
		}
		return retVal;
	}

	/**
	 * Gets the {@link MDDescription} from a file identified by an {@link URI}.
	 *
	 * @param descriptionFileUri the description file uri
	 * @return the description from file
	 */
	public MDDescription getDescriptionFromFile(URI descriptionFileUri){
		MDDescription retVal=null;
		InputStream descriptionFileStream=null;
		try{
			Object unmarshalledObject = null;
			Toolkit tk=null;
			if ((tk=PlatformSupport.getInstance().getToolkit())!=null)
			{
				descriptionFileStream=tk.getResourceAsStream(descriptionFileUri);
				if (descriptionFileStream!=null)
				{
					unmarshalledObject=unmarshaller.unmarshal(descriptionFileStream);
				}else{
					if (Log.isError())
					{
						Log.error("Could not load  MDDescription from InputStream with URI:"+descriptionFileStream+". The InputStream is null. " );
					}
				}

			}else {
				if (Log.isError())
				{
					Log.error("Could not load  MDDescription from InputStream with URI:"+descriptionFileStream+". The toolkit is null.");
				}
			}
			if (unmarshalledObject instanceof JAXBElement<?>)
			{
				MDIB t= (MDIB)((JAXBElement<?>) unmarshalledObject).getValue();
				retVal=t.getDescription();
			}
		}catch(Exception e){
			if (Log.isError())
			{
				Log.error(e);
			}
		}finally{
			if (descriptionFileStream!=null)
			{
				try {
					descriptionFileStream.close();
				} catch (IOException e) {
					Log.warn(e);
				}
			}
		}
		return retVal;
	}

	/**
	 * Gets the {@link MDDescription} from an input stream.
	 *
	 * @param descriptionFileStream the file input stream
	 * @return the description from input stream
	 */
	public MDDescription getDescriptionFromInputStream(InputStream descriptionFileStream){
		MDDescription retVal=null;
		try{
			Object unmarshalledObject = null;
			if (descriptionFileStream!=null)
			{
				unmarshalledObject=unmarshaller.unmarshal(descriptionFileStream);
			}else{
				if (Log.isError())
				{
					Log.error("Could not load  MDDescription from InputStream. The InputStream is null. " );
				}
			}

			if (unmarshalledObject instanceof JAXBElement<?>)
			{
				MDIB t= (MDIB)((JAXBElement<?>) unmarshalledObject).getValue();
				retVal=t.getDescription();
			}
		}catch(Exception e){
			if (Log.isError())
			{
				Log.error(e);
			}
		}
		return retVal;
	}

	/**
	 * Creates the marshaller and unmarshaller for package.
	 *
	 * @param stream the stream
	 * @param packageName the package name
	 * @throws SAXException the SAX exception
	 * @throws JAXBException the JAXB exception
	 */
	private void createMarshallerAndUnmarshallerForPackage( String packageName, InputStream... stream)
			throws SAXException, JAXBException {
		javax.xml.validation.Schema schema = loadSchema(stream);
		JAXBContext jaxbCtxt = initContext(packageName);
		unmarshaller=createUnmarshaller(jaxbCtxt,schema,packageName);
	}

	/**
	 * Creates the unmarshaller.
	 *
	 * @param jaxbCtxt the jaxb ctxt
	 * @param schema the schema
	 * @param packageName the package name
	 * @return the unmarshaller
	 * @throws JAXBException the JAXB exception
	 */
	private Unmarshaller createUnmarshaller(JAXBContext jaxbCtxt,Schema schema, String packageName) throws JAXBException {
		Unmarshaller unmarshaller = jaxbCtxt.createUnmarshaller();
		if (schema!=null) unmarshaller.setSchema(schema);

		return unmarshaller;

	}


	/**
	 * Load schema.
	 *
	 * @param stream the stream
	 * @return the javax.xml.validation. schema
	 * @throws SAXException the SAX exception
	 */
	private javax.xml.validation.Schema loadSchema(InputStream... stream)
			throws SAXException {
		Source[] sources = new Source[stream.length];
		for (int i=0;i<stream.length;i++) {
			sources[i]= new StreamSource(stream[i]);
		}
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		javax.xml.validation.Schema schema = schemaFactory.newSchema(sources);

		if (Float.parseFloat(System.getProperty("java.version").substring(0, 3)) < 1.6)
		{
			if (Log.isDebug())
				Log.debug("Java < 1.6: Add BugFix code for JAXB!");

			Thread.currentThread().setContextClassLoader(JAXBContext.class.getClassLoader());
		}
		return schema;
	}

	/**
	 * Inits the context.
	 *
	 * @param packageName the package name
	 * @return the JAXB context
	 */
	private static JAXBContext initContext(String packageName) {
		try
		{
			return  JAXBContext.newInstance(packageName);
		}
		catch (Exception e)
		{
			if(Log.isError()){
				Log.error("Could not create JAXB context:"+e.getMessage());
				Log.error(e);
			}
		}
		return null;
	}


}
