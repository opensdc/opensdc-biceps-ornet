/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.model.util;

import java.io.ByteArrayOutputStream;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.ws4d.java.constants.XMLConstants;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.HashMap;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.BICEPSControlProxy;
import com.draeger.medical.biceps.client.proxy.BICEPSStateProxy;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.utils.InvokeHelper;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.common.util.XPathNamespaceContext;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.dualchannel.DualChannelPolicy;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.def.ContextDefinition;
import com.draeger.medical.mdpws.qos.safetyinformation.def.DualChannelSelector;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;
import com.draeger.medical.mdpws.utils.InputStreamUtil;

public class MDIBSafetyInformationUtil {

	
	/**
	 * Requires dual channel.
	 *
	 * @param metric the metric
	 * @return true, if successful
	 */
	public static boolean requiresDualChannel(BICEPSMetric metric)
	{
		final String handleExpression = ":OperationHandle/text()"; //TODO SSch Refctor QoS
		boolean result = false;
		BICEPSStateProxy stateProxy = metric.getStateProxy();
		if (stateProxy!=null)
		{
			BICEPSControlProxy settingProxy = stateProxy.getControlProxy();
			if (settingProxy!=null)
			{
				Set<? extends OperationDescriptor> operations = settingProxy.getOperationDescriptors();
				for(OperationDescriptor op : operations)
				{
					String inputAction = InvokeHelper.getInputAction(op);
					result = MDIBSafetyInformationUtil.checkIfActionRequiresSafetyInformation(handleExpression, inputAction);
					if (result) break;
				}
			}
			else
			{
				if (Log.isDebug())
				{
					Log.debug("No state proxy available for metric "+metric);
				}
			}
		}
		return result;
	}
	
	public static boolean checkIfActionRequiresSafetyInformation(final String handleExpression,String inputAction) {

		boolean result=false;
		if (inputAction != null)
		{
			org.ws4d.java.structures.ArrayList policyList = QoSPolicyUtil.getApplicablePoliciesForAction(inputAction);
			for (int i = 0; i < policyList.size(); ++i)
			{
				if(policyList.get(i) instanceof DualChannelPolicy)
				{
					String xPathExpression = ((DualChannelPolicy)policyList.get(i)).getDualChannelPolicyAttributes().getXPath().getExpression();
					result = !xPathExpression.contains(handleExpression);
					if (result) break;
				}
			}
		}
		return result;
	}

	public void attachSafetyInformations(IParameterValue result)
	{
		if (result!=null)
		{

			Iterator policiesIt = QoSPolicyManager.getInstance().getQoSPolicies();
			if (policiesIt!=null && policiesIt.hasNext())
			{
				try {
					java.util.ArrayList<SafetyInformationPolicy> siPolicies=new java.util.ArrayList<SafetyInformationPolicy>();
					while(policiesIt.hasNext())
					{
						QoSPolicy somePolicy=(QoSPolicy)policiesIt.next();
						if (somePolicy instanceof SafetyInformationPolicy)
						{
							SafetyInformationPolicy siPolicy = (SafetyInformationPolicy)somePolicy;
							if (Log.isDebug())
								Log.debug("Try to attach siPolicy to MDIB");

							siPolicies.add(siPolicy);
						}

					}
					if (!siPolicies.isEmpty())
					{
						boolean changedDOM=false;
						Document mdibDocument = getDocumentBuilder().parse (InputStreamUtil.convertStringToInputStream(result.getOverride(), XMLConstants.ENCODING));
						//System.out.println(result.getOverride());
						HashMap declaredNamespaces = XPathInfo.getDeclaredNamesSpaces(mdibDocument);
						XPathNamespaceContext xPathNameCtxt = XPathInfo.createXPathSpaceContext(declaredNamespaces);

						for (SafetyInformationPolicy safetyInformationPolicy : siPolicies) {
							SafetyInformationPolicyAttributes safetyInformationPolicyAttr = safetyInformationPolicy.getSafetyInformationPolicyAttributes();
							XPathInfo operationFilterMDIB=getOperationFilterMDIB(safetyInformationPolicyAttr,xPathNameCtxt);
							if (operationFilterMDIB!=null)
							{
								NodeList operationNodes = getOperationNodes(mdibDocument, operationFilterMDIB);
								if (operationNodes!=null && operationNodes.getLength()>0)
								{
									String currentSINamespacePrefix = (String) declaredNamespaces.get(SafetyInformationConstants.SI_NAMESPACE);
									if (currentSINamespacePrefix == null) {
										currentSINamespacePrefix = SafetyInformationConstants.SI_DEFAULT_NAMESPACE_PREFIX;
										declaredNamespaces.put(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix);
									}

									int nodeCnt=operationNodes.getLength();
									for(int operationNodeCnt=0;operationNodeCnt<nodeCnt;operationNodeCnt++)
									{
										Node operationNode=operationNodes.item(operationNodeCnt);
										//System.out.println(operationNode);

										changedDOM=true;
										Element siTransmissionSOAPHeaderElement = mdibDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, 
												currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_DEFINITION_SAFETY_REQ);

										if (safetyInformationPolicyAttr.isTransmitDualChannel()&& !safetyInformationPolicyAttr.getDualChannelSelectors().isEmpty())
										{
											Element dualChannelDef = mdibDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_DEFINITION_DUALCHANNEL_DEF);
											addQNameAttribute(SafetyInformationConstants.SI_NAMESPACE, dualChannelDef,
													safetyInformationPolicyAttr.getAlgorithm(), declaredNamespaces, currentSINamespacePrefix
													+ ":"+SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_ALGORITHM);

											addQNameAttribute(SafetyInformationConstants.SI_NAMESPACE, dualChannelDef,
													safetyInformationPolicyAttr.getTransform(), declaredNamespaces, currentSINamespacePrefix
													+ ":"+SafetyInformationConstants.SI_ATTRIB_DUALCHANNEL_TRANSFORM);

											siTransmissionSOAPHeaderElement.appendChild(dualChannelDef);

											for (DualChannelSelector dualChannelSelector : safetyInformationPolicyAttr.getDualChannelSelectors()) 
											{
												QName selectorQName = dualChannelSelector.getElementQName();
												appendSelector(
														mdibDocument,
														declaredNamespaces,
														currentSINamespacePrefix,
														dualChannelDef,
														selectorQName);
											}
										}

										if (safetyInformationPolicyAttr.isTransmitSafetyContext() && !safetyInformationPolicyAttr.getContextDefinitions().isEmpty())
										{
											for (ContextDefinition ctxtDef : safetyInformationPolicyAttr.getContextDefinitions()) {
												Element ctxtDefElement = mdibDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_DEFINITION_CONTEXT_DEF);

												ctxtDefElement.setAttributeNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix
														+ ":"+SafetyInformationConstants.SI_ATTRIB_TARGET,ctxtDef.getContextTarget());

												for (QName attributeSelectors : ctxtDef.getAttributeSelectors()) {
													appendSelector(
															mdibDocument,
															declaredNamespaces,
															currentSINamespacePrefix,
															ctxtDefElement,
															attributeSelectors);

												}

												siTransmissionSOAPHeaderElement.appendChild(ctxtDefElement);

											}

										}

										operationNode.appendChild(siTransmissionSOAPHeaderElement);
									}
								}
							}

						}
						if (changedDOM)
						{
							ByteArrayOutputStream baos=new ByteArrayOutputStream();
							Transformer transformer= getTransformer();
							transformer.setOutputProperty(OutputKeys.INDENT, "no");
							transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
							transformer.transform(new DOMSource(mdibDocument), new StreamResult(baos));

							result.overrideSerialization(baos.toString(XMLConstants.ENCODING));
						}
					}

				} catch (Exception e) {
					if (Log.isWarn()){
						Log.warn("Could not attach safety information to MDIB:"+e.getMessage());
						Log.warn(e);
					}
				}
			}


		}
	}




	private void appendSelector(Document mdibDocument,
			HashMap declaredNamespaces, String currentSINamespacePrefix,
			Element dualChannelDef, QName selectorQName) {
		Element dcSelector = mdibDocument.createElementNS(SafetyInformationConstants.SI_NAMESPACE, currentSINamespacePrefix + ":"+SafetyInformationConstants.SI_ELEM_DEFINITION_SELECTOR);


		String selectorPrefix = (String)declaredNamespaces.get(selectorQName.getNamespace());

		dcSelector.setTextContent(selectorPrefix+":"+selectorQName.getLocalPart());

		dualChannelDef.appendChild(dcSelector);
	}

	private void addQNameAttribute(String namespace,
			Element valueElement, QName attrValue, HashMap declaredNameSpaces,
			String attributeNamePrefixed) {
		String nameSpace = attrValue.getNamespace();
		String nameSpacePrefix = getNameSpacePrefix(valueElement, declaredNameSpaces,
				nameSpace);

		valueElement.setAttributeNS(
				namespace,
				attributeNamePrefixed,
				(nameSpace == null ? "" : (nameSpacePrefix + ":"))
				+ attrValue.getLocalPart());
	}

	private String getNameSpacePrefix(Element valueElement,
			HashMap declaredNameSpaces, String nameSpace) {
		String nameSpacePrefix = (String) declaredNameSpaces.get(nameSpace);
		if (nameSpacePrefix == null && nameSpace != null && valueElement!=null) {
			nameSpacePrefix = "i" + System.nanoTime();
			declaredNameSpaces.put(nameSpace, nameSpacePrefix);

			//for the attribute qname value we have to explicitly add the namespace
			valueElement
			.setAttributeNS(XMLConstants.XMLNS_NAMESPACE_NAME,
					XMLConstants.XMLNS_NAMESPACE_PREFIX + ":"
							+ nameSpacePrefix, nameSpace);
		}
		return nameSpacePrefix;
	}

	private XPathInfo getOperationFilterMDIB(
			SafetyInformationPolicyAttributes safetyInformationPolicyAttr, XPathNamespaceContext xPathNameCtxt) 
	{
		XPathInfo xPathFilter=null;
		if (safetyInformationPolicyAttr!=null && xPathNameCtxt!=null  && safetyInformationPolicyAttr.getMessageFilter()!=null)
		{


			String bicepsPrefix = xPathNameCtxt.getPrefix(SchemaHelper.NAMESPACE_MESSAGES);
			if (bicepsPrefix!=null)
			{
				//CMDM:SCO/CMDM:Operation[CMDM:Handle='a' or CMDM:Handle='b']
				java.util.ArrayList<String> operationIds = safetyInformationPolicyAttr.getMessageFilter().getApplicationSpecificIDs();
				if (operationIds.size()>0)
				{
					StringBuilder sb=new StringBuilder("//"+bicepsPrefix+":SCO/"+bicepsPrefix+":Operation["); 

					for (int i=0;i<operationIds.size();i++) 
					{
						if (i>0)
							sb.append(" or ");

						sb.append(bicepsPrefix);
						sb.append(":Handle='");
						sb.append(operationIds.get(i));
						sb.append("'");
					}
					sb.append("]");
					xPathFilter=new XPathInfo(sb.toString(), xPathNameCtxt);
					//System.out.println(xPathFilter);
				}
			}
		}
		return xPathFilter;
	}


	private TransformerFactory factory= TransformerFactory.newInstance();
	private static final DocumentBuilderFactory docBuilderFactory;
	static{
		docBuilderFactory = DocumentBuilderFactory.newInstance();
		docBuilderFactory.setNamespaceAware(true);
	}
	private DocumentBuilder builder;
	private synchronized DocumentBuilder getDocumentBuilder() throws ParserConfigurationException
	{
		if (this.builder==null){
			if (Log.isDebug())
				Log.debug("Create new builder in Thread: "+Thread.currentThread());

			this.builder = docBuilderFactory.newDocumentBuilder();
		}

		return this.builder;
	}

	private synchronized Transformer getTransformer() throws TransformerConfigurationException
	{
		return factory.newTransformer();
	}

	private NodeList getOperationNodes(Document dualChannelDoc, XPathInfo operationFilterMDIB) throws XPathExpressionException {
		return (NodeList) operationFilterMDIB.evaluate(dualChannelDoc,XPathConstants.NODESET);
	}

}
