/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.Extension;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.Retrievability;

public class MetricDescriptorUtil {


	public static Collection<? extends AbstractMetricState> filterMetricStatesByRetrievability(Collection<? extends MetricDescriptor> descriptorList,Collection<? extends AbstractMetricState> modelMetricStates, MetricRetrievability retrievability) 
	{
		ArrayList<AbstractMetricState> retVal=new ArrayList<AbstractMetricState>();
		if (modelMetricStates!=null && !modelMetricStates.isEmpty()){

			Map<String,MetricDescriptor> filteredDescriptorMap=new HashMap<String, MetricDescriptor>();
			for (MetricDescriptor metricDescriptor : descriptorList) {
				List<MetricRetrievability> retrievabilityForMetric =MetricDescriptorUtil.getRetrievabilityOptions(metricDescriptor);
				if (MetricDescriptorUtil.isRetrievable(retrievabilityForMetric,retrievability))
				{
					filteredDescriptorMap.put(metricDescriptor.getHandle(), metricDescriptor);
				}
			}
			for (AbstractMetricState metricState : modelMetricStates) 
			{
				if (filteredDescriptorMap.containsKey(metricState.getReferencedDescriptor()))
				{
					retVal.add(metricState);
				}
			}
		}
		return retVal; 
	}

	public static boolean isRetrievable(Descriptor descriptor, MetricRetrievability retrievabilityMethod) 
	{

		List<MetricRetrievability> retrievabilityOptions=null;

		if (descriptor!=null)
		{
			retrievabilityOptions=getRetrievabilityOptions(descriptor);
		}


		return isRetrievable(retrievabilityOptions, retrievabilityMethod);
	}


	/**
	 * @return
	 */
	public static List<MetricRetrievability> getRetrievabilityOptions(Descriptor descriptor) {
		List<MetricRetrievability> retrievabilityOptions=new  ArrayList<MetricRetrievability>();
		Extension extension = descriptor.getExtension();
		if (extension!=null){
			List<Object> anyList = extension.getAny();

			if (anyList!=null)
			{
				for (Object object : anyList) {
					if (object instanceof Retrievability){
						Retrievability retrievability = (Retrievability)object;
						if (retrievability.getBy()!=null)
							retrievabilityOptions.addAll(retrievability.getBy());
					}
				}				
			}

		}
		//QUICK HACK 
		if (!retrievabilityOptions.contains(MetricRetrievability.EPISODIC))
			retrievabilityOptions.add(MetricRetrievability.EPISODIC);
		// Besting/SurgiTAIX: Another one
		if (descriptor instanceof RealTimeSampleArrayMetricDescriptor && !retrievabilityOptions.contains(MetricRetrievability.STREAM))
			retrievabilityOptions.add(MetricRetrievability.STREAM);
		
		return retrievabilityOptions;
	}

	public static void addRetrievabilityOptions(Descriptor descriptor, List<MetricRetrievability> retrievabilityOptions) {
		if (descriptor!=null)
		{
			Extension extension = descriptor.getExtension();
			if (extension==null){
				extension=new Extension();
				descriptor.setExtension(extension);
			}

			List<Object> anyList = extension.getAny();

			if (anyList!=null)
			{
				Retrievability retrievability=new Retrievability();
				retrievability.getBy().addAll(retrievabilityOptions);
				anyList.add(retrievability);
			}

		}
	}

	public static boolean isRetrievable(List<MetricRetrievability> retrievabilityOptions, MetricRetrievability retrievabilityMethod) 
	{
		boolean isRetrievable=(retrievabilityMethod==null);
		if (retrievabilityOptions!=null && retrievabilityMethod!=null)
		{
			for (MetricRetrievability metricRetrievability : retrievabilityOptions) {
				if (retrievabilityMethod.equals(metricRetrievability)){
					isRetrievable=true;
					break;
				}
			}
		}
		return isRetrievable;
	}
}
