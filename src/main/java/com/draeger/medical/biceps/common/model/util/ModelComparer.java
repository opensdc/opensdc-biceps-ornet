/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.model.util;

import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;
import com.draeger.medical.biceps.common.model.Measure;
import com.draeger.medical.biceps.common.model.PatientDemographicsCoreData;
import com.draeger.medical.biceps.common.model.Range;


public final class ModelComparer
{
//    public static <T> boolean equals(List<T> x, List<T> y)
//    {
//        boolean result = true;
//        if (x == y)
//            result = true;
//        else if (x == null && y.isEmpty() || y == null && x.isEmpty())
//            // An empty list is assumed to be equal to a null-pointer.
//            result = true;
//        else
//        {
//            if (x.size() != y.size())
//                result = false;
//            else if (x.size() == 0)
//                result = true;
//            else
//            {
//                int i = 0;
//                do
//                {
//                    result = equals(x.get(i), y.get(i));
//                    ++i;
//                } while (result && i < x.size());
//            }
//        }
//        return result;
//    }

    public static boolean equals(Object x, Object y)
    {
        if (x == y) return true;
        if (x == null || y == null) return false;
        if (x.getClass().getPackage().getName().equals("com.draeger.medical.biceps.common.model")
                && !x.getClass().isEnum())
        {
            org.ws4d.java.util.Log
                    .warn("Missing equals method for model object! -- ModelComparer.equals(Object("
                            + x.getClass() + "), Object(" + y.getClass() + "))");
        }
        return x.equals(y);
    }

    
    
	public static boolean equals(CodedValue x, CodedValue y) {
		if (x == y)
			return true;
		if (x == null || y==null)
			return false;
		if (x.getClass() != y.getClass())
			return false;
		if (x.getCode() == null) {
			if (y.getCode() != null)
				return false;
		} else if (!x.getCode().equals(y.getCode()))
			return false;
		if (x.getCodingSystem() == null) {
			if (y.getCodingSystem() != null)
				return false;
		} else if (!x.getCodingSystem().equals(y.getCodingSystem()))
			return false;
//		if (x.getDescriptions() == null) {
//			if (y.getDescriptions() != null)
//				return false;
//		} else if (!x.getDescriptions().equals(x.getDescriptions()))
//			return false;
		if (x.getVersion() == null) {
			if (y.getVersion() != null)
				return false;
		} else if (!x.getVersion().equals(y.getVersion()))
			return false;
		return true;
	}
   
//    public static boolean equals(Text x, Text obj)
//    {
//    		if (x == obj)
//    			return true;
//    		if (obj == null)
//    			return false;
//    		if (x.getClass() != obj.getClass())
//    			return false;
//    		Text other = (Text) obj;
//    		if (x.getLang() == null) {
//    			if (other.getLang() != null)
//    				return false;
//    		} else if (!x.getLang().equals(other.getLang()))
//    			return false;
//    		if (x.getValue() == null) {
//    			if (other.getValue() != null)
//    				return false;
//    		} else if (!x.getValue().equals(other.getValue()))
//    			return false;
//    		return true;
//    }

//    public static boolean equals(NuObservedValue x, NuObservedValue obj)
//    {
//    	if (x == obj)
//			return true;
//		if (obj == null || x==null)
//			return false;
//		if (x.getClass() != obj.getClass())
//			return false;
//		NuObservedValue other = (NuObservedValue) obj;
//		if (x.metricCode == null) {
//			if (other.metricCode != null)
//				return false;
//		} else if (!x.metricCode.equals(other.metricCode))
//			return false;
//		if (x.status != other.status)
//			return false;
//		if (x.unitCode == null) {
//			if (other.unitCode != null)
//				return false;
//		} else if (!x.unitCode.equals(other.unitCode))
//			return false;
//		if (x.value == null) {
//			if (other.value != null)
//				return false;
//		} else if (!x.value.equals(other.value))
//			return false;
//		return true;
//    }

//    public static boolean equals(SaObservedValue x, SaObservedValue y)
//    {
//        boolean result;
//        if (x == y)
//            result = true;
//        else if (x == null || y == null)
//            result = false;
//        else
//        {
//            result = equals(x.metricCode, y.metricCode) && equals(x.status, y.status)
//                    && equals(x.values, y.values);
//        }
//        return result;
//    }

	
	public static boolean equals(InstanceIdentifier x, InstanceIdentifier y) {
		if (x == y)
			return true;
		if (y == null)
			return false;
		if (x.getExtension() == null) {
			if (y.getExtension() != null)
				return false;
		} else if (!x.getExtension().equals(y.getExtension()))
			return false;
		if (x.getRoot() == null) {
			if (y.getRoot() != null)
				return false;
		} else if (!x.getRoot().equals(y.getRoot()))
			return false;
		return true;
	}
	
    public static boolean equals(Range x, Range obj)
    {
    		if (x == obj)
    			return true;
    		if (obj == null)
    			return false;
    		if (x.getClass() != obj.getClass())
    			return false;
    		Range other = obj;
    		if (x.getLower() == null) {
    			if (other.getLower() != null)
    				return false;
    		} else if (!x.getLower().equals(other.getLower()))
    			return false;
    		if (x.getUpper() == null) {
    			if (other.getUpper() != null)
    				return false;
    		} else if (!x.getUpper().equals(other.getUpper()))
    			return false;
    		return true;
    }

    public static boolean equals(Measure x, Measure obj)
    {
    		if (x == obj)
    			return true;
    		if (obj == null)
    			return false;
    		if (x.getClass() != obj.getClass())
    			return false;
    		Measure other = obj;
    		if (x.getUnitCode() == null) {
    			if (other.getUnitCode() != null)
    				return false;
    		} else if (!x.getUnitCode().equals(other.getUnitCode()))
    			return false;
    		if (x.getMeasuredValue() == null) {
    			if (other.getMeasuredValue() != null)
    				return false;
    		} else if (!x.getMeasuredValue().equals(other.getMeasuredValue()))
    			return false;
    		return true;
    }

    public static boolean equals(PatientDemographicsCoreData x, PatientDemographicsCoreData obj)
    {
    		if (x == obj)
    			return true;
    		if (obj == null)
    			return false;
    		if (x.getClass() != obj.getClass())
    			return false;
    		PatientDemographicsCoreData other = obj;
    		if (x.getBirthday() == null) {
    			if (other.getBirthday() != null)
    				return false;
    		} else if (!x.getBirthday().equals(other.getBirthday()))
    			return false;
    		if (x.getFirstname() == null) {
    			if (other.getFirstname() != null)
    				return false;
    		} else if (!x.getFirstname().equals(other.getFirstname()))
    			return false;
    		if (x.getSex() != other.getSex())
    			return false;
    		if (x.getHeight() == null) {
    			if (other.getHeight() != null)
    				return false;
    		} else if (!x.getHeight().equals(other.getHeight()))
    			return false;
    		if (x.getLastname() == null) {
    			if (other.getLastname() != null)
    				return false;
    		} else if (!x.getLastname().equals(other.getLastname()))
    			return false;
    		if (x.getMiddlenames() == null) {
    			if (other.getMiddlenames() != null)
    				return false;
    		} else if (!x.getMiddlenames().equals(other.getMiddlenames()))
    			return false;
    		if (x.getType() != other.getType())
    			return false;
    		if (x.getWeight() == null) {
    			if (other.getWeight() != null)
    				return false;
    		} else if (!x.getWeight().equals(other.getWeight()))
    			return false;
    		return true;
    }
}
