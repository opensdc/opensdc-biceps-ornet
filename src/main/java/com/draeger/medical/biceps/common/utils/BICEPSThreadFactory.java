/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.utils;
import java.util.concurrent.ThreadFactory;

import org.ws4d.java.concurrency.ThreadPool;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.util.Log;


/**
 * A factory for creating new daemon thread objects with a name that will be placed in the platform thread group if possible.
 */
public class BICEPSThreadFactory implements ThreadFactory {

	private final String threadName;
	
	
	/**
	 * Instantiates a new BICEPS thread factory.
	 *
	 * @param threadName the thread name
	 */
	public BICEPSThreadFactory(String threadName) {
		super();
		this.threadName=threadName;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
	 */
	@Override
	public Thread newThread(Runnable runnable) {
		
		ThreadPool threadPool = null;
		ThreadGroup threadGroup=null;
		
		Toolkit toolkit = PlatformSupport.getInstance().getToolkit();
		if (toolkit!=null && (threadPool=toolkit.getThreadPool())!=null)
			threadGroup=threadPool.getStackThreadGroup();
			
		Thread t=new PlatformThreadWrapper(runnable,threadName, threadGroup);
		t.setDaemon(true);
		
		return t;
	}
	
	class PlatformThreadWrapper extends Thread
	{

		private Runnable task;

		public PlatformThreadWrapper( Runnable task,
				String threadName, ThreadGroup threadGroup) {
			super(threadGroup, task, threadName);
			this.task=task;
		}

		@Override
		public void run() {

			Toolkit toolkit = PlatformSupport.getInstance().getToolkit();
			ThreadPool threadPool = null;
			
			if (task!=null && toolkit!=null && (threadPool=toolkit.getThreadPool())!=null)
			{
				threadPool.execute(task);
			}else{
				if (Log.isInfo())
					Log.info("Could not start "+this.getName()+" in platform thread pool. Starting as standard thread.");
				
				super.run();	
			}
		}
	}

}
