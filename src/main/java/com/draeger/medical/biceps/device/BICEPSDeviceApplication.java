/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;
import com.draeger.medical.biceps.device.mdi.MedicalDeviceCommunicationInterface;


public abstract class BICEPSDeviceApplication
{
	private static BICEPSDeviceApplication application;

	public static final BICEPSDeviceApplication getApplication()
	{
		if (application == null)
		{
			throw new RuntimeException("Application not initialized.");
		}

		return application;
	}


	private final String[] args;
	private BICEPSDeviceInterface ddi;
	public BICEPSDeviceApplication(String[] args)
	{
		this.args = args;
		application = this;
		callTimerWorkAround();
	}

	protected void callTimerWorkAround() {
		Runnable forceTimeHighResolutionTask=new Runnable() {

			@Override
			public void run() {
				while(true) {
					try {
						Thread.sleep(Integer.MAX_VALUE);
					}
					catch(InterruptedException ex) {
						//void
					}
				}
			}
		};

		new BICEPSThreadFactory("ForceTimeHighResolution_BID_6435126").newThread(forceTimeHighResolutionTask).start();
	}

	public final void run()
	{
		start();
	}

	protected final String[] getArgs()
	{
		return args;
	}

	protected void start()
	{
		try {
			ddi= BICEPSDeviceInterfaceFactory.getInstance().getBICEPSDeviceApplication();

			ddi.prepareBICEPSDeviceInterface(getConfigurationId(),getArgs());

			ddi.initializeBICEPSDeviceInterface();

			ddi.setMedicalDeviceCommunicationInterface(getMedicalDeviceCommunicationInterface());

			ddi.setupMedicalDeviceInterfaceCommunication();

			ddi.setupBICEPSDeviceNodeCommunication();

			ddi.setupMDIB();

			ddi.updateBICEPSDeviceNodeMetaData();

			ddi.registerBICEPSDeviceNodeOperations();

			ddi.createBICEPSDeviceNode();

			ddi.registerMDIHandler();

			ddi.registerBICEPSDeviceNodeCallbacks();

			ddi.start();

		} catch (InstantiationException e) {
			Log.error(e);
		}
	}



	protected void stop()
	{
		ddi.stop();
	}

	protected abstract MedicalDeviceCommunicationInterface getMedicalDeviceCommunicationInterface();

	protected abstract int getConfigurationId();

}
