/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device;

import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;
import com.draeger.medical.biceps.device.mdi.MedicalDeviceCommunicationInterface;

/**
 * A Device Interface that provides methods for initializing the {@link MedicalDeviceCommunicationInterface} as well 
 * as the network communication stack.
 * 
 * {@link BICEPSDeviceApplication} calls during startup:
 * 
 * <ol>
 * 	<li>{@link #prepareBICEPSDeviceInterface(int, String[])} </li>
 * 	<li>{@link #initializeBICEPSDeviceInterface()} </li>
 *  <li>{@link #setMedicalDeviceCommunicationInterface(MedicalDeviceCommunicationInterface)}</li>
 *  <li>{@link #setupMedicalDeviceInterfaceCommunication()}</li>
 *  <li>{@link #setupBICEPSDeviceNodeCommunication()}</li>
 *  <li>{@link #setupMDIB()}</li>
 *  <li>{@link #updateBICEPSDeviceNodeMetaData()}</li>
 *  <li>{@link #registerBICEPSDeviceNodeOperations()}</li>
 *  <li>{@link #createBICEPSDeviceNode()}</li>
 *  <li>{@link #registerBICEPSDeviceNodeCallbacks()}</li>
 *  <li>{@link #start()}</li>
 * </ol>
 * 
 * {@link BICEPSDeviceApplication} calls during shutdown:
 * <ol>
 * 	<li>{@link #stop()} </li>
 * </ol>
 */
public interface BICEPSDeviceInterface 
{

	/**
	 * Setup the network device interface.
	 *
	 * @param deviceConfigId the device config id
	 * @param args the args
	 */
	public abstract void prepareBICEPSDeviceInterface(int deviceConfigId, String[] args);
	
	/**
	 * Initialize biceps device interface.
	 */
	public abstract void initializeBICEPSDeviceInterface();
	
	/**
	 * Sets the medical device communication interface.
	 *
	 * @param mdCom the new medical device communication interface
	 */
	public abstract void setMedicalDeviceCommunicationInterface(MedicalDeviceCommunicationInterface mdCom);
	
	/**
	 * Setup medical device interface communication.
	 */
	public abstract void setupMedicalDeviceInterfaceCommunication();
	
	/**
	 * Setup biceps device node communication.
	 */
	public abstract void setupBICEPSDeviceNodeCommunication();
	
	/**
	 * Setup mdib.
	 */
	public abstract void setupMDIB();
	
	/**
	 * Update biceps device node meta data.
	 */
	public abstract void updateBICEPSDeviceNodeMetaData();
	
	/**
	 * Register biceps device node operations.
	 */
	public abstract void registerBICEPSDeviceNodeOperations();
	
	/**
	 * Creates the biceps device node.
	 */
	public abstract void createBICEPSDeviceNode();
	
	/**
	 * Register mdi handler.
	 */
	public abstract void registerMDIHandler();
	
	/**
	 * Register biceps device node callbacks.
	 */
	public abstract void registerBICEPSDeviceNodeCallbacks();
	
	/**
	 * Start.
	 */
	public abstract void start();
	
	/**
	 * Stop.
	 */
	public abstract void stop();
	
	//other
	/**
	 * Gets the interface description.
	 *
	 * @return the interface description
	 */
	public abstract BICEPSDeviceNodeInterfaceDescription getInterfaceDescription();
	
}
