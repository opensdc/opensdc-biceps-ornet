/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device;

public class BICEPSDeviceInterfaceFactory {
	private static final BICEPSDeviceInterfaceFactory instance=new BICEPSDeviceInterfaceFactory();
	
	private BICEPSDeviceInterfaceFactory()
	{
		//void
	}

	public static BICEPSDeviceInterfaceFactory getInstance() {
		return instance;
	}
	
	private Class<? extends BICEPSDeviceInterface> ddaClz=null;
	private static final String DEFAULT_CLZ_NAME=System.getProperty("BICEPS.Device.BICEPSDeviceInterfaceClass", "com.draeger.medical.biceps.device.impl.DefaultBICEPSDeviceInterface");	//TODO SSch property
	public  BICEPSDeviceInterface getBICEPSDeviceApplication() throws InstantiationException
	{
		Class<? extends BICEPSDeviceInterface> ddaClz;
		try {
			ddaClz = getDDAClz();
			return ddaClz.newInstance();
		} catch (Exception e) {
			//TODO SSch Logger
			e.printStackTrace();
			throw new InstantiationException(e.getMessage());
		}
		
	}

	private synchronized Class<? extends BICEPSDeviceInterface> getDDAClz() throws ClassNotFoundException
	{
		if (ddaClz==null){
			ddaClz=Class.forName(DEFAULT_CLZ_NAME).asSubclass(BICEPSDeviceInterface.class);
		}
//			ddaClz= (Class<BICEPSDeviceInterface>) Class.forName(DEFAULT_CLZ_NAME);
		
		return ddaClz;
	}
}
