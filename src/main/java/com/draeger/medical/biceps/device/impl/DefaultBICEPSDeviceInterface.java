/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.ws4d.java.configuration.FrameworkProperties;
import org.ws4d.java.configuration.Properties;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.device.BICEPSDeviceInterface;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescriptionProvider;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceState;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceStateProvider;
import com.draeger.medical.biceps.device.mdi.MedicalDeviceCommunicationInterface;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSAuthenticationPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyContextPolicyConverter;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSSafetyInformationQoSPolicy;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBaseFactory;
import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.BICEPSEpisodicEventSource;
import com.draeger.medical.biceps.device.mdpws.service.builder.OperationDescriptionRegistry;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicAlertEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicContextChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicMetricEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.FastEpisodicEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.MDSCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.MDSDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.ObjectCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.ObjectDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationInvokedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationalStateChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.SystemErrorEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicAlertEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicContextChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicMetricEventReport;
import com.draeger.medical.biceps.device.mdpws.service.get.GetContainmentTree;
import com.draeger.medical.biceps.device.mdpws.service.get.GetContextStates;
import com.draeger.medical.biceps.device.mdpws.service.get.GetDescriptor;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDDescription;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDIB;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDState;
import com.draeger.medical.biceps.device.mdpws.service.set.ActivateOperation;
import com.draeger.medical.biceps.device.mdpws.service.set.SetContextState;
import com.draeger.medical.biceps.device.mdpws.service.set.SetCurrentAlertConditionState;
import com.draeger.medical.biceps.device.mdpws.service.set.SetRangeAction;
import com.draeger.medical.biceps.device.mdpws.service.set.SetString;
import com.draeger.medical.biceps.device.mdpws.service.set.SetValue;
import com.draeger.medical.biceps.device.mdpws.service.waveform.WaveformStreamSource;
import com.draeger.medical.mdpws.framework.MDPWSFramework;
import com.draeger.medical.mdpws.qos.QoSPolicy;
import com.draeger.medical.mdpws.qos.QoSPolicyExceptionHandlerManager;
import com.draeger.medical.mdpws.qos.QoSPolicyUtil;
import com.draeger.medical.mdpws.qos.management.DefaultQoSPolicyExceptionHandler;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilder;
import com.draeger.medical.mdpws.qos.management.QoSPolicyBuilderInstanceRegistry;
import com.draeger.medical.mdpws.qos.management.QoSPolicyManager;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyBuilder;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelComparatorProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.DualChannelProtocolConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.Local2MDPWSConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.dualchannel.SafetyInformationManager;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelComparatorProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelLocal2MDPWSConverterProvider;
import com.draeger.medical.mdpws.qos.safetyinformation.impl.DefaultDualChannelProtocolConverterProvider;

public class DefaultBICEPSDeviceInterface implements BICEPSDeviceInterface {

	private MedicalDeviceCommunicationInterface mdCom;
	private String[] args;
	private int deviceConfigId;
	private BICEPSDeviceNode diceDeviceNode;
	private MedicalDeviceInformationBase mdib;
	private final Set<BICEPSEpisodicEventSource> episodicEventReports = new HashSet<BICEPSEpisodicEventSource>();
	private final Set<BICEPSQoSPolicyDefinition> policyDefinition = new HashSet<BICEPSQoSPolicyDefinition>();
	private final HashMap<Class<? extends MDICommand>, Set<BICEPSQoSPolicy>> commandPolicyMap = new HashMap<Class<? extends MDICommand>, Set<BICEPSQoSPolicy>>();
	private boolean compressedWaveforms = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.compressedWaveforms","false"));
	private boolean waveformsEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.waveformsEnabled", "true"));
	private boolean waveformsEventingEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.waveformsEventingEnabled", "true"));
	private boolean preStartWarmupEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.PreStartWarmup","false"));
	private boolean disableQoSPolicies = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.DisableQoSPolicies","false"));
	private boolean hideUnhandledOperations = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.HideUnhandledOperations","false"));
	private boolean initQoSFramework = Boolean.parseBoolean(System.getProperty("MDPWS.InitQoSFramework", "true"));
	private boolean configureQoSFramework = Boolean.parseBoolean(System.getProperty("DefaultBICEPSDeviceInterface.configureQoSFramework","true"));
	private boolean shutDownIfStartFailed = Boolean.parseBoolean(System.getProperty("DefaultBICEPSDeviceInterface.shutDownIfStartFailed","true"));

	// ////// Interface implementation

	@Override
	public void prepareBICEPSDeviceInterface(int deviceConfigId, String[] args) {
		this.args = args;
		this.deviceConfigId = deviceConfigId;
	}

	@Override
	public void initializeBICEPSDeviceInterface() {
		if (!MDPWSFramework.getInstance().isRunning())
		{
			MDPWSFramework.getInstance().start(getArgs());

			String dpwsPropertiesFile = System.getProperty("MDPWS.PropertiesFile", "config/mdpws.properties");
			try {
				Properties.getInstance().read(
						PlatformSupport.getInstance().getToolkit()
						.getLocalFileSystem().readFile(dpwsPropertiesFile));
				if (!PlatformSupport
						.getInstance()
						.getToolkit()
						.getThreadPool()
						.setSize(
								FrameworkProperties.getInstance()
								.getThreadPoolSize())) {
					if (Log.isWarn())
						Log.warn("Could not restrict thread pool.");
				}

				if (Log.isInfo())
					Log.info("Initializing QoSFramework: " + initQoSFramework);

				//TODO SSch Maybe disable this
				FrameworkProperties.getInstance().setKillOnShutdownHook(true);

				if (initQoSFramework)
					MDPWSFramework.getInstance().initializeQoSFramework();
			} catch (IOException e) {
				Log.error("Could not read config file " + dpwsPropertiesFile);
				stop();
			}
		}
	}

	@Override
	public void setMedicalDeviceCommunicationInterface(
			MedicalDeviceCommunicationInterface mdCom) {
		this.mdCom = mdCom;
	}

	@Override
	public void setupMedicalDeviceInterfaceCommunication() {
		this.mdCom.initialize();
		this.mdCom.connect();
	}

	@Override
	public void setupBICEPSDeviceNodeCommunication() {
		configureQoSFramework();
	}

	private void configureQoSFramework() {

		if (configureQoSFramework) {
			try {
				QoSPolicyExceptionHandlerManager.getInstance().add(
						new DefaultQoSPolicyExceptionHandler());
				// DualChannelComparatorProvider comparatorProviderImpl=new
				// DefaultDualChannelComparatorProvider();
				// DualChannelProtocolConverterProvider
				// protocolConverterProvider=new
				// DefaultDualChannelProtocolConverterProvider();
				// Local2MDPWSConverterProvider converterProviderImpl=new
				// DefaultDualChannelLocal2MDPWSConverterProvider();
				// DualChannelManager.getInstance().initializeManager(converterProviderImpl,
				// protocolConverterProvider, comparatorProviderImpl);
				DualChannelComparatorProvider comparatorProviderImpl = new DefaultDualChannelComparatorProvider();
				DualChannelProtocolConverterProvider protocolConverterProvider = new DefaultDualChannelProtocolConverterProvider();
				Local2MDPWSConverterProvider converterProviderImpl = new DefaultDualChannelLocal2MDPWSConverterProvider();
				SafetyInformationManager.getInstance().initializeManager(
						converterProviderImpl, protocolConverterProvider,
						comparatorProviderImpl);

			} catch (NoClassDefFoundError e) {
				Log.error(e);
			}
		}
	}

	@Override
	public void setupMDIB() {
		try {
			this.mdib = MedicalDeviceInformationBaseFactory.getInstance()
					.getMedicalDeviceInformationBase();
			this.mdib.create(getInterfaceDescription().getMDIB(),
					getInterfaceState().getInitialMDState());
		} catch (InstantiationException e) {
			Log.error(e);
		}
	}

	@Override
	public void updateBICEPSDeviceNodeMetaData() {
		// TODO SSch modify path in bindings, update scope
	}

	@Override
	public void registerBICEPSDeviceNodeOperations() {
		
		updatePoliciesForCommands();

		OperationDescriptionRegistry registry = OperationDescriptionRegistry.getInstance();

		registerGetOperations(registry);

		registerPeriodicEventReports(registry);

		registerEpisodicEventReports(registry);

		registerSetOperations(registry, commandPolicyMap);

		registerWaveformStreamSource(registry);
		
		//	Update Registry for event sources
		for (BICEPSEpisodicEventSource evtSource : getEpisodicEventReports()) {
			registry.put(evtSource);
		}
		// TODO SSch add all BICEPS operations
	}

	private void updatePoliciesForCommands() {
		Set<Class<? extends MDICommand>> handledCommands = getMdCom()
				.getHandledCommands();

		if (handledCommands != null) {
			for (Class<? extends MDICommand> cmdClz : handledCommands) {
				Set<BICEPSQoSPolicy> policies = getMdCom().getPoliciesForCommand(
						cmdClz);

				attachPoliciesToCommandClass(cmdClz, policies);
			}
		}
	}

	private void attachPoliciesToCommandClass(
			Class<? extends MDICommand> cmdClz, Set<BICEPSQoSPolicy> policies) {
		if (policies!=null)
		{
			Set<BICEPSQoSPolicy> currentPolicies = commandPolicyMap.get(cmdClz);
			if (currentPolicies!=null)
			{
				currentPolicies.addAll(policies);
			}else{
				commandPolicyMap.put(cmdClz, policies);
			}
		}
	}

	protected void registerWaveformStreamSource(
			OperationDescriptionRegistry registry) {
		if (waveformsEnabled)
			registry.put(defineStreamSource(new WaveformStreamSource(getMdib())));
				
		if (waveformsEventingEnabled){
			getEpisodicEventReports().add(new FastEpisodicEventReport(getMdib()));
		}

	}

	private OperationDescription defineStreamSource(
			WaveformStreamSource waveformStreamSource) {
		if (compressedWaveforms && waveformStreamSource != null)
			policyDefinition
			.add(createCompressionPolicyDefinition(waveformStreamSource
					.getOutputAction()));

		return waveformStreamSource;
	}

	protected void registerGetOperations(OperationDescriptionRegistry registry) {
//		registry.put(new GetAlertStates(getMdib()));
		registry.put(new GetMDState(getMdib()));
		registry.put(new GetMDDescription(getMdib()));
		registry.put(new GetMDIB(getMdib()));
//		registry.put(new GetMetricStates(getMdib()));
//		registry.put(new GetIdContextStates(getMdib()));
		registry.put(new GetContextStates(getMdib()));
		registry.put(new GetContainmentTree(getMdib()));
		registry.put(new GetDescriptor(getMdib()));
	}

	protected void registerSetOperations(
			OperationDescriptionRegistry registry,
			HashMap<Class<? extends MDICommand>, Set<BICEPSQoSPolicy>> commandPolicyMap) {
		registry.put(defineOperation(new SetValue(getMdib()), commandPolicyMap));
		registry.put(defineOperation(new SetString(getMdib()), commandPolicyMap));
		registry.put(defineOperation(new SetRangeAction(getMdib()), commandPolicyMap));
		registry.put(defineOperation(new SetCurrentAlertConditionState(
				getMdib()), commandPolicyMap));
		registry.put(defineOperation(new ActivateOperation(getMdib()), commandPolicyMap));
		registry.put(defineOperation(new SetContextState(getMdib()),
				commandPolicyMap));
//		registry.put(defineOperation(new SetContextIdState(getMdib()),
//				commandPolicyMap));
	}

	private OperationDescription defineOperation(
			OperationDescription op,
			HashMap<Class<? extends MDICommand>, Set<BICEPSQoSPolicy>> commandPolicyMap) {

		OperationDescription desc = null;
		Class<? extends MDICommand> cmdClz =  OperationDescriptorCommandRegistry.getCommandClassForOperationDescription(op);
		if (!hideUnhandledOperations
				|| (cmdClz != null && commandPolicyMap.containsKey(cmdClz))) {
			desc = op;
			createPolicies(op, cmdClz, commandPolicyMap);
		}
		return desc;
	}

	private void createPolicies(
			OperationDescription op,
			Class<? extends MDICommand> cmdClz,
			HashMap<Class<? extends MDICommand>, Set<BICEPSQoSPolicy>> commandPolicyMap) {

		if (this.disableQoSPolicies)
			return;

		if (cmdClz != null) {
			Set<BICEPSQoSPolicy> policies = commandPolicyMap.get(cmdClz);
			if (policies != null) {
				for (BICEPSQoSPolicy diceQoSPolicy : policies) {
					String action = op.getInputAction();
					// if(diceQoSPolicy instanceof BICEPSDualChannelQoSPolicy)
					// {
					// createDualChannelPolicyDefinitionDependingOnAction(
					// (BICEPSDualChannelQoSPolicy)diceQoSPolicy, action);
					// }else
					if (diceQoSPolicy instanceof BICEPSAuthenticationPolicy) {
						createAuthenticationPolicyDefinition(
								(BICEPSAuthenticationPolicy) diceQoSPolicy,
								action);
					} else if (diceQoSPolicy instanceof BICEPSSafetyInformationQoSPolicy) {
						createSafetyInformationReqPolicy(
								(BICEPSSafetyInformationQoSPolicy) diceQoSPolicy,
								action);
					}
				}
			}
		}
	}

	private void createSafetyInformationReqPolicy(
			BICEPSSafetyInformationQoSPolicy diceQoSPolicy, String action) {
		BICEPSQoSPolicyDefinition definition = new BICEPSQoSPolicyDefinition();
		definition.inbound = true;
		definition.outbound = false;
		definition.operation = action;

		SafetyInformationPolicyAttributes attributes = BICEPSSafetyContextPolicyConverter.convertFromBICEPSPolicyToAttributes(diceQoSPolicy);

		QoSPolicyBuilder builder = QoSPolicyBuilderInstanceRegistry
				.getInstance().getPolicyBuilder(
						SafetyInformationPolicyBuilder.class);
		if (builder instanceof SafetyInformationPolicyBuilder) {
			definition.policy = builder.createInOutboundPolicy(attributes);
		}

		if (Log.isInfo())
			Log.info("Created policy definition for action: " + action + ": "
					+ definition);

		policyDefinition.add(definition);

	}


	private void createAuthenticationPolicyDefinition(
			BICEPSAuthenticationPolicy diceQoSPolicy, String action) {
		BICEPSQoSPolicyDefinition definition = new BICEPSQoSPolicyDefinition();
		definition.inbound = true;
		definition.outbound = false;
		definition.operation = action;

		try {
			createInboundPolicy(definition,
					"com.draeger.medical.mdpws.qos.nonrepudiation.AuthenticationPolicyBuilder");

			if (definition.policy != null) {
				mdib.getManager()
				.getAuthorizationManager()
				.setMinimalAccessLevelForOperation(
						diceQoSPolicy.getOperationHandle(),
						diceQoSPolicy.getMinimalAccessLevel());

				policyDefinition.add(definition);
			}
		} catch (Exception e) {
			Log.warn(e);
		}
		return;
	}

	private void createInboundPolicy(BICEPSQoSPolicyDefinition definition,
			String className) {

		try {
			Class<?> c = Class.forName(className);

			if (c != null) {
				QoSPolicyBuilder builder = QoSPolicyBuilderInstanceRegistry
						.getInstance().getPolicyBuilder(c);
				if (builder != null && c.isAssignableFrom(builder.getClass())) {
					definition.policy = builder.createInboundPolicy();// AuthenticationPolicyBuilder.getInstance().createInboundPolicy();
				}
			}
		} catch (Exception e) {
			Log.warn(e);
		}
	}



	private BICEPSQoSPolicyDefinition createCompressionPolicyDefinition(
			String portType) {
		BICEPSQoSPolicyDefinition definition = new BICEPSQoSPolicyDefinition();
		definition.service = portType;


		createInboundPolicy(definition,
				"com.draeger.medical.mdpws.qos.compression.CompressionPolicyBuilder");

		if (definition.policy == null) {
			definition = null;
		}

		return definition;
	}

	protected void registerPeriodicEventReports(
			OperationDescriptionRegistry registry) {
		registry.put(new PeriodicMetricEventReport(getMdib()));
		registry.put(new PeriodicAlertEventReport(getMdib()));
		//		registry.put(new PeriodicPatientContextStateEventReport(getMdib()));
		registry.put(new PeriodicContextChangedEventReport(getMdib()));
	}

	protected void registerEpisodicEventReports(
			OperationDescriptionRegistry registry) {

		getEpisodicEventReports().add(new EpisodicMetricEventReport(getMdib()));
		getEpisodicEventReports().add(new EpisodicAlertEventReport(getMdib()));
		//		getEpisodicEventReports().add(
		//				new EpisodicPatientContextStateEventReport(getMdib()));

		getEpisodicEventReports().add(
				new OperationInvokedEventReport(getMdib()));
		getEpisodicEventReports().add(
				new OperationalStateChangedEventReport(getMdib()));




		getEpisodicEventReports().add(
				new EpisodicContextChangedEventReport(getMdib()));


		getEpisodicEventReports().add(new MDSCreatedEventReport(getMdib()));
		getEpisodicEventReports().add(new MDSDeletedEventReport(getMdib()));
		getEpisodicEventReports().add(new ObjectCreatedEventReport(getMdib()));
		getEpisodicEventReports().add(new ObjectDeletedEventReport(getMdib()));
		getEpisodicEventReports().add(new OperationCreatedEventReport(getMdib()));
		getEpisodicEventReports().add(new OperationDeletedEventReport(getMdib()));

		getEpisodicEventReports().add(new SystemErrorEventReport(getMdib()));

		// TODO SSch Patient Core Data Reports ????
		// getEpisodicEventReports().add(new
		// ClockDateTimeChangedEventReport(getMdib()));
		// getEpisodicEventReports().add(new
		// MDSAttributeUpdateEventReport(getMdib()));
	}

	@Override
	public void createBICEPSDeviceNode() {
		setBICEPSDeviceNode(new BICEPSDeviceNode(getDeviceConfigId()));
	}

	protected void setBICEPSDeviceNode(BICEPSDeviceNode diceDeviceNode) {
		this.diceDeviceNode=diceDeviceNode;
	}

	@Override
	public void registerMDIHandler() {
		getMdCom().setMDICommandReceivingQueue(
				getMdib().getManager().getCommandQueue());
		getMdCom().setMDINotificationQueue(
				getMdib().getManager().getNotificationQueue());
		getMdCom().setMDIStreamQueue(getMdib().getManager().getStreamQueue());

	}

	@Override
	public void registerBICEPSDeviceNodeCallbacks() {
		for (BICEPSEpisodicEventSource evtSource : getEpisodicEventReports()) {
			getMdib().getManager().registerEpisodicEventSources(evtSource);
		}
	}

	@Override
	public void start() {
		try {
			PVObjectUtilPoolProvider.getInstance(); // Speed-up first usage and ensure that it works if called from native thread 

			wirePolicies();

			getMdib().getManager().setBICEPSDeviceNode(getBICEPSDeviceNode());

			getMdCom().startCommandHandling();

			getMdib().getManager().start();

			if (preStartWarmupEnabled) {
				if (Log.isInfo())
					Log.info("Initializing pre start warmup for DefaultBICEPSDeviceInterface.");
				try {
					for (int i = 0; i < 20; i++) {
						Thread.sleep(1000);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.gc();
			if (Log.isInfo())
				Log.info("DefaultBICEPSDeviceInterface started.");
			getBICEPSDeviceNode().start();

			getMdCom().startupComplete();
		} catch (IOException e) {
			Log.error(e);

			if (shutDownIfStartFailed) System.exit(-1);
		}
	}

	private void wirePolicies() {

		for (BICEPSQoSPolicyDefinition def : policyDefinition) {
			if (def.isValid()) {
				if (def.operation != null) {
					QoSPolicyUtil.addPolicyToMessage(getBICEPSDeviceNode(),
							def.policy, def.operation, def.inbound,
							def.outbound);
				} else if (def.service != null) {
					String opIdentifier = def.service;
					QoSPolicy policy = def.policy;
					Iterator it = getBICEPSDeviceNode().getServices();
					QoSPolicyUtil.addPolicyToServices(opIdentifier, policy, it);
				}
				QoSPolicyManager.getInstance().registerQoSPolicy(def.policy);
			}
		}
	}

	@Override
	public void stop() {
		try {
			if (getBICEPSDeviceNode() != null) {
				getBICEPSDeviceNode().stop();
			}
			if (MDPWSFramework.getInstance() != null) {
				MDPWSFramework.getInstance().stop();
			}
			if (getMdib() != null && getMdib().getManager() != null) {
				getMdib().getManager().stop();
			}
			if (getMdCom() != null) {
				getMdCom().stopCommandHandling();
				getMdCom().disconnect();
			}
		} catch (IOException e) {
			Log.warn(e);
		}
	}

	@Override
	public BICEPSDeviceNodeInterfaceDescription getInterfaceDescription() {
		BICEPSDeviceNodeInterfaceDescriptionProvider provider = this.mdCom
				.getInterfaceDescriptionProvider();
		return provider.getInterfaceDescription();
	}

	public BICEPSDeviceNodeInterfaceState getInterfaceState() {
		BICEPSDeviceNodeInterfaceStateProvider provider = this.mdCom
				.getInterfaceStateProvider();
		return provider.getInterfaceState();
	}

	// /////// Getter for the member variables
	private Set<BICEPSEpisodicEventSource> getEpisodicEventReports() {
		return episodicEventReports;
	}

	protected BICEPSDeviceNode getBICEPSDeviceNode() {
		return diceDeviceNode;
	}

	protected int getDeviceConfigId() {
		return this.deviceConfigId;
	}

	protected String[] getArgs() {
		return args;
	}

	protected MedicalDeviceInformationBase getMdib() {
		return mdib;
	}

	protected MedicalDeviceCommunicationInterface getMdCom() {
		return mdCom;
	}

	// private class BICEPSNameSpaceContext extends SOAPNameSpaceContext
	// {
	//
	// public BICEPSNameSpaceContext() {
	// super();
	// addNamespace("dice", SchemaHelper.NAMESPACE_MESSAGES);
	// }
	//
	// }

	private class BICEPSQoSPolicyDefinition {
		QoSPolicy policy;
		String operation;
		String service;
		boolean inbound;
		boolean outbound;

		public boolean isValid() {
			return policy != null
					&& ((operation != null && operation.trim().length() > 0) || service != null);
		}

		@Override
		public String toString() {
			return "BICEPSQoSPolicyDefinition [policy=" + policy + ", operation="
					+ operation + ", service=" + service + ", inbound="
					+ inbound + ", outbound=" + outbound + "]";
		}

	}

}
