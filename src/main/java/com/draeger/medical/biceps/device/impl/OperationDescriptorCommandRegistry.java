/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.ActivateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetAlertStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetContextStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetIdentifiableContextCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetRangeCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetStringCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetValueCommand;
import com.draeger.medical.biceps.device.mdpws.service.set.ActivateOperation;
import com.draeger.medical.biceps.device.mdpws.service.set.SetContextState;
import com.draeger.medical.biceps.device.mdpws.service.set.SetCurrentAlertConditionState;
import com.draeger.medical.biceps.device.mdpws.service.set.SetRangeAction;
import com.draeger.medical.biceps.device.mdpws.service.set.SetString;
import com.draeger.medical.biceps.device.mdpws.service.set.SetValue;

public class OperationDescriptorCommandRegistry {

	private static final HashMap<Class<? extends OperationDescription>, Class<? extends MDICommand>> commandClassForOperationRegistry = new HashMap<Class<? extends OperationDescription>, Class<? extends MDICommand>>();

	static{
		setupCommandClassForOperationRegistry();
	}
	
	private static void setupCommandClassForOperationRegistry() {
		commandClassForOperationRegistry.put(SetValue.class,
				SetValueCommand.class);
		commandClassForOperationRegistry.put(ActivateOperation.class,
				ActivateCommand.class);
		commandClassForOperationRegistry.put(SetContextState.class,
				SetContextStateCommand.class);
//		commandClassForOperationRegistry.put(SetContextIdState.class,
//				SetIdentifiableContextCommand.class);
		commandClassForOperationRegistry.put(SetRangeAction.class,
				SetRangeCommand.class);
		commandClassForOperationRegistry.put(SetString.class,
				SetStringCommand.class);
		commandClassForOperationRegistry.put(SetCurrentAlertConditionState.class, SetAlertStateCommand.class);
	}
	
	public static Class<? extends MDICommand> getCommandClassForOperationDescription(
			OperationDescription operation) {
		Class<? extends MDICommand> retVal = null;
		if (operation != null) {
			retVal = commandClassForOperationRegistry.get(operation.getClass());
		}
		if (retVal == null) {
			if (Log.isWarn())
				Log.warn("No command mapping for " + operation);
		}

		return retVal;
	}

	/**
	 * @param commandClz
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "cast" })
	public static Set<Class<? extends OperationDescriptor>> getOperationsDescriptorsForCommandClass(
			Class<? extends MDICommand> commandClz) {
		
		Set<Class<? extends OperationDescriptor>> retVal=new HashSet<Class<? extends OperationDescriptor>>();
		
		Set<Entry<Class<? extends OperationDescription>, Class<? extends MDICommand>>> entrySet = commandClassForOperationRegistry.entrySet();
		for (Entry<Class<? extends OperationDescription>, Class<? extends MDICommand>> entry : entrySet) {
			if (entry.getValue().isAssignableFrom(commandClz)){
				retVal.add((Class) entry.getKey());
			}
		}
		
		return retVal;
	}
	
}
