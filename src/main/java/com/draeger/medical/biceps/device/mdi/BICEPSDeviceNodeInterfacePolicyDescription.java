/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi;

import java.util.Map;
import java.util.Set;

import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;

public interface BICEPSDeviceNodeInterfacePolicyDescription extends
		BICEPSDeviceNodeInterfaceDescription {
	
	public abstract Map<OperationDescriptor,Set<BICEPSQoSPolicy>> getOperationPolicies();
}
