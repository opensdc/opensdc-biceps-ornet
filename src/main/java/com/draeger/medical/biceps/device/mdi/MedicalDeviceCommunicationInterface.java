/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi;

import java.util.Set;
import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.device.BICEPSDeviceInterface;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;


/**
 * A communication interface with a medical device that is used during startup / stop of a {@link BICEPSDeviceInterface}.
 * 
 */
public interface MedicalDeviceCommunicationInterface 
{
	
	/**
	 * Gets the interface description provider.
	 *
	 * @return the interface description provider
	 */
	public abstract BICEPSDeviceNodeInterfaceDescriptionProvider getInterfaceDescriptionProvider();
	
	/**
	 * Gets the interface state provider.
	 *
	 * @return the interface state provider
	 */
	public abstract BICEPSDeviceNodeInterfaceStateProvider getInterfaceStateProvider();
	
	/**
	 * Initializes the communication with the medical device before connecting to it.
	 */
	public abstract void initialize();
	
	/**
	 * Connect. Connect has to be called after initialization.
	 */
	public abstract void connect();
	
	/**
	 * Disconnect.
	 */
	public abstract void disconnect();
	
	/**
	 * Gets the handled commands.
	 *
	 * @return the handled commands
	 */
	public abstract Set<Class<? extends MDICommand>> getHandledCommands();
	
	/**
	 * Sets the MDI command receiving queue.
	 *
	 * @param commandQueue the new MDI command receiving queue
	 */
	public abstract void setMDICommandReceivingQueue(BlockingQueue<MDICommand> commandQueue);
	
	/**
	 * Sets the MDI notification queue.
	 *
	 * @param notificationQueue the new MDI notification queue
	 */
	public abstract void setMDINotificationQueue(BlockingQueue<MDINotification> notificationQueue);
	
	/**
	 * Signals that the medical device should start handling commands now.
	 */
	public abstract void startCommandHandling();
	
	/**
	 * Signals that the medical device may stop handling commands now.
	 */
	public abstract void stopCommandHandling();
	
	/**
	 * Sets the MDI stream queue.
	 *
	 * @param waveformQueue the new MDI stream queue
	 */
	public abstract void setMDIStreamQueue(
			BlockingQueue<StreamFrame> waveformQueue);
	
	/**
	 * Gets the policies for a command class.
	 *
	 * @param cmdClz the cmd clz
	 * @return the policies for command
	 */
	public abstract Set<BICEPSQoSPolicy> getPoliciesForCommand(Class<? extends MDICommand> cmdClz);
	
	/**
	 * Startup complete.
	 */
	public abstract void startupComplete();
	
}
