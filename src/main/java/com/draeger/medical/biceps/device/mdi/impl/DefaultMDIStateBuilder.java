/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.impl;

import java.math.BigInteger;
import java.util.HashMap;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.ComponentActivation;
import com.draeger.medical.biceps.common.model.ComponentState;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.CurrentAlertCondition;
import com.draeger.medical.biceps.common.model.CurrentAlertSignal;
import com.draeger.medical.biceps.common.model.CurrentAlertSystem;
import com.draeger.medical.biceps.common.model.CurrentLimitAlertCondition;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.EnsembleContextState;
import com.draeger.medical.biceps.common.model.EnumStringMetricDescriptor;
import com.draeger.medical.biceps.common.model.EnumStringMetricState;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.LimitAlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.LimitAlertObservationState;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;
import com.draeger.medical.biceps.common.model.LocationContextState;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.OperatorContextDescriptor;
import com.draeger.medical.biceps.common.model.OperatorContextState;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.PatientContextState;
import com.draeger.medical.biceps.common.model.PausableActivation;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.SCODescriptor;
import com.draeger.medical.biceps.common.model.SignalPresence;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.StringMetricState;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.common.model.VMDDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextState;
import com.draeger.medical.biceps.device.mdi.interaction.MDIStateBuilder;

public class DefaultMDIStateBuilder implements MDIStateBuilder {
	//States
	private final String STATE_POSTFIX="s";
	private final String ID_CONTEXT_STATE_POSTFIX="sid";
	private final MDState states=new MDState();
	private final HashMap<String,HashMap<Class<?>,State>> descriptorStatesMap=new HashMap<String, HashMap<Class<?>,State>>();

	@Override
	public void initializeStatesForDescription(MDDescription description) {
		buildStatesForDescription(description);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void buildStatesForDescription(MDDescription description) {
		for(MDSDescriptor mds: description.getMDSDescriptors())
		{
			if (mds instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor hydraMDS=(HydraMDSDescriptor)mds;
				ComponentState mdsState=getOrCreateState(hydraMDS.getHandle(),ComponentState.class);
				mdsState.setState(ComponentActivation.ON);
				AlertSystemDescriptor alertSystemMDS = hydraMDS.getAlertSystem();
				buildAlertSystemStates(alertSystemMDS);
				for(VMDDescriptor vmd: hydraMDS.getVMDs())
				{
					ComponentState vmdState=getOrCreateState(vmd.getHandle(),ComponentState.class);
					vmdState.setState(ComponentActivation.ON);
					AlertSystemDescriptor alertSystemVMD = vmd.getAlertSystem();
					buildAlertSystemStates(alertSystemVMD);
					for(ChannelDescriptor channel: vmd.getChannels())
					{
						ComponentState channelState=getOrCreateState(channel.getHandle(),ComponentState.class);
						channelState.setState(ComponentActivation.ON);

						AlertSystemDescriptor alertSystemChannel= channel.getAlertSystem();
						buildAlertSystemStates(alertSystemChannel);
						for(MetricDescriptor metric: channel.getMetrics())
						{
							Class metricStateClass=null;
							if (metric instanceof NumericMetricDescriptor){
								metricStateClass=NumericMetricState.class;
							}
							else if (metric instanceof EnumStringMetricDescriptor){
								metricStateClass=EnumStringMetricState.class;
							}
							else if (metric instanceof StringMetricDescriptor){
								metricStateClass=StringMetricState.class;
							} else if (metric instanceof RealTimeSampleArrayMetricDescriptor){
								metricStateClass=RealTimeSampleArrayMetricState.class;
							}
							if (metricStateClass!=null)
							{
								AbstractMetricState ams=(AbstractMetricState)getOrCreateState(metric.getHandle(),metricStateClass);
								ams.setState(ComponentActivation.OFF);
							}
						}
					}
				}
				SCODescriptor sco = hydraMDS.getSCO();
				if (sco!=null) {
					ComponentState scoState=getOrCreateState(sco.getHandle(),ComponentState.class);
					scoState.setState(ComponentActivation.ON);

					for(OperationDescriptor operationDescriptor: sco.getOperations())
					{
						OperationState cas=getOrCreateState(operationDescriptor.getHandle(), OperationState.class);
						cas.setState(OperationalState.ENABLED);
					}
				}

				SystemContext context = hydraMDS.getContext();
				if (context!=null)
				{
					PatientAssociationDescriptor patientContextDescriptor = context.getPatientContext();
					if (patientContextDescriptor!=null)
					{
						AbstractIdentifiableContextState pState = getOrCreateState(patientContextDescriptor.getHandle(),PatientContextState.class);
						pState.setContextAssociation(ContextAssociationStateValue.NO_ASSOCIATION);
					}
					LocationContextDescriptor locationContextDescriptor = context.getLocationContext();
					if (locationContextDescriptor!=null)
					{
						AbstractIdentifiableContextState pState = getOrCreateState(locationContextDescriptor.getHandle(),LocationContextState.class);
						pState.setContextAssociation(ContextAssociationStateValue.NO_ASSOCIATION);
					}
					EnsembleContextDescriptor ensembleContextDescriptor = context.getEnsembleContext();
					if(ensembleContextDescriptor != null)
					{
						AbstractIdentifiableContextState pState= getOrCreateState(ensembleContextDescriptor.getHandle(), EnsembleContextState.class);
						pState.setContextAssociation(ContextAssociationStateValue.NO_ASSOCIATION);
					}
					WorkflowContextDescriptor workflowContextDescriptor = context.getWorkflowContext();
					if(workflowContextDescriptor != null)
					{
						AbstractIdentifiableContextState pState = getOrCreateState(workflowContextDescriptor.getHandle(), WorkflowContextState.class);
						pState.setContextAssociation(ContextAssociationStateValue.NO_ASSOCIATION);
					}
					OperatorContextDescriptor operatorContextDescriptor = context.getOperatorContext();
					if(operatorContextDescriptor != null)
					{
						AbstractIdentifiableContextState pState = getOrCreateState(operatorContextDescriptor.getHandle(), OperatorContextState.class);
						pState.setContextAssociation(ContextAssociationStateValue.NO_ASSOCIATION);
					}
				}
			}
		}
	}

	private void buildAlertSystemStates(AlertSystemDescriptor als) {
		if (als!=null)
		{
			CurrentAlertSystem cas = getAlertSystemState(als);
			cas.setState(PausableActivation.ON);
			for (AlertConditionDescriptor acd: als.getAlertConditions())
			{
				CurrentAlertCondition cac = null;
				if (acd instanceof LimitAlertConditionDescriptor)
				{
					cac=getLimitAlertConditionState(acd);
					((CurrentLimitAlertCondition)cac).setLimitObservationState(LimitAlertObservationState.ALL_ON);
				}else{
					cac=getAlertConditionState(acd);	
				}

				cac.setState(PausableActivation.OFF);
				cac.setPresence(false);
			}
			for (AlertSignalDescriptor asd: als.getAlertSignals())
			{
				CurrentAlertSignal cs = getAlertSignalState(asd);
				cs.setState(PausableActivation.OFF);
				cs.setPresence(SignalPresence.OFF);
			}
		}
	}

	protected void addState(State s) 
	{
		if (!states.getStates().contains(s))
		{



			HashMap<Class<?>, State> savedStates = descriptorStatesMap.get(s.getReferencedDescriptor());
			if (savedStates==null)
			{
				savedStates=new HashMap<Class<?>, State>(5);
				descriptorStatesMap.put(s.getReferencedDescriptor(), savedStates);
			}

			State savedStateForClass = getSavedStateForClass(s.getClass(), savedStates);

			if (savedStateForClass!=null) states.getStates().remove(savedStateForClass);

			//update the databases
			savedStates.put(s.getClass(), s);
			states.getStates().add(s);
		}
	}

	@SuppressWarnings("unchecked")
	private <T extends State> T getSavedStateForClass(Class<T> stateClass,
			HashMap<Class<?>, State> savedStates) {
		State savedStateForClass=null;
		if (savedStates!=null && !savedStates.isEmpty())
		{
			savedStateForClass=savedStates.get(stateClass);

			if (savedStateForClass==null)
			{
				for (State currentlySavedState : savedStates.values()) 
				{
					if (stateClass.isAssignableFrom(currentlySavedState.getClass())){
						savedStateForClass=currentlySavedState;
						break;
					}
				}
			}
		}
		return (T)savedStateForClass;
	}

	protected <T extends State> T getOrCreateState(String descriptorHandle, Class<T> stateClass)
	{
		T retVal=null;
		HashMap<Class<?>, State> savedStates = descriptorStatesMap.get(descriptorHandle);
		retVal=getSavedStateForClass(stateClass, savedStates);

		if (retVal==null)
		{
			try {
				retVal= stateClass.newInstance();
				if (retVal instanceof AbstractIdentifiableContextState)
				{
					retVal.setHandle(descriptorHandle+ID_CONTEXT_STATE_POSTFIX);
				}else{
					retVal.setHandle(descriptorHandle+STATE_POSTFIX);	
				}
				
				if (retVal.getStateVersion()==null)
				{
					retVal.setStateVersion(BigInteger.ZERO);
				}
				
				retVal.setReferencedDescriptor(descriptorHandle);
				addState(retVal);
			} catch (Exception e) 
			{
				Log.warn(e);
			}
		}

		return retVal;
	}

//	@Override
//	public ContextAssociationStateValue getContextAssociationStateValue(AbstractContextDescriptor contextDescriptor) {
//		ContextAssociationStateValue cAssociationState=null;
//		if (contextDescriptor!=null){
//			cAssociationState = getOrCreateState(contextDescriptor.getHandle(),ContextAssociationStateValue.class);
//		}
//		return cAssociationState;
//	}

	@Override
	public <T extends AbstractIdentifiableContextState> T getIdentifiableContextState( AbstractContextDescriptor contextDescriptor, Class<T> stateClass)
	{
		T retVal=null;

		if (contextDescriptor!=null && stateClass!=null){
			retVal=getOrCreateState(contextDescriptor.getHandle(),stateClass);
		}

		return retVal;
	}

	@Override
	public MDState getMedicalDeviceInterfaceState() {
		return this.states;
	}

	@Override
	public OperationState getOperationState(OperationDescriptor operationDescriptor )
	{
		OperationState cas=null;
		if (operationDescriptor!=null)cas=getOrCreateState(operationDescriptor.getHandle(), OperationState.class);
		return cas;
	}

	@Override
	public CurrentAlertSystem getAlertSystemState(AlertSystemDescriptor asd) {
		CurrentAlertSystem cas=null;
		if(asd!=null) cas=getOrCreateState(asd.getHandle(), CurrentAlertSystem.class);
		return cas;
	}

	@Override
	public CurrentAlertCondition getAlertConditionState(
			AlertConditionDescriptor asd) {
		CurrentAlertCondition cas=null;
		if(asd!=null) cas=getOrCreateState(asd.getHandle(), CurrentAlertCondition.class);
		return cas;
	}

	@Override
	public CurrentAlertSignal getAlertSignalState(AlertSignalDescriptor asd) {
		CurrentAlertSignal cas=null;
		if(asd!=null) cas=getOrCreateState(asd.getHandle(), CurrentAlertSignal.class);
		return cas;
	}

	@Override
	public CurrentLimitAlertCondition getLimitAlertConditionState(
			AlertConditionDescriptor asd) {
		CurrentLimitAlertCondition cas=null;
		if(asd!=null) cas=getOrCreateState(asd.getHandle(), CurrentLimitAlertCondition.class);
		return cas;
	}

	@Override
	public NumericMetricState getNumericMetricState(NumericMetricDescriptor nmd) {
		NumericMetricState cas=null;
		if(nmd!=null) cas=getOrCreateState(nmd.getHandle(), NumericMetricState.class);
		//		if (cas!=null) cas.getObservedValue().setRelativeTimestamp(0);
		return cas;
	}

	@Override
	public StringMetricState getStringMetricState(StringMetricDescriptor nmd) {
		StringMetricState cas=null;
		if(nmd!=null) cas=getOrCreateState(nmd.getHandle(), StringMetricState.class);
		//		if (cas!=null) cas.getObservedValue().setRelativeTimestamp(0);
		return cas;
	}

	@Override
	public EnumStringMetricState getEnumStringMetricState(EnumStringMetricDescriptor nmd) {
		EnumStringMetricState cas=null;
		if(nmd!=null) cas=getOrCreateState(nmd.getHandle(), EnumStringMetricState.class);
		//		if (cas!=null) cas.getObservedValue().setRelativeTimestamp(0);
		return cas;
	}

	@Override
	public RealTimeSampleArrayMetricState getRealTimeSampleArrayMetricState(
			RealTimeSampleArrayMetricDescriptor rtsamd) {
		RealTimeSampleArrayMetricState cas=null;
		if(rtsamd!=null) cas=getOrCreateState(rtsamd.getHandle(), RealTimeSampleArrayMetricState.class);
		//		if (cas!=null) cas.getObservedValue().setRelativeTimestamp(0);
		return cas;
	}





}
