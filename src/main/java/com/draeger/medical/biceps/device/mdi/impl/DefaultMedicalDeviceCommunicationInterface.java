/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;
import com.draeger.medical.biceps.device.impl.OperationDescriptorCommandRegistry;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescriptionProvider;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfacePolicyDescription;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceStateProvider;
import com.draeger.medical.biceps.device.mdi.MedicalDeviceCommunicationInterface;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommandHandler;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.WaveformGenerator;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;

public abstract class DefaultMedicalDeviceCommunicationInterface implements MedicalDeviceCommunicationInterface{

	
	private BlockingQueue<MDICommand> commandQueue;

	private BlockingQueue<MDINotification> notificationQueue;

	private BlockingQueue<StreamFrame> waveformQueue;

	public DefaultMedicalDeviceCommunicationInterface()
	{
		//	void
	}

	@Override
	public void initialize(){
		addHandlers();
		addWaveformGenerators();
	}


	@Override
	public abstract BICEPSDeviceNodeInterfaceDescriptionProvider getInterfaceDescriptionProvider();
	
	@Override
	public abstract BICEPSDeviceNodeInterfaceStateProvider getInterfaceStateProvider();


	@Override
	public abstract void connect();


	@Override
	public abstract void disconnect();


	@Override
	public void setMDICommandReceivingQueue(
			BlockingQueue<MDICommand> commandQueue) 
	{
		stopCommandHandling();
		this.commandQueue=commandQueue;
		startCommandHandling();
	}

	
	@Override
	public Set<BICEPSQoSPolicy> getPoliciesForCommand(Class<? extends MDICommand> commandClz) {
		HashSet<BICEPSQoSPolicy> retVal=new HashSet<BICEPSQoSPolicy>();
		retrievePoliciesFromInterfaceDescriptionProvider(commandClz, retVal);
		retrievePoliciesFromHandlers(commandClz, retVal);
		return retVal;
	}

	private void retrievePoliciesFromInterfaceDescriptionProvider(
			Class<? extends MDICommand> commandClz,
			HashSet<BICEPSQoSPolicy> setToFill) {
		
		BICEPSDeviceNodeInterfaceDescriptionProvider interfaceDescriptionProvider = getInterfaceDescriptionProvider();
		if (interfaceDescriptionProvider!=null)
		{
			BICEPSDeviceNodeInterfaceDescription interfaceDescription = interfaceDescriptionProvider.getInterfaceDescription();
			if (interfaceDescription instanceof BICEPSDeviceNodeInterfacePolicyDescription)
			{
				BICEPSDeviceNodeInterfacePolicyDescription policyDescription=(BICEPSDeviceNodeInterfacePolicyDescription) interfaceDescription;
				Map<OperationDescriptor, Set<BICEPSQoSPolicy>> operationPolicies = policyDescription.getOperationPolicies();
				
				Set<Class<? extends OperationDescriptor>> opDescriptorsClz= getOperationdescriptorForClass(commandClz);
				
				for (Class<? extends OperationDescriptor> opDescriptorClz : opDescriptorsClz) {
					Set<BICEPSQoSPolicy> policiesToHandle = operationPolicies.get(opDescriptorClz);
					if (policiesToHandle!=null)
						setToFill.addAll(policiesToHandle);
					
				}
			}
		}
		
	}


	private Set<Class<? extends OperationDescriptor>> getOperationdescriptorForClass(
			Class<? extends MDICommand> commandClz) {
		return OperationDescriptorCommandRegistry.getOperationsDescriptorsForCommandClass(commandClz);
	}

	private void retrievePoliciesFromHandlers(
			Class<? extends MDICommand> commandClz,
			HashSet<BICEPSQoSPolicy> setToFill) {
		try{
			handlerLock.readLock().lock();
			Set<MDICommandHandler> allHandlers = handlers.keySet();
			for (MDICommandHandler mdiCommandHandler : allHandlers) {
				if (mdiCommandHandler.canHandleCommand(commandClz))
				{
					Set<BICEPSQoSPolicy> policiesToHandle=mdiCommandHandler.getPoliciesForCommand(commandClz);
					if (policiesToHandle!=null && !policiesToHandle.isEmpty())
					{
						setToFill.addAll(policiesToHandle);
					}
				}
			}
		}catch(Exception e)
		{
			Log.warn(e);
		}finally{
			handlerLock.readLock().unlock();
		}
	}


	@Override
	public void setMDINotificationQueue(
			BlockingQueue<MDINotification> notificationQueue) 
	{
		this.notificationQueue=notificationQueue;
		for (MDICommandHandler cmdHandler : handlers.keySet()) 
		{
			cmdHandler.setMDINotificationQueue(this.notificationQueue);
		}
	}

	@Override
	public void setMDIStreamQueue(
			BlockingQueue<StreamFrame> waveformQueue) 
	{
		this.waveformQueue=waveformQueue;
		for(WaveformGenerator generator: waveformGenerators){
			generator.setWaveformQueue(this.waveformQueue);
		}
	}



	@Override
	public void startCommandHandling() 
	{
		if (!cmdTask.queueProcessingThreadRunning)
		{
			Thread t=new BICEPSThreadFactory("MDCI-CommandHandlingTask").newThread(cmdTask);
			cmdTask.queueProcessingThreadRunning=true;
			t.start();
		}
	}



	@Override
	public void stopCommandHandling() 
	{
		cmdTask.queueProcessingThreadRunning=false;
	}



	@Override
	public Set<Class<? extends MDICommand>> getHandledCommands() 
	{
		HashSet<Class<? extends MDICommand>> retVal=new HashSet<Class<? extends MDICommand>>();
		try{
			handlerLock.readLock().lock();
			retVal.addAll(handledCommands);
		}finally{
			handlerLock.readLock().unlock();
		}
		return retVal;
	}

	////// Logic
	private CommandProcessingTask cmdTask=new CommandProcessingTask();
	private ReadWriteLock handlerLock=new ReentrantReadWriteLock();
	private Set<Class<? extends MDICommand>> handledCommands=new HashSet<Class<? extends MDICommand>>();
	private HashMap<MDICommandHandler,  Collection<BlockingQueue<MDICommand>>> handlers=new HashMap<MDICommandHandler,  Collection<BlockingQueue<MDICommand>>>();
	private HashMap<Class<? extends MDICommand>, Collection<BlockingQueue<MDICommand>>> cmdQueues=new HashMap<Class<? extends MDICommand>, Collection<BlockingQueue<MDICommand>>>();

	private Set<WaveformGenerator> waveformGenerators=new HashSet<WaveformGenerator>();


	protected abstract void addHandlers();

	protected void addHandler(MDICommandHandler commandHandler) 
	{
		if (commandHandler!=null)
		{
			Set<Class<? extends MDICommand>> tempHandledCommands = commandHandler.getHandledCommands();
			if (tempHandledCommands!=null && !tempHandledCommands.isEmpty())
			{
				commandHandler.stopCommandQueueProcessing();
				try{
					handlerLock.writeLock().lock();
					for (Class<? extends MDICommand> cmdClz : tempHandledCommands) {
						Collection<BlockingQueue<MDICommand>> queues= cmdQueues.get(cmdClz);
						if (queues==null)
							queues=new ArrayList<BlockingQueue<MDICommand>>();
						BlockingQueue<MDICommand> queue=commandHandler.getMDICommandQueue();
						if (queue==null)
						{
							queue=new ArrayBlockingQueue<MDICommand>(1);
							commandHandler.setMDICommandQueue(queue);
						}

						queues.add(queue);
						cmdQueues.put(cmdClz, queues);

						queues=handlers.get(commandHandler);
						if (queues==null)
							queues=new ArrayList<BlockingQueue<MDICommand>>();

						queues.add(queue);
						handlers.put(commandHandler, queues);
					}

					handledCommands.addAll(tempHandledCommands);
				}finally{
					handlerLock.writeLock().unlock();
				}
				commandHandler.startCommandQueueProcessing();
			}
		}
	}


	protected abstract void addWaveformGenerators(); 

	protected void addWaveformGenerator(
			WaveformGenerator waveformGenerator) 
	{
		waveformGenerators.add(waveformGenerator);
		waveformGenerator.start();
	}

	private class CommandProcessingTask implements Runnable
	{
		private boolean queueProcessingThreadRunning=true;

		@Override
		public void run() {
			while(queueProcessingThreadRunning && commandQueue!=null)
			{

				try {
					MDICommand command= commandQueue.take();
					
					if (command!=null)
						handleCommand(command);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		private void handleCommand(final MDICommand command) 
		{
			Collection<BlockingQueue<MDICommand>> queues = cmdQueues.get(command.getClass());
			if (queues!=null)
			{
				for (BlockingQueue<MDICommand> queue : queues) {
					try {
						boolean accepted=queue.offer(command,5000, TimeUnit.MILLISECONDS);
						if (!accepted)
						{
							//TODO SSch Logger
						    Log.error("Command not accepted.");
						}
					} catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void startupComplete() 
	{
		if (Log.isInfo())
			Log.info("Startup complete");
	}




}
