/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.impl;

import java.io.FileReader;
import java.io.Reader;

import javax.xml.bind.JAXB;

import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription;

/**
 * The Class FileInterfaceDescription.
 */
public class FileInterfaceDescription implements
BICEPSDeviceNodeInterfaceDescription {


	private static final FileInterfaceDescription instance=new FileInterfaceDescription();


	/**
	 * Gets the single instance of FileInterfaceDescription.
	 *
	 * @return single instance of FileInterfaceDescription
	 */
	public static FileInterfaceDescription getInstance() {
		return instance;
	}

	private MDDescription mdDescription;
	private String fileName=System.getProperty("BICEPS.FileInterfaceDescription","config/MDIB.xml");

	private FileInterfaceDescription()
	{
		//void
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdi.BICEPSDeviceNodeInterfaceDescription#getMDIB()
	 */
	@Override
	public synchronized MDDescription getMDIB() {
		if (this.mdDescription == null)
		{
			this.mdDescription = loadMdib();
		}
		
		return this.mdDescription;
	}


	private MDDescription loadMdib() 
	{
		try
		{
			Reader reader=new FileReader(fileName);
			MDIB mdib = JAXB.unmarshal(reader, MDIB.class);
			MDDescription mdibDescription = mdib.getDescription();
			
			reader.close();
			
			return mdibDescription;

		}catch(Exception e)
		{
			throw new IllegalStateException(e);
		}
	}
}
