/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction;

import java.util.Set;
import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;


public interface MDICommandHandler extends MDIHandler
{
	public abstract boolean canHandleCommand(Class<?> cmdClz);
	public abstract void setMDICommandQueue(BlockingQueue<MDICommand> queue);
	public abstract BlockingQueue<MDINotification> getMDINotificationQueue();
	public abstract void startCommandQueueProcessing();
	public abstract void stopCommandQueueProcessing();
	public Set<Class<? extends MDICommand>> getHandledCommands();
	public abstract BlockingQueue<MDICommand> getMDICommandQueue();
	public abstract Set<BICEPSQoSPolicy> getPoliciesForCommand(
			Class<? extends MDICommand> commandClz);
}
