/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction;

import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.CurrentAlertCondition;
import com.draeger.medical.biceps.common.model.CurrentAlertSignal;
import com.draeger.medical.biceps.common.model.CurrentAlertSystem;
import com.draeger.medical.biceps.common.model.CurrentLimitAlertCondition;
import com.draeger.medical.biceps.common.model.EnumStringMetricDescriptor;
import com.draeger.medical.biceps.common.model.EnumStringMetricState;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;
import com.draeger.medical.biceps.common.model.StringMetricState;

public interface MDIStateBuilder {
	public abstract MDState getMedicalDeviceInterfaceState();
	public abstract void initializeStatesForDescription(MDDescription description);
//	public abstract  ContextAssociationStateValue getContextAssociationStateValue(AbstractContextDescriptor contextDescriptor);
	public abstract <T extends AbstractIdentifiableContextState> T getIdentifiableContextState( AbstractContextDescriptor contextDescriptor, Class<T> stateClass);
	public abstract OperationState getOperationState(OperationDescriptor operationDescriptor );
	
	//Metrics
	public abstract NumericMetricState getNumericMetricState(NumericMetricDescriptor nmd);
	public abstract RealTimeSampleArrayMetricState getRealTimeSampleArrayMetricState(RealTimeSampleArrayMetricDescriptor rtsamd);
	public abstract StringMetricState  getStringMetricState(StringMetricDescriptor nmd);
	public abstract EnumStringMetricState  getEnumStringMetricState(EnumStringMetricDescriptor nmd);
	
	//Alerts
	public abstract CurrentAlertSystem getAlertSystemState(AlertSystemDescriptor asd);
	public abstract CurrentAlertCondition getAlertConditionState(AlertConditionDescriptor asd);
	public abstract CurrentLimitAlertCondition getLimitAlertConditionState(AlertConditionDescriptor asd);
	public abstract CurrentAlertSignal getAlertSignalState(AlertSignalDescriptor asd);
}
