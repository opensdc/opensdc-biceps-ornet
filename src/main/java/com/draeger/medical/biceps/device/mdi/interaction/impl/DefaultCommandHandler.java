/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommandHandler;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSPolicy;

public abstract class DefaultCommandHandler implements MDICommandHandler{

	private final HashSet<Class<? extends MDINotification>> sendNotifications=new HashSet<Class<? extends MDINotification>>();
	private BlockingQueue<MDINotification> notificationQueue;
	private final HashSet<Class<? extends MDICommand>> handledCommands=new HashSet<Class<? extends MDICommand>>();
	private BlockingQueue<MDICommand> commandQueue;
	private final CommandProcessingTask commandProcessingTask;
	private Thread queueProcessingThread;

	protected DefaultCommandHandler(CommandProcessingTask commandProcessingTask)
	{
		this.commandProcessingTask=commandProcessingTask;
		this.commandProcessingTask.setCommandHandler(this);
		fillSendNotifications();
		fillHandledCommands();
	}

	
	@Override
	public boolean canSendNotification(Class<MDINotification> notificationClz) {
		return sendNotifications.contains(notificationClz);
	}

	protected HashSet<Class<? extends MDINotification>> getSendNotifications() {
		return sendNotifications;
	}

	
	@Override
	public void setMDINotificationQueue(BlockingQueue<MDINotification> notificationQueue) 
	{
		this.notificationQueue=notificationQueue;
	}

	@Override
	public Set<BICEPSQoSPolicy> getPoliciesForCommand(Class<? extends MDICommand> commandClz) 
	{
		return null;
	}


	@Override
	public BlockingQueue<MDINotification> getMDINotificationQueue() {
		return notificationQueue;
	}

	
	@Override
	public boolean canHandleCommand(Class<?> cmdClz) {
		return handledCommands.contains(cmdClz);
	}

	
	@Override
	public HashSet<Class<? extends MDICommand>> getHandledCommands() {
		return handledCommands;
	}

	
	@Override
	public void setMDICommandQueue(BlockingQueue<MDICommand> cmdQueue) 
	{
		this.commandQueue=cmdQueue;
	}

	
	@Override
	public BlockingQueue<MDICommand> getMDICommandQueue() {
		return commandQueue;
	}

	
	@Override
	public void startCommandQueueProcessing() 
	{
		this.queueProcessingThread=new BICEPSThreadFactory("CommandHandler-"+this.getClass().getName()+"-"+this.commandProcessingTask).newThread(this.commandProcessingTask);
		this.commandProcessingTask.setRunning(true);
		this.commandProcessingTask.setCommandQueue(this.commandQueue);
		queueProcessingThread.start();
	}

	
	@Override
	public void stopCommandQueueProcessing() 
	{
		this.commandProcessingTask.setRunning(false);
	}
	
	

	public CommandProcessingTask getCommandProcessingTask() {
		return commandProcessingTask;
	}


	protected abstract void fillSendNotifications();
	protected abstract void fillHandledCommands();

	public static abstract class CommandProcessingTask implements Runnable{

		private boolean queueProcessingThreadRunning=false;
		private BlockingQueue<? extends MDICommand> commandQueue;
		private MDICommandHandler commandHandler;

		
		@Override
		public void run() 
		{
			while(queueProcessingThreadRunning)
			{
				if (commandQueue!=null)
				{
					try {
						final MDICommand command = commandQueue.take();
						
						if (command!=null)
							handleCommand(command);

					} catch (InterruptedException e) {
						e.printStackTrace();
						//TODO SSch logger
					}
				}
			}
		}

		public void setCommandHandler(
				MDICommandHandler commandHandler) {
			this.commandHandler=commandHandler;
		}

		public MDICommandHandler getCommandHandler() {
			return commandHandler;
		}

		public void setCommandQueue(BlockingQueue<? extends MDICommand> commandQueue) {
			this.commandQueue = commandQueue;
		}

		protected abstract void handleCommand(MDICommand command);

		public void setRunning(boolean running){
			queueProcessingThreadRunning=running;
		}

	}

}
