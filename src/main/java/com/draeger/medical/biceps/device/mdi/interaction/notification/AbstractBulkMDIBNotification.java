/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import java.util.Set;

import com.draeger.medical.biceps.device.mdi.interaction.AbstractMDINotification;

public abstract class AbstractBulkMDIBNotification extends AbstractMDINotification implements BulkMDIBNotification{
	
	private final Set<String> stateHandles;
	
	AbstractBulkMDIBNotification(Set<String> stateHandles)
	{
		super(null);
		this.stateHandles=stateHandles;
	}
	
	@Override
	public Set<String> getStateHandles() {
		return stateHandles;
	}
	
}
