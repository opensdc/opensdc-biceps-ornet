/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.AbstractAlertState;

public class AlertStateChanged extends AbstractSingleMDIBNotification {

	private AbstractAlertState newAlertState;

	public AlertStateChanged(AbstractAlertState newAlertState) 
	{
		super(newAlertState!=null?newAlertState.getHandle():null);
		this.newAlertState=newAlertState;
	}
	
	@Override
	public AbstractAlertState getNewState() {
		return newAlertState;
	}
}
