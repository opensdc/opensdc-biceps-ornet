/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import java.util.Collection;
import java.util.Map;

import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.State;

public class AlertStatesChanged extends AbstractBulkMDIBNotification implements BulkMDIBStateNotification{

	private Map<String,? extends AbstractAlertState> newAlertStates;

	public AlertStatesChanged(Map<String,? extends AbstractAlertState> newAlertStates) 
	{
		super(newAlertStates!=null?newAlertStates.keySet():null);
		this.newAlertStates=newAlertStates;
	}

	public Map<String,? extends AbstractAlertState> getVmos() {
		return newAlertStates;
	}
	
	@Override
	public Collection<? extends State> getStates() {
		return newAlertStates.values();
	}
}
