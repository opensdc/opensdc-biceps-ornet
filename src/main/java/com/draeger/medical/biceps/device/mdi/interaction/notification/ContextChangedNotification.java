/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;


/**
 * Notification that contains information about a changed context.
 */
public interface ContextChangedNotification<C extends AbstractIdentifiableContextState> extends MDINotification{
	
	/**
	 * Gets the mds handle.
	 *
	 * @return the mds handle
	 */
	public String getMdsHandle();
	
	/**
	 * Gets the referenced descriptor handle.
	 *
	 * @return the referenced descriptor handle
	 */
	public String getReferencedDescriptorHandle();
	

	/**
	 * Gets the context item.
	 *
	 * @return the context item
	 */
	public C getContextItem();
	
}
