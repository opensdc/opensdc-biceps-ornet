/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import java.util.Collection;
import java.util.Map;

import com.draeger.medical.biceps.common.model.ComponentState;
import com.draeger.medical.biceps.common.model.State;

public class CurrentValuesChanged extends AbstractBulkMDIBNotification implements BulkMDIBStateNotification {

	private final Map<String,? extends ComponentState> vmos;

	public CurrentValuesChanged(Map<String,? extends ComponentState> vmos) {
		super(vmos!=null?vmos.keySet():null);
		this.vmos=vmos;
	}

	public Map<String,? extends ComponentState> getVMOStates() {
		return vmos;
	}

	@Override
	public Collection<? extends State> getStates() {
		return vmos.values();
	}
}
