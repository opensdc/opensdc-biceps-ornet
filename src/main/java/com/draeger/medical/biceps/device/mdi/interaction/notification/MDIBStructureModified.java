/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import java.util.HashMap;

import com.draeger.medical.biceps.common.model.MDDescription;

public class MDIBStructureModified extends AbstractBulkMDIBNotification {

	private final HashMap<String,StructureModificationType> modifications;
	private final MDDescription modifiedMDIB;
	
	public MDIBStructureModified(HashMap<String,StructureModificationType> modifications, MDDescription modifiedMDIB ) {
		super(modifications!=null?modifications.keySet():null);
		this.modifications=modifications;
		this.modifiedMDIB=modifiedMDIB;
	}

	public HashMap<String, StructureModificationType> getModifications() {
		return modifications;
	}

	public MDDescription getModifiedMDIB() {
		return modifiedMDIB;
	}
}
