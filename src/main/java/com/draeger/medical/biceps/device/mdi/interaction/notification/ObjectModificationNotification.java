/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import java.util.Map;

import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSContext;

public class ObjectModificationNotification implements DescriptorModificationNotification {

	private final String parentDescriptor;
	private final Map<String, DescriptorModification> modification;
	
	

	public ObjectModificationNotification(String parentDescriptor,
			Map<String, DescriptorModification> modification) {
		super();
		this.parentDescriptor = parentDescriptor;
		this.modification = modification;
	}

	@Override
	public BICEPSQoSContext getQoSContext() {
		return null;
	}

	@Override
	public String getParentDescriptor() {
		return parentDescriptor;
	}

	@Override
	public Map<String, DescriptorModification> getModifications() {
		return modification;
	}

}
