/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.device.mdib.MDIBOperation;

public class OperationStateChangedNotification extends AbstractSingleMDIBNotification {

	private final MDIBOperation operation;
	private final InvocationState newState;
	private final String mdsHandle;

	public OperationStateChangedNotification(String mdsHandle, MDIBOperation operation, InvocationState newState) {
		super(mdsHandle);
		this.mdsHandle=mdsHandle;
		this.operation=operation;
		this.newState=newState;
	}

	public MDIBOperation getOperation() {
		return operation;
	}

	public InvocationState getNewInvocationState() {
		return newState;
	}

	public String getMdsHandle() {
		return mdsHandle;
	}

	@Override
	public State getNewState() {
		return null;
	}
}
