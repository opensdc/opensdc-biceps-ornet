/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.PatientContextState;

public class PatientAssociationChanged extends SingleContextNotification<PatientContextState> {

	private final PatientContextState  oldState;

	public PatientAssociationChanged(String mdsHandle, PatientContextState newState, PatientContextState oldState) {
		super(newState, mdsHandle);
		this.oldState=oldState;
	}

	public PatientContextState getOldState() {
		return oldState;
	}

}
