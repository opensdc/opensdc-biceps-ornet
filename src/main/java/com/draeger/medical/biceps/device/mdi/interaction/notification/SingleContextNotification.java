/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.device.mdi.interaction.AbstractMDINotification;


/**
 * The Class AbstractSingleContextNotification.
 *
 * @param <A> the generic type
 * @param <C> the generic type
 */
public class SingleContextNotification<C extends AbstractIdentifiableContextState> extends AbstractMDINotification implements 
ContextChangedNotification<C> {
	private final String mdsHandle;
	private final String referencedDescriptor;
//	private final A associationState;
	private final C contextState;


	/**
	 * Instantiates a new single context notification.
	 *
	 * @param associationState the association state
	 * @param contextState the context state
	 * @param mdsHandle the mds handle
	 */
	public SingleContextNotification(C contextState, String mdsHandle) {
		super(null);

		String tRefDescriptor=null;
		//		if (associationState!=null)
		//			tRefDescriptor = associationState.getReferencedDescriptor();
		//		else 
		if (contextState!=null)
			tRefDescriptor=contextState.getReferencedDescriptor();

		referencedDescriptor=tRefDescriptor;

//		this.associationState=associationState;
		this.contextState=contextState;
		this.mdsHandle=mdsHandle;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification#getMdsHandle()
	 */
	@Override
	public String getMdsHandle() {
		return mdsHandle;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification#getAssociationState()
	 */
//	@Override
//	public A getAssociationState() {
//		return associationState;
//	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification#getContextItem()
	 */
	@Override
	public C getContextItem() {
		return contextState;
	}

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification#getReferencedDescriptorHandle()
	 */
	@Override
	public String getReferencedDescriptorHandle() {
		return referencedDescriptor;
	}

}
