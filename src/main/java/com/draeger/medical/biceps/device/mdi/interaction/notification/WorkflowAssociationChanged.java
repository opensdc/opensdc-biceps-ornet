package com.draeger.medical.biceps.device.mdi.interaction.notification;

import com.draeger.medical.biceps.common.model.WorkflowContextState;

public class WorkflowAssociationChanged extends SingleContextNotification<WorkflowContextState> {

	private final WorkflowContextState oldState;
	
	public WorkflowAssociationChanged(String mdsHandle, WorkflowContextState workflowContextState, WorkflowContextState oldState) {
		super(workflowContextState, mdsHandle);
		
		this.oldState = oldState;
	}
	
	public WorkflowContextState getOldState() {
		return oldState;
	}

}
