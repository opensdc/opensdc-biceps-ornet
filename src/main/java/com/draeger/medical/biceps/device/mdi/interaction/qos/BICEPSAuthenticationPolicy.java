/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

public class BICEPSAuthenticationPolicy implements BICEPSSingleOperationQoSPolicy{
	private final String operationHandle;
	private final int mal;

	public BICEPSAuthenticationPolicy(String operationHandle, int minimalAccessLevel) 
	{
		super();
		this.operationHandle = operationHandle;
		this.mal=minimalAccessLevel;
	}

	@Override
	public String getOperationHandle() {
		return operationHandle;
	}

	public int getMinimalAccessLevel() {
		return mal;
	}

}
