/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import java.util.ArrayList;

import org.ws4d.java.types.QName;

import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

public class BICEPSDualChannelDefinition {
	private final ArrayList<QName> dualChannelElementSelectors=new ArrayList<QName>();
	private final QName dualChannelAlgorithm;//=SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE;
	private final QName dualChannelTransmission;//=SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE;
	

	public BICEPSDualChannelDefinition()
	{
		this(SafetyInformationConstants.SI_ATTRIB_ALGORITHM_DUALCHANNEL_DEFAULT_VALUE,SafetyInformationConstants.SI_ATTRIB_TRANSFORM_DUALCHANNEL_DEFAULT_VALUE);
	}
	
	public BICEPSDualChannelDefinition(QName dualChannelAlgorithm, QName dualChannelTransmission) {
		super();
		this.dualChannelAlgorithm = dualChannelAlgorithm;
		this.dualChannelTransmission = dualChannelTransmission;
	}
	public ArrayList<QName> getDualChannelElementSelectors() {
		return dualChannelElementSelectors;
	}
	public QName getDualChannelAlgorithm() {
		return dualChannelAlgorithm;
	}
	public QName getDualChannelTransmission() {
		return dualChannelTransmission;
	}

	@Override
	public String toString() {
		return "BICEPSDualChannelDefinition [dualChannelElementSelectors="
				+ dualChannelElementSelectors + ", dualChannelAlgorithm="
				+ dualChannelAlgorithm + ", dualChannelTransmission="
				+ dualChannelTransmission + "]";
	}
}
