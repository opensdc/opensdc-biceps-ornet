/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.mdpws.common.util.SOAPNameSpaceContext;
import com.draeger.medical.mdpws.qos.wsdl.SafetyInformationConstants;

public class BICEPSNameSpaceContext extends SOAPNameSpaceContext
{

	public BICEPSNameSpaceContext() {
		super();
		addNamespace(SafetyInformationConstants.SI_DEFAULT_NAMESPACE_PREFIX, SafetyInformationConstants.SI_NAMESPACE);
		addNamespace(SchemaHelper.NAMESPACE_MESSAGES_PREFIX, SchemaHelper.NAMESPACE_MESSAGES);
	}

}
