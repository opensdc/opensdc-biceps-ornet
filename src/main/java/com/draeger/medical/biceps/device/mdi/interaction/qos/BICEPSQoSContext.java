/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import java.util.List;
import java.util.Set;

import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyInformation;

public interface BICEPSQoSContext {
	public abstract List<SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>> getSafetyInformationToken();
	public abstract Set<QoSPolicyToken<?, ?>> getRawQoSTokens(); 
}
