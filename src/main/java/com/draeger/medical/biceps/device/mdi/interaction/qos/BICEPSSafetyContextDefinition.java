/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import java.util.ArrayList;

import org.ws4d.java.types.QName;

public class BICEPSSafetyContextDefinition {
	private final String contextObjectHandle;
	private final ArrayList<QName> attributeSelectors=new ArrayList<QName>();
	
	
	public BICEPSSafetyContextDefinition(String contextObjectHandle) {
		super();
		this.contextObjectHandle = contextObjectHandle;
	}
	
	public String getContextObjectHandle() {
		return contextObjectHandle;
	}
	
	public ArrayList<QName> getAttributeSelectors() {
		return attributeSelectors;
	}

	@Override
	public String toString() {
		return "BICEPSSafetyContextDefinition [contextObjectHandle="
				+ contextObjectHandle + ", attributeSelectors="
				+ attributeSelectors + "]";
	}

}
