/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.mdpws.common.util.XPathInfo;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyAttributes;
import com.draeger.medical.mdpws.qos.safetyinformation.def.ContextDefinition;
import com.draeger.medical.mdpws.qos.safetyinformation.def.DualChannelSelector;
import com.draeger.medical.mdpws.qos.safetyinformation.def.MessageFilter;

public class BICEPSSafetyContextPolicyConverter {

	public static SafetyInformationPolicyAttributes convertFromBICEPSPolicyToAttributes(
			BICEPSSafetyInformationQoSPolicy diceQoSPolicy) {
		BICEPSNameSpaceContext bicepsNameSpaceCtxt = new BICEPSNameSpaceContext();

		SafetyInformationPolicyAttributes attributes = new SafetyInformationPolicyAttributes();

		if (diceQoSPolicy.getOperationHandles() != null
				&& !diceQoSPolicy.getOperationHandles().isEmpty()) {
			// TODO SSch Message filter???, use OperationHandles???
			XPathInfo xpathMessageFilter = null;
			MessageFilter messageFilter = new MessageFilter(
					diceQoSPolicy.getOperationHandles(), xpathMessageFilter);
			attributes.setMessageFilter(messageFilter);
		}

		attributes
				.setTransmitDualChannel(diceQoSPolicy.isTransmitDualChannel());
		if (!diceQoSPolicy.getDualChannelDefinitions().isEmpty()) {
			QName algorithm = null;
			QName transform = null;

			for (BICEPSDualChannelDefinition dcDef : diceQoSPolicy
					.getDualChannelDefinitions()) {
				if (algorithm == null
						|| algorithm.equals(dcDef.getDualChannelAlgorithm())) {
					algorithm = dcDef.getDualChannelAlgorithm();
				} else {
					Log.warn("Algorithm definition not consistent. "
							+ algorithm
							+ " has been definied before. Ignoring "
							+ dcDef.getDualChannelAlgorithm());
				}

				if (transform == null
						|| transform.equals(dcDef.getDualChannelTransmission())) {
					transform = dcDef.getDualChannelTransmission();
				} else {
					Log.warn("Transform definition not consistent. "
							+ transform
							+ " has been definied before. Ignoring "
							+ dcDef.getDualChannelTransmission());
				}

				for (QName dcElementSelector : dcDef
						.getDualChannelElementSelectors()) {
					// TODO SSch generalize, use namespacecontext
					// TODO SSch use action and namespacecontext
					XPathInfo selectedElementInMessage = new XPathInfo(
							"//s12:Body//"
									+ SchemaHelper.NAMESPACE_MESSAGES_PREFIX
									+ ":" + dcElementSelector.getLocalPart(),
							bicepsNameSpaceCtxt);
					DualChannelSelector dcSelector = new DualChannelSelector(
							selectedElementInMessage, dcElementSelector);
					attributes.getDualChannelSelectors().add(dcSelector);
				}
			}
			attributes.setAlgorithm(algorithm);
			attributes.setTransform(transform);
		}

		attributes.setTransmitSafetyContext(diceQoSPolicy
				.isTransmitSafetyContext());
		if (!diceQoSPolicy.getSafetyContextDefinitions().isEmpty()) {
			for (BICEPSSafetyContextDefinition sCtxtDef : diceQoSPolicy
					.getSafetyContextDefinitions()) {
				if (!sCtxtDef.getAttributeSelectors().isEmpty()) {
					ContextDefinition contextDefinition = new ContextDefinition(
							sCtxtDef.getContextObjectHandle());
					contextDefinition.getAttributeSelectors().addAll(
							sCtxtDef.getAttributeSelectors());
					attributes.getContextDefinitions().add(contextDefinition);
				}
			}
		}
		return attributes;
	}

	
}
