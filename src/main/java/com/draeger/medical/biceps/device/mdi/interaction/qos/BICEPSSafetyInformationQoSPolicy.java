/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.qos;

import java.util.ArrayList;
import java.util.Collection;


public class BICEPSSafetyInformationQoSPolicy implements BICEPSMultiOperationsQoSPolicy {
	private final ArrayList<String> operationHandles;
	private final ArrayList<BICEPSDualChannelDefinition> dualChannelDefinitions=new ArrayList<BICEPSDualChannelDefinition>();
	private final ArrayList<BICEPSSafetyContextDefinition> safetyContextDefinitions=new ArrayList<BICEPSSafetyContextDefinition>();
	
	
	public BICEPSSafetyInformationQoSPolicy(Collection<String> operationHandles) {
		super();
		this.operationHandles = new ArrayList<String>(operationHandles);
	}

	@Override
	public ArrayList<String> getOperationHandles() {
		return operationHandles;
	}


	public boolean isTransmitDualChannel() {
		return !dualChannelDefinitions.isEmpty();
	}

	public Collection<BICEPSDualChannelDefinition> getDualChannelDefinitions() {
		return dualChannelDefinitions;
	}
	
	public boolean isTransmitSafetyContext() {
		return !safetyContextDefinitions.isEmpty();
	}

	public Collection<BICEPSSafetyContextDefinition> getSafetyContextDefinitions() {
		return safetyContextDefinitions;
	}

	@Override
	public String toString() {
		return "BICEPSSafetyInformationQoSPolicy [operationHandles="
				+ operationHandles + ", dualChannelDefinitions="
				+ dualChannelDefinitions + ", safetyContextDefinitions="
				+ safetyContextDefinitions + "]";
	}	
	
	
}
