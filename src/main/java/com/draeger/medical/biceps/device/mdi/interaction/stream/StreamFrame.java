/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdi.interaction.stream;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.draeger.medical.biceps.common.model.Annotation;
import com.draeger.medical.biceps.common.model.ComponentActivation;
import com.draeger.medical.biceps.common.model.MeasurementState;
import com.draeger.medical.biceps.device.mdi.interaction.AbstractMDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSContext;

/**
 * A {@link MDINotification} that contains information about one frame of a stream.
 */
public class StreamFrame extends AbstractMDINotification {

	private final String metricDescriptorHandle;
	private final MeasurementState measurementStatus;
	private static final int initialSampleListCapacity=50;
	private final Collection<BigDecimal> values;
	private final long timestamp;
	private final List<Annotation> annotations;
	private final String metricStateHandle;
	private final ComponentActivation metricState;
	
	private static final int 			maxProvider=Integer.parseInt(System.getProperty("BICEPS.StreamFrame.PoolSize", "5"));
	private static final int 			maxBlockTime=Integer.parseInt(System.getProperty("BICEPS.StreamFrame.maxBlockTime", "1000"));
	private static final byte 			whenExhaustedAction=(byte)Integer.parseInt(System.getProperty("BICEPS.StreamFrame.whenExhaustedAction", String.valueOf(GenericObjectPool.WHEN_EXHAUSTED_GROW)));

	private final static ObjectPool pool=new GenericObjectPool(new ArrayListFactory<BigDecimal>(), maxProvider,whenExhaustedAction,maxBlockTime);

	private static class ArrayListFactory<T> extends BasePoolableObjectFactory{

		/* (non-Javadoc)
		 * @see org.apache.commons.pool.BasePoolableObjectFactory#makeObject()
		 */
		@Override
		public Object makeObject() throws Exception {
			return new ArrayList<T>(initialSampleListCapacity);
		}

		@Override
		public void passivateObject(Object obj) throws Exception {
			((ArrayList<?>) obj).clear();
		}
	}

	/**
	 * Instantiates a new stream frame.
	 *
	 * @param context the context
	 * @param metricHandle the metric handle
	 * @param measurementStatus the measurement status
	 */
	public StreamFrame(BICEPSQoSContext context, String metricHandle,
			MeasurementState measurementStatus) {
		this(context, metricHandle, measurementStatus, initialSampleListCapacity, System.nanoTime(),null);
	}

	/**
	 * Instantiates a new stream frame.
	 *
	 * @param context the context
	 * @param metricDescriptorHandle the metric descriptor handle
	 * @param measurementStatus the measurement status
	 * @param sampleCapacity the sample capacity
	 * @param timestamp the timestamp
	 * @param annotations the annotations
	 */
	public StreamFrame(BICEPSQoSContext context, String metricDescriptorHandle,
			MeasurementState measurementStatus, int sampleCapacity, long timestamp, List<Annotation> annotations) {
		this(context,metricDescriptorHandle,null,measurementStatus, sampleCapacity,timestamp,annotations, ComponentActivation.ON);
	}

	/**
	 * Instantiates a new stream frame.
	 *
	 * @param context the context
	 * @param metricDescriptorHandle the metric descriptor handle
	 * @param metricStateHandle the metric state handle
	 * @param measurementStatus the measurement status
	 * @param sampleCapacity the sample capacity
	 * @param timestamp the timestamp
	 * @param annotations the annotations
	 * @param metricState the metric state
	 */
	public StreamFrame(BICEPSQoSContext context, String metricDescriptorHandle,String metricStateHandle,
			MeasurementState measurementStatus, int sampleCapacity, long timestamp, List<Annotation> annotations, ComponentActivation metricState) {
		super(context);
		this.metricDescriptorHandle = metricDescriptorHandle;
		this.metricStateHandle=(metricStateHandle!=null)?metricStateHandle:metricDescriptorHandle+"_state";
		this.timestamp = timestamp;
		this.measurementStatus = measurementStatus;
		this.annotations=annotations;
		this.values=getCollection(sampleCapacity);
		this.metricState = metricState;
	}

	/**
	 * Release stream frame.
	 */
	public void releaseStreamFrame()
	{
		releaseArrayList(this.values);
	}

	private static void releaseArrayList(Collection<BigDecimal> list) {
		//TODO SSch include Pooling again
//		try {
//			pool.returnObject(list);
//		} catch (Exception e) {
//			Log.error(e);
//		}
	}

	@SuppressWarnings("unchecked")
	private static Collection<BigDecimal> getCollection(int capacity) {
		return new ArrayList<BigDecimal>(initialSampleListCapacity);
		//TODO SSch include Pooling again
//		try {
//			retVal= (ArrayList<BigDecimal>) pool.borrowObject();
//			retVal.ensureCapacity(capacity);
//		} catch (Exception e) {
//			Log.error(e);
//		}
//		return retVal;
	}

	/**
	 * Gets the metric descriptor handle.
	 *
	 * @return the metric descriptor handle
	 */
	public String getMetricDescriptorHandle() {
		return metricDescriptorHandle;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public MeasurementState getStatus() {
		return measurementStatus;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public Collection<BigDecimal> getValues() 
	{
		return values;
	}

	/**
	 * Gets the annotations.
	 *
	 * @return the annotations
	 */
	public List<Annotation> getAnnotations() {
		return annotations;
	}



	/**
	 * Gets the metric state handle.
	 *
	 * @return the metric state handle
	 */
	public String getMetricStateHandle() {
		return metricStateHandle;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return this.timestamp;
	}

	/**
	 * Gets the metric state.
	 *
	 * @return the metric state
	 */
	public ComponentActivation getMetricState()
	{
		return metricState;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("StreamFrame [metricDescriptorHandle=");
		builder.append(metricDescriptorHandle);
		builder.append(", measurementStatus=");
		builder.append(measurementStatus);
		builder.append(", values=");
		builder.append(values);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", annotations=");
		builder.append(annotations);
		builder.append(", metricStateHandle=");
		builder.append(metricStateHandle);
		builder.append(", metricState=");
		builder.append(metricState);
		builder.append("]");
		return builder.toString();
	}
}
