/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

/**
 * A configurable manager that checks authorization of service consumer.
 */
public interface AuthorizationManager {


	/**
	 * Check credentials for operation handle.
	 *
	 * @param msgContextMap the msg context map
	 * @param operationHandle the operation handle
	 * @return true, if successful
	 */
	public abstract boolean checkCredentialsForOperationHandle(
			MDPWSMessageContextMap msgContextMap, String operationHandle);

	/**
	 * Sets the minimal access level for operation.
	 *
	 * @param operationHandle the operation handle
	 * @param minimalAccessLevel the minimal access level
	 */
	public abstract void setMinimalAccessLevelForOperation(String operationHandle, int minimalAccessLevel);


}
