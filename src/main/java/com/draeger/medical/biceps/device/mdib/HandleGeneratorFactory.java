/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

import com.draeger.medical.biceps.device.mdib.impl.HandleGeneratorImpl;

public class HandleGeneratorFactory
{
    private static final HandleGeneratorFactory instance = new HandleGeneratorFactory();

    public static HandleGeneratorFactory getInstance()
    {
        return instance;
    }

    private HandleGenerator handleGenerator;

    private HandleGeneratorFactory()
    {
        handleGenerator = new HandleGeneratorImpl();
    }
    
    public HandleGenerator getHandleGenerator()
    {
        return handleGenerator;
    }
}
