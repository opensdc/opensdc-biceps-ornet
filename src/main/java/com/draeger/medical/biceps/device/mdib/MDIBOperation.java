/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;


public class MDIBOperation
{
    private final MedicalDeviceInformationBase mdib;
    private final OperationRequest operationRequest;
    private final int transactionId;
    private final String mdsHandle;

    public MDIBOperation(OperationRequest request, int transactionId, String mdsHandle, MedicalDeviceInformationBase mdib)
    {
        this.operationRequest = request;
        this.transactionId = transactionId;
        this.mdsHandle = mdsHandle;
        this.mdib = mdib;
    }

    public OperationRequest getOperationRequest()
    {
        return operationRequest;
    }

    public int getTransactionId()
    {
        return transactionId;
    }

    public String getMdsHandle()
    {
        return mdsHandle;
    }

    public MedicalDeviceInformationBase getMdib()
    {
        return mdib;
    }

}
