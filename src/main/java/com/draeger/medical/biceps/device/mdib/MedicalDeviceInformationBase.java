/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import com.draeger.medical.biceps.common.model.AbstractAlertDescriptor;
import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.ContainmentTreeEntry;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDSState;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.State;

/**
*
 *
 */
public interface MedicalDeviceInformationBase 
{
	

	
	/**
	 * Create a MDIB using the provided description. The initial state of the created MDIB is an empty set.
	 * @param mdDescription The description of the medical devices that should be used to create the MDIB.
	 */
	public abstract void create(MDDescription mdDescription);
	
	
	/**
	 * Create a MDIB using the provided description and the initial state.
	 *
	 * @param mdib the mdib
	 * @param initialMDState the initial md state
	 */
	public abstract void create(MDDescription mdib, MDState initialMDState);
	
	/**
	 * Create a MDIB using the provided description and uses the provided lock for all read or write access to the MDIB.
	 * 
	 * @param mdDescription The description of the medical devices that should be used to create the MDIB.
	 * @param mdibLock The Lock that should be used for all locking operations on the MDIB.
	 */
	public abstract void createWithExistingLock(MDDescription mdDescription, ReadWriteLock mdibLock);
	
	/**
	 * Create a MDIB using the provided description and the initial state and uses the provided lock for all read or write access to the MDIB.
	 *
	 * @param mdDescription the md description
	 * @param initialMDState the initial md state
	 * @param mdibLock the mdib lock
	 */
	public abstract void createWithExistingLock(MDDescription mdDescription,MDState initialMDState, ReadWriteLock mdibLock);
	
	/**
	 * Return the Manager that should be used for all direct modifications to the MDIB.
	 * 
	 * @return Manager that should be used for all direct modifications to the MDIB.
	 */
	public abstract MedicalDeviceInformationBaseManager getManager();
	
	/**
	 * Returns the Lock that should be used for locking the MDIB for Read/Write Operations.
	 * 
	 * @return Lock that should be used for locking the MDIB for Read/Write Operations.
	 */
	public abstract ReadWriteLock getMdibLock();


	/**
	 * Returns the current descriptions of the medical devices contained in the MDIB. 
	 * Caveat: The MDDescription is the original object from the MDIB and not a copy. 
	 * The MDIB Lock Should be used for any read/write operations.
	 * 
	 * @return current descriptions of the medical devices contained in the MDIB.
	 */
	public abstract MDDescription getDeviceDescriptions();
	
	/**
	 * Returns the current states of the medical devices contained in the MDIB. 
	 * Caveat: The MDStates is the original object from the MDIB and not a copy. 
	 * The MDIB Lock Should be used for any read/write operations.
	 * 
	 * @return Current states of the medical devices contained in the MDIB.
	 */
	public abstract MDState getDeviceStates();

	/**
	 * Returns the current descriptors for the metrics that are specified with the handles collection. 
	 * If no handle is provided all metrics in the MDIB should be returned. 
	 * If a Handle of a non-metric object (e.g. channel) is provided than all metric that are associated with that object should be returned.
	 * If a Handle of a state object (e.g. metric state) is provided all metric that are associated with that object should be returned.
	 * 
	 * @param handles Collection of handles that specify which metric descriptions should be returned. 
	 * @return Collection of descriptors of metrics.
	 */
	public abstract Collection<? extends MetricDescriptor> getMetricDescriptors(Collection<String> handles);
	
	/**
	 * Returns the current states for the metrics that are specified with the handles collection. 
	 * If no handle is provided all metrics in the MDIB should be returned. 
	 * If a Handle of a non-metric object (e.g. channel) is provided than all metric that are associated with that object should be returned.
	 * If a Handle of a descriptor object (e.g. metric descriptor) is provided all metric that are associated with that object should be returned.
	 * 
	 * @param handles Collection of handles that specify which metric states should be returned. 
	 * @return Collection of states of metrics.
	 */
	public abstract Collection<? extends AbstractMetricState> getMetricStates(Collection<String> handles); 

	/**
	 * Returns the current description for the operations that is specified with the handle. 
	 * If no handle is provided NULL will be returned. 
	 * If a Handle of a state object (e.g. metric state) is provided the operation description that is associated with that object should be returned.
	 * 
	 * @param operationHandle Handle that specify which operation description should be returned. 
	 * @return Description of the specified operation.
	 */
	public abstract OperationDescriptor getOperationDescriptor(String operationHandle);
	
	/**
	 * Returns the current descriptions for the alert systems that are specified with the handles collection. 
	 * If no handle is provided all alert system descriptions in the MDIB should be returned. 
	 * If a Handle of a state object (e.g. channel state) is provided all alert system descriptions that are associated with that object should be returned.
	 * 
	 * @param handles Collection of handles that specify which alert system descriptions should be returned.
	 * @return Collection of alert system descriptions.
	 */
	public abstract Collection<? extends AlertSystemDescriptor> getAlertSystemDescriptions(Collection<String> handles);
	
	/**
	 * Returns the current description for the alert system that is specified with the handle. 
	 * If no handle is provided NULL will be returned. 
	 * If a Handle of a state object (e.g. alert system state) is provided the alert system description that is associated with that object should be returned.
	 * 
	 * @param handle Handle that specify which alert system description should be returned. 
	 * @return Description of the specified alert system.
	 */
	public abstract AlertSystemDescriptor getAlertSystemDescriptor(String handle);
	
	public abstract AlertConditionDescriptor getAlertConditionDescriptor(String handle);
	
	public abstract AlertSignalDescriptor getAlertSignalDescriptor(String handle);

	public abstract Collection<? extends AbstractAlertState> getAlertStates(Collection<String> handles);
	
	public abstract Collection<? extends AbstractAlertDescriptor> getAlertDescriptors(Collection<String> handles);
	

	public abstract Collection<? extends MDSDescriptor> getMDSDescriptions(Collection<String> handles, boolean onlyNonComposite);
	
	public abstract Collection<? extends MDSState> getMDSStates(Collection<String> handles, boolean onlyNonComposite);
	
	public abstract String getMDSDescriptionHandleForOperation(String ophandle);

	public abstract MDSDescriptor getMDSForDescriptor(String handle);

	
	public abstract String getHandle(Object o);

	public abstract Collection<? extends OperationDescriptor> getOperationForOperationTarget(String operationTargetHandle);
	
	public abstract State getState(String stateHandle);
	
	public abstract Descriptor getDescriptor(String referencedDescriptor);
	
	public abstract Collection<Descriptor> getAllDescriptors();

	public abstract ContainmentTreeEntry getContainmentTreeEntry(String referencedDescriptor);
	
	public abstract ContainmentTree getContainmentTree(String referencedDescriptor);
	
	public abstract <T extends AbstractIdentifiableContextState> Collection<T> getIdentifiableContextState(List<String> handles, Class<T> contentClass);
	
	
	public abstract Collection<? extends ContextAssociationStateValue> getContextAssociationStateValues(
			List<String> handles);


	public abstract BigInteger getMdibStateVersion();
}
