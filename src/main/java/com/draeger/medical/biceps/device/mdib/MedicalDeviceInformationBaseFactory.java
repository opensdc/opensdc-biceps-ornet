/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

public class MedicalDeviceInformationBaseFactory {
	private static final MedicalDeviceInformationBaseFactory instance=new MedicalDeviceInformationBaseFactory();
	
	private MedicalDeviceInformationBaseFactory()
	{
		//void
	}

	public static MedicalDeviceInformationBaseFactory getInstance() {
		return instance;
	}
	
	private Class<MedicalDeviceInformationBase> ddaClz=null;
	private static final String DEFAULT_CLZ_NAME="com.draeger.medical.biceps.device.mdib.impl.DefaultMedicalDeviceInformationBase";		//TODO SSch
	public  MedicalDeviceInformationBase getMedicalDeviceInformationBase() throws InstantiationException
	{
		Class<MedicalDeviceInformationBase> ddaClz;
		try {
			ddaClz = getDDAClz();
			return ddaClz.newInstance();
		} catch (Exception e) {
			//TODO SSch Logger
			e.printStackTrace();
			throw new InstantiationException(e.getMessage());
		}
		
	}

	@SuppressWarnings("unchecked")
	private synchronized Class<MedicalDeviceInformationBase> getDDAClz() throws ClassNotFoundException
	{
		if (ddaClz==null)
			ddaClz= (Class<MedicalDeviceInformationBase>) Class.forName(DEFAULT_CLZ_NAME);
		
		return ddaClz;
	}
}
