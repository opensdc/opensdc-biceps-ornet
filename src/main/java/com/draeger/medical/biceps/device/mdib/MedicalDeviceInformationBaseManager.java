/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

import java.util.concurrent.BlockingQueue;

import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;
import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.BICEPSEpisodicEventSource;

public interface MedicalDeviceInformationBaseManager {

	public abstract void registerEpisodicEventSources(BICEPSEpisodicEventSource eventSource);

	public abstract ScriptResponse queueScriptRequestForExecution(ScriptRequest script);

	public abstract OperationResponse queueOperationRequestForExecution(
			OperationRequest request);
	
	public abstract void start();
	
	public abstract void stop();

	public abstract BlockingQueue<MDINotification> getNotificationQueue();

	public abstract BlockingQueue<MDICommand> getCommandQueue();
	
	public abstract BlockingQueue<StreamFrame> getStreamQueue();
	
	public abstract BlockingQueue<StreamFrame> getStreamSinkQueue();

//	public abstract void registerHandledCommandOperations(Set<Class<? extends Operation>> handledCommandOperations);
	
	public abstract void setBICEPSDeviceNode(BICEPSDeviceNode diceDeviceNode);

	public abstract AuthorizationManager getAuthorizationManager();
	
	public abstract PatientDemographicsManager getPatientContextStateManager();

	
	public abstract int getNextTransactionId();
	
	public abstract int getCurrentSequenceId();
}
