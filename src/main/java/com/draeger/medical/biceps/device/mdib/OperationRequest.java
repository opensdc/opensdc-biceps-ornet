/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib;

import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public interface OperationRequest 
{
	public abstract AbstractSet getOperationMessage();
	public abstract OperationDescriptor getOperationDescriptor();
	public abstract MDPWSMessageContextMap getMsgContextMap();
	public abstract MDPWSMessageContextMap getReplyMessageCtxtMap();
	public abstract int getTransactionId();
}
