/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;

import org.ws4d.java.util.Log;
import org.ws4d.java.util.StatisticsHelper;
import org.ws4d.java.util.StatisticsHelper.RunningStat;

import com.draeger.medical.biceps.common.model.WaveformStream;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;
import com.draeger.medical.biceps.device.mdpws.BICEPSWaveformStreamSource;

public class BICEPSWaveformPublisher implements Runnable {
	private int updatePeriod=Integer.parseInt(System.getProperty("BICEPS.WaveformPublisher.UpdatePeriod", "200"));
	private float fudgeFactor=Float.parseFloat(System.getProperty("BICEPS.WaveformPublisher.FudgeFactor", "1.0"));
	private final boolean publishStream=Boolean.parseBoolean(System.getProperty("BICEPS.WaveformPublisher.PublishStream","true"));
	private boolean performanceCntEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.performanceCntEnabled", "false"));
	private boolean running;
	private final BlockingQueue<? extends StreamFrame> streamQueue;
	private final BICEPSWaveformStreamSource streamSource;
	private static final int nanoToMs=(int) Math.pow(10, 6);
	private long updatePeriodNano=(updatePeriod*nanoToMs);
	private int currentUpdatePeriod=updatePeriod;
	private final StatisticsHelper sHelper=new StatisticsHelper("Stream");
	private long sendCnt=0;
	private RunningStat metricCntStat=new RunningStat();

	public BICEPSWaveformPublisher(
			BICEPSWaveformStreamSource diceWaveformStreamSource, BlockingQueue<? extends StreamFrame> streamQueue) {
		this.streamQueue=streamQueue;
		this.streamSource=diceWaveformStreamSource;


		if (this.performanceCntEnabled)
		{
			metricCntStat = new RunningStat();
		}
		else
		{
			metricCntStat = null;
		}
	}




	@Override
	public void run() 
	{
		sHelper.setUpdatePeriodTest(updatePeriod);
		HashMap<String, ArrayList<StreamFrame>> frameMap=new HashMap<String, ArrayList<StreamFrame>>();
		ArrayList<StreamFrame> frameList=new ArrayList<StreamFrame>();
		ArrayList<String> handles=new ArrayList<String>();
		long t1;
		while(isRunning()){
			try {
				t1=System.nanoTime();
				streamQueue.drainTo(frameList);
				WaveformBuilder.addAllFramesToMap(frameMap, frameList, handles);
				frameList.clear();

				//Fire the frames
				fireFrames(frameMap, handles);

				frameMap.clear();
				handles.clear();
				currentUpdatePeriod=(int) (((updatePeriodNano-(System.nanoTime()-t1))/nanoToMs)*fudgeFactor);

				if (currentUpdatePeriod>=1)
					Thread.sleep(currentUpdatePeriod);

				if (publishStream && performanceCntEnabled)
					sHelper.computeStatistics();
			} catch (InterruptedException e) {
				e.printStackTrace();
				//TODO SSch logger
			}
		}
	}







	private synchronized void  fireFrames(HashMap<String, ArrayList<StreamFrame>> frameMap, ArrayList<String> handles) 
	{
		if (sendCnt==0)
		{			
			sendCnt++;
			return;
		}

		if (frameMap!=null && publishStream)
		{
			WaveformStream wvStream = WaveformBuilder.prepareWaveformStream(frameMap, handles);
			if (Log.isDebug()) Log.debug("BICEPSWaveformPublisher is about to publish values from "+handles.size()+" waveforms.");
			publishFrame(handles, wvStream);
		}
	}




	




	private void publishFrame(ArrayList<String> handles,
			WaveformStream wvStream) {
		if ( publishStream && wvStream.getRealTimeSampleArrays().size()>0)
		{
			this.streamSource.publish(wvStream);
			sendCnt++;
			if (performanceCntEnabled && sendCnt%30==0)
			{
				if (Log.isDebug()) Log.debug("[BICEPSWaveformPublisher];Metric;"+metricCntStat.toString());

				metricCntStat.clear();
			}
		}
	}

	private boolean isRunning() 
	{
		return running;
	}

	public void setRunning(boolean b) 
	{
		this.running=b;
	}

}
