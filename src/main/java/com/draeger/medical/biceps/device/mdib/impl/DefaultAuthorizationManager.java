/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.security.auth.x500.X500Principal;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.device.mdib.AuthorizationManager;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyTokenState;
import com.draeger.medical.mdpws.qos.signature.AuthenticationQoSPolicyToken;

/**
 * The default implementation of an {@link AuthorizationManager}.
 */
public class DefaultAuthorizationManager implements AuthorizationManager{

	private final ArrayList<X500Principal> level2=new ArrayList<X500Principal>();
	private final ArrayList<X500Principal> level1=new ArrayList<X500Principal>();
	private final HashMap<String, Integer> malMap=new HashMap<String, Integer>();

	/**
	 * Instantiates a new default authorization manager.
	 */
	public DefaultAuthorizationManager()
	{

	}

	/**
	 * Gets the minial access level for operation.
	 *
	 * @param operationHandle the operation handle
	 * @return the minial access level for operation
	 */
	protected int getMinialAccessLevelForOperation(String operationHandle) {
		//		return Integer.parseInt(System.getProperty("MDPWS."+operationHandle+"MAL", "0"));
		int retVal=0;
		if (malMap.containsKey(operationHandle))
		{
			retVal=malMap.get(operationHandle);
		}
		return retVal;
	}
	

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.AuthorizationManager#setMinimalAccessLevelForOperation(java.lang.String, int)
	 */
	@Override
	public void setMinimalAccessLevelForOperation(String operationHandle,
			int minimalAccessLevel) {
		malMap.put(operationHandle, minimalAccessLevel);
	}

	/**
	 * Gets the minimum access level.
	 *
	 * @return the minimum access level
	 */
	protected int getMinimumAccessLevel() {
		return 0;
	}

	/**
	 * Gets the unknown access level.
	 *
	 * @return the unknown access level
	 */
	protected int getUnknownAccessLevel() {
		return -1;
	}



	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.AuthorizationManager#checkCredentialsForOperationHandle(com.draeger.medical.mdpws.message.MDPWSMessageContextMap, java.lang.String)
	 */
	@Override
	public boolean checkCredentialsForOperationHandle(MDPWSMessageContextMap msgContextMap, String operationHandle) {

		int minimalAccessLevel=this.getMinialAccessLevelForOperation(operationHandle);
		if (minimalAccessLevel<=this.getMinimumAccessLevel())
			return true;

		int currentAccessLevel=this.getUnknownAccessLevel();
		List<QoSPolicyToken<?,?>> qosToken=getQoSTokenList(msgContextMap);
		if (!qosToken.isEmpty())
		{
			for (QoSPolicyToken<?, ?> qoSPolicyToken : qosToken) {
				if (qoSPolicyToken instanceof AuthenticationQoSPolicyToken<?, ?>)
				{
					AuthenticationQoSPolicyToken<?, ?> credentialToken=(AuthenticationQoSPolicyToken<?,?>) qoSPolicyToken;
					currentAccessLevel = processToken(currentAccessLevel, credentialToken);
				}

			}
		}

		return (currentAccessLevel>=minimalAccessLevel);
	}


	private int processToken(int currentAccessLevel, AuthenticationQoSPolicyToken<?, ?> credentialToken) {
		if(credentialToken.getTokenState().equals(QoSPolicyTokenState.VALID))
		{
			List<X509Certificate> certificates= credentialToken.getValue();
			for (X509Certificate certificate : certificates) 
			{
				Log.info("Checking certificate: "+certificate.getSubjectX500Principal().toString());
				X500Principal issuerPrincipal=certificate.getIssuerX500Principal();
				if (issuerPrincipal!=null)
				{
					currentAccessLevel=Math.max(getAccessLevel(issuerPrincipal,certificate),currentAccessLevel);
				}
			}
		}
		return currentAccessLevel;
	}


	private int getAccessLevel(X500Principal issuerPrincipal, X509Certificate certificate) 
	{
		int accessLevel=0;
		if (level2.contains(issuerPrincipal))
		{
			accessLevel=2;
		}else if (level1.contains(issuerPrincipal)) //}else if (issuerPrincipal.equals(new X500Principal("CN=Friendly CA, OU=FE, O=Friend")))
		{
			accessLevel=1;
		}
		return accessLevel;
	}

	/**
	 * Gets the qos token list.
	 *
	 * @param msgContextMap the msg context map
	 * @return the qo s token list
	 */
	protected List<QoSPolicyToken<?,?>> getQoSTokenList(MDPWSMessageContextMap msgContextMap) {
		ArrayList<QoSPolicyToken<?,?>> relevantTokenList=new ArrayList<QoSPolicyToken<?,?>>();

		if (msgContextMap!=null)
		{
			QoSMessageContext qosMsgContext= (QoSMessageContext) msgContextMap.get(QoSMessageContext.class);
			org.ws4d.java.structures.Iterator policyToken=qosMsgContext.getQoSPolicyToken();
			while(policyToken.hasNext())
			{
				QoSPolicyToken<?,?> token=(QoSPolicyToken<?,?>)policyToken.next();
				relevantTokenList.add(token);
			}
		}

		return relevantTokenList;
	}


}
