/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.xml.namespace.QName;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.common.model.AbstractAlertDescriptor;
import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.ContainmentTreeEntry;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDSState;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.PatientContextState;
import com.draeger.medical.biceps.common.model.SCODescriptor;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.common.model.VMDDescriptor;
import com.draeger.medical.biceps.device.mdib.HandleGenerator;
import com.draeger.medical.biceps.device.mdib.HandleGeneratorFactory;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBaseManager;
import com.draeger.medical.biceps.device.mdib.MemoryMedicalDeviceInformationBase;
import com.draeger.medical.biceps.mdib.impl.MdibRepresentation;
import com.draeger.medical.biceps.mdib.impl.MdibRepresentation.DescriptorWithParent;
import com.draeger.medical.biceps.mdib.impl.MdibRepresentation.VmsWithParent;
import com.draeger.medical.common.utils.XMLAnnotationUtil;


public class DefaultMedicalDeviceInformationBase implements MedicalDeviceInformationBase, MemoryMedicalDeviceInformationBase
{

	private final ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
	private MDDescription mdDescription;
	private MDState mdState=new MDState();
	private MDState mdPatDemographicsState=new MDState();
	//	private BigInteger mdibStateVersion=BigInteger.ZERO;
	private ReadWriteLock mdibLock = new ReentrantReadWriteLock();
	private DefaultMedicalDeviceInformationBaseManager manager = new DefaultMedicalDeviceInformationBaseManager(mdibLock, this);

	private HandleGenerator handleGenerator = HandleGeneratorFactory.getInstance().getHandleGenerator();

	private final boolean useSharedMemory = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultMedicalDeviceInformationBase.useSharedMemory", "false"));
	//	private final boolean autoIncrementStateVersion= Boolean.parseBoolean(System.getProperty("BICEPS.DefaultMedicalDeviceInformationBase.autoIncrementStateVersion", "false"));;
	private MdibRepresentation mdibStructure;

	public DefaultMedicalDeviceInformationBase()
	{
		// void
	}


	@Override
	public ReadWriteLock getMdibLock()
	{
		return mdibLock;
	}

	@Override
	public void create(MDDescription mdib)
	{
		createWithExistingLock(mdib, null);
	}

	@Override
	public void create(MDDescription mdib, MDState state)
	{
		createWithExistingLock(mdib,state, null);
	}

	@Override
	public void createWithExistingLock(MDDescription mdib, ReadWriteLock lock)
	{
		createWithExistingLock(mdib,null, lock);
	}

	@Override
	public void createWithExistingLock(MDDescription mdib, MDState state, ReadWriteLock lock)
	{

		if (lock != null)
		{
			ReadWriteLock oldLock = mdibLock;
			try
			{
				oldLock.writeLock().lock();
				mdibLock = lock;
				manager.setMdibLock(mdibLock);
			}
			finally
			{
				oldLock.writeLock().unlock();
			}
		}

		Lock writeLock = mdibLock.writeLock();
		try
		{
			writeLock.lock();

			updateMDIBDescription(mdib);
			createMDIBStructure(mdib);
			createInitialStates(this.mdDescription, state);
			manager.resetMDIBVersion();
		}
		finally
		{
			writeLock.unlock();
		}
	}
	
	private void createMDIBStructure(MDDescription mdib) {
		if (mdib!=null)
		{
			this.mdibStructure=new MdibRepresentation(mdib, null);
		}
	}

	private void updateMDIBDescription(MDDescription mdib) {
		Lock writeLock = mdibLock.writeLock();
		try
		{
			writeLock.lock();

			this.mdDescription = prepareDataModel(mdib);
			this.mdDescription.setDescriptorVersion(BigInteger.ZERO);

			fillHandleMaps(this.mdDescription);
			updateHandles(this.mdDescription);
		}
		finally
		{
			writeLock.unlock();
		}

	}

	private void createInitialStates(MDDescription description, MDState state) 
	{
		//		MDState mdStates=state;
		//		if (mdStates==null)
		//			mdStates=new MDState();

		this.mdState=new MDState();
		this.mdState.setStateVersion(BigInteger.ZERO);
		fillStatesMaps(state);

	}





	@Override
	public boolean isUseSharedMemoryWithDevice()
	{
		return useSharedMemory;
	}

	@Override
	public MedicalDeviceInformationBaseManager getManager()
	{
		return manager;
	}

	@Override
	public MDDescription getDeviceDescriptions()
	{
		return this.mdDescription;
	}

	@Override
	public Collection<? extends MetricDescriptor> getMetricDescriptors(Collection<String> handles)
	{
		Collection<MetricDescriptor> retVal = new ArrayList<MetricDescriptor>();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			if (handles == null || handles.isEmpty())
			{
				retVal = metricHandles.values();
			}
			else
			{
				for (String handle : handles)
				{
					Collection<MetricDescriptor> metrics = findMetricDescriptorsForHandle(handle);
					if (metrics != null) retVal.addAll(metrics);
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;
	}

	@Override
	public Collection<? extends AlertSystemDescriptor> getAlertSystemDescriptions(Collection<String> handles)
	{
		Collection<AlertSystemDescriptor> retVal = new ArrayList<AlertSystemDescriptor>();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			if (handles == null || handles.isEmpty())
			{
				retVal = alertSystemDescriptorHandles.values();
			}
			else
			{
				for (String handle : handles)
				{
					Collection<AlertSystemDescriptor> alerts = findAlertSystemDescriptorsForHandle(handle);
					if (alerts != null) retVal.addAll(alerts);
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;
	}

	@Override
	public AlertSystemDescriptor getAlertSystemDescriptor(String handle)
	{
		Collection<AlertSystemDescriptor> alerts = findAlertSystemDescriptorsForHandle(handle);

		if (alerts != null && alerts.size() == 1)
		{
			return alerts.iterator().next();
		}

		return null;
	}



	@Override
	public AlertConditionDescriptor getAlertConditionDescriptor(
			String handle) {
		AlertConditionDescriptor retVal=null;
		if (handle!=null)
			retVal=alertConditionDescriptorHandles.get(handle);

		return retVal;
	}


	@Override
	public AlertSignalDescriptor getAlertSignalDescriptor(String handle) {
		AlertSignalDescriptor retVal=null;
		if (handle!=null)
			retVal=alertSignalDescriptorHandles.get(handle);

		return retVal;
	}


	//	public Collection<? extends PatientAssociationDescriptor> getPatientAssociationDescriptors(Collection<String> handles)
	//	{
	//		Collection<PatientAssociationDescriptor> retVal = new ArrayList<PatientAssociationDescriptor>();
	//		Lock readLock = mdibLock.readLock();
	//		try
	//		{
	//			readLock.lock();
	//			if (handles == null || handles.isEmpty())
	//			{
	//				Collection<PatientAssociationDescriptor> allPDs = patientAssociationHandles.values();
	//				if (allPDs != null) retVal.addAll(allPDs);
	//			}
	//			else
	//			{
	//				for (String handle : handles)
	//				{
	//					Collection<PatientAssociationDescriptor> pds = findPatientContextStateForHandle(handle);
	//					if (pds != null) retVal.addAll(pds);
	//				}
	//			}
	//
	//		}
	//		finally
	//		{
	//			readLock.unlock();
	//		}
	//
	//		return retVal;
	//	}

	@Override
	public Collection<? extends MDSDescriptor> getMDSDescriptions(Collection<String> handles, boolean onlyNonComposite)
	{
		Collection<MDSDescriptor> retVal = new ArrayList<MDSDescriptor>();
		Lock readLock = mdibLock.readLock();
		try
		{

			HashMap<String, MDSDescriptor> source = null;
			if (onlyNonComposite)
				source = nonCompositeMDSHandles;
			else
				source = mdsHandles;

			readLock.lock();

			if (handles == null || handles.isEmpty())
			{
				Collection<MDSDescriptor> allMDS = source.values();
				if (allMDS != null) retVal.addAll(allMDS);

			}
			else
			{
				for (String handle : handles)
				{
					MDSDescriptor mds = source.get(handle);
					if (mds != null) retVal.add(mds);
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;

	}

	private OperationDescriptor getOperationDescriptor(String operationHandle, boolean secureAccess)
	{
		OperationDescriptor retVal = null;
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			OperationDescriptor origOperation = operationHandles.get(operationHandle);
			if (secureAccess)
				retVal = getOperationSecure(origOperation);
			else
				retVal = origOperation;

		}
		catch (Exception e)
		{
			Log.warn(e);
		}
		finally
		{
			readLock.unlock();
		}
		return retVal;
	}

	@Override
	public OperationDescriptor getOperationDescriptor(String operationHandle)
	{
		return getOperationDescriptor(operationHandle, true);
	}

	@Override
	public Collection<? extends OperationDescriptor> getOperationForOperationTarget(String handle)
	{
		return findOperationForTarget(handle);
	}

	//	public Collection<? extends VMO> getVMODescriptions(Set<String> handles)
	//	{
	//
	//		Collection<VMO> retVal = new HashSet<VMO>();
	//		Lock readLock = mdibLock.readLock();
	//		try
	//		{
	//			readLock.lock();
	//			for (String vmoHandle : handles)
	//			{
	//				Object o = allDescriptorHandles.get(vmoHandle);
	//				if (o instanceof VMO) retVal.add((VMO) o);
	//			}
	//		}
	//		finally
	//		{
	//			readLock.unlock();
	//		}
	//		return retVal;
	//	}

	@Override
	public String getMDSDescriptionHandleForOperation(String operationHandle)
	{
		String retVal = null;
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			OperationDescriptor op = operationHandles.get(operationHandle);
			MDSDescriptor mds = operationMDSMap.get(op);
			if (mds != null)
			{
				retVal = mds.getHandle();
			}
		}
		finally
		{
			readLock.unlock();
		}
		return retVal;
	}

	@Override
	public MDSDescriptor getMDSForDescriptor(String vmoHandle)
	{
		MDSDescriptor mds = null;
		Object vmoO = allDescriptorHandles.get(vmoHandle);

		//		if (vmoO instanceof VMO)
		//		{
		//			mds= vmoMDSMap.get(vmoO);
		//		}
		//		else if (vmoO != null)
		if (vmoO != null)
		{
			mds = descriptorMDSMap.get(vmoO);
		}
		return mds;
	}

	//	public MDSDescriptor getMDSForPatient(String patientHandle)
	//	{
	//		MDSDescriptor mds = null;
	//		Object vmoO = allDescriptorHandles.get(patientHandle);
	//		if (vmoO != null)
	//		{
	//			mds = patientMDSMap.get(vmoO);
	//		}
	//		return mds;
	//	}

	@Override
	public String getHandle(Object o)
	{
		String handle = null;

		if (o instanceof Descriptor)
		{
			Descriptor d=(Descriptor)o;
			handle=d.getHandle();
		}else if (o instanceof State)
		{
			State s=(State)o;
			handle=s.getHandle();
		}

		if (handle == null)
		{
			noHandleObjects.add(o);
		}

		return handle;
	}


	@Override
	public Collection<? extends MDSState> getMDSStates(
			Collection<String> handles, boolean onlyNonComposite) {
		//TODO SSch Refactoring ?
		return null;
	}

	//	public Collection<? extends VMOState> getVMOStates(Set<String> handles) {
	//		//TODO SSch Refactoring ?
	//		return null;
	//	}

	@Override
	public Collection<? extends AbstractAlertState> getAlertStates(
			Collection<String> handles) {
		Collection<AbstractAlertState> retVal = new ArrayList<AbstractAlertState>();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			if (handles == null || handles.isEmpty())
			{
				Collection<AbstractAlertState> allStates = alertStateHandles.values();
				if (allStates!=null)
					retVal.addAll(allStates);
			}
			else
			{
				for (String handle : handles)
				{
					Collection<AbstractAlertState> alertStates = findAlertStatesForHandle(handle);
					if (alertStates != null)
					{
						retVal.addAll(alertStates);
					}
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;
	}

	@Override
	public Collection<? extends AbstractAlertDescriptor> getAlertDescriptors(
			Collection<String> handles) {
		Collection<AbstractAlertDescriptor> retVal = new ArrayList<AbstractAlertDescriptor>();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			if (handles == null || handles.isEmpty())
			{
				Collection<AlertSystemDescriptor> allStates = alertSystemDescriptorHandles.values();
				if (allStates!=null)
					retVal.addAll(allStates);
			}
			else
			{
				for (String handle : handles)
				{
					Collection<AlertSystemDescriptor> alertStates = findAlertSystemDescriptorsForHandle(handle);
					if (alertStates != null && !alertStates.isEmpty())
					{
						retVal.addAll(alertStates);
					}else{
						Object handleObject = allDescriptorHandles.get(handle);
						if (handleObject instanceof AbstractAlertDescriptor)
						{
							retVal.add((AbstractAlertDescriptor) handleObject);
						}
					}
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;
	}


	// /////////Lookup Helper

	// ////Descriptor Handles
	private final HashMap<String, Descriptor> allDescriptorHandles = new HashMap<String, Descriptor>();
	private final HashMap<String, MDSDescriptor> mdsHandles = new HashMap<String, MDSDescriptor>();
	private final HashMap<String, MDSDescriptor> nonCompositeMDSHandles = new HashMap<String, MDSDescriptor>();
	private final HashMap<String, VMDDescriptor> vmdHandles = new HashMap<String, VMDDescriptor>();
	private final HashMap<String, AlertSystemDescriptor> alertSystemDescriptorHandles = new HashMap<String, AlertSystemDescriptor>();
	private final HashMap<String, AlertConditionDescriptor> alertConditionDescriptorHandles = new HashMap<String, AlertConditionDescriptor>();
	private final HashMap<String, AlertSignalDescriptor> alertSignalDescriptorHandles = new HashMap<String, AlertSignalDescriptor>();
	private final HashMap<String, ChannelDescriptor> channelHandles = new HashMap<String, ChannelDescriptor>();
	private final HashMap<String, MetricDescriptor> metricHandles = new HashMap<String, MetricDescriptor>();
	private final HashMap<String, OperationDescriptor> operationHandles = new HashMap<String, OperationDescriptor>();
	private final HashMap<String, SystemContext> systemContextHandles = new HashMap<String, SystemContext>();
	private final HashMap<String, AbstractContextDescriptor> contextHandles = new HashMap<String, AbstractContextDescriptor>();


	//Descriptors <-> States
	private final HashMap<String, ArrayList<State>> descriptor2StatesMap=new HashMap<String, ArrayList<State>>();
	private final HashMap<String, Descriptor> states2DescriptorMap=new HashMap<String, Descriptor>();

	//States
	private final HashMap<String, State> allStatesHandle = new HashMap<String, State>();
	private final HashMap<String, AbstractAlertState> alertStateHandles = new HashMap<String, AbstractAlertState>();
	private final HashMap<String, AbstractAlertDescriptor> alertDescriptorHandles = new HashMap<String, AbstractAlertDescriptor>();
	private final HashMap<String, AbstractMetricState> metricStateHandles = new HashMap<String, AbstractMetricState>();
	private final HashMap<String, OperationState> operationStateHandles = new HashMap<String, OperationState>();
//	private final HashMap<String, ContextAssociationStateValue> contextStateHandles = new HashMap<String, ContextAssociationStateValue>();
	private final HashMap<String, AbstractIdentifiableContextState> identifiableContextHandles = new HashMap<String, AbstractIdentifiableContextState>();


	//No handles available
	private final ArrayList<Object> noHandleObjects = new ArrayList<Object>();

	// ////Quick access
	private final HashMap<MDSDescriptor, Collection<VMDDescriptor>> mdsVMDMap = new HashMap<MDSDescriptor, Collection<VMDDescriptor>>();
	//	private final HashMap<VMO, MDSDescriptor> vmoMDSMap = new HashMap<VMO, MDSDescriptor>();
	private final HashMap<Descriptor, MDSDescriptor> descriptorMDSMap = new HashMap<Descriptor, MDSDescriptor>();
	//	private final HashMap<PatientAssociationDescriptor, MDSDescriptor> patientMDSMap = new HashMap<PatientAssociationDescriptor, MDSDescriptor>();
	private final HashMap<OperationDescriptor, MDSDescriptor> operationMDSMap = new HashMap<OperationDescriptor, MDSDescriptor>();
	private final HashMap<MDSDescriptor, ArrayList<AbstractContextDescriptor>> mdsContextDescriptorMap = new HashMap<MDSDescriptor, ArrayList<AbstractContextDescriptor>>();



	private Collection<? extends OperationDescriptor> findOperationForTarget(String handle)
	{
		ArrayList<OperationDescriptor> retVal = new ArrayList<OperationDescriptor>();
		try
		{
			mdibLock.readLock().lock();
			Collection<OperationDescriptor> operations = operationHandles.values();
			if (handle != null)
			{
				for (OperationDescriptor operation : operations)
				{
					if (handle.equals(operation.getOperationTarget()))
					{
						retVal.add(operation);
					}
				}
			}
			else if (operations != null)
			{
				retVal.addAll(operations);
			}
		}
		finally
		{
			mdibLock.readLock().unlock();
		}

		return retVal;
	}

	private MDDescription prepareDataModel(MDDescription mdibOrig)
	{
		MDDescription dataModel = mdibOrig;
		ParameterValueObjectUtil util=null;
		if (!isUseSharedMemoryWithDevice())
		{
			try
			{
				util = pool.borrowObject();
				dataModel = util.deepcopy(dataModel);
			}
			catch (Exception e)
			{
				Log.warn(e);
			}finally{
				if (util!=null){
					try {
						pool.returnObject(util);
					} catch (Exception e) 
					{
						Log.warn(e);
					}
				}
			}
		}
		return dataModel;
	}

	private void fillHandleMaps(MDDescription mdib)
	{
		if (mdib!=null)
		{
			List<MDSDescriptor> mdsList = mdib.getMDSDescriptors();
			for (MDSDescriptor mds : mdsList)
			{
				fillMDSMaps(mds);
			}
		}
	}

	private <H extends Descriptor> void addToDescriptorHandleMap(HashMap<String, H> map, H object)
	{
		String handle = getHandle(object);
		if (handle != null)
		{
			map.put(handle, object);
			assignDescriptorVersionInfo(handle, object);
			allDescriptorHandles.put(handle, object);

		}
	}

	private <H extends Descriptor> void assignDescriptorVersionInfo(String handle, H object) {
		if (object!=null)
		{
			Descriptor oldDescriptor = allDescriptorHandles.get(handle);
			BigInteger descVersion=BigInteger.ZERO;
			if (oldDescriptor!=null && object.getDescriptorVersion()==null)
			{
				descVersion=oldDescriptor.getDescriptorVersion();
				if (descVersion!=null)
				{
					descVersion=descVersion.add(BigInteger.ONE);
				}
			}

			if (object.getDescriptorVersion()==null)
				object.setDescriptorVersion(descVersion);

			//			this.mdibStateVersion=this.mdibStateVersion.add(BigInteger.ONE);
			manager.incrementMDIBVersion(object);
		}
	}


	private void fillMDSMaps(MDSDescriptor mds)
	{
		if (mds != null)
		{
			addToDescriptorHandleMap(mdsHandles, mds);

			if (mds instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor hMDS = ((HydraMDSDescriptor) mds);
				addToDescriptorHandleMap(nonCompositeMDSHandles, hMDS);

				fillAlertSystemDescriptorMap(hMDS.getAlertSystem(), mds);

				fillOperationMap(hMDS.getSCO(), mds);

				List<VMDDescriptor> vmds = hMDS.getVMDs();
				for (VMDDescriptor vmd : vmds)
				{
					fillVMDMap(vmd, mds);
				}
			}
			else
			{
				// TODO SSch Log
			}
			fillSystemContextMap(mds);
		}

	}

	private void fillSystemContextMap(MDSDescriptor mds)
	{
		if (mds!=null && mds.getContext()!=null)
		{
			SystemContext context = mds.getContext();

			if (context!=null)
			{
				//TODO SSch Check if needed
				if (context.getHandle()==null)context.setHandle("sysctxt");

				addToDescriptorHandleMap(systemContextHandles, context);
				descriptorMDSMap.put(context, mds);

				fillContextItemsMap(mds, mds, context);
			}
		}
	}

	private void fillContextItemsMap(MDSDescriptor key, MDSDescriptor mds, SystemContext systemContext)
	{
		if (systemContext!=null)
		{
			addContextDescriptorElement(key, mds, systemContext.getPatientContext());
			addContextDescriptorElement(key, mds, systemContext.getLocationContext());
			addContextDescriptorElement(key, mds, systemContext.getEnsembleContext());
			addContextDescriptorElement(key, mds, systemContext.getWorkflowContext());
			addContextDescriptorElement(key, mds, systemContext.getOperatorContext());
		}

	}


	private void addContextDescriptorElement(MDSDescriptor key,
			MDSDescriptor mds, AbstractContextDescriptor pd) {
		if (pd!=null)
		{
			addToMapping(key, pd);
			addToMapping(mds, pd);
		}
	}



	private void addToMapping(MDSDescriptor mds, AbstractContextDescriptor pd)
	{
		if (pd != null && mds != null)
		{
			ArrayList<AbstractContextDescriptor> pds = mdsContextDescriptorMap.get(mds);
			if (pds == null)
			{
				pds = new ArrayList<AbstractContextDescriptor>();
			}
			pds.add(pd);
			mdsContextDescriptorMap.put(mds, pds);
			//			patientMDSMap.put(pd, mds);
			addToDescriptorHandleMap(contextHandles, pd);
		}
	}

	private void fillVMDMap(VMDDescriptor vmd, MDSDescriptor mds)
	{
		if (vmd != null)
		{
			addToDescriptorHandleMap(vmdHandles, vmd);
			descriptorMDSMap.put(vmd, mds);

			AlertSystemDescriptor alert = vmd.getAlertSystem();
			//for (Alert alert : alerts)
			{
				fillAlertSystemDescriptorMap(alert, mds);
			}

			List<ChannelDescriptor> channels = vmd.getChannels();
			for (ChannelDescriptor channel : channels)
			{
				fillChannelMap(channel, mds);
			}

			if (mds != null)
			{
				Collection<VMDDescriptor> col = mdsVMDMap.get(mds);
				if (col == null) col = new ArrayList<VMDDescriptor>();

				col.add(vmd);
				mdsVMDMap.put(mds, col);
			}
		}
	}

	private void fillOperationMap(SCODescriptor sco, MDSDescriptor mds)
	{
		if (sco != null)
		{
			allDescriptorHandles.put(sco.getHandle(), sco);
			descriptorMDSMap.put(sco, mds);
			List<OperationDescriptor> operations = sco.getOperations();
			for (OperationDescriptor operation : operations)
			{
				addToDescriptorHandleMap(operationHandles, operation);
				operationMDSMap.put(operation, mds);
			}
		}
	}

	void fillAlertSystemDescriptorMap(AlertSystemDescriptor alertSystemDescriptor, MDSDescriptor mds)
	{
		if (alertSystemDescriptor != null)
		{
			addToDescriptorHandleMap(alertSystemDescriptorHandles, alertSystemDescriptor);
			List<AlertConditionDescriptor> conditions = alertSystemDescriptor.getAlertConditions();
			if (conditions!=null){
				for (AlertConditionDescriptor alertConditionDescriptor : conditions) 
				{
					fillAlertConditionDescriptorMap(alertConditionDescriptor, mds);
				}
			}
			List<AlertSignalDescriptor> signal = alertSystemDescriptor.getAlertSignals();
			if (signal!=null){
				for (AlertSignalDescriptor alertSignalDescriptor : signal) 
				{
					fillAlertSignalDescriptorMap(alertSignalDescriptor, mds);
				}
			}
			descriptorMDSMap.put(alertSystemDescriptor, mds);
			alertDescriptorHandles.put(alertSystemDescriptor.getHandle(), alertSystemDescriptor);
		}
	}

	private void fillAlertConditionDescriptorMap(AlertConditionDescriptor alertConditionDescriptor, MDSDescriptor mds)
	{
		if (alertConditionDescriptor != null)
		{
			addToDescriptorHandleMap(alertConditionDescriptorHandles, alertConditionDescriptor);
			descriptorMDSMap.put(alertConditionDescriptor, mds);
			alertDescriptorHandles.put(alertConditionDescriptor.getHandle(), alertConditionDescriptor);
		}
	}

	private void fillAlertSignalDescriptorMap(AlertSignalDescriptor alertSignalDescriptor, MDSDescriptor mds)
	{
		if (alertSignalDescriptor != null)
		{
			addToDescriptorHandleMap(alertSignalDescriptorHandles, alertSignalDescriptor);
			descriptorMDSMap.put(alertSignalDescriptor, mds);
			alertDescriptorHandles.put(alertSignalDescriptor.getHandle(), alertSignalDescriptor);
		}
	}

	private void fillChannelMap(ChannelDescriptor channel, MDSDescriptor mds)
	{
		if (channel != null)
		{
			addToDescriptorHandleMap(channelHandles, channel);
			descriptorMDSMap.put(channel, mds);

			AlertSystemDescriptor alertSystem = channel.getAlertSystem();
			//            for (AlertSystemDescriptor alert : alerts)
			{
				fillAlertSystemDescriptorMap(alertSystem, mds);
			}

			//TODO SSch save AlertCondionDesc...

			List<MetricDescriptor> metrics = channel.getMetrics();
			for (MetricDescriptor metric : metrics)
			{
				fillMetricMap(metric, mds);
			}
		}

	}

	void fillMetricMap(MetricDescriptor metric, MDSDescriptor mds)
	{
		if (metric != null)
		{
			addToDescriptorHandleMap(metricHandles, metric);
			descriptorMDSMap.put(metric, mds);
		}
	}

	private String generateUniqueHandle()
	{
		String uniqueHandleStr = null;
		do
		{
			uniqueHandleStr = handleGenerator.nextHandle(null);
		}
		while (allDescriptorHandles.containsKey(uniqueHandleStr));

		return uniqueHandleStr;
	}

	private String setHandle(Object o, String handle)
	{
		String oldHandle = null;
		if (o instanceof Descriptor)
		{
			oldHandle = ((Descriptor) o).getHandle();
			((Descriptor) o).setHandle(handle);
		}else if (o instanceof State)
		{
			oldHandle = ((State) o).getHandle();
			((State) o).setHandle(handle);
		}
		return oldHandle;
	}

	private Collection<MetricDescriptor> findMetricDescriptorsForHandle(String handle)
	{
		Collection<MetricDescriptor> retVal = new ArrayList<MetricDescriptor>();

		Object handleObject = allDescriptorHandles.get(handle);

		if (handleObject instanceof MetricDescriptor)
		{
			retVal.add((MetricDescriptor) handleObject);
		}
		else if (handleObject instanceof MDSDescriptor)
		{
			Collection<VMDDescriptor> vmds = mdsVMDMap.get(handleObject);
			for (VMDDescriptor vmd : vmds)
			{
				retVal.addAll(findMetricDescriptorsForHandle(vmd.getHandle()));
			}
		}
		else if (handleObject instanceof VMDDescriptor)
		{
			List<ChannelDescriptor> channels = ((VMDDescriptor) handleObject).getChannels();
			for (ChannelDescriptor channel : channels)
			{
				retVal.addAll(findMetricDescriptorsForHandle(channel.getHandle()));
			}
		}
		else if (handleObject instanceof ChannelDescriptor)
		{
			List<MetricDescriptor> metrics = ((ChannelDescriptor) handleObject).getMetrics();
			retVal.addAll(metrics);
		}

		return retVal;
	}


	private Collection<AbstractAlertState> findAlertStatesForHandle(String handle)
	{
		Collection<AbstractAlertState> retVal = new ArrayList<AbstractAlertState>();


		//		AbstractAlertState s= alertStateHandles.get(alertStateHandles.get(handle));
		AbstractAlertState s= alertStateHandles.get(handle);
		if (s!=null)
		{
			retVal.add( s);
		}else{

			//Seems not to be a handle for an alert state, find alert states for related objects 
			Object handleObject = allDescriptorHandles.get(handle);

			if (handleObject instanceof AlertSystemDescriptor)
			{
				AlertSystemDescriptor asd=(AlertSystemDescriptor)handleObject;
				String descriptorHandle=asd.getHandle();
				if (descriptor2StatesMap.containsKey(descriptorHandle))
				{
					ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(descriptorHandle);
					if (descriptorAssociatedStates!=null)
					{
						for (State state : descriptorAssociatedStates) {
							if (state instanceof AbstractAlertState)
							{
								retVal.add((AbstractAlertState)state);
							}
						}
					}

				}

				List<AlertConditionDescriptor> conditions = asd.getAlertConditions();
				if (conditions!=null)
				{
					for (AlertConditionDescriptor alertConditionDescriptor : conditions) {
						Collection<AbstractAlertState> alertStatesForCondition = findAlertStatesForHandle(alertConditionDescriptor.getHandle());
						if (alertStatesForCondition!=null)
						{
							retVal.addAll(alertStatesForCondition);
						}
					}
				}

				List<AlertSignalDescriptor> signals = asd.getAlertSignals();
				if (signals!=null)
				{
					for (AlertSignalDescriptor alertSignalDescriptor : signals) {
						Collection<AbstractAlertState> alertStatesForCondition = findAlertStatesForHandle(alertSignalDescriptor.getHandle());
						if (alertStatesForCondition!=null)
						{
							retVal.addAll(alertStatesForCondition);
						}
					}
				}
			}else if (handleObject instanceof AlertConditionDescriptor || handleObject instanceof AlertSignalDescriptor){
				Descriptor alertDescriptor=(Descriptor)handleObject;
				String descriptorHandle=alertDescriptor.getHandle();
				if (descriptor2StatesMap.containsKey(descriptorHandle))
				{
					//					State state= descriptor2StatesMap.get(descriptorHandle);
					//					if (state instanceof AbstractAlertState)
					//					{
					//						retVal.add((AbstractAlertState)state);
					//					}
					ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(descriptorHandle);
					if (descriptorAssociatedStates!=null)
					{
						for (State state : descriptorAssociatedStates) {
							if (state instanceof AbstractAlertState)
							{
								retVal.add((AbstractAlertState)state);
							}
						}
					}
				}
			}else if (handleObject instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds=(HydraMDSDescriptor)handleObject;
				AlertSystemDescriptor asd= mds.getAlertSystem();
				Collection<AbstractAlertState> alertStatesForCondition=null;

				if (asd!=null)
				{
					alertStatesForCondition = findAlertStatesForHandle(asd.getHandle());
					if (alertStatesForCondition!=null)
					{
						retVal.addAll(alertStatesForCondition);
					}
				}
				List<VMDDescriptor> vmds = mds.getVMDs();
				if (vmds!=null)
				{
					for (VMDDescriptor vmdDescriptor : vmds) {
						alertStatesForCondition = findAlertStatesForHandle(vmdDescriptor.getHandle());
						if (alertStatesForCondition!=null)
						{
							retVal.addAll(alertStatesForCondition);
						}
					}

				}
			}
			else if (handleObject instanceof VMDDescriptor)
			{
				VMDDescriptor vmd=(VMDDescriptor)handleObject;
				Collection<AbstractAlertState> alertStatesForCondition=null;
				AlertSystemDescriptor asd= vmd.getAlertSystem();
				if (asd!=null){
					alertStatesForCondition = findAlertStatesForHandle(asd.getHandle());
					if (alertStatesForCondition!=null)
					{
						retVal.addAll(alertStatesForCondition);
					}
				}
				List<ChannelDescriptor> channels = vmd.getChannels();
				if (channels!=null)
				{
					for (ChannelDescriptor channelDescriptor : channels) 
					{
						alertStatesForCondition = findAlertStatesForHandle(channelDescriptor.getHandle());
						if (alertStatesForCondition!=null)
						{
							retVal.addAll(alertStatesForCondition);
						}
					}
				}
			}
			else if (handleObject instanceof ChannelDescriptor)
			{
				ChannelDescriptor channel=(ChannelDescriptor)handleObject;
				AlertSystemDescriptor asd= channel.getAlertSystem();
				if (asd!=null)
				{
					Collection<AbstractAlertState> alertStatesForCondition = findAlertStatesForHandle(asd.getHandle());
					if (alertStatesForCondition!=null)
					{
						retVal.addAll(alertStatesForCondition);
					}
				}
			}
		}

		return retVal;
	}


	private Collection<AbstractMetricState> findMetricStatesForHandle(String handle)
	{
		Collection<AbstractMetricState> retVal = new ArrayList<AbstractMetricState>();


		AbstractMetricState s= metricStateHandles.get(handle);
		if (s!=null)
		{
			retVal.add( s);
		}else{

			//Seems to be a handle for an alert state, find alert states for related objects 
			Object handleObject = allDescriptorHandles.get(handle);

			if (handleObject instanceof MetricDescriptor)
			{
				MetricDescriptor asd=(MetricDescriptor)handleObject;
				String descriptorHandle=asd.getHandle();
				if (descriptor2StatesMap.containsKey(descriptorHandle))
				{
					//					State state= descriptor2StatesMap.get(descriptorHandle);
					//					if (state instanceof AbstractMetricState)
					//					{
					//						retVal.add((AbstractMetricState)state);
					//					}
					ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(descriptorHandle);
					if (descriptorAssociatedStates!=null)
					{
						for (State state : descriptorAssociatedStates) {
							if (state instanceof AbstractMetricState)
							{
								retVal.add((AbstractMetricState)state);
							}
						}
					}
				}
			}else if (handleObject instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds=(HydraMDSDescriptor)handleObject;
				Collection<AbstractMetricState> metricStatesForVMD=null;
				List<VMDDescriptor> vmds = mds.getVMDs();
				if (vmds!=null)
				{
					for (VMDDescriptor vmdDescriptor : vmds) {
						metricStatesForVMD = findMetricStatesForHandle(vmdDescriptor.getHandle());
						if (metricStatesForVMD!=null)
						{
							retVal.addAll(metricStatesForVMD);
						}
					}
				}
			}
			else if (handleObject instanceof VMDDescriptor)
			{
				VMDDescriptor vmd=(VMDDescriptor)handleObject;
				Collection<AbstractMetricState> metricStatesForChannel=null;
				List<ChannelDescriptor> channels = vmd.getChannels();
				if (channels!=null)
				{
					for (ChannelDescriptor channelDescriptor : channels) 
					{
						metricStatesForChannel = findMetricStatesForHandle(channelDescriptor.getHandle());
						if (metricStatesForChannel!=null)
						{
							retVal.addAll(metricStatesForChannel);
						}
					}
				}
			}
			else if (handleObject instanceof ChannelDescriptor)
			{
				ChannelDescriptor channel=(ChannelDescriptor)handleObject;
				Collection<AbstractMetricState> metricStates=null;
				List<MetricDescriptor> metrics = channel.getMetrics();
				if (metrics!=null)
				{
					for (MetricDescriptor metricDescriptor : metrics) 
					{
						metricStates = findMetricStatesForHandle(metricDescriptor.getHandle());
						if (metricStates!=null)
						{
							retVal.addAll(metricStates);
						}
					}
				}
			}
		}

		return retVal;
	}

	private Collection<AlertSystemDescriptor> findAlertSystemDescriptorsForHandle(String handle)
	{
		Collection<AlertSystemDescriptor> retVal = new ArrayList<AlertSystemDescriptor>();

		Object handleObject = allDescriptorHandles.get(handle);

		if (handleObject instanceof AlertSystemDescriptor)
		{
			retVal.add((AlertSystemDescriptor) handleObject);
		}
		else if (handleObject instanceof MetricDescriptor)
		{

			// TODO SSch find supervising alerts???
			// Metric metric=(Metric)handleObject;
		}
		else if (handleObject instanceof MDSDescriptor)
		{
			MDSDescriptor mds = (MDSDescriptor) handleObject;


			Collection<VMDDescriptor> vmds = mdsVMDMap.get(mds);
			for (VMDDescriptor vmd : vmds)
			{
				retVal.addAll(findAlertSystemDescriptorsForHandle(vmd.getHandle()));
			}
		}
		else if (handleObject instanceof VMDDescriptor)
		{
			VMDDescriptor vmd = (VMDDescriptor) handleObject;
			retVal.add(vmd.getAlertSystem());

			List<ChannelDescriptor> channels = vmd.getChannels();
			for (ChannelDescriptor channel : channels)
			{
				retVal.addAll(findAlertSystemDescriptorsForHandle(channel.getHandle()));
			}
		}
		else if (handleObject instanceof ChannelDescriptor)
		{
			ChannelDescriptor channel = (ChannelDescriptor) handleObject;
			AlertSystemDescriptor alertSystem = channel.getAlertSystem();
			retVal.add(alertSystem);
		}

		return retVal;
	}

	//	private Collection<PatientAssociationDescriptor> findPatientContextStateForHandle(String handle)
	//	{
	//		Collection<PatientAssociationDescriptor> retVal = null;
	//		Object handleObject = allDescriptorHandles.get(handle);
	//		if (handleObject instanceof MDSDescriptor)
	//		{
	//			retVal = mdsContextDescriptorMap.get(handleObject);
	//		}
	//		else if (handleObject instanceof PatientAssociationDescriptor)
	//		{
	//			retVal = new ArrayList<PatientAssociationDescriptor>();
	//			retVal.add((PatientAssociationDescriptor) handleObject);
	//		}
	//
	//		return retVal;
	//	}

	private void updateHandles(MDDescription mdib)
	{
		// TODO SSch check invalid references
		for (Object noHandleObject : noHandleObjects)
		{
			setHandle(noHandleObject, generateUniqueHandle());
		}
	}

	private OperationDescriptor getOperationSecure(OperationDescriptor origOperation) throws Exception
	{
		ParameterValueObjectUtil util=null;
		OperationDescriptor retVal=null;
		try{
			util= pool.borrowObject();
			retVal=util.deepcopy(origOperation);
		}finally{
			if (util!=null){
				try {
					pool.returnObject(util);
				} catch (Exception e) 
				{
					Log.warn(e);
				}
			}
		}
		return retVal;
	}


	@Override
	public MDState getDeviceStates() 
	{
		MDState retVal=new MDState();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			retVal.getStates().addAll(mdState.getStates());
		}
		finally
		{
			readLock.unlock();
		}
		return retVal;
	}


	@Override
	public Collection<? extends AbstractMetricState> getMetricStates(
			Collection<String> handles) {
		Collection<AbstractMetricState> retVal = new ArrayList<AbstractMetricState>();
		Lock readLock = mdibLock.readLock();
		try
		{
			readLock.lock();
			if (handles == null || handles.isEmpty())
			{
				Collection<AbstractMetricState> allStates = metricStateHandles.values();
				if (allStates!=null)
					retVal.addAll(allStates);
			}
			else
			{
				for (String handle : handles)
				{
					Collection<AbstractMetricState> metricStates = findMetricStatesForHandle(handle);
					if (metricStates != null)
					{
						retVal.addAll(metricStates);
					}
				}
			}

		}
		finally
		{
			readLock.unlock();
		}

		return retVal;
	}



	//	public AbstractAlertState getAlertState(String handle) {
	//		AbstractAlertState retVal=null;
	//		retVal=alertStateHandles.get(handle);
	//		if (retVal==null)
	//		{
	//			//State associatedState=descriptor2StatesMap.get(handle);
	////			if (associatedState instanceof AbstractAlertState)
	////			retVal=(AbstractAlertState) associatedState;
	//			
	//			ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(handle);
	//			for (State state : descriptorAssociatedStates) {
	//				if (state instanceof AbstractMetricState)
	//				{
	//					retVal=(AbstractAlertState)state;
	//				}
	//			}
	//		}
	//		return retVal;
	//	}


	@Override
	public State getState(String stateHandle) 
	{

		State retVal=null;
		retVal=allStatesHandle.get(stateHandle);
		if (retVal==null)
		{
			//			retVal=descriptor2StatesMap.get(handle);

			ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(stateHandle);
			if (descriptorAssociatedStates!=null && !descriptorAssociatedStates.isEmpty())
			{
				retVal=descriptorAssociatedStates.get(0); //get the first one
			}

		}

		return retVal;
	}

	boolean addState(State newState)
	{
		boolean retVal=false;



		if (newState!=null)
		{

			if (!isUseSharedMemoryWithDevice())
			{
				newState = copyState(newState);
			}

			State oldState= allStatesHandle.remove(newState.getHandle());

			//			assignDescriptorVersionInfo(newState, oldState);

			if (oldState!=null && newState.getHandle()!=null)
			{
				Descriptor oldDescriptor=states2DescriptorMap.get(newState.getHandle());
				ArrayList<State> descriptorAssociatedStates = descriptor2StatesMap.get(oldDescriptor.getHandle());
				if (descriptorAssociatedStates!=null)
				{
					ListIterator<State> descriptorAssociatedStatesIterator = descriptorAssociatedStates.listIterator();
					while(descriptorAssociatedStatesIterator.hasNext())
					{
						State state = descriptorAssociatedStatesIterator.next();
						if (newState.getHandle().equals(state.getHandle()))
						{
							descriptorAssociatedStatesIterator.remove();
						}
					}
				}
			}

			if (mdState!=null)
			{
				if (mdState.getStateVersion()!=null)
					mdState.setStateVersion(mdState.getStateVersion().add(BigInteger.ONE));
				else 
					mdState.setStateVersion(BigInteger.ZERO);

				if (newState instanceof PatientContextState)
				{
					if (oldState!=null)
						mdPatDemographicsState.getStates().remove(oldState);

					retVal=mdPatDemographicsState.getStates().add(newState);
				}else
				{
					if (oldState!=null) mdState.getStates().remove(oldState);

					if (!mdState.getStates().contains(newState)) retVal=mdState.getStates().add(newState);
				}
			}

			if (retVal)
			{
				Descriptor d=getDescriptor(newState.getReferencedDescriptor());
				if (d!=null){
					states2DescriptorMap.put(newState.getHandle(), d);
					addToDescriptor2StatesMap(d,newState);
					allStatesHandle.put(newState.getHandle(), newState);
					addToSpecificStatesMap(newState);

					manager.incrementMDIBVersion(newState);
				}
			}
		}


		return retVal;
	}

	@Override
	public BigInteger getMdibStateVersion() {
		return BigInteger.valueOf(manager.getCurrentSequenceId());
	}


	private void addToDescriptor2StatesMap(Descriptor d, State newState) {
		//		descriptor2StatesMap.put(d.getHandle(), newState);

		ArrayList<State> descriptorAssociatedStates = descriptor2StatesMap.get(d.getHandle());
		if (descriptorAssociatedStates==null)
		{
			descriptorAssociatedStates=new ArrayList<State>();
			descriptor2StatesMap.put(d.getHandle(), descriptorAssociatedStates);
		}

		descriptorAssociatedStates.add(newState);

	}


	Descriptor copyDescriptor(Descriptor descriptor) {
		return copy(descriptor);
	}

	State copyState(State newState) 
	{
		return copy(newState);
	}

	private final <T> T copy(T o)
	{
		ParameterValueObjectUtil util=null;
		try
		{
			util = pool.borrowObject();
			o = util.deepcopy(o);
		}
		catch (Exception e)
		{
			Log.warn(e);
		}finally{
			if (util!=null){
				try {
					pool.returnObject(util);
				} catch (Exception e) 
				{
					Log.warn(e);
				}
			}
		}
		return o;
	}


	@Override
	public Descriptor getDescriptor(String referencedDescriptor) 
	{
		return allDescriptorHandles.get(referencedDescriptor);
	}
	
	@Override
	public Collection<Descriptor> getAllDescriptors() 
	{
		return allDescriptorHandles.values();
	}

	private void addToSpecificStatesMap(State newState) {
		if (newState instanceof AbstractAlertState){
			alertStateHandles.put(newState.getHandle(), (AbstractAlertState) newState);
		}else if (newState instanceof AbstractMetricState)
		{
			metricStateHandles.put(newState.getHandle(), (AbstractMetricState) newState);
		}else if (newState instanceof OperationState){
			operationStateHandles.put(newState.getHandle(), (OperationState) newState);
		}else if (newState instanceof AbstractIdentifiableContextState)
		{
			identifiableContextHandles.put(newState.getHandle(), (AbstractIdentifiableContextState) newState);
		}
	}

	private void fillStatesMaps( MDState mdStates) 
	{
		if (mdStates!=null)
		{
			List<State> states = mdStates.getStates();
			for (State state : states) {
				manager.assignStateVersionInfo(state,null);

				addState(state);
			}
			//TODO SSch check for duplicate states ????
		}
	}


	public OperationState getOperationalStateForDescriptor(String operationDescriptorHandle) 
	{
		//return (OperationState) descriptor2StatesMap.get(operationHandle);
		OperationState retVal=null;
		if (descriptor2StatesMap.containsKey(operationDescriptorHandle))
		{
			ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(operationDescriptorHandle);
			if (descriptorAssociatedStates!=null)
			{
				for (State state : descriptorAssociatedStates) {
					if (state instanceof OperationState)
					{
						retVal=(OperationState)state;
					}
				}
			}
		}

		return retVal;
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends AbstractIdentifiableContextState> Collection<T> getIdentifiableContextState(List<String> handles, Class<T> contentClass)
	{

		ArrayList<T> retVal=new ArrayList<T>();
		if (handles==null|| handles.isEmpty())
		{
			Collection<AbstractIdentifiableContextState> idContextStates = identifiableContextHandles.values();
			if (idContextStates!=null)
			{
				for (State state : idContextStates) {
					if(contentClass.isAssignableFrom(state.getClass()))
					{
						retVal.add((T) state);
					}
				}
			}
		}else{
			for (String handle : handles) {
				AbstractIdentifiableContextState idContextState= identifiableContextHandles.get(handle);
				if (idContextState !=null){
					if(contentClass.isAssignableFrom(idContextState.getClass()))
					{
						retVal.add((T)idContextState);
					}
				}else{
					//					if (descriptor2StatesMap.containsKey(handle))
					//					{
					//						State state= descriptor2StatesMap.get(handle);
					//						if (state instanceof PatientContextState)
					//						{
					//							retVal.add((PatientContextState)state);
					//						}
					//					}

					if (descriptor2StatesMap.containsKey(handle))
					{
						ArrayList<State> descriptorAssociatedStates= descriptor2StatesMap.get(handle);
						if (descriptorAssociatedStates!=null)
						{
							for (State state : descriptorAssociatedStates) {
								if(contentClass.isAssignableFrom(state.getClass()))
								{
									retVal.add((T)state);
								}
							}
						}
					}
				}
			}

		}


		return retVal;
	}


	@Override
	public Collection<? extends ContextAssociationStateValue> getContextAssociationStateValues(
			List<String> handles) 
			{

		ArrayList<ContextAssociationStateValue> retVal=new ArrayList<ContextAssociationStateValue>();
		if (handles==null|| handles.isEmpty())
		{
			Collection<AbstractIdentifiableContextState> contextStates = identifiableContextHandles.values();
			if (contextStates!=null)
			{
				for (AbstractIdentifiableContextState state : contextStates) {
					if(state !=null && state.getContextAssociation()!=null)
					{
						retVal.add(state.getContextAssociation());
					}
				}
			}
		}else{
			for (String handle : handles) {
				AbstractIdentifiableContextState state= identifiableContextHandles.get(handle);
				if (state !=null && state.getContextAssociation()!=null){
					retVal.add( state.getContextAssociation());
				}else{
					Descriptor descriptor = allDescriptorHandles.get(handle);
					if (descriptor instanceof AbstractContextDescriptor)
					{
						Collection<AbstractIdentifiableContextState> mdContextAssociationStateValues = identifiableContextHandles.values();
						if (mdContextAssociationStateValues!=null)
						{
							for (AbstractIdentifiableContextState mdContextAssociationStateValue : mdContextAssociationStateValues) {
								if(descriptor.getHandle().equals(mdContextAssociationStateValue.getReferencedDescriptor()) && mdContextAssociationStateValue.getContextAssociation()!=null)
								{
									retVal.add(mdContextAssociationStateValue.getContextAssociation());
								}
							}
						}
					}else{
						Set<MDSDescriptor> mdsDescriptors = mdsContextDescriptorMap.keySet();
						if (mdsDescriptors!=null)
						{
							for (MDSDescriptor mdsDescriptor : mdsDescriptors) {
								if (mdsDescriptor.getHandle().equals(handle)){
									ArrayList<AbstractContextDescriptor> arrayList = mdsContextDescriptorMap.get(mdsDescriptor);
									for (AbstractContextDescriptor abstractContextDescriptor : arrayList) {
										Collection<AbstractIdentifiableContextState> mdContextAssociationStateValues = identifiableContextHandles.values();
										if (mdContextAssociationStateValues!=null)
										{
											for (AbstractIdentifiableContextState mdContextAssociationStateValue : mdContextAssociationStateValues) {
												if(mdContextAssociationStateValue!=null 
														&& abstractContextDescriptor.getHandle().equals(mdContextAssociationStateValue.getReferencedDescriptor()))
												{
													if (	mdContextAssociationStateValue.getContextAssociation()!=null 
															&& !retVal.contains(mdContextAssociationStateValue.getContextAssociation()))
														retVal.add(mdContextAssociationStateValue.getContextAssociation());
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}

		return retVal;
			}


	@SuppressWarnings("unchecked")
	<T extends Descriptor>  T getDescriptorForHandle(String descriptorHandle) 
	{
		T retVal=null;
		Object handleObject = allDescriptorHandles.get(descriptorHandle);
		if (handleObject!=null)
		{
			try{
				retVal=(T) handleObject;
			}catch(ClassCastException cce){
				if (Log.isWarn()) Log.warn("Tried to get a Descriptor with wrong type. Handle was "+descriptorHandle+". Object found:"+handleObject+".");
			}
		}

		return retVal;
	}

	@Override
	public ContainmentTreeEntry getContainmentTreeEntry(
			String referencedDescriptor) {
		ContainmentTreeEntry entry=null;
		if (referencedDescriptor!=null)
		{
			DescriptorWithParent descriptorWithParent = this.mdibStructure.getDescriptorWithParent(referencedDescriptor);
			Descriptor entryDescriptor=null;
			Descriptor parentDescriptor=null;
			CodedValue type =null;
			QName typeQName=null;
			if (descriptorWithParent!=null)
			{
				
				entryDescriptor=descriptorWithParent.getVmo();
				
				if (entryDescriptor!=null)
				{
					entry=new ContainmentTreeEntry();
					parentDescriptor = descriptorWithParent.getParent();
					//type= entryDescriptor.getType();
					typeQName=XMLAnnotationUtil.getQNameFromAnnotation(entryDescriptor);
				}
			}else{
				VmsWithParent vmsWithParent = this.mdibStructure.getVmsWithParent(referencedDescriptor);
				if (vmsWithParent!=null)
				{
					entryDescriptor=vmsWithParent.getVms();
					
					if (entryDescriptor!=null)
					{
						entry=new ContainmentTreeEntry();
						parentDescriptor = vmsWithParent.getParent();
						//type= entryDescriptor.getType();
						typeQName=XMLAnnotationUtil.getQNameFromAnnotation(entryDescriptor);
					}
				}
			}
			if (entry!=null)
			{
				entry.setHandleRef(referencedDescriptor);
				if (parentDescriptor!=null){
					entry.setParentHandleRef(parentDescriptor.getHandle());
				}
				entry.setType(type);
				entry.setEntryType(typeQName);
			}
		}
		return entry;
	}


	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase#getContainmentTreeEntries(java.lang.String)
	 */
	@Override
	public ContainmentTree getContainmentTree(
			String referencedDescriptor) {
		
		ContainmentTree retVal=new ContainmentTree();
		
		List<ContainmentTreeEntry> entryList=retVal.getEntries();
		if (referencedDescriptor==null)
		{
			List<MDSDescriptor> mdsDescriptors = this.mdibStructure.getMDSDescriptors();
			if (mdsDescriptors!=null && !mdsDescriptors.isEmpty())
			{
				for (MDSDescriptor mdsDescriptor : mdsDescriptors) {
					VmsWithParent vmsWithParent = this.mdibStructure.getVmsWithParent(mdsDescriptor.getHandle());
					if (vmsWithParent!=null)
					{
						MDSDescriptor parentDescriptor = vmsWithParent.getParent();
						if (parentDescriptor==null)
						{
							ContainmentTreeEntry entry = new ContainmentTreeEntry();
							CodedValue type = mdsDescriptor.getType();
							QName typeQName = XMLAnnotationUtil.getQNameFromAnnotation(mdsDescriptor);

							entry.setHandleRef(mdsDescriptor.getHandle());

							entry.setParentHandleRef(null);							
							entry.setType(type);
							entry.setEntryType(typeQName);
							entry.setChildren(this.mdibStructure.getChildrenCnt(mdsDescriptor.getHandle()));
							entryList.add(entry);
						}
					}
				}
			}
		}else{
			//referenced descriptor != null
			//fill the parent info in the containment tree 
			//as well as the child list if available
			List<DescriptorWithParent> childrenDescriptors = this.mdibStructure.getChildrenDescriptors(referencedDescriptor);
			DescriptorWithParent refDescriptorWithParent = this.mdibStructure.getDescriptorWithParent(referencedDescriptor);
			int childCnt=0;
			if (childrenDescriptors!=null && (childCnt=childrenDescriptors.size())>0)
			{	
				for (DescriptorWithParent descriptorWithParent : childrenDescriptors) {
					Descriptor entryDescriptor = descriptorWithParent.getVmo();
					appendEntry(entryList, descriptorWithParent, entryDescriptor);
				}
			}
			
			if (refDescriptorWithParent!=null)
			{
				Descriptor refDescriptor = refDescriptorWithParent.getVmo();
				QName typeQName = XMLAnnotationUtil.getQNameFromAnnotation(refDescriptor);
				
				retVal.setChildren(childCnt);
				retVal.setHandleRef(refDescriptor.getHandle());
				retVal.setEntryType(typeQName);
				
				Descriptor parentDescriptor = refDescriptorWithParent.getParent();
				if (parentDescriptor!=null)
					retVal.setParentHandleRef(parentDescriptor.getHandle());
			}
			
		}

		return retVal;
	}


	private void appendEntry(List<ContainmentTreeEntry> retVal,
			DescriptorWithParent descriptorWithParent,
			Descriptor entryDescriptor) {
		if (entryDescriptor!=null)
		{
			ContainmentTreeEntry entry = new ContainmentTreeEntry();
			Descriptor parentDescriptor = descriptorWithParent.getParent();
			CodedValue type = entryDescriptor.getType();
			QName typeQName = XMLAnnotationUtil.getQNameFromAnnotation(entryDescriptor);

			entry.setHandleRef(entryDescriptor.getHandle());
			if (parentDescriptor!=null){
				entry.setParentHandleRef(parentDescriptor.getHandle());
			}							
			entry.setType(type);
			entry.setEntryType(typeQName);
			entry.setChildren(this.mdibStructure.getChildrenCnt(entryDescriptor.getHandle()));
			retVal.add(entry);
		}
	}


}
