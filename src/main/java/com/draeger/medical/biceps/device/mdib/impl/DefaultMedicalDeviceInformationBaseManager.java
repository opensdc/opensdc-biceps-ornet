/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.Activate;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.ActivateResponse;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.ContextChangedReportPart;
import com.draeger.medical.biceps.common.model.DescriptionModificationReport;
import com.draeger.medical.biceps.common.model.DescriptionModificationReportPart;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.EpisodicAlertReport;
import com.draeger.medical.biceps.common.model.EpisodicContextChangedReport;
import com.draeger.medical.biceps.common.model.EpisodicMetricReport;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.ObjectCreatedReport;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.OperationInvokedReport;
import com.draeger.medical.biceps.common.model.OperationInvokedReportPart;
import com.draeger.medical.biceps.common.model.OperationState;
import com.draeger.medical.biceps.common.model.OperationalState;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReport;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReportPart;
import com.draeger.medical.biceps.common.model.SetAlertState;
import com.draeger.medical.biceps.common.model.SetAlertStateOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetAlertStateResponse;
import com.draeger.medical.biceps.common.model.SetContextOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetContextState;
import com.draeger.medical.biceps.common.model.SetContextStateResponse;
import com.draeger.medical.biceps.common.model.SetRange;
import com.draeger.medical.biceps.common.model.SetRangeOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetRangeResponse;
import com.draeger.medical.biceps.common.model.SetString;
import com.draeger.medical.biceps.common.model.SetStringOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetStringResponse;
import com.draeger.medical.biceps.common.model.SetValue;
import com.draeger.medical.biceps.common.model.SetValueOperationDescriptor;
import com.draeger.medical.biceps.common.model.SetValueResponse;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.WaveformStream;
import com.draeger.medical.biceps.common.model.util.MetricDescriptorUtil;
import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;
import com.draeger.medical.biceps.device.mdi.interaction.MDICommand;
import com.draeger.medical.biceps.device.mdi.interaction.MDINotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.BulkMDIBStateNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.DescriptorModification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.DescriptorModificationNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.OperationStateChangedNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.OperationalStateChangedNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.SingleContextNotification;
import com.draeger.medical.biceps.device.mdi.interaction.notification.SingleMDIBNotification;
import com.draeger.medical.biceps.device.mdi.interaction.operation.ActivateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetAlertStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetContextStateCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetRangeCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetStringCommand;
import com.draeger.medical.biceps.device.mdi.interaction.operation.SetValueCommand;
import com.draeger.medical.biceps.device.mdi.interaction.qos.BICEPSQoSContext;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;
import com.draeger.medical.biceps.device.mdib.AuthorizationManager;
import com.draeger.medical.biceps.device.mdib.MDIBOperation;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBaseManager;
import com.draeger.medical.biceps.device.mdib.OperationRequest;
import com.draeger.medical.biceps.device.mdib.OperationResponse;
import com.draeger.medical.biceps.device.mdib.PatientDemographicsManager;
import com.draeger.medical.biceps.device.mdib.ScriptRequest;
import com.draeger.medical.biceps.device.mdib.ScriptResponse;
import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.BICEPSEpisodicEventSource;
import com.draeger.medical.biceps.device.mdpws.service.event.AlertEventReporterHelper;
import com.draeger.medical.biceps.device.mdpws.service.event.ContextEventReportHelper;
import com.draeger.medical.biceps.device.mdpws.service.event.FastEpisodicEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationEventReporterHelper;
import com.draeger.medical.biceps.device.mdpws.service.helper.MetricEventReporterHelper;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;
import com.draeger.medical.mdpws.qos.QoSMessageContext;
import com.draeger.medical.mdpws.qos.interception.QoSPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicy;
import com.draeger.medical.mdpws.qos.safetyinformation.SafetyInformationPolicyToken;
import com.draeger.medical.mdpws.qos.safetyinformation.transmission.SafetyInformation;

class DefaultMedicalDeviceInformationBaseManager implements
MedicalDeviceInformationBaseManager {

	private final boolean autoAssignStateVersion= Boolean.parseBoolean(System.getProperty("BICEPS.DefaultMedicalDeviceInformationBaseManager.autoAssignStateVersion", "false"));
	private volatile boolean isRunning = false;
	private ReadWriteLock mdibLock;
	private final HashMap<Class<?>, BICEPSEpisodicEventSource> eventSources = new HashMap<Class<?>, BICEPSEpisodicEventSource>();
	private final DefaultMedicalDeviceInformationBase medicalDeviceInformationBase;
	private BICEPSDeviceNode diceDeviceNode;
	private final AuthorizationManager authManager = AuthorizationManagerFactory
			.getInstance().newAuthorizationManager();
	private final DeviceNodeScopeUpdater scopeUpdater=new DeviceNodeScopeUpdater(this);
	DefaultMedicalDeviceInformationBaseManager(ReadWriteLock lock,
			DefaultMedicalDeviceInformationBase mdib) {
		this.mdibLock = lock;
		this.medicalDeviceInformationBase = mdib;

		createWorkerThreads();
	}


	private void createWorkerThreads() {
		new BICEPSThreadFactory("DefaultMedicalDeviceInformationBaseManager-NotificationTask").newThread(notificationTask).start();

		new BICEPSThreadFactory("DefaultMedicalDeviceInformationBaseManager-CmdTask").newThread(cmdTask).start();


		Thread streamThread = new BICEPSThreadFactory("DefaultMedicalDeviceInformationBaseManager-StreamTask").newThread(streamTask);
		streamThread.setDaemon(true);
		streamThread.setPriority(Thread.MAX_PRIORITY);
		streamThread.start();

	}


	DefaultMedicalDeviceInformationBase getMedicalDeviceInformationBase() {
		return medicalDeviceInformationBase;
	}

	@Override
	public AuthorizationManager getAuthorizationManager() {
		return authManager;
	}

	public ReadWriteLock getMdibLock() {
		return mdibLock;
	}

	public void setMdibLock(ReadWriteLock mdibLock) {
		this.mdibLock = mdibLock;
	}

	@Override
	public void registerEpisodicEventSources(BICEPSEpisodicEventSource eventSource) {
		if (eventSource != null
				&& eventSource.getHandledReportFromModelDomain() != null) {
			eventSources.put(eventSource.getHandledReportFromModelDomain(),
					eventSource);
		}
	}

	@Override
	public void setBICEPSDeviceNode(BICEPSDeviceNode diceDeviceNode) {
		this.diceDeviceNode = diceDeviceNode;
		initializeCurrentContext();
	}

	@Override
	public ScriptResponse queueScriptRequestForExecution(ScriptRequest script) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BlockingQueue<MDINotification> getNotificationQueue() {
		return notificationQueue;
	}

	@Override
	public BlockingQueue<MDICommand> getCommandQueue() {
		return cmdTask.getMdiCmdQueue();
	}

	@Override
	public BlockingQueue<StreamFrame> getStreamQueue() {
		return streamFrameQueue;
	}

	@Override
	public BlockingQueue<StreamFrame> getStreamSinkQueue() {
		return streamFrameSinkQueue;
	}

	public BICEPSDeviceNode getBICEPSDeviceNode() {
		return diceDeviceNode;
	}

	// /////// SCO
	private final AtomicInteger eventId = new AtomicInteger(0);
	private final AtomicInteger transactionId = new AtomicInteger(0);
	private final LinkedBlockingQueue<StreamFrame> streamFrameQueue = new LinkedBlockingQueue<StreamFrame>();
	private final LinkedBlockingQueue<StreamFrame> streamFrameSinkQueue = new LinkedBlockingQueue<StreamFrame>();
	private final LinkedBlockingQueue<MDIBOperation> operationsQueue = new LinkedBlockingQueue<MDIBOperation>();
	private final LinkedBlockingQueue<MDINotification> notificationQueue = new LinkedBlockingQueue<MDINotification>();
	private final HashMap<MDIBOperation, BICEPSQoSContext> contextMap = new HashMap<MDIBOperation, BICEPSQoSContext>();
	private final CommandTask cmdTask = new CommandTask(operationsQueue,
			contextMap);
	private final NotificationTask notificationTask = new NotificationTask(
			notificationQueue);

	private final StreamTask streamTask = new StreamTask(this.streamFrameQueue, this.streamFrameSinkQueue);

	private PatientDemographicsManager PatientContextStateMgr = new DefaultPatientDemographicsManager();

	@Override
	public int getNextTransactionId() {
		return transactionId.addAndGet(1);
	}

	@Override
	public int getCurrentSequenceId() {
		return eventId.get();
	}

	private AbstractSetResponse createAbstractSetResponse(
			AbstractSet operation, AbstractSetResponse response) {
		// TODO SSch Remove if-then-else-tree
		if (operation instanceof SetValue) {
			response = new SetValueResponse();
		} else if (operation instanceof SetString) {
			response = new SetStringResponse();
		} else if (operation instanceof SetRange) {
			response = new SetRangeResponse();
		} else if (operation instanceof SetAlertState) {
			response = new SetAlertStateResponse();
		} else if (operation instanceof Activate) {
			response = new ActivateResponse();
		} else if (operation instanceof SetContextState) {
			response = new SetContextStateResponse();
			//		} else if (operation instanceof SetIdentifiableContext) {
			//			response = new SetIdentifiableContextResponse();
		}

		return response;
	}

	private final synchronized void addToOperationQueue(
			OperationRequest request, AbstractSetResponse operationResponse) {
		int transId = -1;
		if (request != null) {
			transId = request.getTransactionId();
			if (transId < 0)
				transId = getNextTransactionId();

			OperationState opState = medicalDeviceInformationBase
					.getOperationalStateForDescriptor(request
							.getOperationMessage().getOperationHandle());
			if (opState != null
					&& OperationalState.ENABLED.equals(opState.getState())) {

				String mdsHandle = medicalDeviceInformationBase
						.getMDSDescriptionHandleForOperation(request
								.getOperationMessage().getOperationHandle());
				final MDIBOperation queuedOp = new MDIBOperation(request,
						transId, mdsHandle, medicalDeviceInformationBase);
				InvocationState state = queueOperation(queuedOp,
						request.getMsgContextMap());
				operationResponse.setInvocationState(state);
				operationResponse.setTransactionId(transId);
			} else {
				fillFailedResponse(operationResponse, transId);
			}
		} else {
			fillFailedResponse(operationResponse, transId);
		}
	}

	private void fillFailedResponse(AbstractSetResponse response, int transId) {
		response.setInvocationState(InvocationState.FAILED);

		if (transId < 0)
			transId = getNextTransactionId();

		response.setTransactionId(transId);
	}

	private InvocationState queueOperation(final MDIBOperation queuedOp,
			final MDPWSMessageContextMap msgContextMap) {
		if (isRunning) {
			final InvocationState state = InvocationState.WAITING;
			PlatformSupport.getInstance().getToolkit().getThreadPool()
			.execute(new Runnable() {

				@Override
				public void run() {
					try {
						fireOperationInvokedReport(queuedOp, state);
						contextMap.put(queuedOp, new DefaultQoSContext(
								msgContextMap));
						operationsQueue.put(queuedOp);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			return state;
		} else {
			return InvocationState.FAILED;
		}

	}

	private final void fireOperationInvokedReport(
			MDIBOperation queuedOperation, InvocationState invocationState) {
		if (isRunning) {
			BICEPSEpisodicEventSource eventSource = eventSources
					.get(OperationInvokedReport.class);
			if (eventSource != null) {
				if (eventSource.getSubscriptionCount() > 0) {
					OperationInvokedReport modelReport = new OperationInvokedReport();

					ArrayList<OperationInvokedReportPart> parts = new ArrayList<OperationInvokedReportPart>();

					OperationInvokedReportPart reportPart = new OperationInvokedReportPart();
					reportPart.setSourceMDS(queuedOperation.getMdsHandle());

					reportPart.setTransactionId(queuedOperation
							.getTransactionId());
					reportPart.setOperation(queuedOperation
							.getOperationRequest().getOperationDescriptor()
							.getHandle());
					reportPart.setOperationTarget(queuedOperation
							.getOperationRequest().getOperationDescriptor()
							.getOperationTarget());
					reportPart.setOperationState(invocationState);

					parts.add(reportPart);

					OperationInvokedReport messageReport =  OperationEventReporterHelper.getEventMessageForReport(modelReport,getMdibLock(), parts, eventId);
					eventSource.fireReport(messageReport);
				}
			} else {
				if (Log.isWarn())
					Log.warn("No event source 'OperationInvokedReport' found.");
			}
		}
	}

	private void fireObjectCreatedReport(String parent,
			List<Descriptor> descriptors) {
		BICEPSEpisodicEventSource eventSource = eventSources
				.get(ObjectCreatedReport.class);
		if (eventSource != null) {
			if (eventSource.getSubscriptionCount() > 0) {
				ObjectCreatedReport modelReport = new ObjectCreatedReport();

				ArrayList<DescriptionModificationReportPart> parts = new ArrayList<DescriptionModificationReportPart>();

				if (descriptors != null && !descriptors.isEmpty()){

					for (Descriptor descriptor : descriptors) {
						DescriptionModificationReportPart reportPart = new DescriptionModificationReportPart();
						reportPart.setParentDescriptor(parent);
						reportPart.setDescriptor(descriptor);
						parts.add(reportPart);
					}
				}

				ObjectCreatedReport messageReport = (ObjectCreatedReport) OperationEventReporterHelper
						.getEventMessageForReport(modelReport, getMdibLock(),
								parts, eventId);
				eventSource.fireReport(messageReport);
			}
		} else {
			if (Log.isWarn())
				Log.warn("No event source 'ObjectCreatedReport' found.");
		}
	}

	@SuppressWarnings("unused")
	private void fireObjectDeletedReport(String parent,
			List<Descriptor> descripors) {
		BICEPSEpisodicEventSource eventSource = eventSources.get(DescriptionModificationReport.class);
		if (eventSource != null) {
			if (eventSource.getSubscriptionCount() > 0) {
				//				TODO
				//				ObjectCreatedReport modelReport = new ObjectCreatedReport();
				//
				//				ArrayList<DescriptionModificationReportPart> parts = new ArrayList<DescriptionModificationReportPart>();
				//
				//				DescriptionModificationReportPart reportPart = new DescriptionModificationReportPart() {
				//				};
				//
				//				reportPart.setParentDescriptor(parent);
				//
				//				if (descripors != null && !descripors.isEmpty())
				//					reportPart.getVMOs().addAll(descripors);
				//
				//				parts.add(reportPart);
				//
				//				com.draeger.medical.biceps.common.messages.v2.ObjectCreatedReport messageReport = (com.draeger.medical.biceps.common.messages.v2.ObjectCreatedReport) OperationEventReporterHelper
				//						.getEventMessageForReport(modelReport, getMdibLock(),
				//								parts, eventId);
				//				eventSource.fireReport(messageReport);
			}
		} else {
			if (Log.isWarn())
				Log.warn("No event source 'DescriptionModificationReport' found.");
		}
	}

	private void fireObjectUpdatedReport(String parent,
			List<Descriptor> descripors) {
		BICEPSEpisodicEventSource eventSource = eventSources.get(DescriptionModificationReport.class);
		if (eventSource != null) {
			if (eventSource.getSubscriptionCount() > 0) {
				//				TODO
				//				ObjectCreatedReport modelReport = new ObjectCreatedReport();
				//
				//				ArrayList<DescriptionModificationReportPart> parts = new ArrayList<DescriptionModificationReportPart>();
				//
				//				DescriptionModificationReportPart reportPart = new DescriptionModificationReportPart() {
				//				};
				//
				//				reportPart.setParentDescriptor(parent);
				//
				//				if (descripors != null && !descripors.isEmpty())
				//					reportPart.getVMOs().addAll(descripors);
				//
				//				parts.add(reportPart);
				//
				//				com.draeger.medical.biceps.common.messages.v2.ObjectCreatedReport messageReport = (com.draeger.medical.biceps.common.messages.v2.ObjectCreatedReport) OperationEventReporterHelper
				//						.getEventMessageForReport(modelReport, getMdibLock(),
				//								parts, eventId);
				//				eventSource.fireReport(messageReport);
			}
		} else {
			if (Log.isWarn())
				Log.warn("No event source 'DescriptionModificationReport' found.");
		}
	}

	private void fireOperationalStateChangedReport(String mdsHandle,
			OperationState newState) {
		BICEPSEpisodicEventSource eventSource = eventSources
				.get(OperationalStateChangedReport.class);
		if (eventSource != null) {
			if (eventSource.getSubscriptionCount() > 0) {
				OperationalStateChangedReport modelReport = new OperationalStateChangedReport();

				ArrayList<OperationalStateChangedReportPart> parts = new ArrayList<OperationalStateChangedReportPart>();

				OperationalStateChangedReportPart reportPart = new OperationalStateChangedReportPart();

				if (mdsHandle != null)
					reportPart.setSourceMDS(mdsHandle);

				reportPart.getOperations().add(newState);
				parts.add(reportPart);

				OperationalStateChangedReport messageReport = (OperationalStateChangedReport) OperationEventReporterHelper
						.getEventMessageForReport(modelReport, getMdibLock(),
								parts, eventId);
				eventSource.fireReport(messageReport);
			}
		} else {
			if (Log.isWarn())
				Log.warn("No event source 'OperationalStateChangedReport' found.");
		}
	}


	private void fireContextChangedReport(String mdsHandle,
			AbstractIdentifiableContextState newAssociationState) {
		BICEPSEpisodicEventSource eventSource = eventSources
				.get(EpisodicContextChangedReport.class);
		if (eventSource != null) {
			if (eventSource.getSubscriptionCount() > 0) {
				EpisodicContextChangedReport psr = new EpisodicContextChangedReport();
				psr.setSequenceNumber(BigInteger.valueOf(eventId.get()));
				ContextChangedReportPart part = new ContextChangedReportPart();
				part.setSourceMDS(mdsHandle);

				part.getChangedContextStates().add(newAssociationState.getReferencedDescriptor());
				psr.getReportParts().add(part);

				Object report = ContextEventReportHelper
						.getEventMessageForReport(psr,
								medicalDeviceInformationBase.getMdibLock(),
								eventId);
				eventSource.fireReport(report);
			}
		} else {
			if (Log.isWarn())
				Log.warn("No Event source available for "
						+ EpisodicContextChangedReport.class);
		}
	}

	private void fireVMOUpdatedReport(State vmoState) {

		BICEPSEpisodicEventSource eventSource = null;
		Object report = null;
		if (vmoState instanceof AbstractMetricState) {

			MetricDescriptor descriptor = medicalDeviceInformationBase
					.getDescriptorForHandle(vmoState.getReferencedDescriptor());
			if (descriptor != null) {
				boolean isEpisodicRetrievable = MetricDescriptorUtil
						.isRetrievable(descriptor,
								MetricRetrievability.EPISODIC);

				if (isEpisodicRetrievable) {
					eventSource = eventSources.get(EpisodicMetricReport.class);
					if (eventSource != null) {
						if (eventSource.getSubscriptionCount() > 0) {
							EpisodicMetricReport eaRep = new EpisodicMetricReport();
							HashMap<String, Collection<? extends AbstractMetricState>> reportInfo = new HashMap<String, Collection<? extends AbstractMetricState>>();
							MDSDescriptor mds = medicalDeviceInformationBase
									.getMDSForDescriptor(vmoState
											.getReferencedDescriptor());
							if (mds != null) {
								ArrayList<AbstractMetricState> metricStatesForMDS = new ArrayList<AbstractMetricState>();
								metricStatesForMDS
								.add((AbstractMetricState) vmoState);
								reportInfo.put(mds.getHandle(),
										metricStatesForMDS);

								report = MetricEventReporterHelper
										.getEventMessageForReport(eaRep,
												medicalDeviceInformationBase
												.getMdibLock(),
												reportInfo, eventId);
							}
						}
					} else {
						if (Log.isWarn())
							Log.warn("No Event source available for "
									+ EpisodicMetricReport.class);
					}
				}
			}
		} else if (vmoState instanceof AbstractAlertState) {
			eventSource = eventSources.get(EpisodicAlertReport.class);
			if (eventSource != null) {
				if (eventSource.getSubscriptionCount() > 0) {

					EpisodicAlertReport eaRep = new EpisodicAlertReport();
					HashMap<String, Collection<? extends AbstractAlertState>> reportInfo = new HashMap<String, Collection<? extends AbstractAlertState>>();
					MDSDescriptor mds = medicalDeviceInformationBase
							.getMDSForDescriptor(vmoState
									.getReferencedDescriptor());
					if (mds != null) {
						ArrayList<AbstractAlertState> alertStatesForMDS = new ArrayList<AbstractAlertState>();
						alertStatesForMDS.add((AbstractAlertState) vmoState);
						reportInfo.put(mds.getHandle(), alertStatesForMDS);

						report = AlertEventReporterHelper
								.getEventMessageForReport(eaRep,
										medicalDeviceInformationBase
										.getMdibLock(), reportInfo,
										eventId);
					}
				}
			} else {
				if (Log.isWarn())
					Log.warn("No Event source available for "
							+ EpisodicAlertReport.class);
			}
		}

		if (eventSource != null && report != null)
			eventSource.fireReport(report);
	}

	private boolean mergeAndAddState(State newState, State oldState)
			throws Exception {
		boolean updated= false;

		assignStateVersionInfo(newState,oldState);

		if (oldState!=null && newState!=null)
		{
			if ((oldState.getStateVersion()!=null && !oldState.getStateVersion().equals(newState.getStateVersion()))
					|| (oldState.getStateVersion()==null && newState.getStateVersion()!=null))
			{
				updated=medicalDeviceInformationBase.addState(newState);
			}else {
				if (Log.isWarn())
					Log.warn("DefaultMedicalDeviceInformationBaseManager was not able to merge states due to equal state versions. "
							+ "\n Use either setStateVersion to change the state version or the VM arg -DBICEPS.DefaultMedicalDeviceInformationBaseManager.autoAssignStateVersion=true. "
							+ "\nNew State: "+newState+" "+newState.getHandle()+". "
							+ "\nOld State: "+oldState+" "+oldState.getHandle()+". "
							+ "\nNew State: "+newState.getStateVersion()+" OldState:"+oldState.getStateVersion());
			}
		}else{
			updated=medicalDeviceInformationBase.addState(newState);
		}

		return updated;
	}


	public void assignStateVersionInfo(State newState, State oldState) {
		if (newState!=null)
		{

			if (autoAssignStateVersion)
			{

				BigInteger stateVersion=BigInteger.ZERO;

				if (oldState!=null)
				{
					stateVersion=oldState.getStateVersion();
					if (stateVersion!=null)
					{
						stateVersion=stateVersion.add(BigInteger.ONE);
					}
				}

				if (newState.getStateVersion()!=null)
				{
					if (Log.isDebug())
						Log.debug("");
				}
				newState.setStateVersion(stateVersion);
			}


		}
	}

	/**
	 * 
	 */
	private void initializeCurrentContext() {
		scopeUpdater.initializeDeviceNodeMetaData();
	}

	private class CommandTask implements Runnable {

		private final LinkedBlockingQueue<MDICommand> mdiCmdQueue = new LinkedBlockingQueue<MDICommand>(
				1);
		private final LinkedBlockingQueue<MDIBOperation> mdibOperationQueue;
		private final HashMap<MDIBOperation, BICEPSQoSContext> contextMap;
		private boolean runCommandTask = true;

		public CommandTask(LinkedBlockingQueue<MDIBOperation> operationsQueue,
				HashMap<MDIBOperation, BICEPSQoSContext> contextMap) {
			this.mdibOperationQueue = operationsQueue;
			this.contextMap = contextMap;
		}

		@Override
		public void run() {
			try {
				while (runCommandTask) {
					MDIBOperation op = this.mdibOperationQueue.take();

					if (op != null) {
						BICEPSQoSContext ctxt = this.contextMap.remove(op);

						// TODO SSch is MDICommand allowed -> Transaction
						// abort/failed notification???
						MDICommand cmd = createMDICommand(op, ctxt);
						if (cmd != null)
							mdiCmdQueue.put(cmd);
					}
				}
			} catch (InterruptedException e) {
				Log.warn(e);
			}
		}

		private MDICommand createMDICommand(MDIBOperation operation,
				BICEPSQoSContext context) {
			MDICommand cmd = null;
			OperationDescriptor descriptor = operation.getOperationRequest()
					.getOperationDescriptor();

			// TODO SSch use lookup table and ClassFor + reflection constructor
			if (descriptor != null) {
				if (descriptor instanceof SetValueOperationDescriptor) {
					cmd = new SetValueCommand(context, operation);
				} else if (descriptor instanceof SetStringOperationDescriptor) {
					cmd = new SetStringCommand(context, operation);
				} else if (descriptor instanceof SetAlertStateOperationDescriptor) {
					cmd = new SetAlertStateCommand(context, operation);
				} else if (descriptor instanceof ActivateOperationDescriptor) {
					cmd = new ActivateCommand(context, operation);
					//				} else if (descriptor instanceof SetIdentifiableContextOperationDescriptor) {
					//					cmd = new SetIdentifiableContextCommand(context, operation);
				} else if (descriptor instanceof SetContextOperationDescriptor) {
					cmd = new SetContextStateCommand(context, operation);
				} else if (descriptor instanceof SetRangeOperationDescriptor) {
					cmd = new SetRangeCommand(context, operation);
				} else {
					if (Log.isWarn())
						Log.warn("No Mapping from op -> cmd! "
								+ operation.getOperationRequest());
				}
			}
			return cmd;
		}

		public LinkedBlockingQueue<MDICommand> getMdiCmdQueue() {
			return mdiCmdQueue;
		}

	}

	private class StreamTask implements Runnable {

		private int updatePeriod=Integer.parseInt(System.getProperty("BICEPS.WaveformPublisher.UpdatePeriod", "200"));
		private float fudgeFactor=Float.parseFloat(System.getProperty("BICEPS.WaveformPublisher.FudgeFactor", "1.0"));
		private final int nanoToMs=(int) Math.pow(10, 6);
		private long updatePeriodNano=(updatePeriod*nanoToMs);
		private int currentUpdatePeriod=updatePeriod;

		private final LinkedBlockingQueue<StreamFrame> streamFrameQueue;
		private final LinkedBlockingQueue<StreamFrame> streamFrameSinkQueue;
		private final ArrayList<StreamFrame> frameList=new ArrayList<StreamFrame>();
		private boolean runStreamTask = Boolean.parseBoolean(System.getProperty("BICEPS.StreamTask.run","true"));

		/**
		 * @param streamFrameQueue
		 * @param streamFrameSinkQueue
		 */
		public StreamTask(LinkedBlockingQueue<StreamFrame> streamFrameQueue,
				LinkedBlockingQueue<StreamFrame> streamFrameSinkQueue) {
			this.streamFrameQueue=streamFrameQueue;
			this.streamFrameSinkQueue=streamFrameSinkQueue;
		}

		@Override
		public void run() {
			long t1=0;
			HashMap<String, ArrayList<StreamFrame>> frameMap=new HashMap<String, ArrayList<StreamFrame>>();
			ArrayList<StreamFrame> frameList=new ArrayList<StreamFrame>();
			ArrayList<String> handles=new ArrayList<String>();

			while (runStreamTask) {
				t1 = System.nanoTime();

				this.streamFrameQueue.drainTo(frameList);
				this.streamFrameSinkQueue.addAll(frameList);

				if (isRunning){
					WaveformBuilder.addAllFramesToMap(frameMap, frameList, handles);

					WaveformStream wvStream = WaveformBuilder.prepareWaveformStream(frameMap, handles);
					fireWaveformReport(handles, wvStream);

					frameList.clear();
					frameMap.clear();
					handles.clear();
				}

				currentUpdatePeriod=(int) (((updatePeriodNano-(System.nanoTime()-t1))/nanoToMs)*fudgeFactor);

				if (currentUpdatePeriod<1){
					currentUpdatePeriod=1;
				}
				try {
					Thread.sleep(currentUpdatePeriod);
				} catch (InterruptedException e) {
					if (Log.isWarn())Log.warn(e);
				}
			}
		}

	}


	private void fireWaveformReport(ArrayList<String> handles,
			WaveformStream wvStream) {
		BICEPSEpisodicEventSource eventSource = eventSources
				.get(WaveformStream.class);
		if (eventSource instanceof FastEpisodicEventReport) {
			if (eventSource.getSubscriptionCount() > 0) {

				WaveformStream messageReport =  OperationEventReporterHelper.getEventMessageForReport(wvStream, getMdibLock(), eventId);
				eventSource.fireReport(messageReport);
			}
		} else {
			if (Log.isDebug())
				Log.debug("No event source 'waveformStream' found.");
		}
	}


	private class NotificationTask implements Runnable {

		private final LinkedBlockingQueue<? extends MDINotification> notificationQueue;
		private boolean runNotificationTask = true;

		public NotificationTask(
				LinkedBlockingQueue<? extends MDINotification> notificationQueue) {
			this.notificationQueue = notificationQueue;
		}

		@Override
		public void run() {
			while (runNotificationTask) {
				try {
					MDINotification notification = this.notificationQueue
							.take();
					if (isRunning) {
						if (notification instanceof OperationStateChangedNotification) {
							OperationStateChangedNotification oscNotification = (OperationStateChangedNotification) notification;
							fireOperationInvokedReport(
									oscNotification.getOperation(),
									oscNotification.getNewInvocationState());

						} else if (notification instanceof DescriptorModificationNotification) {
							DescriptorModificationNotification oscNotification = (DescriptorModificationNotification) notification;
							ArrayList<Descriptor> createdDescriptors = new ArrayList<Descriptor>();
							ArrayList<Descriptor> deletedDescriptors = new ArrayList<Descriptor>();
							ArrayList<Descriptor> updatedDescriptors = new ArrayList<Descriptor>();
							for (DescriptorModification modification : oscNotification
									.getModifications().values()) {
								switch (modification.getType()) {
								case UPDATED:
									updatedDescriptors.add(modification
											.getModifiedDescriptor());
									break;
								case DELETED:
									deletedDescriptors.add(modification
											.getModifiedDescriptor());
									break;
								case CREATED:
									createdDescriptors.add(modification
											.getModifiedDescriptor());
									break;
								default:
									break;
								}
							}
							try {
								mdibLock.writeLock().lock();
								deleteDescriptorsAndNotify(oscNotification,updatedDescriptors);
								createDescriptorsAndNotify(oscNotification,createdDescriptors);
								updateDescriptorsAndNotify(oscNotification,updatedDescriptors);

							} finally {
								mdibLock.writeLock().unlock();
							}
						} else if (notification instanceof OperationalStateChangedNotification) {
							OperationalStateChangedNotification oscNotification = (OperationalStateChangedNotification) notification;
							OperationState newState = (OperationState) oscNotification
									.getNewState();
							if (oscNotification.getNewState() != null) {
								try {
									mdibLock.writeLock().lock();
									if (Log.isDebug())
										Log.debug("Received new OperationalStateChangedNotification: "
												+ newState);

									boolean updated = false;

									updated = updateState(newState);
									if (updated) {
										fireOperationalStateChangedReport(
												oscNotification.getMdsHandle(),
												newState);
									}
								} finally {
									mdibLock.writeLock().unlock();
								}
							} else {
								if (Log.isDebug())
									Log.debug("No new state!!!");
							}

						} else if (notification instanceof ContextChangedNotification) {
							handleContextChangedNotification((ContextChangedNotification)notification);
							//						} else if (notification instanceof PatientContextStateChanged) {
							//							PatientContextStateChanged pdc = (PatientContextStateChanged) notification;
							//							if (pdc != null
							//									&& pdc.getPatientContextState() != null) {
							//								try {
							//									mdibLock.writeLock().lock();
							//
							//									boolean updated = updatePatientContextState(
							//											pdc.getMdsHandle(),
							//											pdc.getPatientContextState());
							//									if (updated) {
							//										firePatientContextStateChangedReport(
							//												pdc.getMdsHandle(),
							//												pdc.getPatientContextState());
							//										scopeUpdater.changeBICEPSDeviceNodeMetaData(pdc);
							//									}
							//								} finally {
							//									mdibLock.writeLock().unlock();
							//								}
							//							}
						} else if (notification instanceof SingleMDIBNotification) {
							SingleMDIBNotification ascNotification = (SingleMDIBNotification) notification;
							State newState = ascNotification.getNewState();
							if (newState != null) {
								try {
									mdibLock.writeLock().lock();
									if (Log.isDebug())
										Log.debug("Received new State!!! "
												+ newState);

									boolean updated = false;

									updated = updateState(newState);
									if (updated) {
										fireVMOUpdatedReport(newState);
									}
								} finally {
									mdibLock.writeLock().unlock();
								}
							} else {
								if (Log.isDebug())
									Log.debug("No new state!!!");

							}

						} else if (notification instanceof BulkMDIBStateNotification) {
							BulkMDIBStateNotification ascNotification = (BulkMDIBStateNotification) notification;
							Collection<? extends State> newStates = ascNotification
									.getStates();
							if (newStates != null) {
								try {
									mdibLock.writeLock().lock();
									if (Log.isDebug())
										Log.debug("Received new States!!! "
												+ newStates.size());
									for (State s : newStates) {
										boolean updated = false;

										updated = updateState(s);
										if (updated) {
											fireVMOUpdatedReport(s);
										}
									}
								} finally {
									mdibLock.writeLock().unlock();
								}
							} else {
								if (Log.isDebug())
									Log.debug("No new state!!!");

							}

						}else if (notification instanceof StreamFrame){
							//VOID
						}

					}
				} catch (InterruptedException e) {
					Log.warn(e);
				} catch (Exception e) {
					Log.warn(e);
				}
			}

		}

		/**
		 * @param notification
		 * @throws Exception 
		 */
		private void handleContextChangedNotification(
				ContextChangedNotification<AbstractIdentifiableContextState> notification) throws Exception {
			//			if (notification!=null)
			//			{
			////				PatientAssociationChanged psc = ((PatientAssociationChanged) notification);
			//				if (psc.getContextItem() != null) {
			//					try {
			//						mdibLock.writeLock().lock();
			//
			////						boolean fireStateEvent = updatePatientState(
			////								psc.getMdsHandle(),
			////								psc.getAssociationState());
			//
			//						boolean fireContextEvent = updateIdentifiableContextItem(
			//								psc.getMdsHandle(),
			//								psc.getContextItem());
			//
			//						if (fireContextEvent) {
			//							fireContextChangedReport(
			//									psc.getMdsHandle(),
			//									psc.getContextItem());
			//							
			//							scopeUpdater.changeBICEPSDeviceNodeMetaData(psc);
			//						}
			//
			//					} finally {
			//						mdibLock.writeLock().unlock();
			//					}
			//				}
			//			}else 
			if  (notification instanceof SingleContextNotification){
				//				SingleContextNotification<?, ?> locAssocChanged = ((SingleContextNotification<?, ?>) notification);
				if ( notification.getContextItem() != null) {
					try {
						mdibLock.writeLock().lock();
						//
						//						boolean fireStateEvent = updateContextStateAssocation(
						//								locAssocChanged.getMdsHandle(),
						//								locAssocChanged.getAssociationState());

						boolean fireContextEvent = updateIdentifiableContextItem(
								notification.getMdsHandle(),
								notification.getContextItem());

						if (fireContextEvent) {
							fireContextChangedReport(
									notification.getMdsHandle(),
									notification.getContextItem());

							scopeUpdater.changeBICEPSDeviceNodeMetaData(notification);
						}


					} finally {
						mdibLock.writeLock().unlock();
					}
				}
			}

		}

		private void updateDescriptorsAndNotify(
				DescriptorModificationNotification oscNotification,
				ArrayList<Descriptor> updateddDescriptors) {
			if (!updateddDescriptors.isEmpty()) {

				if (Log.isDebug())
					Log.debug("Received new DescriptorModificationNotification <UPDATE>: "
							+ updateddDescriptors);

				boolean updated = false;
				//TODO SSch implement
				//				updated = updateCreatedDescriptors(oscNotification.getParentDescriptor(), updateddDescriptors);
				if (updated) {
					fireObjectUpdatedReport(
							oscNotification
							.getParentDescriptor(),
							updateddDescriptors);
				}

			}
		}


		private void deleteDescriptorsAndNotify(
				DescriptorModificationNotification oscNotification,
				ArrayList<Descriptor> createdDescriptors) {
			if (!createdDescriptors.isEmpty()) {

				if (Log.isDebug())
					Log.debug("Received new DescriptorModificationNotification <DELETED>:"
							+ createdDescriptors);

				boolean updated = false;
				//TODO SSch implement
				//				updated = updateCreatedDescriptors(
				//						oscNotification
				//								.getParentDescriptor(),
				//						createdDescriptors);
				if (updated) {
					fireObjectCreatedReport(
							oscNotification
							.getParentDescriptor(),
							createdDescriptors);
				}

			}
		}

		private void createDescriptorsAndNotify(
				DescriptorModificationNotification oscNotification,
				ArrayList<Descriptor> createdDescriptors) {
			if (!createdDescriptors.isEmpty()) {

				if (Log.isDebug())
					Log.debug("Received new DescriptorModificationNotification <CREATE>:"
							+ createdDescriptors);

				boolean updated = false;
				updated = updateCreatedDescriptors(
						oscNotification
						.getParentDescriptor(),
						createdDescriptors);
				if (updated) {
					fireObjectCreatedReport(
							oscNotification
							.getParentDescriptor(),
							createdDescriptors);
				}

			}
		}

		private boolean updateCreatedDescriptors(String parent,
				ArrayList<Descriptor> createdDescriptors) {
			boolean retVal = false;
			Descriptor descriptor = medicalDeviceInformationBase
					.getDescriptor(parent);
			if (descriptor instanceof ChannelDescriptor) {
				ChannelDescriptor channel = (ChannelDescriptor) descriptor;
				MDSDescriptor mds = medicalDeviceInformationBase
						.getMDSForDescriptor(channel.getHandle());
				for (Descriptor newDescriptor : createdDescriptors) {
					if (medicalDeviceInformationBase
							.isUseSharedMemoryWithDevice()){
						newDescriptor = medicalDeviceInformationBase.copyDescriptor(newDescriptor);
					}

					if (newDescriptor instanceof MetricDescriptor) {
						MetricDescriptor metricDescriptor = (MetricDescriptor) newDescriptor;
						if (!channel.getMetrics().contains(newDescriptor)) {
							channel.getMetrics().add(metricDescriptor);
							medicalDeviceInformationBase.fillMetricMap(
									metricDescriptor, mds);
							retVal = true;
						}

					} else if (newDescriptor instanceof AlertSystemDescriptor) {
						AlertSystemDescriptor asd = (AlertSystemDescriptor) newDescriptor;
						if (channel.getAlertSystem() == null) {
							channel.setAlertSystem(asd);
							medicalDeviceInformationBase
							.fillAlertSystemDescriptorMap(asd, mds);
							retVal = true;
						}

					}
				}
			}
			return retVal;
		}

		private boolean updateState(State newState) throws Exception {

			boolean retVal = false;
			if (newState != null)
			{
				State oldState = medicalDeviceInformationBase.getState(newState.getHandle());
				//			if (oldState != null) {
				retVal = mergeAndAddState(newState, oldState);
				//			} else {
				//				retVal = medicalDeviceInformationBase.addState(newState);
				//			}
			}
			return retVal;
		}

		//		private boolean updatePatientState(String mdsHandle,
		//				ContextAssociationStateValue patientAssociationState)
		//						throws Exception {
		//			//			boolean updated = updateState(patientAssociationState);
		//			//
		//			//			return updated;
		//			return updateContextStateAssocation(mdsHandle,patientAssociationState);
		//		}

		//		private boolean updatePatientContextState(String mdsHandle,
		//				PatientContextState p) throws Exception {
		//			//			boolean updated = updateState(p);
		//			//			return updated;
		//			return updateIdentifiableContextItem(mdsHandle,p);
		//		}

		//		private boolean updateContextStateAssocation(String mdsHandle,
		//				ContextStateElement associationState)
		//						throws Exception {
		//			boolean updated = updateState(associationState);
		//
		//			return updated;
		//		}

		private boolean updateIdentifiableContextItem(String mdsHandle,
				AbstractIdentifiableContextState p) throws Exception {
			boolean updated = updateState(p);
			return updated;
		}


	}

	private static class DefaultQoSContext implements BICEPSQoSContext {

		private final List<SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>> safetyToken = new ArrayList<SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>>();
		private final HashSet<QoSPolicyToken<?, ?>> tokens = new HashSet<QoSPolicyToken<?, ?>>();

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public DefaultQoSContext(MDPWSMessageContextMap msgContextMap) {
			List<QoSPolicyToken<?, ?>> tokenList = getQoSTokenList(msgContextMap);
			if (tokenList != null) {
				for (QoSPolicyToken<?, ?> qoSPolicyToken : tokenList) {
					if (qoSPolicyToken instanceof SafetyInformationPolicyToken) {
						safetyToken
						.add((SafetyInformationPolicyToken) qoSPolicyToken);
					}
					tokens.add(qoSPolicyToken);
				}
			}
		}

		protected List<QoSPolicyToken<?, ?>> getQoSTokenList(
				MDPWSMessageContextMap msgContextMap) {
			ArrayList<QoSPolicyToken<?, ?>> relevantTokenList = new ArrayList<QoSPolicyToken<?, ?>>();

			if (msgContextMap != null) {
				QoSMessageContext qosMsgContext = (QoSMessageContext) msgContextMap
						.get(QoSMessageContext.class);
				// BUGFIX SSch: NPE if no policies have been defined
				if (qosMsgContext != null) {
					Iterator policyToken = qosMsgContext.getQoSPolicyToken();
					while (policyToken.hasNext()) {
						QoSPolicyToken<?, ?> token = (QoSPolicyToken<?, ?>) policyToken
								.next();
						relevantTokenList.add(token);
					}
				}
			}

			return relevantTokenList;
		}

		@Override
		public String toString() {
			return "DefaultQoSContext [safetyToken=" + safetyToken + "]";
		}

		@Override
		public List<SafetyInformationPolicyToken<SafetyInformation, SafetyInformationPolicy>> getSafetyInformationToken() {
			return safetyToken;
		}

		@Override
		public Set<QoSPolicyToken<?, ?>> getRawQoSTokens() {
			return tokens;
		}
	}

	@Override
	public OperationResponse queueOperationRequestForExecution(
			OperationRequest request) {

		OperationResponseImpl opResponse = new OperationResponseImpl();
		AbstractSetResponse response = null;
		if (request != null) {
			AbstractSet operationMessage = request.getOperationMessage();
			response = createAbstractSetResponse(operationMessage, response);

			if (operationMessage != null)
				addToOperationQueue(request, response);
		}
		opResponse.setContent(response);

		return opResponse;
	}

	@Override
	public PatientDemographicsManager getPatientContextStateManager() {
		return this.PatientContextStateMgr;
	}

	@Override
	public void start() {
		isRunning = true;
	}

	@Override
	public void stop() {
		isRunning = false;
	}

	public void incrementMDIBVersion(Object object) {
		eventId.getAndIncrement();
	}

	public void resetMDIBVersion() {
		eventId.set(0);
	}

}
