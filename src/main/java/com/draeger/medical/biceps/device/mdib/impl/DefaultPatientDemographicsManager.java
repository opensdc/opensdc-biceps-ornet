/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import com.draeger.medical.biceps.common.model.PatientContextState;
import com.draeger.medical.biceps.device.mdib.PatientDemographicsManager;

public class DefaultPatientDemographicsManager implements
		PatientDemographicsManager{

    @Override
	public void anonyminize(PatientContextState original)
    {
    	//TODO SSch Refactoring Patient
//        PatientData originalPD = original.getPatientData();
//        if (originalPD != null)
//        {
//            if (original.getState().equals(PatientState.EMPTY))
//            {
//                originalPD.setPatientId(MDPWSConstants.PATIENT_NO_URN);
//            }
//            else
//            {
//                originalPD.setPatientId(MDPWSConstants.PATIENT_ANONYMOUS_URN);
//            }
//            originalPD.setAge(null);
//            originalPD.setFirstname(null);
//            originalPD.setBirthday(null);
//            originalPD.setHeight(null);
//            originalPD.setLastname(null);
//            originalPD.setLocation(null);
//            originalPD.setType(null);
//            originalPD.setWeight(null);
//        }
    }

}
