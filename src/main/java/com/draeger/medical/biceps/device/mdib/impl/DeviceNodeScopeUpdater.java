/**
 * 
 */
package com.draeger.medical.biceps.device.mdib.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.ScopeSet;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.context.ContextHelper;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.InstanceIdentifier;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.device.mdi.interaction.notification.ContextChangedNotification;
import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;

/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
public class DeviceNodeScopeUpdater {

	private DefaultMedicalDeviceInformationBaseManager manager;
	private HashMap<AbstractContextDescriptor,ArrayList<URI>>  currentContextScopes=new HashMap<AbstractContextDescriptor,ArrayList<URI>>();
	private ArrayList<URI>									   nonContextScopes=new ArrayList<URI>();
	DeviceNodeScopeUpdater(DefaultMedicalDeviceInformationBaseManager manager)
	{
		this.manager=manager;
	}


	void initializeDeviceNodeMetaData() {
		if (this.manager.getMedicalDeviceInformationBase()!=null)
		{

			try{
				this.manager.getMdibLock().readLock().lock();

				fillNonContextScopes();

				MDDescription mdDescription = this.manager.getMedicalDeviceInformationBase().getDeviceDescriptions();
				if (mdDescription!=null)
				{	
					List<MDSDescriptor> mdsDescriptors = mdDescription.getMDSDescriptors();
					for (MDSDescriptor mdsDescriptor : mdsDescriptors) {
						SystemContext systemContext = mdsDescriptor.getContext();
						initializeScopeMap(systemContext);
					}
				}

				MDState deviceStates = this.manager.getMedicalDeviceInformationBase().getDeviceStates();
				updateScopeListsForStates(deviceStates);
				updateScopesForDeviceNode();
			}finally{
				this.manager.getMdibLock().readLock().unlock();
			}
		}
	}


	private void updateScopeListsForStates(MDState deviceStates) {
		if (deviceStates!=null)
		{
			HashMap<AbstractContextDescriptor,ArrayList<AbstractIdentifiableContextState>> descriptorStatesMap=new HashMap<AbstractContextDescriptor, ArrayList<AbstractIdentifiableContextState>>();
			for(State deviceState: deviceStates.getStates())
			{
				if (deviceState instanceof AbstractIdentifiableContextState)
				{
					AbstractIdentifiableContextState acs=(AbstractIdentifiableContextState)deviceState;
					String referencedDescriptor = acs.getReferencedDescriptor();
					Descriptor descriptor = this.manager.getMedicalDeviceInformationBase().getDescriptor(referencedDescriptor);
					if (descriptor instanceof AbstractContextDescriptor)
					{
						AbstractContextDescriptor acd=(AbstractContextDescriptor) descriptor;
						ArrayList<AbstractIdentifiableContextState> statesForDescriptor = descriptorStatesMap.get(acd);
						if (statesForDescriptor==null)
						{
							statesForDescriptor=new ArrayList<AbstractIdentifiableContextState>();
							descriptorStatesMap.put(acd, statesForDescriptor);
						}
						statesForDescriptor.add(acs);
					}
				}
			}
			updateScopeListForDescriptorMap(descriptorStatesMap);
		}
	}


	/**
	 * 
	 */
	private void updateScopesForDeviceNode() {
		BICEPSDeviceNode deviceNode=manager.getBICEPSDeviceNode();
		if (deviceNode!=null)
		{
			ScopeSet newScopeSet = new ScopeSet();
			for (URI nonContextScope : nonContextScopes) {
				newScopeSet.addScope(nonContextScope.toString());
			}
			for (Entry<AbstractContextDescriptor, ArrayList<URI>> contextScopes : currentContextScopes.entrySet()) {
				ArrayList<URI> scopes = contextScopes.getValue();
				for (URI scope : scopes) {
					newScopeSet.addScope(scope.toString());	
				}
			}
			deviceNode.setScopes(newScopeSet);
		}else{
			if (Log.isDebug()){
				Log.debug("Could not change device node meta data! No device node available.");
			}
		}
	}


	private void fillNonContextScopes() {
		if (this.manager!=null && this.manager.getBICEPSDeviceNode()!=null)
		{
			Iterator scopesIt = this.manager.getBICEPSDeviceNode().getScopes();
			if (scopesIt!=null)
			{
				while(scopesIt.hasNext())
				{
					URI scope=(URI) scopesIt.next();
					nonContextScopes.add(scope);
				}
			}
		}

	}


	private void updateScopeListForDescriptorMap(HashMap<AbstractContextDescriptor, ArrayList<AbstractIdentifiableContextState>> descriptorStatesMap) {
		if ( descriptorStatesMap!=null)
		{
			for (Entry<AbstractContextDescriptor, ArrayList<AbstractIdentifiableContextState>> entry : descriptorStatesMap.entrySet()) {

				AbstractContextDescriptor ctxtDescriptor=entry.getKey();
				ArrayList<URI> currentScopeList = currentContextScopes.get(ctxtDescriptor);
				if (currentScopeList==null)
				{
					addToScopeMap(ctxtDescriptor);
				}
				updateScopeList(ctxtDescriptor, currentScopeList, entry.getValue());
			}


		}
	}

	private void updateScopeList(AbstractContextDescriptor ctxtDescriptor, ArrayList<URI> currentScopeList, ArrayList<AbstractIdentifiableContextState> contextStates) {
		ArrayList<URI> tCurrentScopeList=new ArrayList<URI>();
		currentScopeList.clear();
		for (AbstractIdentifiableContextState identifiableState : contextStates) {
//			if (ctxtState instanceof ContextAssociationStateValue)
//			{
//				ContextAssociationStateValue pas=(ContextAssociationStateValue)ctxtState;
//				ContextAssociationStateValue stateValue = pas.getState();
//				if (stateValue!=null)
//				{
//					URI assocationURI = ContextHelper.getAssociationUri(ctxtDescriptor, pas,
//							stateValue);
//
//					if (assocationURI!=null)
//						tCurrentScopeList.add(assocationURI);
//				}
//			}else
				
			if (identifiableState!=null)
			{
				List<InstanceIdentifier> ids = identifiableState.getIdentification();
				if (ids!=null)
				{
					for (InstanceIdentifier instanceIdentifier : ids) {
						if (ctxtDescriptor!=null)
						{
							URI idUri = ContextHelper.getIdUri(ctxtDescriptor.getClass(), instanceIdentifier);

							if (idUri!=null)
								tCurrentScopeList.add(idUri);
						}
					}
				}
			}

		}
		if (!tCurrentScopeList.isEmpty())
		{

			currentScopeList.addAll(tCurrentScopeList);
		}
	}








	private void initializeScopeMap(SystemContext systemContext) {
		if (systemContext!=null)
		{
			addToScopeMap(systemContext.getEnsembleContext());
			addToScopeMap(systemContext.getLocationContext());
			addToScopeMap(systemContext.getOperatorContext());
			addToScopeMap(systemContext.getPatientContext());
			addToScopeMap(systemContext.getWorkflowContext());
		}
	}


	private <T extends AbstractContextDescriptor> void addToScopeMap(T contextDescriptor) {
		if (contextDescriptor!=null)
		{
			currentContextScopes.put(contextDescriptor, new ArrayList<URI>());
		}
	}

	void changeBICEPSDeviceNodeMetaData(ContextChangedNotification<AbstractIdentifiableContextState> contextNotification) {

		if (contextNotification!=null && contextNotification.getReferencedDescriptorHandle()!=null)
		{
			HashMap<AbstractContextDescriptor, ArrayList<AbstractIdentifiableContextState>> descriptorStatesMap=new HashMap<AbstractContextDescriptor, ArrayList<AbstractIdentifiableContextState>>();
			Descriptor descriptor = this.manager.getMedicalDeviceInformationBase().getDescriptor(contextNotification.getReferencedDescriptorHandle());
			if (descriptor instanceof AbstractContextDescriptor)
			{
				ArrayList<AbstractIdentifiableContextState> stateList=new ArrayList<AbstractIdentifiableContextState>();

				if (contextNotification.getContextItem()!=null)
					stateList.add(contextNotification.getContextItem());

				descriptorStatesMap.put((AbstractContextDescriptor)descriptor, stateList);
				updateScopeListForDescriptorMap(descriptorStatesMap);
				updateScopesForDeviceNode();
			}
		}
	}


}
