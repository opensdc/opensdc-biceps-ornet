/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.util.concurrent.atomic.AtomicLong;

import com.draeger.medical.biceps.device.mdib.HandleGenerator;

public class HandleGeneratorImpl implements HandleGenerator
{
    private static AtomicLong currentHandle = new AtomicLong(0);

    @Override
	public String nextHandle(Object top)
    {
        return String.valueOf(currentHandle.incrementAndGet());
    }

}
