/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.device.mdib.OperationRequest;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class OperationRequestImpl implements OperationRequest {
	OperationDescriptor operation;
	MDPWSMessageContextMap msgContextMap;
	MDPWSMessageContextMap replyMessageCtxtMap;
	private AbstractSet operationMessage;
	private int transactionId=-1;
	
	
	
	@Override
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.OperationRequest#getOperation()
	 */
	@Override
	public OperationDescriptor getOperationDescriptor() {
		return operation;
	}
	public void setOperation(OperationDescriptor operation) {
		this.operation = operation;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.OperationRequest#getMsgContextMap()
	 */
	@Override
	public MDPWSMessageContextMap getMsgContextMap() {
		return msgContextMap;
	}
	public void setMsgContextMap(MDPWSMessageContextMap msgContextMap) {
		this.msgContextMap = msgContextMap;
	}
	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.OperationRequest#getReplyMessageCtxtMap()
	 */
	@Override
	public MDPWSMessageContextMap getReplyMessageCtxtMap() {
		return replyMessageCtxtMap;
	}
	public void setReplyMessageCtxtMap(MDPWSMessageContextMap replyMessageCtxtMap) {
		this.replyMessageCtxtMap = replyMessageCtxtMap;
	}
	
	@Override
	public AbstractSet getOperationMessage() {
		return operationMessage;
	}
	public void setOperationMessage(AbstractSet operationMessage) {
		this.operationMessage = operationMessage;
	}
}
