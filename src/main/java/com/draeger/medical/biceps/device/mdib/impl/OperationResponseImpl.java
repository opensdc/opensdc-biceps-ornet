/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.device.mdib.OperationResponse;

public class OperationResponseImpl implements OperationResponse {
	AbstractSetResponse content;

	/* (non-Javadoc)
	 * @see com.draeger.medical.biceps.device.mdib.impl.OperationResponse#getContent()
	 */
	@Override
	public AbstractSetResponse getContent() {
		return content;
	}

	public void setContent(AbstractSetResponse content) {
		this.content = content;
	}
	
}
