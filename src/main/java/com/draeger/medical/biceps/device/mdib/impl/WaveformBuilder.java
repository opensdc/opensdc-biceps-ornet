/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdib.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayValue;
import com.draeger.medical.biceps.common.model.WaveformStream;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;

public class WaveformBuilder {
	
	private static final int maxValuesPerPublish=Integer.parseInt(System.getProperty("BICEPS.WaveformPublisher.MaxValuesPerPublish", "1600"));
	private static final boolean OneFramePerMessage=Boolean.parseBoolean(System.getProperty("BICEPS.WaveformPublisher.OneFramePerMessage", "false"));
	private static final int initialSampleListCapacity=50;

	
	static void addAllFramesToMap(HashMap<String, ArrayList<StreamFrame>> frameMap, ArrayList<StreamFrame> frameList, ArrayList<String> handles)
	{
		for (StreamFrame streamFrame : frameList) {
			WaveformBuilder.addFrameToMap(frameMap,streamFrame, handles);
		}
	}
	
	static ArrayList<StreamFrame> addFrameToMap(
			HashMap<String, ArrayList<StreamFrame>> frameMap, StreamFrame frame, ArrayList<String> handles) {
		String handle=frame.getMetricDescriptorHandle();
		ArrayList<StreamFrame> frames=frameMap.get(handle);
		if (frames==null)
		{
			frames=new ArrayList<StreamFrame>();
			frameMap.put(handle, frames);
		}
		if (!handles.contains(handle))
			handles.add(handle);

		frames.add(frame);


		return frames;
	}

	static WaveformStream prepareWaveformStream(
			HashMap<String, ArrayList<StreamFrame>> frameMap,
			ArrayList<String> handles) {

		WaveformStream wvStream=null;
		if (!OneFramePerMessage)
		{
			wvStream=new WaveformStream();

			int valueCnt=0;
			for (String handle : handles) {
				ArrayList<StreamFrame> frames= frameMap.get(handle);
				if (frames!=null && frames.size()>0)
				{
					int startFrameNo=0;

					RealTimeSampleArrayMetricState rtsa=new RealTimeSampleArrayMetricState();
					rtsa.setReferencedDescriptor(handle);

					RealTimeSampleArrayValue value=new RealTimeSampleArrayValue();
					ensureValuesCapacity(value);


					for (int i=startFrameNo;i<frames.size();i++) 
					{
						StreamFrame streamFrame=frames.get(i);
						if (i==startFrameNo)
						{	
							//TODO SSch first sample only???
							rtsa.setHandle(streamFrame.getMetricStateHandle());
							rtsa.setState(streamFrame.getMetricState());
							//									value.setRelativeTimestamp(streamFrame.getTimestamp());
							value.setTimeOfObservation(BigInteger.valueOf(streamFrame.getTimestamp()));
							value.setMeasurementState(streamFrame.getStatus());
						}
						int sizeToAdd=streamFrame.getValues().size();

						if (maxValuesPerPublish-(valueCnt+sizeToAdd)>-1)
						{
							for(BigDecimal vContent: streamFrame.getValues())
							{
								value.getValues().add(vContent);
							}

							valueCnt+=sizeToAdd;
						}else{
							if (Log.isWarn()) Log.warn("Could not add "+sizeToAdd+" samples to WaveformStream .Limit="+maxValuesPerPublish+" Current:"+valueCnt);
						}
						value.setAnnotations(streamFrame.getAnnotations());
						streamFrame.releaseStreamFrame();
					}

					if (!value.getValues().isEmpty())
						rtsa.setObservedValue(value);

					wvStream.getRealTimeSampleArrays().add(rtsa);

				}
			}
		}else{
			//TODO SSch one frame per message

		}

		return wvStream;
	}
	
	
	@SuppressWarnings("rawtypes")
	private static void ensureValuesCapacity(RealTimeSampleArrayValue value) {
		List<BigDecimal> c=value.getValues();
		if (c instanceof ArrayList){
			((ArrayList)c).ensureCapacity(initialSampleListCapacity);
		}
	}
	
	
}
