/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.util.ArrayList;
import java.util.List;

import com.draeger.medical.biceps.device.mdpws.service.builder.ServiceBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.event.DefaultEventReportServicesBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.fastEpisodic.DefaultFastEpisodicServiceBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.get.DefaultGetServicesBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.secure.DefaultContextServicesBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.set.DefaultSetServicesBuilder;
import com.draeger.medical.biceps.device.mdpws.service.builder.impl.waveforms.DefaultWaveformServicesBuilder;
import com.draeger.medical.mdpws.domainmodel.MDPWSDefaultDevice;


public class BICEPSDeviceNode extends MDPWSDefaultDevice {

	public BICEPSDeviceNode(int configurationId) {
		super(configurationId);
        List<ServiceBuilder> builders = createServiceBuilders();
        setupServices(builders);
	}
	
    protected List<ServiceBuilder> createServiceBuilders()
    {
        List<ServiceBuilder> builders = new ArrayList<ServiceBuilder>();
        builders.add(new DefaultGetServicesBuilder(this));
        builders.add(new DefaultContextServicesBuilder(this));
        builders.add(new DefaultEventReportServicesBuilder(this));
        builders.add(new DefaultSetServicesBuilder(this));
        builders.add(new DefaultWaveformServicesBuilder(this));
        builders.add(new DefaultFastEpisodicServiceBuilder(this));
        
//        builders.add(new DefaultAlertServicesBuilder(this));
//        builders.add(new DefaultWaveformServicesBuilder(this));
//        builders.add(new DefaultPatientContextStateServicesBuilder(this));
//        builders.add(new DefaultDeviceContextServicesBuilder(this));
        
        return builders;
    }

    /**
     * Setup the services of this device. Create a service and add it to the list of services hosted by this device.
     * 
     * @param builders
     */
    protected void setupServices(List<ServiceBuilder> builders)
    {
        for (ServiceBuilder builder : builders)
        {
            builder.createServices();
        }
    }

}
