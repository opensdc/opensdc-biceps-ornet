/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.util.concurrent.atomic.AtomicInteger;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public abstract class BICEPSEpisodicEventSource extends BICEPSEventSource{

	public BICEPSEpisodicEventSource(String name, QName portType,
			MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, portType, medicalDeviceInformationBase);
	}
	
	private static final AtomicInteger operationInvokedEventCounter=new AtomicInteger(0);
	public synchronized void fireReport(Object report)
	{
		IParameterValue pv=null;
		try
        {
            pv = createParameterValue(report, getOutput());
            this.fire(pv, operationInvokedEventCounter.addAndGet(1));
        }
        catch (Exception e)
        {
			Log.error(e);
        }
	}
	public abstract Class<?> getHandledReportFromModelDomain();

}
