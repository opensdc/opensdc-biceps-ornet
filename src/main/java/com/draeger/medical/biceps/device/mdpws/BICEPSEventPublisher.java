/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;

class BICEPSEventPublisher
{
	private final int pollingIntervalInMillis;
	private final int initialDelayInMillis;
	private final ScheduledExecutorService pollingService;
	private final BICEPSEventPublisherTask task;
	private BICEPSPeriodicEventSource periodicEventSource;

	public BICEPSEventPublisher(int initialDelayInMillis, int pollingIntervalInMillis,BICEPSEventPublisherTask task)
	{
		this.pollingIntervalInMillis = pollingIntervalInMillis;
		this.initialDelayInMillis = initialDelayInMillis;
		this.pollingService= Executors.newSingleThreadScheduledExecutor(
				new BICEPSThreadFactory("BICEPSEventPublisher-"+task.getClass()+" initDelayMillis: "+initialDelayInMillis+" intervalMillis"+pollingIntervalInMillis)
				);
		this.task = task;
	}

	public void start()
	{
		pollingService.scheduleWithFixedDelay(task, initialDelayInMillis, pollingIntervalInMillis, TimeUnit.MILLISECONDS);
	}

	public void setPeriodicEventSource(
			BICEPSPeriodicEventSource dicePeriodicEventSource) {
		this.periodicEventSource=dicePeriodicEventSource;
		this.task.setEventSource(this.periodicEventSource);
	}
}
