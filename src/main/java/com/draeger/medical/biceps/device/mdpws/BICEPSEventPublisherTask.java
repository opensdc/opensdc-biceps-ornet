/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public abstract class BICEPSEventPublisherTask implements Runnable
{

    private BICEPSPeriodicEventSource eventSource;
    private final MedicalDeviceInformationBase medicalDeviceInformationBase;

    public BICEPSEventPublisherTask(MedicalDeviceInformationBase medicalDeviceInformationBase)
    {
        this.medicalDeviceInformationBase = medicalDeviceInformationBase;
    }

    @Override
	public void run()
    {
        try
        {
            if (getEventSource() != null && getEventSource().getSubscriptionCount() > 0)
            {
                this.medicalDeviceInformationBase.getMdibLock().readLock().lock();

                try
                {
                    Object response = getEventMessage();

                    if (response != null)
                    {
                        getEventSource().publish(response);
                    }
                }
                finally
                {
                    this.medicalDeviceInformationBase.getMdibLock().readLock().unlock();
                }
            }

        }
        catch (Exception e)
        {
            // TODO SSch Logger
            e.printStackTrace();
        }
    }

    protected abstract Object getEventMessage();

    protected BICEPSPeriodicEventSource getEventSource()
    {
        return eventSource;
    }

    protected MedicalDeviceInformationBase getMedicalDeviceInformationBase()
    {
        return medicalDeviceInformationBase;
    }

    protected void setEventSource(BICEPSPeriodicEventSource eventSource)
    {
        this.eventSource = eventSource;
    }

}
