/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import org.ws4d.java.schema.Element;
import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;


public class BICEPSEventSource extends DefaultEventSource {

	private final ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
	private final MedicalDeviceInformationBase medicalDeviceInformationBase;

	public BICEPSEventSource(String name, QName portType, MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, portType);
		this.medicalDeviceInformationBase=medicalDeviceInformationBase;
	}

	public MedicalDeviceInformationBase getMedicalDeviceInformationBase() {
		return medicalDeviceInformationBase;
	}

	@Override
	public int getSubscriptionCount() {
		return super.getSubscriptionCount();
	}

	protected IParameterValue createParameterValue(Object report, Element reportElement) {
		IParameterValue pv=null;
		ParameterValueObjectUtil util=null;
		if (report!=null){
			try{
				util = pool.borrowObject();
				pv=util.convertObject(report, reportElement);

			}catch(Exception e){
				Log.warn(e);
			}finally{
				if (util!=null){
					try {
						pool.returnObject(util);
					} catch (Exception e) 
					{
						Log.warn(e);
					}
				}
			}
		}
		return pv;
	}
}
