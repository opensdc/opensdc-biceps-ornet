/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.structures.Iterator;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.java.util.StatisticsHelper.RunningStat;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractGetResponse;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.mdpws.domainmodel.MDPWSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public abstract class BICEPSOperation extends MDPWSOperation{

	private final ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
	private final MedicalDeviceInformationBase medicalDeviceInformationBase;


	private boolean preInvokeEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.GetAlerts.PreInvoke", "false"));
	private boolean performanceCntEnabled = Boolean.parseBoolean(System.getProperty("BICEPS.DefaultBICEPSDeviceInterface.performanceCntEnabled", "false"));
	public static final double nanoToMilli = Math.pow(10, 6);
	private RunningStat rttRStat = new RunningStat();

	public BICEPSOperation(String name, QName portType,MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, portType);
		this.medicalDeviceInformationBase=medicalDeviceInformationBase;

		if (preInvokeEnabled)
		{
			int maxPreInvoke=200;
			if (Log.isInfo())
				Log.info("Starting "+name);

			IParameterValue pv=null;
			for (int i=0;i<maxPreInvoke;i++){

				try {
					pv=handleInvoke(null,null,null);
					Thread.sleep(30);
				} catch (InterruptedException e) {
					Log.info(e);
				} catch (InvocationException e) {
					Log.info(e);
				} catch (TimeoutException e) {
					Log.info(e);
				}
			}
			Log.info("Started "+name+" "+pv);
		}
	}

	protected MedicalDeviceInformationBase getMedicalDeviceInformationBase() {
		return medicalDeviceInformationBase;
	}

	protected List<String> getHandles(IParameterValue inputValue) {
		//TODO SSch Extract to ServiceHelper ?
		List<String> handles = new ArrayList<String>();

		for (Iterator it = inputValue.children(); it.hasNext();)
		{
			IParameterValue handleParam = (IParameterValue) it.next();
			if (SchemaHelper.HANDLEREF_ELEMENT_QN.equals(handleParam.getName()))
				handles.add(handleParam.toString());
		}
		//TODO Ssch handle IParameterValue for handle extraction
		return handles;
	}

	protected IParameterValue getResultParameterValue(Object response) throws Exception {
		IParameterValue pv=null;
		if (response!=null){
			ParameterValueObjectUtil util = pool.borrowObject();
			pv=util.convertObject(response, getOutput());
			pool.returnObject(util);
		}
		return pv;
	}

	@SuppressWarnings("unchecked")
	protected <T> T getInputValue(
			IParameterValue payload) throws Exception {
		T input=null;
		ParameterValueObjectUtil util = pool.borrowObject();

		input = (T) util.unmarshallParameterValue(payload);
		pool.returnObject(util);
		return input;
	}

	@SuppressWarnings("unchecked")
	protected <M> M createFinalResponseObject(ReadWriteLock lock,
			M modelResponse) {

		//TODO SSch Remove dependency
		if (modelResponse instanceof AbstractGetResponse)
			((AbstractGetResponse)modelResponse).setSequenceNumber(BigInteger.valueOf(getMedicalDeviceInformationBase().getManager().getCurrentSequenceId()));
		
		else if (modelResponse instanceof AbstractSetResponse)
			((AbstractSetResponse)modelResponse).setSequenceNumber(BigInteger.valueOf(getMedicalDeviceInformationBase().getManager().getCurrentSequenceId()));

//		T messageResponse;
//		try{
//			if (lock!=null)
//				lock.readLock().lock();
//
//			messageResponse=(T) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelResponse);
//
//		}finally{
//			if (lock!=null)
//				lock.readLock().unlock();	
//		}
		return modelResponse;
	}

	protected void handleException(Exception e) throws InvocationException {
		String message= e.getMessage();
		while (message==null && e.getCause()!=null)
			message="Cause:"+e.getCause().getMessage();

		Log.error(e);

		Fault f= this.getFault("SystemErrorReport");
		//TODO SSch add informative error message
		throw new InvocationException(f);
	}

	protected IParameterValue createFailedResponse(com.draeger.medical.biceps.common.model.AbstractSetResponse modelResponse, int transactionID) throws Exception {
		modelResponse.setTransactionId(transactionID);
		modelResponse.setInvocationState(com.draeger.medical.biceps.common.model.InvocationState.FAILED);

		AbstractSetResponse messageResponse= createFinalResponseObject(null, modelResponse);
		return getResultParameterValue(messageResponse);
	}


	//public interface implementation for MDPWS stack
	@Override
	public IParameterValue invoke(IParameterValue IParameterValue) throws InvocationException, TimeoutException 
	{
		return invoke(IParameterValue,null,null);
	}

	@Override
	public IParameterValue invoke(IParameterValue IParameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap)
					throws InvocationException, TimeoutException {
		long t1=System.nanoTime();
		IParameterValue retVal=handleInvoke(IParameterValue, msgContextMap, replyMessageCtxtMap);
		handleInvokeDone(t1);
		return retVal;
	}


	//abstract
	protected abstract IParameterValue handleInvoke(IParameterValue IParameterValue, MDPWSMessageContextMap msgContextMap, MDPWSMessageContextMap replyMessageCtxtMap) throws InvocationException, TimeoutException;


	//helper methods
	private void handleInvokeDone(long t1) {
		if (performanceCntEnabled)
		{
			long t2=System.nanoTime();
			long rtt = (long) ((t2 - t1) / nanoToMilli);
			rttRStat.push(rtt);
			if (rttRStat.cnt % 30 == 0)
			{
				System.out.println("[GetMetricsValue];"+rttRStat.toString());
			}
		}
	}
}
