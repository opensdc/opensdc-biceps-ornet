/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.util.concurrent.atomic.AtomicInteger;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;


public class BICEPSPeriodicEventSource extends BICEPSEventSource{

	private final ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
	private final BICEPSEventPublisher publisher;
	private static AtomicInteger eventCounter = new AtomicInteger();

	public BICEPSPeriodicEventSource(String name, QName portType,
			MedicalDeviceInformationBase medicalDeviceInformationBase, int initialDelayInMillis, int pollingIntervalInMillis, BICEPSEventPublisherTask task) {
		super(name, portType, medicalDeviceInformationBase);
		this.publisher=new BICEPSEventPublisher(initialDelayInMillis,pollingIntervalInMillis,task);
		this.publisher.setPeriodicEventSource(this);
		this.publisher.start();
	}

	protected BICEPSEventPublisher getPublisher() {
		return publisher;
	}

	protected IParameterValue getResultParameterValue(Object msg) throws Exception {
//		return JAXBBuilder.getInstance().convertObject(msg, getOutput());
		ParameterValueObjectUtil util = pool.borrowObject();
		IParameterValue pv=util.convertObject(msg, getOutput());
		pool.returnObject(util);
		return pv;
	}

	public void publish(Object msg)
	{
		if (getSubscriptionCount()>0 && msg !=null)
		{
			try{
				IParameterValue eventValue=getResultParameterValue(msg);
				fire(eventValue, eventCounter.addAndGet(1));
			}catch(Exception e)
			{
				//TODO SSch Logger
				e.printStackTrace();
			}
		}
	}


}
