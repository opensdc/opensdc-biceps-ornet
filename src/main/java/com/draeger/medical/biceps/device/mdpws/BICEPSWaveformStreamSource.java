/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider;
import com.draeger.medical.biceps.common.PVObjectUtilPoolProvider.ParameterValueObjectUtilPool;
import com.draeger.medical.biceps.common.ParameterValueObjectUtil;
import com.draeger.medical.biceps.common.model.WaveformStream;
import com.draeger.medical.biceps.common.utils.BICEPSThreadFactory;
import com.draeger.medical.biceps.device.mdi.interaction.stream.StreamFrame;
import com.draeger.medical.biceps.device.mdib.impl.BICEPSWaveformPublisher;
import com.draeger.medical.mdpws.domainmodel.impl.device.DefaultStreamSource;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamConfiguration;

public class BICEPSWaveformStreamSource extends DefaultStreamSource {

	private final ParameterValueObjectUtilPool pool = PVObjectUtilPoolProvider.getInstance().getPool();
	private final BlockingQueue<? extends StreamFrame> streamQueue;
	private final BICEPSWaveformPublisher publisher;
	private static AtomicInteger streamCounter = new AtomicInteger();

	public BICEPSWaveformStreamSource(String name, QName portType,
			StreamConfiguration streamConfig, BlockingQueue<? extends StreamFrame> streamQueue) {
		super(name, portType, streamConfig);
		this.streamQueue=streamQueue;
		this.publisher=new BICEPSWaveformPublisher(this, this.streamQueue);
		startStreamSource();
	}

	public void startStreamSource() {
		this.publisher.setRunning(true);
		Thread publisherThread=new BICEPSThreadFactory("BICEPSWaveformStreamSource-"+this+"-"+this.publisher).newThread(this.publisher);
		publisherThread.setDaemon(true);
		publisherThread.setPriority(Thread.MAX_PRIORITY);
		publisherThread.start();
	}

	public void stopStreamSource()
	{
		this.publisher.setRunning(false);
	}

	public void publish(WaveformStream wvStream) 
	{
		if (getSubscriptionCount()>0 && wvStream !=null)
		{
			try{
				IParameterValue eventValue=getResultParameterValue(wvStream);
				fire(eventValue, streamCounter.addAndGet(1));
			}catch(Exception e)
			{
				Log.warn(e);
			}
		}
	}


	protected IParameterValue getResultParameterValue(final Object modelResponse) throws Exception {

		IParameterValue pv=null;


		ParameterValueObjectUtil util = pool.borrowObject();

		try{
			pv=util.convertObject(modelResponse, getOutput());
		}finally{
			if (util!=null) pool.returnObject(util);
		}
		return pv;
	}

}
