/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.mdpws.domainmodel.StreamSourceCommons;

public class OperationDescriptionRegistry
{
    public static final OperationDescriptionRegistry instance = new OperationDescriptionRegistry();

    public static OperationDescriptionRegistry getInstance()
    {
        return instance;
    }

    public final Map<String, OperationDescription> streamMap = Collections.synchronizedMap(new HashMap<String, OperationDescription>());
    public final Map<String, OperationDescription> map = Collections.synchronizedMap(new HashMap<String, OperationDescription>());

    
    public OperationDescription lookup(String name, Class<?> descriptionClazz)
    {
         OperationDescription operationDescription =null;
         
         if (descriptionClazz!=null && StreamSourceCommons.class.isAssignableFrom(descriptionClazz))
         {
        	 operationDescription= streamMap.get(name);
         }else{
        	 operationDescription=map.get(name);
        	 
        	 if (operationDescription==null)
        		 operationDescription= streamMap.get(name);
         }
         
         return operationDescription;
    }

    public OperationDescription put(OperationDescription op)
    {
    	if(op instanceof StreamSourceCommons)
    		return streamMap.put(QNameFactory.getInstance().getQName(op.getName(),op.getPortType().toStringPlain()).toStringPlain(), op);
    	
    	if(op!=null)
    		return map.put(QNameFactory.getInstance().getQName(op.getName(),op.getPortType().toStringPlain()).toStringPlain(), op);
    	
    	return null;
    }

    public OperationDescription put(String qName, OperationDescription op)
    {
    	if(op instanceof StreamSourceCommons)
    		return streamMap.put(qName, op);
    	
    	if(op!=null)
    		return map.put(qName, op);
    	
    	return null;
    }
}
