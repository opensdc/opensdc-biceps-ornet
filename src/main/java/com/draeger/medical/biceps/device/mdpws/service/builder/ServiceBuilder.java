/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder;

import org.ws4d.java.service.DefaultEventSource;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.OperationDescription;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.mdpws.domainmodel.impl.device.DefaultStreamSource;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public abstract class ServiceBuilder {

	protected final BICEPSDeviceNode medicalDevice;

	public ServiceBuilder(BICEPSDeviceNode medicalDevice) {
		this.medicalDevice = medicalDevice;
	}

	public void createServices() {
    	addServiceIfRequired(makeGetService());
		addServiceIfRequired(makeEventReportService());
		addServiceIfRequired(makeSetService());
        addServiceIfRequired(makeActionService());
	}

	protected MDPWSService makeGetService() {
		return null;
	}

	protected MDPWSService makeEventReportService() {
		return null;
	}

	protected MDPWSService makeSetService() {
		return null;
	}

	protected MDPWSService makeActionService() {
		return null;
	}

    protected void addServiceIfRequired(MDPWSService service) {
		if (service != null){			
			if (hasAttachedSource(service)) {
				medicalDevice.addService(service);
			}
		}
	}

	private boolean hasAttachedSource(MDPWSService service) {
		return (service.getStreamSources().hasNext()
				|| service.getEventSources().hasNext() || service
				.getOperations().hasNext());
	}
	
	protected void addOperationToService(MDPWSService service, String operationFullQualifiedName, Class<?> operationClazz) {
		OperationDescription description = lookupOperationDescription(operationFullQualifiedName, operationClazz);

		if (description instanceof DefaultStreamSource) {
			service.addStreamSource((DefaultStreamSource) description);
		} else if (description instanceof DefaultEventSource) {
			service.addEventSource((DefaultEventSource) description);
		} else if (description instanceof Operation) {
			service.addOperation((Operation) description);
		} else {
			Log.info("Could not add Operation '" + operationFullQualifiedName
					+ "' to Service '" + service + "' for Device "
					+ this.getClass().getName());
		}
	}

	protected void addOperationToService(MDPWSService service, String operationFullQualifiedName) {
		addOperationToService(service, operationFullQualifiedName,null);
	}

	private OperationDescription lookupOperationDescription(String operationFullQualifiedName, Class<?> operationClazz) {
		OperationDescriptionRegistry registry = OperationDescriptionRegistry.getInstance();
		OperationDescription description = registry.lookup(operationFullQualifiedName, operationClazz);

		return description;
	}
}
