/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.event;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicAlertEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicMetricEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.MDSCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.MDSDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.ObjectCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.ObjectDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationCreatedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationDeletedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationInvokedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.OperationalStateChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.SystemErrorEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicAlertEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicMetricEventReport;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public class DefaultEventReportServicesBuilder extends EventServicesBuilder
{
    public DefaultEventReportServicesBuilder(BICEPSDeviceNode medicalDevice)
    {
        super(medicalDevice);
    }

    
    @Override
	protected MDPWSService makeEventReportService()
    {
        MDPWSService service = new MDPWSService(EVENT_REPORT_SERVICE_CONFIG_ID);
        addOperationToService(service, PeriodicMetricEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, EpisodicMetricEventReport.QUALIFIED_NAME.toStringPlain());
        
        addOperationToService(service, PeriodicAlertEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, EpisodicAlertEventReport.QUALIFIED_NAME.toStringPlain());
       
        addOperationToService(service, OperationInvokedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, OperationalStateChangedEventReport.QUALIFIED_NAME.toStringPlain());

        addOperationToService(service, ObjectCreatedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, ObjectDeletedEventReport.QUALIFIED_NAME.toStringPlain());
       
        addOperationToService(service, MDSCreatedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, MDSDeletedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, OperationCreatedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, OperationDeletedEventReport.QUALIFIED_NAME.toStringPlain());
        
        addOperationToService(service, SystemErrorEventReport.QUALIFIED_NAME.toStringPlain());
       
//      addOperationToService(service, MDSAttributeUpdateEventReport.MDS_ATTR_UER_QN.toStringPlain());
//      addOperationToService(service, ClockDateTimeChangedEventReport.CLOCK_DTCR_QN.toStringPlain());

        
        return service;
    }

}
