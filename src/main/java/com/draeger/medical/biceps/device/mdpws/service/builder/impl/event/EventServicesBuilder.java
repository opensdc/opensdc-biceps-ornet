/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.event;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.builder.ServiceBuilder;

public abstract class EventServicesBuilder extends ServiceBuilder
{
    protected static int EVENT_REPORT_SERVICE_CONFIG_ID = 13;

    public EventServicesBuilder(BICEPSDeviceNode medicalDevice)
    {
        super(medicalDevice);
    }
}
