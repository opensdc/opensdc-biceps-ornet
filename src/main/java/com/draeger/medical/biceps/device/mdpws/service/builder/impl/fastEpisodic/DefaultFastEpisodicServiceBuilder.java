/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.fastEpisodic;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.event.FastEpisodicEventReport;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public class DefaultFastEpisodicServiceBuilder extends FastEpisodicServicesBuilder  
{
	public DefaultFastEpisodicServiceBuilder(BICEPSDeviceNode medicalDevice) {
		super(medicalDevice);
	}

	@Override
	protected MDPWSService makeEventReportService() {
    	MDPWSService service = new MDPWSService(EVENT_REPORT_SERVICE_CONFIG_ID);
    	addOperationToService(service, FastEpisodicEventReport.QUALIFIED_NAME.toStringPlain());
    	return service;
	}
}
