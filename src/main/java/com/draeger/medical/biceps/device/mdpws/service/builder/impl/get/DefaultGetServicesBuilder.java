/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.get;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.get.GetContainmentTree;
import com.draeger.medical.biceps.device.mdpws.service.get.GetDescriptor;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDDescription;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDIB;
import com.draeger.medical.biceps.device.mdpws.service.get.GetMDState;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public class DefaultGetServicesBuilder extends GetServicesBuilder  
{
	public DefaultGetServicesBuilder(BICEPSDeviceNode medicalDevice) {
		super(medicalDevice);
	}

	@Override
	protected MDPWSService makeGetService() {
    	MDPWSService service = new MDPWSService(GET_SERVICE_CONFIG_ID);
    	
//    	addOperationToService(service, GetAlertStates.GET_ALERT_STATES_QN.toStringPlain());

    	addOperationToService(service, GetMDIB.GET_MDIB_QN.toStringPlain());
    	addOperationToService(service, GetMDState.GET_MDSTATE_QN.toStringPlain());
    	addOperationToService(service, GetMDDescription.GET_MDDESCRIPTION_QN.toStringPlain());
//    	addOperationToService(service, GetMetricStates.GET_METRIC_STATES_QN.toStringPlain());
    	addOperationToService(service, GetContainmentTree.GET_CONTAINMENTTREE_QN.toStringPlain());
    	addOperationToService(service, GetDescriptor.GET_DESCRIPTOR_QN.toStringPlain());
    	
       	return service;
	}

}
