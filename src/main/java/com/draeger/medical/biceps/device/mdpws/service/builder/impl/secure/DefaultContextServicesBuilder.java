/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.secure;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.event.EpisodicContextChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.event.periodic.PeriodicContextChangedEventReport;
import com.draeger.medical.biceps.device.mdpws.service.get.GetContextStates;
import com.draeger.medical.biceps.device.mdpws.service.set.SetContextState;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public class DefaultContextServicesBuilder extends ProtectedHealthInformationServicesBuilder  
{
	public DefaultContextServicesBuilder(BICEPSDeviceNode medicalDevice) {
		super(medicalDevice);
	}

	@Override
	protected MDPWSService makeGetService() {
    	MDPWSService service = new MDPWSService(GET_SERVICE_CONFIG_ID);
//    	addOperationToService(service, GetIdContextStates.GET_ID_CONTEXTSTATES_QN.toStringPlain());
    	addOperationToService(service, GetContextStates.GET_CONTEXT_STATES_QN.toStringPlain());
//    	addOperationToService(service, SetContextIdState.SET_CONTEXT_ID_STATE_QN.toStringPlain());
        addOperationToService(service, SetContextState.SET_ASSOCIATION_STATE.toStringPlain());
        
//        addOperationToService(service, PeriodicPatientContextStateEventReport.QUALIFIED_NAME.toStringPlain());
//        addOperationToService(service, EpisodicPatientContextStateEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, PeriodicContextChangedEventReport.QUALIFIED_NAME.toStringPlain());
        addOperationToService(service, EpisodicContextChangedEventReport.QUALIFIED_NAME.toStringPlain());
       
    	return service;
	}

}
