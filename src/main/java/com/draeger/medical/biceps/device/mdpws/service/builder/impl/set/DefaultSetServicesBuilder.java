/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.set;


import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.set.ActivateOperation;
import com.draeger.medical.biceps.device.mdpws.service.set.SetCurrentAlertConditionState;
import com.draeger.medical.biceps.device.mdpws.service.set.SetRangeAction;
import com.draeger.medical.biceps.device.mdpws.service.set.SetString;
import com.draeger.medical.biceps.device.mdpws.service.set.SetValue;
import com.draeger.medical.mdpws.domainmodel.impl.device.MDPWSService;

public class DefaultSetServicesBuilder extends SetServicesBuilder
{
    public DefaultSetServicesBuilder(BICEPSDeviceNode medicalDevice)
    {
        super(medicalDevice);
    }

    
    @Override
	protected MDPWSService makeSetService()
    {
        MDPWSService service = new MDPWSService(SET_SERVICE_CONFIG_ID);
        addOperationToService(service, SetValue.SET_VALUE_QN.toStringPlain());
        addOperationToService(service, SetString.SET_STRING_QN.toStringPlain());
        addOperationToService(service, SetRangeAction.SET_RANGE_QN.toStringPlain());
        addOperationToService(service, SetCurrentAlertConditionState.LIMIT_ALERT_QN.toStringPlain());
        addOperationToService(service, ActivateOperation.ACTIVATE_QN.toStringPlain());
        
        return service;
    }

}
