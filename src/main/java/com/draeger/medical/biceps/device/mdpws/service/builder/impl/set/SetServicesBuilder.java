/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.builder.impl.set;

import com.draeger.medical.biceps.device.mdpws.BICEPSDeviceNode;
import com.draeger.medical.biceps.device.mdpws.service.builder.ServiceBuilder;

public abstract class SetServicesBuilder extends ServiceBuilder
{
	protected static int SET_SERVICE_CONFIG_ID = 12;

    public SetServicesBuilder(BICEPSDeviceNode medicalDevice)
    {
        super(medicalDevice);
    }
}
