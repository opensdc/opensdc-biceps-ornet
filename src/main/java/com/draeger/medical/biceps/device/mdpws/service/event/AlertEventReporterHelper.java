/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;

import com.draeger.medical.biceps.common.model.AbstractAlertState;
import com.draeger.medical.biceps.common.model.AlertReportPart;

public class AlertEventReporterHelper {

	public static Object getEventMessageForReport(com.draeger.medical.biceps.common.model.AbstractAlertReport modelReport, ReadWriteLock mdibLock,HashMap<String, Collection<? extends AbstractAlertState>> reportInfo, AtomicInteger eventCounter) {
//		return modelReport;
		
//		AbstractAlertReport alertReport=null;
//
		if (reportInfo!=null && eventCounter!=null)
		{
			Set<Entry<String, Collection<? extends AbstractAlertState>>> mdsList = reportInfo.entrySet();
			if (mdsList!=null)
			{
				for (Entry<String, Collection<? extends AbstractAlertState>> reportEntryPerMDS : mdsList) 
				{
					com.draeger.medical.biceps.common.model.AlertReportPart modelReportPart=new AlertReportPart();
					Collection<? extends AbstractAlertState> modelAlertStates = reportEntryPerMDS.getValue();
					if(modelAlertStates!=null){
						modelReportPart.getAlertStates().addAll(modelAlertStates);
					}
					modelReport.getReportParts().add(modelReportPart);
					
					modelReport.setSequenceNumber(BigInteger.valueOf(eventCounter.getAndAdd(1)));
				}

			}
			
		}
		return modelReport;
//
//		return alertReport;
	}


//	private static <M extends com.draeger.medical.biceps.common.model.AbstractAlertReport> AbstractAlertReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
//	{	
//
//
//		AbstractAlertReport messageResponse=null;
//		if (modelReport.getReportParts()!=null && modelReport.getReportParts().size()>0)
//		{
//			try{
//				if (lock!=null)
//					lock.readLock().lock();
//
//				messageResponse=(AbstractAlertReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
//
//				if (Log.isDebug())
//					Log.debug("Alerts: "+messageResponse.getReportParts().size());
//
//				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.getAndAdd(1)));
//			}finally{
//				if (lock!=null)
//					lock.readLock().unlock();	
//			}
//		}
//		return messageResponse;
//	}

}
