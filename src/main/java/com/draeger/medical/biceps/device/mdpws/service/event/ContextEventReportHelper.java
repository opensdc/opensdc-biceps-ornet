/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;

import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.ContextChangedReportPart;

public class ContextEventReportHelper {
	public static Object getEventMessageForReport(com.draeger.medical.biceps.common.model.EpisodicContextChangedReport modelReport, ReadWriteLock mdibLock,AtomicInteger eventCounter) {

//		AbstractContextChangedReport metricReport=null;

		if (modelReport!=null && eventCounter!=null)
		{
//			modelReport=convertModelToMessage(modelReport,mdibLock, eventCounter);
			modelReport.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
		}

		return modelReport;
	}
	
	public static Object getEventMessageForReport(com.draeger.medical.biceps.common.model.PeriodicContextChangedReport modelReport, ReadWriteLock mdibLock,HashMap<String, Collection<? extends AbstractIdentifiableContextState>> reportInfo,AtomicInteger eventCounter) {
		if (modelReport!=null && eventCounter!=null)
		{
			Set<Entry<String, Collection<? extends AbstractIdentifiableContextState>>> mdsList = reportInfo.entrySet();
			if (mdsList!=null)
			{
				for (Entry<String, Collection<? extends AbstractIdentifiableContextState>> reportEntryPerMDS : mdsList) 
				{
					com.draeger.medical.biceps.common.model.ContextChangedReportPart modelReportPart=new ContextChangedReportPart();
					Collection<? extends AbstractIdentifiableContextState> modelStates = reportEntryPerMDS.getValue();
					if(modelStates!=null){
						for (AbstractIdentifiableContextState ContextAssociationStateValue : modelStates) {
							modelReportPart.getChangedContextStates().add(ContextAssociationStateValue.getReferencedDescriptor());
						}
						
					}
					modelReport.getReportParts().add(modelReportPart);
				}

			}
			modelReport.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
		}

		return modelReport;
	}


//	private static <M extends com.draeger.medical.biceps.common.model.AbstractContextChangedReport> AbstractContextChangedReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
//	{	
//
//
//		AbstractContextChangedReport messageResponse=null;
//		if (modelReport.getReportParts()!=null && modelReport.getReportParts().size()>0)
//		{
//			try{
//				if (lock!=null)
//					lock.readLock().lock();
//
//				messageResponse=(AbstractContextChangedReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
//
//				if (Log.isDebug())
//					Log.debug("Patient States Parts: "+messageResponse.getReportParts().size());
//
//				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
//			}finally{
//				if (lock!=null)
//					lock.readLock().unlock();	
//			}
//		}
//		return messageResponse;
//	}
//	
	
//	private static <M extends com.draeger.medical.biceps.common.model.AbstractPatientDataReport> AbstractPatientDataReport convertModelToMessagePD(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
//	{	
//
//
//		AbstractPatientDataReport messageResponse=null;
//		if (modelReport.getReportParts()!=null && modelReport.getReportParts().size()>0)
//		{
//			try{
//				if (lock!=null)
//					lock.readLock().lock();
//
//				messageResponse=(AbstractPatientDataReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
//
//				if (Log.isDebug())
//					Log.debug("Patient States Parts: "+messageResponse.getReportParts().size());
//
//				messageResponse.setSequenceNumber(eventCounter.get());
//			}finally{
//				if (lock!=null)
//					lock.readLock().unlock();	
//			}
//		}
//		return messageResponse;
//	}
//
//
//	public static Object getEventMessageForPDReport(
//			com.draeger.medical.biceps.common.model.AbstractPatientDataReport modelReport, ReadWriteLock mdibLock,
//			AtomicInteger eventCounter) {
//		AbstractPatientDataReport metricReport=null;
//
//		if (modelReport!=null && eventCounter!=null)
//		{
//			metricReport=convertModelToMessagePD(modelReport,mdibLock, eventCounter);
//		}
//
//		return metricReport;
//	}
}
