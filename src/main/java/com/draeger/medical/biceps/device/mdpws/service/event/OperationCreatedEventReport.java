/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.OperationCreatedReport;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSEpisodicEventSource;

public class OperationCreatedEventReport extends BICEPSEpisodicEventSource {
	static final String name     = SchemaHelper.OPERATION_CREATED_REPORT_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_REPORT_SERVICE;
	public static final QName QUALIFIED_NAME=QNameFactory.getInstance().getQName(name,portType);

	public OperationCreatedEventReport(
			MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, QNameFactory.getInstance().getQName(portType), medicalDeviceInformationBase);
		setOutput(SchemaHelper.getInstance().getOperationCreatedReportElement());
	}

	
	@Override
	public Class<OperationCreatedReport> getHandledReportFromModelDomain() {
		return OperationCreatedReport.class;
	}
	
}
