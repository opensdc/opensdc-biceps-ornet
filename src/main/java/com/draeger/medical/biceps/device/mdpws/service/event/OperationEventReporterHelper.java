/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event;

import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.DescriptionModificationReportPart;
import com.draeger.medical.biceps.common.model.ObjectCreatedReport;
import com.draeger.medical.biceps.common.model.OperationInvokedReport;
import com.draeger.medical.biceps.common.model.OperationInvokedReportPart;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReport;
import com.draeger.medical.biceps.common.model.OperationalStateChangedReportPart;
import com.draeger.medical.biceps.common.model.WaveformStream;

public class OperationEventReporterHelper {

	public static Object getEventMessageForReport(ObjectCreatedReport modelReport, ReadWriteLock mdibLock, List<DescriptionModificationReportPart> reportInfos, AtomicInteger eventCounter) {

		ObjectCreatedReport opInvokedReport=null;

		if (reportInfos!=null && eventCounter!=null)
		{
			modelReport.getReportDetails().addAll(reportInfos);
			opInvokedReport=convertModelToMessage(modelReport,mdibLock, eventCounter);
		}

		return opInvokedReport;
	}
	
	public static WaveformStream getEventMessageForReport(com.draeger.medical.biceps.common.model.WaveformStream modelReport, ReadWriteLock mdibLock, AtomicInteger eventCounter) {

		WaveformStream messageResponse=null;

		if (eventCounter!=null)
		{
			if(modelReport!=null)
			{
				try{

//					messageResponse=(WaveformStream) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
					messageResponse=modelReport;
//					messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));

					if (Log.isDebug()) 
						Log.debug("WaveformStream #SA: "+(modelReport.getRealTimeSampleArrays()!=null?modelReport.getRealTimeSampleArrays().size():"0"));

				}finally{
					//void
				}
			}
		}
		
		System.out.println("WaveformStream #SA: "+(modelReport.getRealTimeSampleArrays()!=null?modelReport.getRealTimeSampleArrays().size():"0"));
		
		return messageResponse;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends OperationInvokedReport> T getEventMessageForReport(T modelReport, ReadWriteLock mdibLock, List<OperationInvokedReportPart> reportInfos, AtomicInteger eventCounter) {

		T opInvokedReport=null;

		if (reportInfos!=null && eventCounter!=null)
		{
			modelReport.getReportDetails().addAll(reportInfos);
			opInvokedReport=(T)convertModelToMessage(modelReport,mdibLock, eventCounter);
		}

		return opInvokedReport;
	}
	
	public static Object getEventMessageForReport(com.draeger.medical.biceps.common.model.OperationalStateChangedReport modelReport, ReadWriteLock mdibLock, List<OperationalStateChangedReportPart> reportInfos, AtomicInteger eventCounter) {

		OperationalStateChangedReport opInvokedReport=null;

		if (reportInfos!=null && eventCounter!=null)
		{
			modelReport.getReportDetails().addAll(reportInfos);
			opInvokedReport=convertModelToMessage(modelReport,mdibLock, eventCounter);
		}

		return opInvokedReport;
	}

	private static <M extends ObjectCreatedReport> ObjectCreatedReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
	{	
		ObjectCreatedReport messageResponse=null;
		if( modelReport.getReportDetails()!=null && modelReport.getReportDetails().size()>0)
		{
			try{
				if (lock!=null)
					lock.readLock().lock();

//				messageResponse=(ObjectCreatedReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
				messageResponse=modelReport;

				if (Log.isDebug()) Log.debug("ObjectCreatedReport Details: "+messageResponse.getReportDetails().size());

				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.getAndAdd(1)));
			}finally{
				if (lock!=null) lock.readLock().unlock();	
			}
		}
		return messageResponse;
	}

	private static <M extends OperationInvokedReport> OperationInvokedReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
	{	
		OperationInvokedReport messageResponse=null;
		if( modelReport.getReportDetails()!=null && modelReport.getReportDetails().size()>0)
		{
			try{
				if (lock!=null)
					lock.readLock().lock();

//				messageResponse=(OperationInvokedReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
				messageResponse=modelReport;

				if (Log.isDebug())
					Log.debug("OperationInvoked Details: "+messageResponse.getReportDetails().size());

				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
			}finally{
				if (lock!=null)
					lock.readLock().unlock();	
			}
		}
		return messageResponse;
	}
	
	private static <M extends com.draeger.medical.biceps.common.model.OperationalStateChangedReport> OperationalStateChangedReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
	{	
		OperationalStateChangedReport messageResponse=null;
		if( modelReport.getReportDetails()!=null && modelReport.getReportDetails().size()>0)
		{
			try{
				if (lock!=null)
					lock.readLock().lock();

//				messageResponse=(OperationalStateChangedReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
				messageResponse=modelReport;

				if (Log.isDebug())
					Log.debug("OperationalStateChangedReport Details: "+messageResponse.getReportDetails().size());

				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
			}finally{
				if (lock!=null)
					lock.readLock().unlock();	
			}
		}
		return messageResponse;
	}


}
