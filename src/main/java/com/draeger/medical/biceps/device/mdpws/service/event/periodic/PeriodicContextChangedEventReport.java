/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event.periodic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.ContextAssociationStateValue;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSEventPublisherTask;
import com.draeger.medical.biceps.device.mdpws.BICEPSPeriodicEventSource;
import com.draeger.medical.biceps.device.mdpws.service.event.ContextEventReportHelper;

public class PeriodicContextChangedEventReport extends BICEPSPeriodicEventSource {
	static final String name     = SchemaHelper.PERIODIC_CONTEXT_CHANGED_REPORT_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_CONTEXT_SERVICE;
	public static final QName QUALIFIED_NAME=QNameFactory.getInstance().getQName(name,portType);

	public PeriodicContextChangedEventReport(MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase, 2000, 2000,new ContextPublisherTask(medicalDeviceInformationBase));
		setOutput(SchemaHelper.getInstance().getPeriodicContextChangedReportElement());
	}

	
	public Class<PeriodicContextChangedEventReport> getHandledReportFromModelDomain() {
		return PeriodicContextChangedEventReport.class;
	}
	
	public static class ContextPublisherTask extends BICEPSEventPublisherTask{

		private static AtomicInteger eventCounter = new AtomicInteger();
		

		public ContextPublisherTask(MedicalDeviceInformationBase medicalDeviceInformationBase) 
		{
			super(medicalDeviceInformationBase);
		}

		@Override
		protected Object getEventMessage() {
			
			Collection<? extends MDSDescriptor> mdsList = getMedicalDeviceInformationBase().getMDSDescriptions(null, true);
			HashMap<String,Collection<? extends AbstractIdentifiableContextState>> reportInfo=new HashMap<String,Collection<? extends AbstractIdentifiableContextState>>();
			if (mdsList!=null)
			{
				ArrayList<String> handleList= new ArrayList<String>();
				for (MDSDescriptor mds : mdsList) {
					String mdsHandle=mds.getHandle();

					handleList.clear();
					handleList.add(mdsHandle);
					Collection<AbstractIdentifiableContextState> contextStates=getMedicalDeviceInformationBase().getIdentifiableContextState(handleList, AbstractIdentifiableContextState.class);

					if(contextStates!=null)
					{
						reportInfo.put(mdsHandle, contextStates);
					}

				}

			}
			com.draeger.medical.biceps.common.model.PeriodicContextChangedReport modelReport=new com.draeger.medical.biceps.common.model.PeriodicContextChangedReport();
			return ContextEventReportHelper.getEventMessageForReport(modelReport,getMedicalDeviceInformationBase().getMdibLock(), reportInfo, eventCounter);
		}


	}

}
