/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.event.periodic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.util.MetricDescriptorUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSEventPublisherTask;
import com.draeger.medical.biceps.device.mdpws.BICEPSPeriodicEventSource;
import com.draeger.medical.biceps.device.mdpws.service.helper.MetricEventReporterHelper;


public class PeriodicMetricEventReport extends BICEPSPeriodicEventSource {

	static final String name     = SchemaHelper.PERIODIC_METRIC_REPORT_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_REPORT_SERVICE;
	public static final QName QUALIFIED_NAME=QNameFactory.getInstance().getQName(name,portType);

	public PeriodicMetricEventReport(MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase, 2000, 2000,new MetricPublisherTask(medicalDeviceInformationBase));
		setOutput(SchemaHelper.getInstance().getPeriodicMetricReportElement());
	}

	public static class MetricPublisherTask extends BICEPSEventPublisherTask{

		private static AtomicInteger eventCounter = new AtomicInteger();

		public MetricPublisherTask(MedicalDeviceInformationBase medicalDeviceInformationBase) 
		{
			super(medicalDeviceInformationBase);
		}


		@Override
		protected Object getEventMessage() {
			Collection<? extends MDSDescriptor> mdsList = getMedicalDeviceInformationBase().getMDSDescriptions(null, true);
			HashMap<String,Collection<? extends AbstractMetricState>> reportInfo=new HashMap<String,Collection<? extends AbstractMetricState>>();
			if (mdsList!=null)
			{
				ArrayList<String> handleList= new ArrayList<String>();
				for (MDSDescriptor mds : mdsList) {
					String mdsHandle=mds.getHandle();

					handleList.clear();
					handleList.add(mdsHandle);
					Collection<? extends AbstractMetricState> modelMetricStates=getFilteredMetricStates(handleList);

					if(modelMetricStates!=null)
					{
						if (!modelMetricStates.isEmpty()){
							reportInfo.put(mdsHandle, modelMetricStates);
						}
					}

				}

			}
			com.draeger.medical.biceps.common.model.PeriodicMetricReport modelReport=new com.draeger.medical.biceps.common.model.PeriodicMetricReport();
			return MetricEventReporterHelper.getEventMessageForReport(modelReport,getMedicalDeviceInformationBase().getMdibLock(), reportInfo, eventCounter);

		}


		private Collection<? extends AbstractMetricState> getFilteredMetricStates(
				ArrayList<String> handleList) {

			Collection<? extends AbstractMetricState> metricStates = getMedicalDeviceInformationBase().getMetricStates(handleList);
			if(metricStates!=null)
			{
				Collection<? extends MetricDescriptor> descriptorList = getMedicalDeviceInformationBase().getMetricDescriptors(null);
				metricStates=MetricDescriptorUtil.filterMetricStatesByRetrievability(descriptorList,metricStates,MetricRetrievability.PERIODIC);
			}
			return metricStates;
		}




	}
}
