/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.GetContainmentTreeResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetContainmentTree extends BICEPSOperation {

	private static final String name     = SchemaHelper.GET_CONTAINMENTTREE_ELEMENT_NAME;
	private static final String portType = MDPWSConstants.PORTTYPE_GET_SERVICE;
	public static final QName GET_CONTAINMENTTREE_QN=QNameFactory.getInstance().getQName(name,portType);


	public GetContainmentTree(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType), medicalDeviceInformationBase);
		setInput(SchemaHelper.getInstance().getGetContainmentTreeElement());
		setOutput(SchemaHelper.getInstance().getGetContainmentTreeResponseElement());
	}




	@Override
	protected IParameterValue handleInvoke(IParameterValue requestPV,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
		IParameterValue result = null;
		List<String> handles = getHandles(requestPV);
		try
		{
			//		System.out.println(handles);
			MedicalDeviceInformationBase mdib=getMedicalDeviceInformationBase();
			ReadWriteLock lock= mdib.getMdibLock();
			com.draeger.medical.biceps.common.model.GetContainmentTree request = getInputValue(requestPV);
			ContainmentTree cTreeInfo=getContainmentTreeInfo(mdib,handles, request);
			GetContainmentTreeResponse response = convertModelToMessage(cTreeInfo, lock);

			result = getResultParameterValue(response);
		}
		catch (Exception e)
		{
			if (Log.isWarn())
				Log.warn(e);
		}
		return result;
	}




	private GetContainmentTreeResponse convertModelToMessage(
			ContainmentTree cTreeInfo, ReadWriteLock lock) {
		com.draeger.medical.biceps.common.model.GetContainmentTreeResponse modelResponse=new com.draeger.medical.biceps.common.model.GetContainmentTreeResponse();
		modelResponse.setContainmentTree(cTreeInfo);
		GetContainmentTreeResponse response=createFinalResponseObject(lock, modelResponse);
		return response;
	}



	protected ContainmentTree getContainmentTreeInfo(MedicalDeviceInformationBase mdib, List<String> handles, com.draeger.medical.biceps.common.model.GetContainmentTree request) 
	{
		ContainmentTree retVal=null;

		if (mdib!=null)
		{

			if (handles!=null && !handles.isEmpty())
			{
				for (String handle : handles) {
					retVal=retrieveCTree(mdib, handle);
				}	
			}else{
				retVal=retrieveCTree(mdib,  null);
			}

		}
		
		if (retVal==null)
			retVal=new ContainmentTree();
		
		return retVal;
	}




	private ContainmentTree retrieveCTree(MedicalDeviceInformationBase mdib, String handle) {
		return mdib.getContainmentTree(handle);
	}





}
