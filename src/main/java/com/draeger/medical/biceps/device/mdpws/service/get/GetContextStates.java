/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractIdentifiableContextState;
import com.draeger.medical.biceps.common.model.GetContextStatesResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetContextStates extends BICEPSOperation {

    static final String name     = SchemaHelper.GET_CONTEXT_STATES_ELEMENT_NAME;
    static final String portType = MDPWSConstants.PORTTYPE_CONTEXT_SERVICE;
    public static final QName GET_CONTEXT_STATES_QN=QNameFactory.getInstance().getQName(name,portType);
	
    public GetContextStates(MedicalDeviceInformationBase medicalDeviceInformationBase)
    {
        super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase);
       
        setInput(SchemaHelper.getInstance().getGetContextStatesElement());
        setOutput(SchemaHelper.getInstance().getGetContextStatesResponseElement());
    }
    
	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
        
        
		IParameterValue result = null;

		List<String> handles = getHandles(parameterValue);
		try
		{
			MedicalDeviceInformationBase mdib=getMedicalDeviceInformationBase();
			ReadWriteLock lock= mdib.getMdibLock();

			Collection<AbstractIdentifiableContextState> identifiableContextState = mdib.getIdentifiableContextState(handles, AbstractIdentifiableContextState.class);
			GetContextStatesResponse messageResponse = convertModelToMessage(identifiableContextState, lock);

			result = getResultParameterValue(messageResponse);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}
	
	private GetContextStatesResponse convertModelToMessage( Collection<AbstractIdentifiableContextState> identifiableContextState, ReadWriteLock lock) 
	{	
		GetContextStatesResponse modelResponse=new GetContextStatesResponse();
		
		for (AbstractIdentifiableContextState contextState : identifiableContextState) {
			modelResponse.getContextStates().add(contextState);
		}
		
		GetContextStatesResponse finalResponse= createFinalResponseObject(lock, modelResponse);

		if (Log.isDebug() && finalResponse!=null )
			Log.debug("Context States: "+modelResponse.getContextStates().size());

		return finalResponse;
	}



}
