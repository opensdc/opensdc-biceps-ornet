/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.GetDescriptorResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetDescriptor extends BICEPSOperation {

	private static final String name     = SchemaHelper.GET_DESCRIPTOR_ELEMENT_NAME;
	private static final String portType = MDPWSConstants.PORTTYPE_GET_SERVICE;
	public static final QName GET_DESCRIPTOR_QN=QNameFactory.getInstance().getQName(name,portType);
	public GetDescriptor(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType), medicalDeviceInformationBase);
		setInput(SchemaHelper.getInstance().getGetDescriptorElement());
		setOutput(SchemaHelper.getInstance().getGetDescriptorResponseElement());
	}




	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
		IParameterValue result = null;
		List<String> handles = getHandles(parameterValue);
		try
		{


			MedicalDeviceInformationBase mdib=getMedicalDeviceInformationBase();
			ReadWriteLock lock= mdib.getMdibLock();

			Collection<? extends Descriptor> descriptors =getDescriptor(mdib,handles);
			GetDescriptorResponse response = convertModelToMessage(descriptors, lock);

			result = getResultParameterValue(response);


		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}




	private GetDescriptorResponse convertModelToMessage(
			Collection<? extends Descriptor> descriptors, ReadWriteLock lock) {
		GetDescriptorResponse modelResponse=new GetDescriptorResponse();
		modelResponse.getDescriptor().addAll(descriptors);
		GetDescriptorResponse response=createFinalResponseObject(lock, modelResponse);
		if (response!=null)
		{
			List<Descriptor> descriptorList = response.getDescriptor();
			for (Descriptor descriptor : descriptorList) {
				if (descriptor!=null)
				{
					try {
						resetListFields(descriptor.getClass(),descriptor);
					} catch (Exception e) {
						Log.error(e);
						return response;
					}
				}
			}

			
		}
		return response;
	}



	protected Collection<? extends Descriptor> getDescriptor(MedicalDeviceInformationBase mdib, List<String> handles) 
	{
		Collection<Descriptor> retVal=null;

		if (mdib!=null)
		{
			if (handles!=null && handles.size()>0 )
			{
				retVal=new ArrayList<Descriptor>();
				for (String handle : handles) {

					Descriptor descriptor = mdib.getDescriptor(handle);
					if (descriptor!=null)retVal.add(descriptor);
				}
			}else{
				retVal=mdib.getAllDescriptors();
			}
		}
		return retVal;
	}

	private static void resetListFields(Class<?> clazz, Object o) throws Exception{
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if  (java.util.List.class.equals(field.getType()))
			{
				field.setAccessible(true);

				java.util.List<?> l=(List<?>) field.get(o);
				if (l!=null){
					boolean clear=true;
					for (Object listentry : l) {
						if (!(listentry instanceof Descriptor))
						{
							clear=false;
							break;
						}
					}
					if (clear)
						l.clear();
				}

			}else if (Descriptor.class.isAssignableFrom(field.getType())){
				field.setAccessible(true);
				Object object = field.get(o);
				if (object!=null)
				{
					resetListFields(object.getClass(), object);
				}
			}
		}


		Class<?> superClass = clazz.getSuperclass();
		if (superClass!=null)
		{
			resetListFields(superClass,o);
		}

		return;
	}
}
