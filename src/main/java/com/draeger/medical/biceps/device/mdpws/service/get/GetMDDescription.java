/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.GetMDDescriptionResponse;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.util.MDIBSafetyInformationUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetMDDescription extends BICEPSOperation {

	private static final String name     = SchemaHelper.GET_MDDESCRIPTION_ELEMENT_NAME;
	private static final String portType = MDPWSConstants.PORTTYPE_GET_SERVICE;
	public static final QName GET_MDDESCRIPTION_QN=QNameFactory.getInstance().getQName(name,portType);

	private final MDIBSafetyInformationUtil safetyInformationUtil = new MDIBSafetyInformationUtil();
	
	public GetMDDescription(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType), medicalDeviceInformationBase);
		setInput(SchemaHelper.getInstance().getGetMDDescriptionElement());
		setOutput(SchemaHelper.getInstance().getGetMDDescriptionResponseElement());
	}




	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
		IParameterValue result = null;
		List<String> handles = getHandles(parameterValue);
		try
		{
		
			
			MedicalDeviceInformationBase mdib=getMedicalDeviceInformationBase();
			ReadWriteLock lock= mdib.getMdibLock();
			
			MDIB modelMDIB=getMDIB(mdib,handles);
			GetMDDescriptionResponse response = convertModelToMessage(modelMDIB, lock);

			result = getResultParameterValue(response);
			
			safetyInformationUtil.attachSafetyInformations(result);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}




	private GetMDDescriptionResponse convertModelToMessage(
			MDIB modelMDIB, ReadWriteLock lock) {
		com.draeger.medical.biceps.common.model.GetMDDescriptionResponse modelResponse=new com.draeger.medical.biceps.common.model.GetMDDescriptionResponse();
		modelResponse.setStaticDescription(modelMDIB.getDescription());
		GetMDDescriptionResponse response=createFinalResponseObject(lock, modelResponse);
		return response;
	}



	protected MDIB getMDIB(MedicalDeviceInformationBase mdib, List<String> handles) 
	{
		MDIB retVal=new MDIB();

		if (mdib!=null)
		{
			com.draeger.medical.biceps.common.model.MDDescription desc=new com.draeger.medical.biceps.common.model.MDDescription();
			if (handles!=null && handles.size()>0 )
			{
				Collection<? extends MDSDescriptor> descriptors = mdib.getMDSDescriptions(handles, false);
				if (descriptors!=null && descriptors.size()>0) desc.getMDSDescriptors().addAll(descriptors);				
			}else{
				desc=mdib.getDeviceDescriptions();
			}
			
			retVal.setDescription(desc);	
			retVal.setStates(mdib.getDeviceStates());
		}
		return retVal;
	}





}
