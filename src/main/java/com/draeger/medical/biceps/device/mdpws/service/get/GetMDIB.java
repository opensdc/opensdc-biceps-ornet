/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.GetMDIBResponse;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.util.MDIBSafetyInformationUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.biceps.device.mdpws.service.helper.StateFilterUtil;
import com.draeger.medical.biceps.device.mdpws.service.helper.StateFilterUtil.StateFilter;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetMDIB extends BICEPSOperation {

	private static final String name = SchemaHelper.GET_MDIB_ELEMENT_NAME;
	private static final String portType = MDPWSConstants.PORTTYPE_GET_SERVICE;
	public static final QName GET_MDIB_QN = QNameFactory.getInstance()
			.getQName(name, portType);
	private static final StateFilter stateFilter = new StateFilter();

	static {
		stateFilter.retrievabilityFilter = MetricRetrievability.GET;
	}

	private final MDIBSafetyInformationUtil safetyInformationUtil = new MDIBSafetyInformationUtil();

	public GetMDIB(MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, QNameFactory.getInstance().getQName(portType),
				medicalDeviceInformationBase);
		setInput(SchemaHelper.getInstance().getGetMDIBElement());
		setOutput(SchemaHelper.getInstance().getGetMDIBResponseElement());
	}

	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
		IParameterValue result = null;
		try {
			MedicalDeviceInformationBase mdib = getMedicalDeviceInformationBase();
			ReadWriteLock lock = mdib.getMdibLock();

			MDIB modelMDIB = getMDIB(mdib);
			GetMDIBResponse response = convertModelToMessage(modelMDIB, lock);

			result = getResultParameterValue(response);

			safetyInformationUtil.attachSafetyInformations(result);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private GetMDIBResponse convertModelToMessage(MDIB modelMDIB,
			ReadWriteLock lock) {
		com.draeger.medical.biceps.common.model.GetMDIBResponse modelResponse = new com.draeger.medical.biceps.common.model.GetMDIBResponse();
		modelResponse.setMDIB(modelMDIB);

		GetMDIBResponse response = createFinalResponseObject(lock,
				modelResponse);
		return response;
	}

	protected MDIB getMDIB(MedicalDeviceInformationBase mdib) {
		MDIB retVal = new MDIB();

		if (mdib != null) {
			retVal.setDescription(mdib.getDeviceDescriptions());
			retVal.setStates(StateFilterUtil.getFilteredMDState(
					mdib.getDeviceStates(), getMedicalDeviceInformationBase(),
					stateFilter));
			retVal.setSequenceNumber(mdib.getMdibStateVersion());
		}
		return retVal;
	}

}
