/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.get;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.GetMDStateResponse;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.biceps.device.mdpws.service.helper.StateFilterUtil;
import com.draeger.medical.biceps.device.mdpws.service.helper.StateFilterUtil.StateFilter;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public class GetMDState extends BICEPSOperation {

	private static final String name     = SchemaHelper.GET_MDSTATE_ELEMENT_NAME;
	private static final String portType = MDPWSConstants.PORTTYPE_GET_SERVICE;
	public static final QName GET_MDSTATE_QN=QNameFactory.getInstance().getQName(name,portType);
	


	public GetMDState(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType), medicalDeviceInformationBase);
		setInput(SchemaHelper.getInstance().getGetMDStateElement());
		setOutput(SchemaHelper.getInstance().getGetMDStateResponseElement());
	}




	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) {
		IParameterValue result = null;
		List<String> handles = getHandles(parameterValue);
		try
		{
			StateFilter stateFilter=new StateFilter();
			stateFilter.retrievabilityFilter=MetricRetrievability.GET;
			stateFilter.handleWhiteliste=(!handles.isEmpty()?handles:null);
			MedicalDeviceInformationBase mdib=getMedicalDeviceInformationBase();
			ReadWriteLock lock= mdib.getMdibLock();
			
			MDIB modelMDIB=getMDIB(mdib, stateFilter);
			GetMDStateResponse response = createResponse(modelMDIB, lock);			
			result = getResultParameterValue(response);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return result;
	}




	private GetMDStateResponse createResponse(
			MDIB modelMDIB, ReadWriteLock lock) {
		GetMDStateResponse modelResponse=new GetMDStateResponse();
		modelResponse.setMDState(modelMDIB.getStates());
		GetMDStateResponse response=createFinalResponseObject(lock, modelResponse);
		return response;
	}



	protected MDIB getMDIB(MedicalDeviceInformationBase mdib, StateFilter stateFilter) 
	{
		MDIB retVal=new MDIB();

		if (mdib!=null)
		{
			retVal.setDescription(mdib.getDeviceDescriptions());
			retVal.setStates(StateFilterUtil.getFilteredMDState(mdib.getDeviceStates(), getMedicalDeviceInformationBase(), stateFilter));
		}
		return retVal;
	}
}
