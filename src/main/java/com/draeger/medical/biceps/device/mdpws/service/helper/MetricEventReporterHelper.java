/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.helper;

import java.math.BigInteger;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;

import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.MetricReportPart;

public class MetricEventReporterHelper {

	public static Object getEventMessageForReport(com.draeger.medical.biceps.common.model.AbstractMetricReport modelReport, ReadWriteLock mdibLock,HashMap<String, Collection<? extends AbstractMetricState>> reportInfo, AtomicInteger eventCounter) {

//		AbstractMetricReport metricReport=null;

		if (reportInfo!=null && eventCounter!=null)
		{


			//TODO SSch Refactoring
			Set<Entry<String, Collection<? extends AbstractMetricState>>> mdsList = reportInfo.entrySet();
			if (mdsList!=null)
			{
				for (Entry<String, Collection<? extends AbstractMetricState>> reportEntryPerMDS : mdsList) 
				{
					com.draeger.medical.biceps.common.model.MetricReportPart modelReportPart=new MetricReportPart();
					//modelReportPart.setSourceMDS(reportEntryPerMDS.getKey()); //Removed SSch
					Collection<? extends AbstractMetricState> metricStates = reportEntryPerMDS.getValue();
					if(metricStates!=null){
						modelReportPart.getMetrics().addAll(metricStates);
					}
					modelReport.getReportParts().add(modelReportPart);
				}

			}
//			metricReport=convertModelToMessage(modelReport,mdibLock, eventCounter);
			modelReport.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
		}

		return modelReport;
	}

//
//	private static <M extends com.draeger.medical.biceps.common.model.AbstractMetricReport> AbstractMetricReport convertModelToMessage(M modelReport, ReadWriteLock lock, AtomicInteger eventCounter) 
//	{	
//
//
//		AbstractMetricReport messageResponse=null;
//		if (modelReport.getReportParts()!=null && modelReport.getReportParts().size()>0)
//		{
//			try{
//				if (lock!=null)
//					lock.readLock().lock();
//
//				messageResponse=(AbstractMetricReport) ModelSchemaConverterProvider.getInstance().getConverter().convertFromModel(modelReport);
//
//				if (Log.isDebug())
//					Log.debug("Metrics: "+messageResponse.getReportParts().size());
//
//				messageResponse.setSequenceNumber(BigInteger.valueOf(eventCounter.get()));
//			}finally{
//				if (lock!=null)
//					lock.readLock().unlock();	
//			}
//		}
//		return messageResponse;
//	}

}
