/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.MDState;
import com.draeger.medical.biceps.common.model.MetricRetrievability;
import com.draeger.medical.biceps.common.model.State;
import com.draeger.medical.biceps.common.model.util.MetricDescriptorUtil;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public class StateFilterUtil {
	public static final MDState getFilteredMDState( MDState mdState, MedicalDeviceInformationBase mdib, StateFilter filter ) {
		MDState retVal=new MDState();
		if(mdState!=null && mdState.getStates()!=null)
		{
			ArrayList<AbstractMetricState> metricStates = new ArrayList<AbstractMetricState>(mdState.getStates().size());
			for (State state : mdState.getStates()) {
				if (state.getHandle()!=null)
				{
					boolean includeIntoResult=true;
					if (filter.handleWhiteliste!=null)
						{
							includeIntoResult=filter.handleWhiteliste.contains(state.getHandle()) || filter.handleWhiteliste.contains(state.getReferencedDescriptor());
						}
					
					if (includeIntoResult)
					{
						if (state instanceof AbstractMetricState){
							metricStates.add((AbstractMetricState) state);
						}else{
							retVal.getStates().add(state);
						}
					}
				}
			}

			Collection<? extends AbstractMetricState> filteredMetricStates=MetricDescriptorUtil.filterMetricStatesByRetrievability(mdib.getMetricDescriptors(null),metricStates,filter.retrievabilityFilter);
			if (filteredMetricStates!=null && !filteredMetricStates.isEmpty()) retVal.getStates().addAll(filteredMetricStates);
		}
		return retVal;
	}


	public static final class StateFilter{
		public MetricRetrievability retrievabilityFilter;
		public List<String> handleWhiteliste;
	}

}
