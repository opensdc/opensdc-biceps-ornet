/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.set;

import java.util.concurrent.locks.ReadWriteLock;

import org.ws4d.java.service.Fault;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.IParameterValue;
import org.ws4d.java.types.QName;

import com.draeger.medical.biceps.common.model.AbstractSet;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBaseManager;
import com.draeger.medical.biceps.device.mdib.OperationRequest;
import com.draeger.medical.biceps.device.mdib.OperationResponse;
import com.draeger.medical.biceps.device.mdib.impl.OperationRequestImpl;
import com.draeger.medical.biceps.device.mdpws.BICEPSOperation;
import com.draeger.medical.mdpws.message.MDPWSMessageContextMap;

public abstract class BICEPSSetOperation<RESPONSE extends AbstractSetResponse,  
REQUEST extends AbstractSet> extends BICEPSOperation {

	public BICEPSSetOperation(String name, QName portType,
			MedicalDeviceInformationBase medicalDeviceInformationBase) {
		super(name, portType, medicalDeviceInformationBase);
		addFault(new Fault("SystemErrorReport"));
	}

	protected OperationRequest createOperationRequest(AbstractSet operationMessage, OperationDescriptor descriptor, MDPWSMessageContextMap msgContextMap, MDPWSMessageContextMap replyMessageCtxtMap, int transactionID) {
		OperationRequestImpl request=new OperationRequestImpl();
		request.setOperationMessage(operationMessage);
		request.setOperation(descriptor);
		request.setMsgContextMap(msgContextMap);
		request.setReplyMessageCtxtMap(replyMessageCtxtMap);
		request.setTransactionId(transactionID);
		return request;
	}
	
	@Override
	protected IParameterValue handleInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap) throws InvocationException {
		IParameterValue resultPV = null;
		try
		{
			com.draeger.medical.biceps.common.model.AbstractSetResponse response=createModelResponse();
			resultPV = handleSetInvoke(parameterValue, msgContextMap,
					replyMessageCtxtMap, response);
		}
		catch (Exception e)
		{
			handleException(e);
		}
		return resultPV;
	}


	protected abstract AbstractSetResponse createModelResponse();

	@SuppressWarnings("unchecked")
	private IParameterValue handleSetInvoke(IParameterValue parameterValue,
			MDPWSMessageContextMap msgContextMap,
			MDPWSMessageContextMap replyMessageCtxtMap,
			com.draeger.medical.biceps.common.model.AbstractSetResponse response)
			throws Exception, InvocationException {
		IParameterValue resultPV;
		MedicalDeviceInformationBaseManager manager= getMedicalDeviceInformationBase().getManager();
		int transactionID = manager.getNextTransactionId(); 
		resultPV=createFailedResponse(response,transactionID);

		REQUEST input = (REQUEST) getInputValue(parameterValue);
		String operationHandle = input.getOperationHandle();
		
		
		if(manager.getAuthorizationManager().checkCredentialsForOperationHandle(msgContextMap, operationHandle))
		{
//			REQUEST operation=convertMessageToModel(input);
			REQUEST operation = input;

			OperationDescriptor operationDescriptor =  getMedicalDeviceInformationBase().getOperationDescriptor(operationHandle);
			if (operationDescriptor!=null)
			{
				OperationRequest request=createOperationRequest(operation, operationDescriptor,msgContextMap, replyMessageCtxtMap, transactionID);
				OperationResponse opResponse=manager.queueOperationRequestForExecution(request);
				if (opResponse!=null)
				{
					RESPONSE messageResponse = convertModelToMessage(opResponse.getContent(),null);
					resultPV = getResultParameterValue(messageResponse);
				}
			}
		}else{
			throw new InvocationException(this.getFault("SystemErrorReport"));
		}
		return resultPV;
	}

//	@SuppressWarnings("unchecked")
//	protected  REQUEST convertMessageToModel(
//			REQUEST input) {
////		MODEL_REQUEST messageResponse;
////		messageResponse=(MODEL_REQUEST) ModelSchemaConverterProvider.getInstance().getConverter().convertToModel(input);
//		return input;
//	}

	@SuppressWarnings("unchecked")
	protected  RESPONSE convertModelToMessage(AbstractSetResponse modelResponse, ReadWriteLock lock) 
	{	
		RESPONSE messageResponse=(RESPONSE) createFinalResponseObject(lock, modelResponse);

		return messageResponse;
	}

}
