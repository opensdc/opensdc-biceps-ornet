/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.set;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.SetPatientState;
import com.draeger.medical.biceps.common.model.SetPatientStateResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public class SetContextState extends BICEPSSetOperation<SetPatientStateResponse,SetPatientState>
{
	static final String name     = SchemaHelper.SET_CONTEXT_STATE_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_CONTEXT_SERVICE;
	public static final QName SET_ASSOCIATION_STATE=QNameFactory.getInstance().getQName(name,portType);

	public SetContextState(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase);

		setInput(SchemaHelper.getInstance().getSetContextStateElement());
		setOutput(SchemaHelper.getInstance().getSetContextStateResponseElement());
	}

	@Override
	protected AbstractSetResponse createModelResponse() {
		return new SetPatientStateResponse();
	}


}
