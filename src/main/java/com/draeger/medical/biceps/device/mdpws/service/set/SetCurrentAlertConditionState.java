/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.set;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.SetAlertState;
import com.draeger.medical.biceps.common.model.SetAlertStateResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public class SetCurrentAlertConditionState extends BICEPSSetOperation<SetAlertStateResponse,SetAlertState>
{
	static final String name     = SchemaHelper.SET_ALERT_STATE_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_SET_SERVICE;
	public static final QName LIMIT_ALERT_QN=QNameFactory.getInstance().getQName(name,portType);

	public SetCurrentAlertConditionState(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase);

		setInput(SchemaHelper.getInstance().getSetAlertStateElement());
		setOutput(SchemaHelper.getInstance().getSetAlertStateResponseElement());
	}

	@Override
	protected AbstractSetResponse createModelResponse() {
		return new SetAlertStateResponse();
	}
}
