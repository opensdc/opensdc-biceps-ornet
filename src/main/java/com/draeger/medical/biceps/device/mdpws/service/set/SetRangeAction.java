/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.set;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.common.model.AbstractSetResponse;
import com.draeger.medical.biceps.common.model.SetRange;
import com.draeger.medical.biceps.common.model.SetRangeResponse;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;

public class SetRangeAction extends BICEPSSetOperation<SetRangeResponse,SetRange>
{
	static final String name     = SchemaHelper.SET_RANGE_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_SET_SERVICE;
	public static final QName SET_RANGE_QN=QNameFactory.getInstance().getQName(name,portType);

	public SetRangeAction(MedicalDeviceInformationBase medicalDeviceInformationBase)
	{
		super(name, QNameFactory.getInstance().getQName(portType),medicalDeviceInformationBase);

		setInput(SchemaHelper.getInstance().getSetRangeElement());
		setOutput(SchemaHelper.getInstance().getSetRangeResponseElement());
	}

	@Override
	protected AbstractSetResponse createModelResponse() {
		return new SetRangeResponse();
	}

	
//	protected ParameterValue handleInvoke(ParameterValue parameterValue,
//			MDPWSMessageContextMap msgContextMap,
//			MDPWSMessageContextMap replyMessageCtxtMap) {
//		ParameterValue result = null;
////		try
////		{
////			de.draeger.run.common.model.SetRange input = getInputValue(parameterValue);
////			String operationHandle = input.getOperationHandle();
////			SetRangeOperation operation = (SetRangeOperation) getMedicalDeviceInformationBase().getOperation(operationHandle);
////			if (operation != null)
////			{
////				operation.setRange(input.getRange());
////				MedicalDeviceInformationBaseManager manager= getMedicalDeviceInformationBase().getManager();
////				AbstractSetResponse response=manager.queueOperationRequestForExecution(operation, msgContextMap, replyMessageCtxtMap);
////				result = getResultParameterValue(response);
////			}else{
////				AbstractSetResponse response=new SetRangeResponse();
////				result=createFailedResponse(response);
////			}
////		}
////		catch (Exception e)
////		{
////			Log.error(e.getMessage());
////			Log.printStackTrace(e);
////		}
//		return result;
//	}



}
