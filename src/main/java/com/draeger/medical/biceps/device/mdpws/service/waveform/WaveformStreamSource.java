/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.device.mdpws.service.waveform;

import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameFactory;

import com.draeger.medical.biceps.common.messages.MDPWSConstants;
import com.draeger.medical.biceps.common.messages.v2.xsd.SchemaHelper;
import com.draeger.medical.biceps.device.mdib.MedicalDeviceInformationBase;
import com.draeger.medical.biceps.device.mdpws.BICEPSWaveformStreamSource;
import com.draeger.medical.mdpws.framework.configuration.streaming.StreamingConfigurationPropertiesHandler;

public class WaveformStreamSource extends BICEPSWaveformStreamSource {
	static final String name     = SchemaHelper.WAVEFORM_STREAM_ELEMENT_NAME;
	static final String portType = MDPWSConstants.PORTTYPE_STREAM_SERVICE;
	public static final QName WAVEFORM_STREAM_SOURCE=QNameFactory.getInstance().getQName(name,portType);




	public WaveformStreamSource(MedicalDeviceInformationBase mdib) 
	{
		super(name, QNameFactory.getInstance().getQName(portType),(StreamingConfigurationPropertiesHandler.getInstance()!=null? StreamingConfigurationPropertiesHandler.getInstance().getStreamConfiguration("med-service-waveform"):null) , mdib.getManager().getStreamSinkQueue());
		//TODO SSch Use appliesTO from StreamConfigurations
		setOutput(SchemaHelper.getInstance().getWaveformStreamElement());
	}

}
