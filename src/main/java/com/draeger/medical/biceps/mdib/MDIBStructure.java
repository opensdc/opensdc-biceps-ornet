/*******************************************************************************
 * Copyright (c) 2011 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.mdib;

import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.SCODescriptor;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.mdib.impl.MdibRepresentation.DescriptorWithParent;

public interface MDIBStructure {

	public abstract void removeVMO(String handle);

	public abstract void addDescriptor(Descriptor vmo, String parentHandle, String mdsHandle);

	public abstract boolean removeOperation(String handle);

	public abstract void addOperation(OperationDescriptor operation, SCODescriptor scoDescriptor, String mdsHandle);

	public abstract List<AbstractContextDescriptor> findMDSContextElementsForVms(String vmsHandle);

	public abstract List<AbstractContextDescriptor> findMDSContextElements();
	
	public abstract SystemContext findMDSContextForVms(String vmsHandle);

	public abstract List<SystemContext> findMDSContexts();
	
	public abstract List<RealTimeSampleArrayMetricDescriptor> findStreamsForVms(String vmsHandle);

	public abstract List<RealTimeSampleArrayMetricDescriptor> findStreamMetrics(CodedValue code);

	public abstract List<RealTimeSampleArrayMetricDescriptor> findStreamMetrics();

	public abstract RealTimeSampleArrayMetricDescriptor findStreamMetric(String handle);

	public abstract ActivateOperationDescriptor findManeuver(String deviceLocalHandle);
	
	public abstract List<ActivateOperationDescriptor> findManeuvers(CodedValue code);

	public abstract List<ActivateOperationDescriptor> findManeuvers();
	
	public abstract List<ActivateOperationDescriptor> findManeuverForVMS(String handle);

	public abstract List<AlertSignalDescriptor> findAlertSignalsForVms(String vmsHandle);

	public abstract List<AlertSignalDescriptor> findAlertSignals();

	public abstract AlertSignalDescriptor findAlertSignal(String handle);

	public abstract List<AlertConditionDescriptor> findAlertsConditionsForVms(String vmsHandle);

	public abstract List<AlertConditionDescriptor> findAlertConditions(CodedValue code);

	public abstract List<AlertConditionDescriptor> findAlertConditions();

	public abstract AlertConditionDescriptor findAlertCondition(String handle);

	public abstract List<AlertSystemDescriptor> findAlertsSystemsForVms(String vmsHandle);

	public abstract List<AlertSystemDescriptor> findAlertSystems();

	public abstract AlertSystemDescriptor findAlertSystem(String handle);

	public abstract List<? extends MetricDescriptor> findMetricsForVms(String vmsHandle);

	public abstract List<? extends MetricDescriptor> findMetrics(CodedValue code);

	public abstract MetricDescriptor findMetric(String handle);

	public abstract OperationDescriptor findOperation(String handle);

	public abstract List<OperationDescriptor> findOperationsForOperationDescriptor(Descriptor vmo);

	public abstract Descriptor getVMO(String handle);

	public abstract MDSDescriptor getVMS(String handle);
	
	public abstract MDSDescriptor getParentVMSForDescriptorHandle(String aDescriptorHandle);

	public abstract List<MDSDescriptor> getMDSDescriptors();

	public abstract EndpointReference getDeviceEndpointRef();
	
	public abstract List<DescriptorWithParent> getChildrenDescriptors(String parentHandle);

}
