/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.mdib.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ws4d.java.communication.TimeoutException;
import org.ws4d.java.dispatch.DeviceServiceRegistryProvider;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.reference.DeviceReference;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.communication.BICEPSSafetyInformationPolicyElement;
import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.client.communication.CommunicationContainer;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.ContainmentTree;
import com.draeger.medical.biceps.common.model.ContainmentTreeEntry;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.util.ModelComparer;

/**
 * The Class DescriptionLazyLoader.
 */
public class DescriptionLazyLoader {
	
	private final EndpointReference deviceEndpointRef;
	private final CommunicationAdapter communicationAdapter;

	/**
	 * Instantiates a new description lazy loader.
	 *
	 * @param deviceEndpointRef the device endpoint ref
	 * @param comAdapter the com adapter
	 */
	DescriptionLazyLoader(EndpointReference deviceEndpointRef, CommunicationAdapter comAdapter)
	{
		this.deviceEndpointRef=deviceEndpointRef;
		this.communicationAdapter=comAdapter;
	}

	/**
	 * Retrieve descriptor.
	 *
	 * @param <T> the generic type
	 * @param code the code
	 * @param descriptorClass the descriptor class
	 * @return the list
	 */
	<T  extends Descriptor> List<T> retrieveDescriptor(CodedValue code, Class<T> descriptorClass) 
	{
		List<T> retVal=new ArrayList<T>();

		try {
			fillDescriptorList(code, descriptorClass, retVal, null);
		} catch (Exception e) {
			Log.warn(e);
		}
		return retVal;
	}

	/**
	 * Fill descriptor list.
	 *
	 * @param <T> the generic type
	 * @param code the code
	 * @param descriptorClass the descriptor class
	 * @param retVal the ret val
	 * @param handle the handle
	 */
	<T extends Descriptor> void fillDescriptorList(CodedValue code,
			Class<T> descriptorClass, List<T> retVal, String handle) {
		ContainmentTree retrievedContainmentTree = null;

		try{
			retrievedContainmentTree=retrieveContainmentTree(handle);

			if (retrievedContainmentTree!=null)
			{
				List<ContainmentTreeEntry> entries = retrievedContainmentTree.getEntries();
				if (entries!=null && entries.size()>0)
				{
					for (ContainmentTreeEntry containmentTreeEntry : entries) {
						if (code==null || ModelComparer.equals(code,containmentTreeEntry.getType()))
						{
							T retrievedDescriptorWithHandle = retrieveDescriptorWithHandle(containmentTreeEntry.getHandleRef(), descriptorClass);
							if (retrievedDescriptorWithHandle!=null )
							{
								retVal.add(retrievedDescriptorWithHandle);
							}
						}
						if (containmentTreeEntry.getHandleRef()!=null && containmentTreeEntry.getChildren()>0)
						{
							fillDescriptorList(code, descriptorClass, retVal, containmentTreeEntry.getHandleRef());
						}
					}
				}
			}
		}catch(Exception e){
			Log.warn(e);
		}
	}

	/**
	 * Retrieve containment tree info.
	 *
	 * @param descriptor the descriptor
	 * @return the containment tree entry
	 */
	ContainmentTreeEntry retrieveContainmentTreeInfo(Descriptor descriptor) {
		ContainmentTreeEntry retVal=null; 
		try {
			ContainmentTree retrievedContainmentTree = retrieveContainmentTree(descriptor!=null?descriptor.getHandle():null);

			if (retrievedContainmentTree!=null &&
					retrievedContainmentTree.getEntries()!=null && retrievedContainmentTree.getEntries().size()>0)
			{
				retVal=retrievedContainmentTree.getEntries().get(0);
			}
		} catch (Exception e) {
			Log.warn(e);
		}

		return retVal;
	}

	/**
	 * Retrieve containment tree.
	 *
	 * @param descriptorHandle the descriptor handle
	 * @return the containment tree
	 * @throws InvocationException the invocation exception
	 * @throws TimeoutException the timeout exception
	 */
	ContainmentTree retrieveContainmentTree(
			String descriptorHandle)
					throws InvocationException, TimeoutException {
		DeviceReference staticDeviceReference = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(deviceEndpointRef);
		CommunicationContainer<ContainmentTree, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrievedDescription = this.communicationAdapter.retrieveContainmentTreeInfo(staticDeviceReference, descriptorHandle);

		ContainmentTree retVal=null;

		if (retrievedDescription!=null)
		{
			retVal = retrievedDescription.getMessageElem();
		}
		return retVal;
	}

	/**
	 * Retrieve descriptor with handle.
	 *
	 * @param <T> the generic type
	 * @param handle the handle
	 * @param descriptorClass the descriptor class
	 * @return the t
	 */
	<T extends Descriptor> T retrieveDescriptorWithHandle(String handle, Class<T> descriptorClass) {
		T retVal=null; 
		try {
			DeviceReference staticDeviceReference = DeviceServiceRegistryProvider.getInstance().getDeviceServiceRegistry().getStaticDeviceReference(deviceEndpointRef);
			CommunicationContainer<List<Descriptor>, HashMap<String, BICEPSSafetyInformationPolicyElement>> retrievedDescription = this.communicationAdapter.retrieveDescription(staticDeviceReference, handle);

			if (retrievedDescription!=null)
			{
				List<Descriptor> descriptors = retrievedDescription.getMessageElem();
				if (descriptors!=null && descriptors.size()>0)
				{
					Descriptor descriptor = descriptors.get(0);
					if (descriptorClass.isAssignableFrom(descriptor.getClass()))
					{
						retVal=descriptorClass.cast(descriptor);
					}
				}
			}

		} catch (Exception e) {
			Log.warn(e);
		}

		return retVal;
	}


	/**
	 * Checks if lazy loading is enabled.
	 *
	 * @param <T> the generic type
	 * @param code the code
	 * @param descriptorClass the descriptor class
	 * @return true, if  lazy loading is enabled
	 */
	<T  extends Descriptor> boolean isLazyLoadingEnabled(CodedValue code, Class<T> descriptorClass) 
	{
		//TODO SSch Check if we need to cache the already tried codes+descriptor classes or if we can just retry to find them in the descriptor tree

		return this.communicationAdapter!=null;
	}
}
