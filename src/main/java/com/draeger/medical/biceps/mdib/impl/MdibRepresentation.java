/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.mdib.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ws4d.java.types.EndpointReference;

import com.draeger.medical.biceps.client.communication.CommunicationAdapter;
import com.draeger.medical.biceps.common.model.AbstractContextDescriptor;
import com.draeger.medical.biceps.common.model.ActivateOperationDescriptor;
import com.draeger.medical.biceps.common.model.AlertConditionDescriptor;
import com.draeger.medical.biceps.common.model.AlertSignalDescriptor;
import com.draeger.medical.biceps.common.model.AlertSystemDescriptor;
import com.draeger.medical.biceps.common.model.ChannelDescriptor;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.Descriptor;
import com.draeger.medical.biceps.common.model.EnsembleContextDescriptor;
import com.draeger.medical.biceps.common.model.HydraMDSDescriptor;
import com.draeger.medical.biceps.common.model.LocationContextDescriptor;
import com.draeger.medical.biceps.common.model.MDDescription;
import com.draeger.medical.biceps.common.model.MDIB;
import com.draeger.medical.biceps.common.model.MDSDescriptor;
import com.draeger.medical.biceps.common.model.MetricDescriptor;
import com.draeger.medical.biceps.common.model.OperationDescriptor;
import com.draeger.medical.biceps.common.model.PatientAssociationDescriptor;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricDescriptor;
import com.draeger.medical.biceps.common.model.SCODescriptor;
import com.draeger.medical.biceps.common.model.SystemContext;
import com.draeger.medical.biceps.common.model.VMDDescriptor;
import com.draeger.medical.biceps.common.model.WorkflowContextDescriptor;
import com.draeger.medical.biceps.common.model.util.ModelComparer;
import com.draeger.medical.biceps.mdib.MDIBStructure;


public class MdibRepresentation implements MDIBStructure
{
	public class VmsWithParent
	{
		final MDSDescriptor vms;
		final MDSDescriptor parent;

		public VmsWithParent(MDSDescriptor vms, MDSDescriptor parent)
		{
			this.vms = vms;
			this.parent = parent;
		}

		public MDSDescriptor getVms() {
			return vms;
		}

		public MDSDescriptor getParent() {
			return parent;
		}
	}

	public class DescriptorWithParent
	{
		final Descriptor vmo;
		final MDSDescriptor vms;
		final Descriptor parent;

		public DescriptorWithParent(Descriptor vmo, Descriptor parent, MDSDescriptor vms)
		{
			this.vmo = vmo;
			this.vms = vms;
			this.parent = parent;
		}

		public Descriptor getVmo() {
			return vmo;
		}

		public MDSDescriptor getVms() {
			return vms;
		}

		public Descriptor getParent() {
			return parent;
		}


	}

	public class OperationWithVms
	{
		final OperationDescriptor operation;
		final MDSDescriptor       vms;

		public OperationWithVms(OperationDescriptor operation, MDSDescriptor vms)
		{
			this.operation = operation;
			this.vms = vms;
		}

		public OperationDescriptor getOperation() {
			return operation;
		}

		public MDSDescriptor getVms() {
			return vms;
		}
	}

	private final HashMap<String, ArrayList<DescriptorWithParent>>    childMap= new HashMap<String, ArrayList<DescriptorWithParent>>();
	private final HashMap<String, VmsWithParent>    			vmsMap= new HashMap<String, MdibRepresentation.VmsWithParent>();
	private final HashMap<String, DescriptorWithParent>    		vmoMap=new HashMap<String, MdibRepresentation.DescriptorWithParent>();
	private final HashMap<String, OperationWithVms> 			operationsMap=new HashMap<String, MdibRepresentation.OperationWithVms>();
	private final EndpointReference deviceEndpointRef;
	private final CommunicationAdapter communicationAdapter;
	private final DescriptionLazyLoader lazyLoader;



	MdibRepresentation(EndpointReference endpointReference, CommunicationAdapter communicationAdapter) {
		this.deviceEndpointRef=endpointReference;
		this.communicationAdapter=communicationAdapter;
		this.lazyLoader=new DescriptionLazyLoader(endpointReference, communicationAdapter);
	}

	public MdibRepresentation(MDIB mdib, EndpointReference endpointReference,CommunicationAdapter communicationAdapter)
	{
		this(endpointReference, communicationAdapter);
		if (mdib!=null)
		{
			MDDescription description=mdib.getDescription();
			if (description!=null)
			{
				for (MDSDescriptor mds : description.getMDSDescriptors())
				{
					addMDS(mds, null);
				}
			}
		}
	}

	public MdibRepresentation(MDIB mdib, EndpointReference endpointReference)
	{
		this(mdib,endpointReference,null);
	}

	public MdibRepresentation(MDDescription description, EndpointReference endpointReference, CommunicationAdapter communicationAdapter)
	{
		this(endpointReference, communicationAdapter);
		if (description!=null)
		{
			for (MDSDescriptor mds : description.getMDSDescriptors())
			{
				addMDS(mds, null);
			}
		}
	}

	public MdibRepresentation(MDDescription description, EndpointReference endpointReference)
	{
		this(description,endpointReference,null);
	}


	public VmsWithParent getVmsWithParent(String handle) {
		return vmsMap.get(handle);
	}

	public DescriptorWithParent getDescriptorWithParent(String handle) {
		return vmoMap.get(handle);
	}

	public OperationWithVms getOperationWithVms(String handle) {
		return operationsMap.get(handle);
	}

	@Override
	public EndpointReference getDeviceEndpointRef() {
		return deviceEndpointRef;
	}

	private void addMDS(MDSDescriptor mds, MDSDescriptor parent)
	{
		if (mds!=null &&  !vmsMap.containsKey(mds.getHandle()))
		{
			vmsMap.put(mds.getHandle(), new VmsWithParent(mds, parent));
			//			vmoMap.put(mds.getHandle(), new DescriptorWithParent(mds, parent, mds));
			DescriptorWithParent descriptorWithParent = new DescriptorWithParent(mds, parent, mds);
			addToDescriptorMap(descriptorWithParent);
			if (mds instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor vms = (HydraMDSDescriptor) mds;
				if (communicationAdapter==null)
				{
					addContext(mds,vms.getContext());
					addAlerts(mds, vms.getAlertSystem(), vms);
					addOperations(vms);
					addVMD(mds, vms.getVMDs());
				}
			}
		}
	}




	private void addVMD(MDSDescriptor mds, List<VMDDescriptor> vmds) {
		if (vmds!=null)
		{
			for (VMDDescriptor vmd : vmds)
			{
				DescriptorWithParent descriptorWithParent = new DescriptorWithParent(vmd, mds, mds);
				//				vmoMap.put(vmd.getHandle(), descriptorWithParent);
				addToDescriptorMap(descriptorWithParent);
				addAlerts(vmd, vmd.getAlertSystem(), mds);
				addChannels(vmd, vmd.getChannels(), mds);
			}
		}
	}

	private void addContext(MDSDescriptor vms, SystemContext context) {
		if (vms!=null && context!=null)
		{
			DescriptorWithParent descriptorWithParent = new DescriptorWithParent(context, vms, vms);
			addToDescriptorMap(descriptorWithParent);
			if(context.getLocationContext()!=null)
			{
				LocationContextDescriptor locCtxtDesc = context.getLocationContext();
				if (!vmoMap.containsKey(locCtxtDesc.getHandle()))
				{
					//					vmoMap.put(locCtxtDesc.getHandle(), new DescriptorWithParent(locCtxtDesc, vms, vms));
					DescriptorWithParent locationContext = new DescriptorWithParent(locCtxtDesc, context, vms);
					addToDescriptorMap(locationContext);
				}
			}

			if(context.getPatientContext()!=null)
			{
				PatientAssociationDescriptor patCtxtDesc = context.getPatientContext();
				if (!vmoMap.containsKey(patCtxtDesc.getHandle()))
				{
					//					vmoMap.put(patCtxtDesc.getHandle(), new DescriptorWithParent(patCtxtDesc, context, vms));
					DescriptorWithParent patContext = new DescriptorWithParent(patCtxtDesc, context, vms);
					addToDescriptorMap(patContext);
				}
			}
			if(context.getEnsembleContext()!=null) {
				EnsembleContextDescriptor ensembleContextDescriptor = context.getEnsembleContext();
				if(!vmoMap.containsKey(ensembleContextDescriptor.getHandle())) {
					DescriptorWithParent patContext = new DescriptorWithParent(ensembleContextDescriptor, context, vms);
					addToDescriptorMap(patContext);
				}
			}
			if(context.getWorkflowContext()!=null) {
				WorkflowContextDescriptor workflowContextDescriptor = context.getWorkflowContext();
				if(!vmoMap.containsKey(workflowContextDescriptor.getHandle())) {
					DescriptorWithParent patContext = new DescriptorWithParent(workflowContextDescriptor, context, vms);
					addToDescriptorMap(patContext);
				}
			}
			
			//TODO SSch add operator context
		}
	}




	private void addOperations(HydraMDSDescriptor mds)
	{

		if (mds!=null && mds.getSCO()!=null)
		{
			List<OperationDescriptor> operations = mds.getSCO().getOperations();
			DescriptorWithParent descriptorWithParent = new DescriptorWithParent(mds.getSCO(), mds, mds);
			addToDescriptorMap(descriptorWithParent);
			for (OperationDescriptor operation : operations)
			{
				addOperation(operation,mds.getSCO(), mds.getHandle());
				//				if (!operationsMap.containsKey(operation.getHandle()))
				//				{
				//					operationsMap.put(operation.getHandle(), new OperationWithVms(operation, mds));
				//				}
			}
		}
	}

	private void addAlerts(Descriptor parent, AlertSystemDescriptor alertSystem, MDSDescriptor vms)
	{
		if (alertSystem !=null)
		{
			DescriptorWithParent asdWithParent = new DescriptorWithParent(alertSystem, parent, vms);
			//			vmoMap.put(alertSystem.getHandle(),asdWithParent );
			addToDescriptorMap(asdWithParent);

			List<AlertConditionDescriptor> alerts = alertSystem.getAlertConditions();
			for (AlertConditionDescriptor alertCondition : alerts)
			{
				if (!vmoMap.containsKey(alertCondition.getHandle()))
				{
					DescriptorWithParent acdWithParent = new DescriptorWithParent(alertCondition, alertSystem, vms);
					//					vmoMap.put(alertCondition.getHandle(), );
					addToDescriptorMap(acdWithParent);
				}
			}
			List<AlertSignalDescriptor> alertSignals = alertSystem.getAlertSignals();
			for (AlertSignalDescriptor alertSignal : alertSignals)
			{
				if (!vmoMap.containsKey(alertSignal.getHandle()))
				{
					DescriptorWithParent asigdWithParent = new DescriptorWithParent(alertSignal, alertSystem, vms);
					addToDescriptorMap(asigdWithParent);
					//					vmoMap.put(alertSignal.getHandle(), new DescriptorWithParent(alertSignal, alertSystem, vms));
				}
			}
		}
	}

	private void addChannels(Descriptor parent, List<ChannelDescriptor> channels, MDSDescriptor vms)
	{
		if (channels!=null)
		{
			for (ChannelDescriptor channel : channels)
			{
				if (!vmoMap.containsKey(channel.getHandle()))
				{
					DescriptorWithParent cdWithParent = new DescriptorWithParent(channel, parent, vms);
					addToDescriptorMap(cdWithParent);
					//					vmoMap.put(channel.getHandle(), );
					addAlerts(channel, channel.getAlertSystem(), vms);
					addMetrics(channel, channel.getMetrics(), vms);
				}
			}
		}
	}

	private void addMetrics(Descriptor parent, List<MetricDescriptor> metrics, MDSDescriptor vms)
	{
		if (metrics!=null)
		{
			for (MetricDescriptor metric : metrics)
			{
				if (!vmoMap.containsKey(metric.getHandle()))
				{
					DescriptorWithParent mdWithParent = new DescriptorWithParent(metric, parent, vms);
					//					vmoMap.put(metric.getHandle(), mdWithParent);
					addToDescriptorMap(mdWithParent);
				}
			}
		}
	}

	@Override
	public List<MDSDescriptor> getMDSDescriptors()
	{
		List<MDSDescriptor> result = new ArrayList<MDSDescriptor>();
		for(VmsWithParent v : vmsMap.values())
		{
			result.add(v.vms);
		}
		return result;
	}


	@Override
	public MDSDescriptor getVMS(String handle)
	{
		if (vmsMap.containsKey(handle)) { return vmsMap.get(handle).vms; }
		return null;
	}


	@Override
	public Descriptor getVMO(String handle)
	{
		Descriptor retVal=null;
		if (vmoMap.containsKey(handle)) {
			retVal= vmoMap.get(handle).vmo; 
		}

		if (retVal==null && this.lazyLoader.isLazyLoadingEnabled(null, Descriptor.class))
		{
			findVMO(handle, Descriptor.class);
		}
		return retVal;
	}

	@Override
	public List<OperationDescriptor> findOperationsForOperationDescriptor(Descriptor targetDescriptor)
	{
		List<OperationDescriptor> result = new ArrayList<OperationDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, OperationDescriptor.class))
		{
			List<OperationDescriptor> operations = findOperations(null,OperationDescriptor.class);
			if (operations!=null)
			{
				for (OperationDescriptor operationDescriptor : operations) {
					if (ModelComparer.equals(targetDescriptor.getHandle(), operationDescriptor.getOperationTarget()))
					{
						result.add(operationDescriptor);
					}
				}
			}
		}else{
			if (vmoMap.containsKey(targetDescriptor.getHandle()))
			{
				for (OperationWithVms entry : operationsMap.values())
				{
					if (ModelComparer.equals(targetDescriptor.getHandle(), entry.operation.getOperationTarget()))
					{
						result.add(entry.operation);
					}
				}
			}
		}
		return result;
	}


	@Override
	public OperationDescriptor findOperation(String handle)
	{
		//		OperationDescriptor retVal=null;
		//		OperationWithVms t = operationsMap.get(handle);
		//		if (t!=null)
		//			retVal=t.operation;
		//
		//		return retVal;

		return findOperationDescriptor(handle,OperationDescriptor.class);
	}


	//	public List<OperationDescriptor> findOperations1(Descriptor contextDescriptor)
	//	{
	//		List<OperationDescriptor> result = new ArrayList<OperationDescriptor>();
	//		if (findPatients().contains(patient))
	//		{
	//			for (OperationWithVms entry : operationsMap.values())
	//			{
	//				if (ModelComparer.equals(patient.getHandle(), entry.operation.getOperationTarget()))
	//				{
	//					result.add(entry.operation);
	//				}
	//			}
	//		}
	//		return result;
	//	}


	@Override
	public MetricDescriptor findMetric(String handle)
	{
		return findVMO(handle, MetricDescriptor.class);
	}

	private <T extends Descriptor> T findVMO(String handle, Class<T> descriptorClass) {
		T retVal=null;

		DescriptorWithParent v = vmoMap.get(handle);
		if (v==null && this.lazyLoader.isLazyLoadingEnabled(null, descriptorClass))
		{
			retVal = this.lazyLoader.retrieveDescriptorWithHandle(handle, descriptorClass);
			retVal=updateMDIBRepresentation(retVal);
		}
		//		else if(v != null && v.vmo instanceof MetricDescriptor)
		else if(v != null && v.vmo!=null && descriptorClass.isAssignableFrom(v.vmo.getClass()))
		{
			//			retVal= (MetricDescriptor) v.vmo;
			retVal=descriptorClass.cast(v.vmo);
		}
		return retVal;
	}

	private <T extends Descriptor> T updateMDIBRepresentation(T d) {

		if (d!=null)
		{
			//TODO SSch check if we need to update all the maps
			//ContainmentTreeEntry cTreeEntry = retrieveContainmentTreeInfo( d);

		}

		return d;
	}

	@Override
	public List<? extends MetricDescriptor> findMetrics(CodedValue code)
	{
		return findVMOWithCode(code,MetricDescriptor.class);
	}

	private <T extends Descriptor> List<T> findVMOWithCode(CodedValue code, Class<T> descriptorClass) {
		List<T> result = new ArrayList<T>();

		if (this.lazyLoader.isLazyLoadingEnabled(code, descriptorClass))
		{
			if (result.isEmpty())
			{
				List<T> retrievedDescriptors = this.lazyLoader.retrieveDescriptor(code, descriptorClass);
				if (retrievedDescriptors!=null)
					result.addAll(retrievedDescriptors);
			}
		}else{

			for (DescriptorWithParent vmoEntry : vmoMap.values())
			{
				//			if (vmoEntry.vmo instanceof MetricDescriptor)
				if (vmoEntry.vmo !=null && descriptorClass.isAssignableFrom(vmoEntry.vmo.getClass()))
				{
					T descriptor = descriptorClass.cast(vmoEntry.vmo);
					if (code == null || ModelComparer.equals(code, descriptor.getType())) // if (code == m.getCode())
					{
						result.add(descriptor);
					}
				}
			}
		}

		return result;
	}






	@Override
	public List<? extends MetricDescriptor> findMetricsForVms(String vmsHandle)
	{
		List<MetricDescriptor> result = new ArrayList<MetricDescriptor>();

		VmsWithParent vmsEntry = vmsMap.get(vmsHandle);

		if (this.lazyLoader.isLazyLoadingEnabled(null, MetricDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,MetricDescriptor.class,result,vmsHandle);
		}else{

			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				for(VMDDescriptor vmd : mds.getVMDs())
				{
					for(ChannelDescriptor channel : vmd.getChannels())
					{
						result.addAll(channel.getMetrics());
					}
				}
			}
		}


		return result;
	}

	//

	@Override
	public AlertSystemDescriptor findAlertSystem(String handle)
	{
		//		DescriptorWithParent v = vmoMap.get(handle);
		//		if(v != null && v.vmo instanceof AlertSystemDescriptor)
		//		{
		//			return (AlertSystemDescriptor) v.vmo;
		//		}
		//		return null;
		return findVMO(handle, AlertSystemDescriptor.class);
	}


	@Override
	public List<AlertSystemDescriptor> findAlertSystems()
	{
		//		List<AlertSystemDescriptor> result = new ArrayList<AlertSystemDescriptor>();
		//		for (DescriptorWithParent entry : vmoMap.values())
		//		{
		//			if (entry.vmo instanceof AlertSystemDescriptor)
		//			{
		//				result.add((AlertSystemDescriptor) entry.vmo);
		//			}
		//		}
		//		return result;
		return findVMOWithCode(null, AlertSystemDescriptor.class);
	}


	@Override
	public List<AlertSystemDescriptor> findAlertsSystemsForVms(String vmsHandle)
	{
		List<AlertSystemDescriptor> result = new ArrayList<AlertSystemDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, AlertSystemDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,AlertSystemDescriptor.class,result,vmsHandle);
		}else{

			VmsWithParent vmsEntry = vmsMap.get(vmsHandle);
			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				if (mds.getAlertSystem()!=null)
					result.add(mds.getAlertSystem());

				for(VMDDescriptor vmd : mds.getVMDs())
				{
					if (vmd.getAlertSystem()!=null)
						result.add(vmd.getAlertSystem());

					for(ChannelDescriptor channel : vmd.getChannels())
					{
						if (channel.getAlertSystem()!=null)
							result.add(channel.getAlertSystem());
					}
				}
			}
		}

		return result;
	}
	//



	@Override
	public AlertConditionDescriptor findAlertCondition(String handle)
	{
		//		DescriptorWithParent v = vmoMap.get(handle);
		//		if(v != null && v.vmo instanceof AlertConditionDescriptor)
		//		{
		//			return (AlertConditionDescriptor) v.vmo;
		//		}
		//		return null;
		return findVMO(handle, AlertConditionDescriptor.class);
	}


	@Override
	public List<AlertConditionDescriptor> findAlertConditions()
	{
		//		List<AlertConditionDescriptor> result = new ArrayList<AlertConditionDescriptor>();
		//		for (DescriptorWithParent entry : vmoMap.values())
		//		{
		//			if (entry.vmo instanceof AlertConditionDescriptor)
		//			{
		//				result.add((AlertConditionDescriptor) entry.vmo);
		//			}
		//		}
		//		return result;
		return findVMOWithCode(null, AlertConditionDescriptor.class);
	}


	@Override
	public List<AlertConditionDescriptor> findAlertConditions(CodedValue code)
	{
		//		List<AlertConditionDescriptor> result = new ArrayList<AlertConditionDescriptor>();
		//		for (DescriptorWithParent entry : vmoMap.values())
		//		{
		//			if (entry.vmo instanceof AlertConditionDescriptor)
		//			{
		//				AlertConditionDescriptor a = (AlertConditionDescriptor) entry.vmo;
		//				if (ModelComparer.equals(a.getType(), code))
		//				{
		//					result.add(a);
		//				}
		//			}
		//		}
		//		return result;
		return findVMOWithCode(code, AlertConditionDescriptor.class);
	}


	@Override
	public List<AlertConditionDescriptor> findAlertsConditionsForVms(String vmsHandle)
	{
		List<AlertConditionDescriptor> result = new ArrayList<AlertConditionDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, AlertConditionDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,AlertConditionDescriptor.class,result,vmsHandle);
		}else{
			VmsWithParent vmsEntry = vmsMap.get(vmsHandle);
			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				if (mds.getAlertSystem()!=null)
					result.addAll(mds.getAlertSystem().getAlertConditions());

				for(VMDDescriptor vmd : mds.getVMDs())
				{
					if (vmd.getAlertSystem()!=null)
						result.addAll(vmd.getAlertSystem().getAlertConditions());

					for(ChannelDescriptor channel : vmd.getChannels())
					{
						if (channel.getAlertSystem()!=null)
							result.addAll(channel.getAlertSystem().getAlertConditions());
					}
				}
			}
		}

		return result;
	}


	@Override
	public AlertSignalDescriptor findAlertSignal(String handle)
	{
		//		DescriptorWithParent v = vmoMap.get(handle);
		//		if(v != null && v.vmo instanceof AlertSignalDescriptor)
		//		{
		//			return (AlertSignalDescriptor) v.vmo;
		//		}
		//		return null;
		return findVMO(handle, AlertSignalDescriptor.class);
	}


	@Override
	public List<AlertSignalDescriptor> findAlertSignals()
	{
		//		List<AlertSignalDescriptor> result = new ArrayList<AlertSignalDescriptor>();
		//		for (DescriptorWithParent entry : vmoMap.values())
		//		{
		//			if (entry.vmo instanceof AlertSignalDescriptor)
		//			{
		//				result.add((AlertSignalDescriptor) entry.vmo);
		//			}
		//		}
		//		return result;
		return findVMOWithCode(null, AlertSignalDescriptor.class);
	}



	@Override
	public List<AlertSignalDescriptor> findAlertSignalsForVms(String vmsHandle)
	{
		List<AlertSignalDescriptor> result = new ArrayList<AlertSignalDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, AlertSignalDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,AlertSignalDescriptor.class,result,vmsHandle);
		}else{

			VmsWithParent vmsEntry = vmsMap.get(vmsHandle);
			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				if (mds.getAlertSystem()!=null)
					result.addAll(mds.getAlertSystem().getAlertSignals());

				for(VMDDescriptor vmd : mds.getVMDs())
				{
					if (vmd.getAlertSystem()!=null)
						result.addAll(vmd.getAlertSystem().getAlertSignals());

					for(ChannelDescriptor channel : vmd.getChannels())
					{
						if (channel.getAlertSystem()!=null)
							result.addAll(channel.getAlertSystem().getAlertSignals());
					}
				}
			}
		}


		return result;
	}




	@Override
	public ActivateOperationDescriptor findManeuver(String handle) {
		//		ActivateOperationDescriptor retVal=null;
		//		OperationWithVms t = operationsMap.get(handle);
		//		if (t!=null && (t.operation instanceof ActivateOperationDescriptor))
		//			retVal=(ActivateOperationDescriptor)t.operation;
		//
		//		return retVal;
		return findOperationDescriptor(handle,ActivateOperationDescriptor.class);
	}

	private <T extends OperationDescriptor> T findOperationDescriptor(String handle, Class<T> opDescriptorClass) {
		T retVal=null;

		OperationWithVms v = operationsMap.get(handle);
		if (v==null && this.lazyLoader.isLazyLoadingEnabled(null, opDescriptorClass))
		{
			retVal = this.lazyLoader.retrieveDescriptorWithHandle(handle, opDescriptorClass);
			retVal=updateMDIBRepresentation(retVal);
		}
		else if(v != null && v.operation!=null && opDescriptorClass.isAssignableFrom(v.operation.getClass()) )
		{
			retVal=opDescriptorClass.cast(v.operation);
		}
		return retVal;
	}




	@Override
	public List<ActivateOperationDescriptor> findManeuvers()
	{
		return findManeuvers(null);
	}


	@Override
	public List<ActivateOperationDescriptor> findManeuvers(CodedValue code)
	{
		return findOperations(code,ActivateOperationDescriptor.class);
	}

	private <T extends OperationDescriptor> List<T> findOperations(CodedValue code, Class<T> descriptorClass) {
		List<T> result = new ArrayList<T>();
		//		for (OperationWithVms entry : operationsMap.values())
		//		{
		//			if (entry.operation instanceof ActivateOperationDescriptor)
		//			{
		//				if (code == null || ModelComparer.equals(((ActivateOperationDescriptor)entry.operation).getType(), code))
		//				{
		//					result.add((ActivateOperationDescriptor) entry.operation);
		//				}
		//			}
		//		}
		//		return result;

		if (this.lazyLoader.isLazyLoadingEnabled(code,descriptorClass))
		{
			List<T> retrievedDescriptors = this.lazyLoader.retrieveDescriptor(code, descriptorClass);
			if (retrievedDescriptors!=null)
				result.addAll(retrievedDescriptors);
		}else{
			for (OperationWithVms vmoEntry : operationsMap.values())
			{
				if (vmoEntry.operation !=null && descriptorClass.isAssignableFrom(vmoEntry.operation.getClass()))
				{
					T descriptor = descriptorClass.cast(vmoEntry.operation);
					if (code == null || ModelComparer.equals(code, descriptor.getType()))
					{
						result.add(descriptor);
					}
				}
			}
		}

		return result;
	}


	@Override
	public RealTimeSampleArrayMetricDescriptor findStreamMetric(String handle)
	{
		//		MetricDescriptor m = findMetric(handle);
		//		if (m instanceof RealTimeSampleArrayMetricDescriptor)
		//		{
		//			return (RealTimeSampleArrayMetricDescriptor)m;
		//		}
		//		return null;
		return findVMO(handle, RealTimeSampleArrayMetricDescriptor.class);
	}


	@Override
	public List<RealTimeSampleArrayMetricDescriptor> findStreamMetrics()
	{
		//		List<RealTimeSampleArrayMetricDescriptor> result = new ArrayList<RealTimeSampleArrayMetricDescriptor>();
		//		for (DescriptorWithParent entry : vmoMap.values())
		//		{
		//			if (entry.vmo instanceof RealTimeSampleArrayMetricDescriptor)
		//			{
		//				result.add((RealTimeSampleArrayMetricDescriptor) entry.vmo);
		//			}
		//		}
		//		return result;
		return findVMOWithCode(null, RealTimeSampleArrayMetricDescriptor.class);
	}


	@Override
	public List<RealTimeSampleArrayMetricDescriptor> findStreamMetrics(CodedValue code)
	{
		//		List<RealTimeSampleArrayMetricDescriptor> result = new ArrayList<RealTimeSampleArrayMetricDescriptor>();
		//		List<? extends MetricDescriptor> metrics = findMetrics(code);
		//		for(MetricDescriptor m : metrics)
		//		{
		//			if (m instanceof RealTimeSampleArrayMetricDescriptor)
		//			{
		//				result.add((RealTimeSampleArrayMetricDescriptor)m);
		//			}
		//		}
		return findVMOWithCode(code, RealTimeSampleArrayMetricDescriptor.class);
		//		return result;
	}


	//	public List<RealTimeSampleArrayMetricDescriptor> findStreamMetrics(String patientId)
	//	{
	//		List<RealTimeSampleArrayMetricDescriptor> rtMetrics = findStreamMetrics();
	//		List<RealTimeSampleArrayMetricDescriptor> result = new ArrayList<RealTimeSampleArrayMetricDescriptor>();
	//		for (RealTimeSampleArrayMetricDescriptor m : rtMetrics)
	//		{
	//			if (getPatientIdsForVmo(m.getHandle()).contains(patientId))
	//			{
	//				result.add(m);
	//			}
	//		}
	//		return result;
	//	}


	//	public boolean belongsVmoToPatient(String vmoHandle, String patientId)
	//	{
	//		boolean patientFound = false;
	//		//TODO SSch refactor patient
	//
	//		//		if (vmoMap.containsKey(vmoHandle))
	//		//		{
	//		//			for (PatientAssociationDescriptor patient : findPatients())
	//		//			{
	//		//		
	//		//				//if (ModelComparer.equals(patient.getPatientData().getPatientId(), patientId))
	//		//				{
	//		//					patientFound = true;
	//		//					break;
	//		//				}
	//		//			}
	//		//			if (patientFound)
	//		//			{
	//		//				patientFound = false;
	//		//
	//		//				VmoWithParent entry = vmoMap.get(vmoHandle);
	//		//				while (entry != null)
	//		//				{
	//		//					if (entry.vms != null)
	//		//					{
	//		////						if (entry.vms instanceof SimpleMDS)
	//		////						{
	//		////							patientFound = ((SimpleMDS) entry.vms).getPatient().getPatientData()
	//		////							.getPatientId().equals(patientId);
	//		////							break;
	//		////						}
	//		//						else if (entry.vms instanceof HydraMDSDescriptor)
	//		//						{
	//		//							//TODO SSch refactor patient
	//		////							patientFound = ((HydraMDSDescriptor) entry.vms).getPatient().getPatientData()
	//		////							.getPatientId().equals(patientId);
	//		//							break;
	//		//						}
	//		//					}
	//		//					if (entry.parent != null)
	//		//					{
	//		//						entry = vmoMap.get(entry.parent.getHandle());
	//		//					}
	//		//					else
	//		//					{
	//		//						entry = null;
	//		//					}
	//		//				}
	//		//				while (entry != null);
	//		//			}
	//		//		}
	//		return patientFound;
	//	}


	//	public List<String> getPatientIdsForVms(String vmsHandle)
	//	{	
	//
	//		List<String> result = new ArrayList<String>();
	//		//TODO SSch refactor patient
	//		//		VmsWithParent entry = vmsMap.get(vmsHandle);
	//		//		while (entry != null)
	//		//		{
	//		//			if (entry.vms instanceof SimpleMDS)
	//		//			{
	//		//				result.add(((SimpleMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		//			}
	//		//			else if (entry.vms instanceof HydraMDS)
	//		//			{
	//		//				result.add(((HydraMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		//			}
	//		//			if (entry.parent != null)
	//		//			{
	//		//				entry = vmsMap.get(entry.parent.getHandle());
	//		//			}
	//		//			else
	//		//			{
	//		//				entry = null;
	//		//			}
	//		//		}
	//		return result;
	//	}


	//	public List<String> getPatientIdsForVmo(String vmoHandle)
	//	{
	//		List<String> result = new ArrayList<String>();
	//		//TODO SSch refactor patient
	//		//		VmoWithParent entry = vmoMap.get(vmoHandle);
	//		//		while (entry != null)
	//		//		{
	//		//			if (entry.vms != null)
	//		//			{
	//		//				if (entry.vms instanceof SimpleMDS)
	//		//				{
	//		//					result.add(((SimpleMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		//				}
	//		//				else if (entry.vms instanceof HydraMDS)
	//		//				{
	//		//					result.add(((HydraMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		//				}
	//		//			}
	//		//			if (entry.parent != null)
	//		//			{
	//		//				entry = vmoMap.get(entry.parent.getHandle());
	//		//			}
	//		//			else
	//		//			{
	//		//				entry = null;
	//		//			}
	//		//		}
	//		return result;
	//	}

	@Override
	public List<AbstractContextDescriptor> findMDSContextElements()
	{
		List<AbstractContextDescriptor> result = new ArrayList<AbstractContextDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, AbstractContextDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null, AbstractContextDescriptor.class, result,  null);
		}else{
			for (VmsWithParent entry : vmsMap.values())
			{
				SystemContext ctxt = entry.vms.getContext();
				if (ctxt!=null)
				{
					if (ctxt.getPatientContext()!=null) result.add(ctxt.getPatientContext());
					if (ctxt.getLocationContext()!=null) result.add(ctxt.getLocationContext());
				}
			}
		}
		return result;
	}

	@Override
	public SystemContext findMDSContextForVms(String vmsHandle)
	{
		SystemContext result = null;

		if ( this.lazyLoader.isLazyLoadingEnabled(null, SystemContext.class))
		{

			List<SystemContext> systemContextList=new ArrayList<SystemContext>();
			//			retVal = this.lazyLoader.retrieveDescriptorWithHandle(vmsHandle, SystemContext.class);
			//			retVal=updateMDIBRepresentation(retVal);
			this.lazyLoader.fillDescriptorList(null,SystemContext.class,systemContextList,vmsHandle);

			if (!systemContextList.isEmpty())
			{
				result=systemContextList.get(0);
			}
		}else{

			for (VmsWithParent entry : vmsMap.values())
			{
				if (entry.vms.getHandle().equals(vmsHandle))
				{
					result = entry.vms.getContext();
					break;
				}
			}
		}


		return result;
	}


	@Override
	public List<SystemContext> findMDSContexts()
	{
		List<SystemContext> result = null;
		if ( this.lazyLoader.isLazyLoadingEnabled(null, SystemContext.class))
		{
			result=findVMOWithCode(null, SystemContext.class);
		}else{
			result=new ArrayList<SystemContext>();

			for (VmsWithParent entry : vmsMap.values())
			{
				SystemContext ctxt = entry.vms.getContext();
				if (ctxt!=null)
				{
					result.add(ctxt);
				}
			}
		}
		return result;
	}

	@Override
	public List<AbstractContextDescriptor> findMDSContextElementsForVms(String vmsHandle)
	{
		List<AbstractContextDescriptor> result = new ArrayList<AbstractContextDescriptor>();
		if ( this.lazyLoader.isLazyLoadingEnabled(null, AbstractContextDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,AbstractContextDescriptor.class,result,vmsHandle);
		}else{
			for (VmsWithParent entry : vmsMap.values())
			{
				if (entry.vms.getHandle().equals(vmsHandle))
				{
					SystemContext ctxt = entry.vms.getContext();
					if (ctxt!=null)
					{
						if (ctxt.getPatientContext()!=null) result.add(ctxt.getPatientContext());
						if (ctxt.getLocationContext()!=null) result.add(ctxt.getLocationContext());
					}

				}
			}
		}
		return result;
	}


	//	public List<String> getPatientIds()
	//	{
	//		List<String> result = new ArrayList<String>();
	//		//TODO SSch Refactor get the patient id
	//		//		for (VmsWithParent entry : vmsMap.values())
	//		//		{
	//		////			if (entry.vms instanceof SimpleMDS)
	//		////			{
	//		////				result.add(((SimpleMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		////			}
	//		////			else 
	//		//			if (entry.vms instanceof HydraMDSDescriptor)
	//		//			{
	//		//
	//		//				result.add(((HydraMDS) entry.vms).getPatient().getPatientData().getPatientId());
	//		//			}
	//		//		}
	//		return result;
	//	}


	@Override
	public void addOperation(OperationDescriptor operation, SCODescriptor scoDescriptor, String mdsHandle)
	{
		if (operation!=null)
		{
			if (!operationsMap.containsKey(operation.getHandle()) && vmsMap.containsKey(mdsHandle))
			{
				operationsMap.put(operation.getHandle(),
						new OperationWithVms(operation, vmsMap.get(mdsHandle).vms));
				DescriptorWithParent descriptorWithParent = new DescriptorWithParent(operation, scoDescriptor, vmsMap.get(mdsHandle).vms);

				addToDescriptorMap(descriptorWithParent);
			}
		}
	}


	/**
	 * @param descriptorWithParent
	 */
	private void addToDescriptorMap(DescriptorWithParent descriptorWithParent) {
		if (descriptorWithParent!=null && descriptorWithParent.vmo!=null)
		{
			if (!vmoMap.containsKey(descriptorWithParent.vmo.getHandle()))
			{
				vmoMap.put(descriptorWithParent.vmo.getHandle(), descriptorWithParent);
				if (descriptorWithParent.getParent()!=null)
				{
					ArrayList<DescriptorWithParent> childList = childMap.get(descriptorWithParent.getParent().getHandle());
					if (childList==null)
					{
						childList=new ArrayList<MdibRepresentation.DescriptorWithParent>();
						childMap.put(descriptorWithParent.getParent().getHandle(), childList);
					}
					if (!childList.contains(descriptorWithParent))
					{
						childList.add(descriptorWithParent);
					}
				}
			}
		}
	}

	@Override
	public boolean removeOperation(String handle)
	{
		return operationsMap.remove(handle) != null;
	}


	@Override
	public void addDescriptor(Descriptor vmo, String parentHandle, String mdsHandle)
	{
		if (vmo!=null)
		{
			if (!vmoMap.containsKey(vmo.getHandle()) && vmoMap.containsKey(parentHandle) && vmsMap.containsKey(mdsHandle))
			{
				Descriptor parent = vmoMap.get(parentHandle).parent;
				MDSDescriptor vms = vmsMap.get(mdsHandle).vms;
				if (vmo instanceof VMDDescriptor)
				{
					List<VMDDescriptor> list = new ArrayList<VMDDescriptor>();
					list.add((VMDDescriptor) vmo);
					addVMD(vms, list);
				}
				else if (vmo instanceof ChannelDescriptor)
				{
					List<ChannelDescriptor> list = new ArrayList<ChannelDescriptor>();
					list.add((ChannelDescriptor) vmo);
					addChannels(parent, list, vms);
				}
				else
				{
					DescriptorWithParent descriptorWithParent = new DescriptorWithParent(vmo, parent, vms);
					//					vmoMap.put(vmo.getHandle(), descriptorWithParent);
					addToDescriptorMap(descriptorWithParent);
				}
				//TODO SSch implement the rest ??? Alerts...
			}
		}
	}


	@Override
	public void removeVMO(String handle)
	{
		if (vmoMap.containsKey(handle))
		{
			DescriptorWithParent entry = vmoMap.get(handle);
			if (entry!=null)
			{
				if (entry.getParent()!=null)
				{
					ArrayList<DescriptorWithParent> childList = childMap.get(entry.getParent().getHandle());
					childList.remove(entry);
				}
				// TODO MVo If this is a parent of some other VMOs, remove its children too.
				// TODO MVo When removing metrics check for operations to remove, too.
			}
			vmoMap.remove(handle);
			childMap.remove(handle);
		}
	}




	@Override
	public List<RealTimeSampleArrayMetricDescriptor> findStreamsForVms(String vmsHandle) {
		List<RealTimeSampleArrayMetricDescriptor> result = new ArrayList<RealTimeSampleArrayMetricDescriptor>();


		if (this.lazyLoader.isLazyLoadingEnabled(null, RealTimeSampleArrayMetricDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null,RealTimeSampleArrayMetricDescriptor.class,result,vmsHandle);
		}else{
			VmsWithParent vmsEntry = vmsMap.get(vmsHandle);

			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				for(VMDDescriptor vmd : mds.getVMDs())
				{
					for(ChannelDescriptor channel : vmd.getChannels())
					{
						List<MetricDescriptor> allMetrics = channel.getMetrics();
						if (allMetrics!=null && !allMetrics.isEmpty())
						{
							for (MetricDescriptor metricDescriptor : allMetrics) {
								if (metricDescriptor instanceof RealTimeSampleArrayMetricDescriptor)
								{
									result.add((RealTimeSampleArrayMetricDescriptor) metricDescriptor);
								}
							}
						}
					}
				}
			}
		}

		return result;
	}




	@Override
	public MDSDescriptor getParentVMSForDescriptorHandle(String aDescriptorHandle) {
		if (vmsMap.containsKey(aDescriptorHandle)) { return vmsMap.get(aDescriptorHandle).vms; }
		if (vmoMap.containsKey(aDescriptorHandle)) { return vmoMap.get(aDescriptorHandle).vms; }

		return null;
	}




	@Override
	public List<ActivateOperationDescriptor> findManeuverForVMS(String vmsHandle) {
		List<ActivateOperationDescriptor> result = new ArrayList<ActivateOperationDescriptor>();

		if (this.lazyLoader.isLazyLoadingEnabled(null, ActivateOperationDescriptor.class))
		{
			this.lazyLoader.fillDescriptorList(null, ActivateOperationDescriptor.class, result, null);
		}else{

			VmsWithParent vmsEntry = vmsMap.get(vmsHandle);
			if (vmsEntry.vms instanceof HydraMDSDescriptor)
			{
				HydraMDSDescriptor mds = (HydraMDSDescriptor)vmsEntry.vms;
				if (mds.getSCO()!=null)
				{
					List<OperationDescriptor> operations = mds.getSCO().getOperations();
					if (operations!=null)
					{
						for (OperationDescriptor operationDescriptor : operations) {
							if (operationDescriptor instanceof ActivateOperationDescriptor)
							{
								result.add((ActivateOperationDescriptor)operationDescriptor);	
							}						
						}
					}
				}
			}
		}
		return result;
	}


	@Override
	public List<DescriptorWithParent> getChildrenDescriptors(String parentHandle)
	{
		List<DescriptorWithParent> retVal=null;

		ArrayList<DescriptorWithParent> children = childMap.get(parentHandle);
		if (children!=null && !children.isEmpty())
		{
			retVal=new ArrayList<MdibRepresentation.DescriptorWithParent>(children);
		}

		return retVal;
	}

	
	/**
	 * Gets the children cnt.
	 *
	 * @param handle the handle
	 * @return the children cnt
	 */
	public int getChildrenCnt(String handle) {
		int childCnt=0;
		ArrayList<DescriptorWithParent> children = childMap.get(handle);
		if (children!=null)
		{
			childCnt=children.size();
		}
		return childCnt;
	}


}
