/*******************************************************************************
 * Copyright (c) 2012 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.common.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.swing.AbstractListModel;

@SuppressWarnings("rawtypes")
public class RingBufferModel<T> extends AbstractListModel implements Collection<T>
{

	private static final long serialVersionUID = -4682679110386467974L;
	private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
	private Lock readLock = rwLock.readLock();
	private Lock writeLock = rwLock.writeLock();
	private final int size;
	private T[] values;
	private volatile int indexHead;
	private volatile int storedElements;

	@SuppressWarnings("unchecked")
	public RingBufferModel(int size)
	{
		this.size = size;
		this.values = (T[]) new Object[size];
		this.indexHead = -1;
		this.storedElements=0;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void clear()
	{
		writeLock.lock();

		try
		{
			this.values = (T[]) new Object[size];
			this.indexHead = -1;
			this.storedElements=0;
		}
		finally
		{
			writeLock.unlock();
		}

		fireContentsChanged(this, 0, size);
	}

	@Override
	public int getSize()
	{
		readLock.lock();

		try
		{
			return size;
		}
		finally
		{
			readLock.unlock();
		}
	}

	public T peek(final int index)
	{
		readLock.lock();

		try
		{
			return values[(size + index) % size];
		}
		finally
		{
			readLock.unlock();
		}
	}

	public T head()
	{
		return peek(indexHead);
	}

	private int putValue(final T value)
	{
		if (++indexHead == size)
		{
			indexHead = 0;
		}

		if (++storedElements>=size)
		{
			storedElements=size;
		}

		values[(size + indexHead) % size] = value;

		return indexHead;
	}

	public void put(final T value)
	{
		if (value == null)
		{
			return;
		}

		writeLock.lock();

		final int insertIndex;

		try
		{
			insertIndex = putValue(value);
		}
		finally
		{
			writeLock.unlock();
		}

		fireContentsChanged(this, insertIndex, insertIndex);
	}

	public void putAll(final T[] values)
	{
		if (values == null || values.length <= 0)
		{
			return;
		}

		writeLock.lock();

		try
		{
			int insertIndex;

			for (int i = 0; i < values.length; i++)
			{
				T value = values[i];

				if (value != null)
				{
					insertIndex = putValue(values[i]);

					fireContentsChanged(this, insertIndex, insertIndex);
				}
			}
		}
		finally

		{
			writeLock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public void putAll(final Collection<T> values)
	{
		putAll((T[]) values.toArray());
	}

	@Override
	public T getElementAt(int index)
	{
		return peek(index);
	}

	@Override
	public int size()
	{
		readLock.lock();

		try
		{
			return storedElements;
		}
		finally
		{
			readLock.unlock();
		}
	}

	@Override
	public boolean isEmpty()
	{
		readLock.lock();

		try
		{
			return (storedElements==0);
		}
		finally
		{
			readLock.unlock();
		}

	}

	@Override
	public boolean contains(Object o)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<T> iterator()
	{

		return null;
	}

	@Override
	public Object[] toArray()
	{

		Object[] array=null;
		readLock.lock();
		try
		{
			int size=size();
			if (size>0)
			{
				array=new Object[size];
				int indexLookAhead=indexHead+1;;
				int tail=indexLookAhead%values.length;
				if (storedElements==values.length)
				{
					int startCnt=storedElements-tail;
					System.arraycopy(values,tail , array, 0, startCnt);
					System.arraycopy(values,0 , array, startCnt, size-startCnt);
				}else{
					System.arraycopy(values, 0, array, 0, indexLookAhead);
						
				}

			}
		}
		finally
		{
			readLock.unlock();
		}


		return array;
	}


	@Override
	public <S> S[] toArray(S[] a)
	{
		return null;
	}

	@Override
	public boolean add(T e)
	{
		if (e == null)
		{
			return false;
		}

		put(e);

		return true;
	}

	@Override
	public boolean remove(Object o)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends T> c)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
