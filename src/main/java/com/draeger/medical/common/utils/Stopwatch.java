/*******************************************************************************
 * Copyright (c) 2012 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.common.utils;

public class Stopwatch
{
    private String name;
    private long startTime;
    private long splitTime;
    private long stopTime;

    public Stopwatch()
    {
        this(null);
    }

    public Stopwatch(String name)
    {
        this.name = name;
        reset();
    }

    public void reset()
    {
        startTime = 0;
        splitTime = 0;
        stopTime = 0;
    }

    public long start()
    {
        if (isRunning())
        {
            throw new IllegalStateException(String.format("Stopwatch %s already running.", name));
        }

        reset();
        
        startTime = System.currentTimeMillis();

        return getElapsedTime();
    }

    public long split()
    {
        if (!isRunning())
        {
            throw new IllegalStateException(String.format("Stopwatch %s not running.", name));
        }
        
        splitTime = System.currentTimeMillis();

        return (splitTime - startTime);
    }

    public long lap()
    {
        if (!isRunning())
        {
            throw new IllegalStateException(String.format("Stopwatch %s not running.", name));
        }

        long lastSplitTime = splitTime;
        splitTime = System.currentTimeMillis();

        return (splitTime - lastSplitTime);
    }

    public long stop()
    {
        if (!isRunning())
        {
            throw new IllegalStateException(String.format("Stopwatch %s not running.", name));
        }
        
        stopTime = System.currentTimeMillis();

        return getElapsedTime();
    }

    private long getElapsedTime()
    {
        if (isStarted())
        {
            if (isStopped())
            {
                return (stopTime - startTime);
            }
            else 
            {
                return (System.currentTimeMillis() - startTime);
            }
        }

        throw new IllegalStateException(String.format("Stopwatch %s not yet started.", name));
    }

    // states
    
    private boolean isStarted()
    {
        return (startTime > 0);
    }

    private boolean isRunning()
    {
        return (startTime > 0 && stopTime == 0);
    }

    private boolean isStopped()
    {
        return (stopTime > 0);
    }

    @Override
    public String toString()
    {
        return "Stopwatch [name=" + name
                        + ", elapsedTime="
                        + getElapsedTime()
                        + " (startTime="
                        + startTime
                        + ", stopTime="
                        + stopTime
                        + ")]";
    }
}
