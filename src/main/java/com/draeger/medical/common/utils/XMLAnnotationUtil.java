package com.draeger.medical.common.utils;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *    Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
public class XMLAnnotationUtil {
	public static QName getQNameFromAnnotation(Object o){
		QName retVal=null;
		if (o!=null){
			String localName=null;
			String nameSpace=null;
			Class<?> clazz = o.getClass();
			XmlType xmlTypeAnnotation = clazz.getAnnotation(XmlType.class);
			if (xmlTypeAnnotation!=null)
			{
				localName=xmlTypeAnnotation.name();
				nameSpace=xmlTypeAnnotation.namespace();
			}
			
			if (localName==null || localName.isEmpty())
			{
				XmlRootElement xmlRootAnnotation = clazz.getAnnotation(XmlRootElement.class);
				if (xmlRootAnnotation!=null)
				{
					localName=xmlRootAnnotation.name();
					nameSpace=xmlRootAnnotation.namespace();
				}
			}
			
			if (localName!=null && nameSpace!=null)
			{
				retVal=new QName(nameSpace, localName);
			}
		}
		return retVal;
	}
}
