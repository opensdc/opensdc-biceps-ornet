openSDC
========
The openSDC libraries are communication library that facilitate the development of distributed systems of medical devices in high-acuity environments.

OpenSDC has been developed in an Dräger-internal technology project called “Device & System Connectivity” (DSC) that had the goal to meet increasing demand for medical device interoperability in an Integrated Clinical Environment (ICE).  An ICE is a distributed system of medical devices for one clinical workplace that may have an external interface to other systems. ASTM F2761-1:2009 describes the components that are required for safe and effective “Plug & Play” operation of an ICE in high-acuity environments.
The project had the objective to develop an efficient, future-proof architecture, protocol stack, and middleware that satisfies the derived requirements and facilitates the implementation of the concept of an ICE.  The middleware is comprised of the openSDC libraries.

For more details on the architecture and the protocol stack see [the technical whitepaper](https://sourceforge.net/projects/opensdc/files/framework/V1.0-Beta01/Documentation/).

Intended Use
-------
The openSDC libraries are a reference implementation of the published documents and are not intended to be used in clinical trials, clinical studies or in clinical routine use.
See "IntendedUse.txt" for more details.

Components
-------
* BICEPS - BICEPS implements the BICEPS specification.
    * BICEPS Common - Common components of the implementation of the BICEPS protocol including messages, services, and utils.
	* BICEPS Device - Component that implements the framework for a BICEPS Device.
	* BICEPS Client - Component that implements the framework for a BICEPS Client.
* MDPWS - An extension to JDPWS that implements the extensions of the MDPWS specification on top of the DPWS specification. Furthermore, a QoS framework as well as some QoS Policies are included.
* JDPWS - JDPWS implements the DPWS specification. It is a modified version of the [JMEDS beta 4 library](https://sourceforge.net/projects/ws4d-javame/) that allows the plugin of the QoS framework as well as the MDPWS extensions. Furthermore, it contains bugfixes that are currently not patched into the JMEDS beta 4 library.

License
-------
The openSDC libraries and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at (http://www.eclipse.org/legal/epl-v10.html).


Notice
-------
* Protocol may change in the future due to additional requirements identified during standardization. For this reason until a standard has been established all version of the openSDC libraries are released as beta version.
* The package names will be changed in release beta02.