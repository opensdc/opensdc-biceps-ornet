<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
  All rights reserved. This program and the accompanying materials
  are made available under the terms of the Eclipse Public License v1.0
  which accompanies this distribution, and is available at
  http://www.eclipse.org/legal/epl-v10.html
  
  Contributors:
      Drägerwerk AG & Co. KGaA - initial API and implementation
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:jaxb="http://java.sun.com/xml/ns/jaxb">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" ></xsl:output>
	<xsl:template match="*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="jaxb:schemaBindings">
	</xsl:template>
	<xsl:template match="jaxb:property">
	</xsl:template>
	<xsl:template match="jaxb:typesafeEnumClass ">
	</xsl:template>
	<xsl:template match="jaxb:class ">
	</xsl:template>
</xsl:stylesheet>
