<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
  All rights reserved. This program and the accompanying materials
  are made available under the terms of the Eclipse Public License v1.0
  which accompanies this distribution, and is available at
  http://www.eclipse.org/legal/epl-v10.html
  
  Contributors:
      Drägerwerk AG & Co. KGaA - initial API and implementation
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:jaxb="http://java.sun.com/xml/ns/jaxb">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="*">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="jaxb:schemaBindings">
	</xsl:template>
	<xsl:template match="xs:annotation">
		<xsl:element name="xs:annotation" namespace="http://www.w3.org/2001/XMLSchema">
			<xsl:copy-of select="@*"/>
			<xsl:copy-of select="xs:documentation"/>
			<xsl:if test="./xs:appinfo">
				<xsl:element name="xs:appinfo">
					<xsl:if test="./xs:appinfo/*[namespace-uri()='http://java.sun.com/xml/ns/jaxb']">
						<xsl:call-template name="copyDocu"/>
					</xsl:if>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<xsl:template name="copyDocu">
	<xsl:for-each select="./xs:appinfo/*[namespace-uri()='http://java.sun.com/xml/ns/jaxb']">
		<xsl:copy>
							<xsl:copy-of select="@*"/>
							<xsl:if test="not(local-name()='schemaBindings')">
								<xsl:apply-templates select="../../xs:documentation"/>
							</xsl:if>
							<xsl:apply-templates select="./*"/>
			</xsl:copy>
		</xsl:for-each>
	</xsl:template>
<xsl:template match="xs:documentation">
							<xsl:element name="jaxb:javadoc" namespace="http://java.sun.com/xml/ns/jaxb">
							<xsl:apply-templates/>
							</xsl:element>
</xsl:template>
	<!--xsl:template match="xs:any">
	</xsl:template>
	<xsl:template match="xs:anyAttribute">
	</xsl:template-->
</xsl:stylesheet>
