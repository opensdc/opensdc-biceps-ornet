package com.draeger.medical.biceps.common.context;

import static org.junit.Assert.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.ws4d.java.types.URI;

import com.draeger.medical.biceps.common.model.InstanceIdentifier;

public class ContextHelperTest {

	@Test
	public void testCreateInterpretableInstanceIdentifier_empty() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/?RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_hospitalPocBed() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("fac", "Hospital1");
		interpretationParticles.put("poc", "CU1");
		interpretationParticles.put("bed", "Bed1");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/Hospital1/CU1/Bed1?I=fac/poc/bed;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_hospitalPocBedChangedInOrderIsOrdered() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("poc", "CU1");
		interpretationParticles.put("fac", "Hospital1");
		interpretationParticles.put("bed", "Bed1");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/Hospital1/CU1/Bed1?I=fac/poc/bed;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_checkOrder() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("fac", "1");
		interpretationParticles.put("bdg", "2");
		interpretationParticles.put("poc", "3");
		interpretationParticles.put("flr", "4");
		interpretationParticles.put("ro", "5");
		interpretationParticles.put("bed", "6");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/1/2/3/4/5/6?I=fac/bdg/poc/flr/ro/bed;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_hospital() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("fac", "Hospital1");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/Hospital1?I=fac;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_newKey() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("fac2", "Hospital");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/Hospital?I=fac2;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

	@Test
	public void testCreateInterpretableInstanceIdentifier_mioxedOldAndNew_newKeysAtEnd() {
		String root = "UNK";
		Map<String,String> interpretationParticles = new HashMap<String, String>();
		interpretationParticles.put("fac2", "Hospital2");
		interpretationParticles.put("fac", "Hospital1");
		interpretationParticles.put("poc", "CU1");
		interpretationParticles.put("poc2", "CU2");
		interpretationParticles.put("bed", "Bed1");
		InstanceIdentifier ii = ContextHelper.createInterpretableInstanceIdentifier(root, interpretationParticles);
		
		URI output = ContextHelper.createLocationIdUri(ii);
		String outputString = "biceps.ctxt.location:/id/Hospital1/CU1/Bed1/Hospital2/CU2?I=fac/poc/bed/fac2/poc2;RT=UNK";
		assertThat(output.toString(), is(outputString));
	}

}
