/*******************************************************************************
 * Copyright (c) 2010 - 2015 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package  com.draeger.medical.biceps.common.impl.replicator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.BeforeClass;
import org.junit.Test;

import com.draeger.medical.biceps.common.model.PatientDemographicsCoreData;
import com.draeger.medical.biceps.common.model.StringMetricDescriptor;

public class DeepCopyTest {

	
	private static DatatypeFactory datatypeFactory=null;
	private static DefaultReplicator replicator;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		datatypeFactory = DatatypeFactory.newInstance();		
		replicator=new DefaultReplicator();
		
	}


	@Test
	public void testDuration() {

		StringMetricDescriptor metricDescriptor=new StringMetricDescriptor();
		Duration maxDelayTime = datatypeFactory.newDuration(10);
		metricDescriptor.setMaxDelayTime(maxDelayTime);
		StringMetricDescriptor deepCopy=null;
		try {
			deepCopy = replicator.deepcopy(metricDescriptor);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertEquals(maxDelayTime, deepCopy.getMaxDelayTime());
	}

	@Test
	public void testDateTime() {
		XMLGregorianCalendar birthday =  null;
		PatientDemographicsCoreData coreData=new PatientDemographicsCoreData();


		birthday =  
				datatypeFactory.newXMLGregorianCalendar("2015-10-16T15:38:00");

		coreData.setBirthday(birthday);

		PatientDemographicsCoreData deepCopy=null;
		try {
			deepCopy = replicator.deepcopy(coreData);
		} catch (Exception e) {
			fail(e.getMessage());
		}

		assertEquals(birthday, deepCopy.getBirthday());

	}

}
