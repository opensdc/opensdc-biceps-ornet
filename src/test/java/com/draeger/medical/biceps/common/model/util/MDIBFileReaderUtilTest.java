/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.model.util;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.util.SEToolkit;
import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.common.model.MDDescription;


public class MDIBFileReaderUtilTest {
	 @Rule
	 public ExpectedException exception = ExpectedException.none();
	
	 
	 @BeforeClass 
	 public static void setup()
	 {
		Log.setLogLevel(Log.DEBUG_LEVEL_NO_LOGGING);
		PlatformSupport.getInstance().setToolkit(new SEToolkit());
	 }
	
	/**
	 * Test method for {@link com.draeger.medical.biceps.common.model.util.MDIBFileReaderUtil#getDescriptionFromFile(java.io.File)}.
	 */
	@Test
	public void testGetDescriptionFromFileFile() {
		MDIBFileReaderUtil util=new MDIBFileReaderUtil();
		
		File file=new File(getClass().getResource("/MdibFileReaderTest.xml").getFile());
		MDDescription descriptionFromFile = util.getDescriptionFromFile(file);
		
		assertNotNull(descriptionFromFile);
	}
	
	@Test
	public void testGetDescriptionFromFileWithNull(){
		MDIBFileReaderUtil util=new MDIBFileReaderUtil();
		
		File file=null;	
		exception.expect(IllegalArgumentException.class);
		util.getDescriptionFromFile(file);
	}

	/**
	 * Test method for {@link com.draeger.medical.biceps.common.model.util.MDIBFileReaderUtil#getDescriptionFromFile(org.ws4d.java.types.URI)}.
	 */
	@Test
	public void testGetDescriptionFromFileURI() {
		MDIBFileReaderUtil util=new MDIBFileReaderUtil();
		
		URI uri=new URI(getClass().getResource("/MdibFileReaderTest.xml").getFile());
		MDDescription descriptionFromFile = util.getDescriptionFromFile(uri);
		
		assertNotNull(descriptionFromFile);
	}

	/**
	 * Test method for {@link com.draeger.medical.biceps.common.model.util.MDIBFileReaderUtil#getDescriptionFromInputStream(java.io.InputStream)}.
	 */
	@Test
	public void testGetDescriptionFromFileInputStream()  {
		MDIBFileReaderUtil util=new MDIBFileReaderUtil();
		
		InputStream inputStream=getClass().getResourceAsStream("/MdibFileReaderTest.xml");
		MDDescription descriptionFromFile = util.getDescriptionFromInputStream(inputStream);
		try {
			inputStream.close();
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		assertNotNull(descriptionFromFile);
	}
	
	/**
	 * Test method for {@link com.draeger.medical.biceps.common.model.util.MDIBFileReaderUtil#getDescriptionFromInputStream(java.io.InputStream)}.
	 */
	@Test
	public void testGetDescriptionFromFileInputStreamWithUcs2FileWhereEncodingUtf8()  {
		MDIBFileReaderUtil util=new MDIBFileReaderUtil();
		
		InputStream inputStream=getClass().getResourceAsStream("/MDIB_MiniMDS_UTF16_EncInfoUTF8.xml");
		MDDescription descriptionFromFile = util.getDescriptionFromInputStream(inputStream);
		try {
			inputStream.close();
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		assertNull(descriptionFromFile);
	}

}
