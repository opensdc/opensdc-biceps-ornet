/*******************************************************************************
 * Copyright (c) 2010 - 2014 Drägerwerk AG & Co. KGaA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Drägerwerk AG & Co. KGaA - initial API and implementation
 ******************************************************************************/
package com.draeger.medical.biceps.common.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.ws4d.java.platform.PlatformSupport;
import org.ws4d.java.platform.Toolkit;
import org.ws4d.java.platform.util.SEToolkit;


public class BICEPSThreadFactoryTest {

	private Runnable task=new Runnable() {

		@Override
		public void run() {
			try {
				Thread.sleep(Long.MAX_VALUE);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	@Test
	public void test() {
		ThreadGroup threadGroup=null;

		PlatformSupport.getInstance().setToolkit(new SEToolkit());

		String threadName="Test-"+System.nanoTime();
		BICEPSThreadFactory factory=new BICEPSThreadFactory(threadName);

		Thread newThread = factory.newThread(task);

		assertEquals(threadName, newThread.getName());
		assertTrue(newThread.isDaemon());

		Toolkit toolkit = PlatformSupport.getInstance().getToolkit();
		toolkit.init(2);
		
		threadGroup=toolkit.getThreadPool().getStackThreadGroup();
	
		assertNotEquals(threadGroup,newThread.getThreadGroup());
	
		String threadName2="Test-"+System.nanoTime();
		BICEPSThreadFactory factory2=new BICEPSThreadFactory(threadName2);
		
		Thread newThread2 = factory2.newThread(task);
		assertNotEquals(threadName, newThread2.getName());	
		
		assertEquals(threadGroup,newThread2.getThreadGroup());
	}

}
